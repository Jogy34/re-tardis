package com.adx.jogy34.tardis.eventlisteners;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.enums.TARDISWorlds;
import com.adx.jogy34.tardis.eventlisteners.annotations.TardisEventListener;
import com.adx.jogy34.tardis.providers.ServiceProvider;

import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

@TardisEventListener
public class TardisWorldProtectionsListener implements Listener
{
    private EList<SpawnReason> _validSpawnReasons = new EList<SpawnReason>(new SpawnReason[] {
        SpawnReason.BREEDING, SpawnReason.BUILD_IRONGOLEM, SpawnReason.BUILD_SNOWMAN,
        SpawnReason.CUSTOM, SpawnReason.DISPENSE_EGG, SpawnReason.EGG, SpawnReason.OCELOT_BABY
    });

    @EventHandler
    public void preventEntitySpawns(CreatureSpawnEvent event)
    {
        if(!_validSpawnReasons.contains(event.getSpawnReason()))
        {
            basicCancel(event, event.getLocation().getWorld());
        }
    }

    @EventHandler
    public void preventRain(WeatherChangeEvent event)
    {
        if(event.toWeatherState())
        {
            basicCancel(event, event.getWorld());
        }
    }

    @EventHandler
    public void preventBreakingInsideTardis(BlockBreakEvent event)
    {
        if(event.getPlayer().getGameMode() != GameMode.CREATIVE)
        {
            basicCancel(event, event.getBlock().getWorld());
        }
    }

    @EventHandler
    public void preventPlacingInsideTardis(BlockPlaceEvent event)
    {
        if(event.getPlayer().getGameMode() != GameMode.CREATIVE)
        {
            basicCancel(event, event.getBlock().getWorld());
        }
    }

    @EventHandler
    public void preventPlacingLiquidsInsideTardis(PlayerBucketEmptyEvent event)
    {
        if(event.getPlayer().getGameMode() != GameMode.CREATIVE)
        {
            basicCancel(event, event.getBlockClicked().getWorld());
        }
    }

    @EventHandler
    public void preventPickingUpLiquidsInsideTardis(PlayerBucketFillEvent event)
    {
        if(event.getPlayer().getGameMode() != GameMode.CREATIVE)
        {
            basicCancel(event, event.getBlockClicked().getWorld());
        }
    }

    private void basicCancel(Cancellable event, World w)
    {
        TARDISWorlds worldType = ServiceProvider.worldService().getTypeForWorld(w);
        switch(worldType)
        {
            case TARDIS_WORLD:
            {
                event.setCancelled(true);
                break;
            }
            case NONE:
            default:
            {
                break;
            }
        }
    }
}