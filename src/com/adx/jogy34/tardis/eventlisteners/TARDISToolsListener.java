package com.adx.jogy34.tardis.eventlisteners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.text.MessageFormat;
import java.util.List;

import com.adx.jlinq.EList;
import com.adx.jlinq.tuples.Tuples.Tuple;
import com.adx.jogy34.tardis.enums.TARDISItems;
import com.adx.jogy34.tardis.eventlisteners.annotations.TardisEventListener;
import com.adx.jogy34.tardis.menu.actions.TARDISCustomizeActions.UseSpawnEggAction;
import com.adx.jogy34.tardis.menu.customize.CustomizeTARDISMenu;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.providers.UtilProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.helpers.StringHelper;
import com.adx.jogy34.tardis.util.spacial.Point;

@TardisEventListener
public class TARDISToolsListener implements Listener
{
    @EventHandler
    public void onPlayerUseTool(PlayerInteractEvent evt)
    {
        if(evt.getHand() != EquipmentSlot.HAND) return;

        ItemStack item = evt.getItem();
        TARDISItems heldItem = ServiceProvider.itemService().getItem(item);
        Action action = evt.getAction();
        boolean rightClick = action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK;
        
        switch(heldItem)
        {
            case TARDIS_EGG:
            {
                if(rightClick) 
                {
                    evt.setCancelled(true);
                    Player player = evt.getPlayer();
                    TARDIS existingTardis = TARDISProvider.getTardisForPlayer(player.getUniqueId());
                    if(existingTardis != null) 
                    {
                        MessageService.say(player, "You already have a TARDIS");
                        return;
                    }

                    CustomizeTARDISMenu menu = ServiceProvider.inventoryMenuService().getCustomizeTardisMenu(player.getUniqueId());
                    menu.update(item);
                    UseSpawnEggAction spawnAction = menu.getUseSpawnEggAction();
                    if((spawnAction == UseSpawnEggAction.CREATE || spawnAction == UseSpawnEggAction.PREVIEW) && !player.isSneaking())
                    {
                        if(action == Action.RIGHT_CLICK_BLOCK)
                        {
                            Block block = evt.getClickedBlock();
                            if(spawnAction == UseSpawnEggAction.CREATE)
                            {
                                player.getInventory().remove(item);
                                new TARDIS(player.getUniqueId(), menu.getAppearance(), block.getX(), block.getY() + 1, block.getZ(), block.getWorld(), player.getFacing().getOppositeFace());
                            }
                            else 
                            {
                                if(!TARDISProvider.isPreviewingTardis(player.getUniqueId()))
                                {
                                    TARDIS tardis = new TARDIS(player.getName(), menu.getAppearance(), block.getX(), block.getY() + 1, block.getZ(), block.getWorld(), player.getFacing().getOppositeFace());
                                    TARDISProvider.addTemporaryTardis(player.getUniqueId(), tardis);
                                    Bukkit.getScheduler().scheduleSyncDelayedTask(UtilProvider.TARDISPlugin(), () -> tardis.remove(false, false), 20 * 7);
                                }
                            }
                        }
                        else 
                        {
                            MessageService.say(player, "Right click on a block to " + (menu.getUseSpawnEggAction() == UseSpawnEggAction.CREATE ? "spawn" : "preview") + " a TARDIS or crouch right click to open up the customization menu.");
                        }
                    }
                    else 
                    {
                        menu.open(player);
                    }
                }
                break;
            }
            case MEASURING_STICK:
            {
                if(evt.hasBlock())
                {
                    evt.setCancelled(true);
                    ItemMeta meta = item.getItemMeta();
                    List<String> lore = meta.getLore();
                    if(lore == null) lore = new EList<String>();
                    Point to = new Point(evt.getClickedBlock());
                    if(lore.isEmpty())
                    {
                        lore.add(MessageFormat.format("{0,number,#}, {1,number,#}, {2,number,#}", to.getX(), to.getY(), to.getZ()));
                    }
                    else
                    {
                        String[] parts = lore.get(0).split(", ");
                        Point from = new Point(StringHelper.toIntOrDefault(parts[0]), StringHelper.toIntOrDefault(parts[1]), StringHelper.toIntOrDefault(parts[2]));
                        Point dist = from.subtract(to).abs();
                        MessageService.say(evt.getPlayer(), MessageFormat.format("Distance: {0} x {1} x {2}", dist.getX() + 1, dist.getY() + 1, dist.getZ() + 1));
                        lore.clear();
                        Player p = evt.getPlayer();
                        ItemStack is = p.getInventory().getItemInOffHand();
                        Material mat = is == null ? Material.AIR : is.getType();
                        if(item.getAmount() > 1)
                        {
                            if(!p.isSneaking())
                            {
                                item.setAmount(1);
                            }
                            World w = evt.getClickedBlock().getWorld();
                            Tuple<Point, Point> pp = from.order(to);
                            for(int x = pp.item1.getX(); x <= pp.item2.getX(); x++)
                            {
                                for(int y = pp.item1.getY(); y <= pp.item2.getY(); y++)
                                {
                                    for(int z = pp.item1.getZ(); z <= pp.item2.getZ(); z++)
                                    {
                                        w.getBlockAt(x, y, z).setType(mat);
                                    }
                                }
                            }
                        }
                    }
                    meta.setLore(lore);
                    item.setItemMeta(meta);
                }
                break;
            }
            case TELEPORT_STICK:
            {
                evt.setCancelled(true);
                Player player = evt.getPlayer();
                Vector toAdd = player.getLocation().getDirection().normalize().multiply(10);
                //Something weirds going on that triggers this twice when clicking on a block
                new BukkitRunnable(){
                    public void run() {
                        player.teleport(player.getLocation().add(toAdd));
                    }
                }.runTaskLater(UtilProvider.TARDISPlugin(), 1);
                break;
            }
            case TOAST:
            case BURNT_TOAST:
            {
                if(rightClick)
                {
                    Player player = evt.getPlayer();
                    if(player.getFoodLevel() < 20)
                    {
                        evt.setCancelled(true);
                        boolean isBurnt = heldItem == TARDISItems.BURNT_TOAST;
                        item.setAmount(item.getAmount() - 1);
                        player.setFoodLevel(Math.min(player.getFoodLevel() + (isBurnt ? 2 : 6), 20));
                        if(isBurnt)
                        {
                            double rand = Math.random();
                            if(rand > 0.25)
                            {
                                int level = 1;
                                int time = (int) (15 * 20 * rand);
                                if(player.hasPotionEffect(PotionEffectType.HUNGER))
                                {
                                    PotionEffect current = player.getPotionEffect(PotionEffectType.HUNGER);
                                    time += current.getDuration();
                                    level += Math.round(current.getAmplifier() * 1.7);
                                    player.removePotionEffect(PotionEffectType.HUNGER);
                                }
                                player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, Math.min(time, 30 * 20), Math.min(level, 35)));
                            }
                        }
                        else 
                        {
                            player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 5, 2, true));
                        }

                    }
                }
            }
            case NONE:
            default:
            {
                break;
            }
        }
    }
}
