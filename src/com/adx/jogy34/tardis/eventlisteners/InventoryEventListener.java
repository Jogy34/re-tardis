package com.adx.jogy34.tardis.eventlisteners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.adx.jogy34.tardis.enums.TARDISItems;
import com.adx.jogy34.tardis.eventlisteners.annotations.TardisEventListener;
import com.adx.jogy34.tardis.menu.base.InventoryId;
import com.adx.jogy34.tardis.providers.ServiceProvider;

@TardisEventListener
public class InventoryEventListener implements Listener
{
    @EventHandler
    public void onMenuUse(InventoryClickEvent evnt)
    {
        if(evnt.getWhoClicked() instanceof Player)
        {
            Player player = ((Player) evnt.getWhoClicked());
            Inventory inven = player.getOpenInventory().getTopInventory();
            if(inven.getHolder() instanceof InventoryId)
            {
                evnt.setCancelled(true);
                if(evnt.getRawSlot() < inven.getSize())
                {
                    InventoryId id = ((InventoryId) inven.getHolder());
                    id.notifyUse(player, evnt.getCurrentItem());
                }
            }
        }
    }

    @EventHandler
    public void onMenuClose(InventoryCloseEvent evnt)
    {
        if(evnt.getPlayer() instanceof Player && evnt.getInventory().getHolder() != null)
        {
            Player player = ((Player) evnt.getPlayer());
            if(evnt.getInventory().getHolder() instanceof InventoryId)
            {
                InventoryId id = ((InventoryId) evnt.getInventory().getHolder());
                id.notifyClose(player);
            }
        }
    }

    @EventHandler
    public void fixAnvilNamingOfItems(PrepareAnvilEvent event)
    {
        ItemStack in = event.getInventory().getItem(0);
        ItemStack result = event.getResult();
        if(in != null && result != null && ServiceProvider.itemService().getItem(in) == TARDISItems.DIAMOND_PLATED_ELYTRA) {
            ItemMeta meta = result.getItemMeta();
            meta.setDisplayName(ChatColor.stripColor(result.getItemMeta().getDisplayName()));
            result.setItemMeta(meta);
            event.setResult(result);
        }
    }

    @EventHandler
    public void fixAnvilNamingOfItems(InventoryClickEvent event)
    {
        if(event.getView().getTopInventory() instanceof AnvilInventory && event.getSlot() == 2)
        {
            ItemStack in = event.getView().getTopInventory().getItem(0);
            if(ServiceProvider.itemService().getItem(in) == TARDISItems.DIAMOND_PLATED_ELYTRA)
            {
                ItemStack changed = event.getCurrentItem();
                ItemMeta meta = changed.getItemMeta();
                if(meta.getDisplayName().startsWith("b"))
                {
                    meta.setDisplayName(meta.getDisplayName().replaceFirst("b", ""));
                }
                changed.setItemMeta(meta);
            }
        }
    }
}
