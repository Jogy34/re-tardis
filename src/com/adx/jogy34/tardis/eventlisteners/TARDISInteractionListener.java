package com.adx.jogy34.tardis.eventlisteners;

import com.adx.jogy34.tardis.enums.TARDISWorlds;
import com.adx.jogy34.tardis.eventlisteners.annotations.TardisEventListener;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Door;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

@TardisEventListener
public class TARDISInteractionListener implements Listener
{
    @EventHandler
    public void enterOrLeaveTardis(PlayerInteractEvent event)
    {
        if(event.getHand() != EquipmentSlot.HAND) return;
        
        if(event.getAction() == Action.RIGHT_CLICK_BLOCK)
        {
            Player p = event.getPlayer();
            Block clickedBlock = event.getClickedBlock();
            BlockData blockData = clickedBlock.getBlockData();
            TARDIS clicked = TARDISProvider.getTardisForLocation(event.getClickedBlock());
            if(blockData instanceof Door)
            {
                Door door = (Door) blockData;
                TARDIS t = TARDISProvider.getTardisForLocation(p.getLocation());
                if(t != null && !clicked.isTemporary() && t == clicked && t.isAllowed(p))
                {
                    if(door.isOpen()) 
                    {
                        t.enter(p);
                    }
                }
                else if(ServiceProvider.worldService().getTypeForWorld(p.getWorld()) == TARDISWorlds.TARDIS_WORLD)
                {
                    TARDIS inside = TARDISProvider.getTardisForInterior(clickedBlock);
                    if(inside != null && inside.isExitDoor(clickedBlock))
                    {
                        event.setCancelled(true);
                        if(!inside.leave(p))
                        {
                            MessageService.say(p, "You are not currently able to leave this TARDIS");
                        }
                    }
                }
            }

            if(clicked != null && p.isSneaking() && clicked.getPermissionLevel(p).canFly())
            {
                if(event.getBlockFace().getModY() == 0)
                {
                    event.setCancelled(true);
                    BlockFace dir = event.getBlockFace();
                    
                    if(clicked.isInsideTardis(p.getLocation()))
                    {
                        dir = dir.getOppositeFace();
                    }
                    
                    clicked.faceDirection(dir);
                }
            }
        }
    }
    
    @EventHandler
    public void interactInsideTardis(PlayerInteractEvent event)
    {
        if(event.getHand() != EquipmentSlot.HAND) return;
        
        if(event.getAction() == Action.RIGHT_CLICK_BLOCK)
        {
            Block clicked = event.getClickedBlock();
            TARDIS inside = TARDISProvider.getTardisForInterior(clicked);
            if(inside != null)
            {
                if(inside.interactWith(event.getPlayer(), clicked, event.getBlockFace()))
                {
                    event.setCancelled(true);
                }
            }
        }
    }
}