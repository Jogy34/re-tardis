package com.adx.jogy34.tardis.eventlisteners;

import java.util.UUID;

import com.adx.jogy34.tardis.actions.data.SelectBlockActionData;
import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;
import com.adx.jogy34.tardis.eventlisteners.annotations.TardisEventListener;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.services.interfaces.IActionService;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;

@TardisEventListener
public class ActionEventListener implements Listener
{
    @EventHandler
    public void onBlockAction(PlayerInteractEvent event)
    {
        if(event.getHand() != EquipmentSlot.HAND) return;
        
        IActionService actionService = ServiceProvider.actionService();
        Player player = event.getPlayer();
        UUID playerId = player.getUniqueId();
        if(actionService.isValidUpdateAction(playerId, TardisActionDataTypes.SELECT_BLOCK))
        {
            if(actionService.performAction(playerId, new SelectBlockActionData(event.getClickedBlock(), event.getAction(), player.getInventory().getItemInMainHand(), player.isSneaking(), event.getBlockFace())))
            {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event)
    {
        ServiceProvider.actionService().stopAction(event.getPlayer().getUniqueId());
    }
}