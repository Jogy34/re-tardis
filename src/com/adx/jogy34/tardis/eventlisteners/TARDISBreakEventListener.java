package com.adx.jogy34.tardis.eventlisteners;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.eventlisteners.annotations.TardisEventListener;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.tardis.TARDIS;

import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

@TardisEventListener
public class TARDISBreakEventListener implements Listener
{
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) { preventIfInsideTardis(event); }

    @EventHandler
    public void onBlockDamage(BlockDamageEvent event) { preventIfInsideTardis(event); }
    
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) { preventIfInsideTardis(event); }

    @EventHandler
    public void onBlockBurn(BlockBurnEvent event)  { preventIfInsideTardis(event); }

    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent event)  { preventIfInsideTardis(event); }

    protected void preventIfInsideTardis(BlockEvent event)
    {
        if(event instanceof Cancellable)
        {
            TARDIS tardis = TARDISProvider.getTardisForLocation(event.getBlock());
            if(tardis != null)
            {
                ((Cancellable) event).setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent event)
    {
        EList<Block> remove = new EList<Block>();
        for(Block b : event.blockList())
        {
            if(TARDISProvider.getTardisForLocation(b) != null) remove.add(b);
        }

        event.blockList().removeAll(remove);
    }
}