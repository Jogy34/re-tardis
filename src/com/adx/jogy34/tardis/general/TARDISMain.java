package com.adx.jogy34.tardis.general;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.providers.CostProvider;
import com.adx.jogy34.tardis.providers.QuoteProvider;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.providers.UtilProvider;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.Text;

public class TARDISMain extends JavaPlugin
{
    @Override
    public void onEnable()
    {
        //These needs to be first
        UtilProvider.TARDISPlugin(this);
        ConfigProvider.initDefaultConfig();
        Text.setLocale(ConfigProvider.getConfig().getString(ConfigProvider.Constants.LOCALE));

        Logger.init();
        CostProvider.init();
        QuoteProvider.init();

        ServiceProvider.itemService().init();
        ServiceProvider.commandService().init();
        ServiceProvider.worldService().init();
        ServiceProvider.eventService().init();

        ServiceProvider.schematicService().loadSchematics();
        TARDISProvider.loadTardises();
        
        ConsoleCommandSender console = Bukkit.getConsoleSender();

        console.sendMessage(MessageFormat.format("{0}___{1}[]{0}___", ChatColor.BLUE, ChatColor.GOLD));
        console.sendMessage(MessageFormat.format("{0}|__||__|", ChatColor.BLUE));
        console.sendMessage(MessageFormat.format("{0}||_||_||", ChatColor.BLUE));
        console.sendMessage(MessageFormat.format("{0}|{1}##{0}||__|", ChatColor.BLUE, ChatColor.WHITE));
        console.sendMessage(MessageFormat.format("{0}||_||_||", ChatColor.BLUE));
        console.sendMessage(MessageFormat.format("{0}||_||_||", ChatColor.BLUE));
        console.sendMessage(MessageFormat.format("{0}||_||_||", ChatColor.BLUE));
        console.sendMessage(MessageFormat.format("{0}========", ChatColor.BLUE));
    }
    
    @Override
    public void onDisable()
    {
        TARDISProvider.onPluginDisable();
        ServiceProvider.actionService().stopAllActions();
        ServiceProvider.inventoryMenuService().onPluginDisable();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        String cmd = command.getName().toLowerCase();
        if(cmd.equals("tardis") && args.length > 0)
        {
            return ServiceProvider.commandService().handleCommand(args[0], sender, args.length > 1 ? Arrays.copyOfRange(args, 1, args.length) : new String[0]);
        }

        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) 
    {
        String cmd = command.getName().toLowerCase();
        if(cmd.equals("tardis"))
        {
            return ServiceProvider.commandService().handleTabComplete(args.length > 0 ? args[0] : null, sender, args.length > 1 ? Arrays.copyOfRange(args, 1, args.length) : new String[0]);
        }

        return new EList<String>();
    }

    public File getJarFile()
    {
        return this.getFile();
    }

    public ClassLoader getBukkitClassLoader()
    {
        return this.getClassLoader();
    }
}
