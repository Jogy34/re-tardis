package com.adx.jogy34.tardis.hierarchy.interfaces;

public interface IHierarchy<T extends IHierarchyBase> extends IHierarchyBase
{
    public T getParent();
    public <G extends T> G getParentAs(Class<G> clazz);
    public <TAncestor> TAncestor getAncestor(Class<TAncestor> clazz);
    <TAncestor> TAncestor getAncestorOrDefault(Class<TAncestor> clazz);
    public void initializeHierarchy(T parent);
}