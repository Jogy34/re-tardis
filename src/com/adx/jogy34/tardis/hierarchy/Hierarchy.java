package com.adx.jogy34.tardis.hierarchy;

import com.adx.jogy34.tardis.hierarchy.interfaces.IHierarchy;
import com.adx.jogy34.tardis.hierarchy.interfaces.IHierarchyBase;

public abstract class Hierarchy<T extends IHierarchyBase> implements IHierarchy<T>
{
    private T parent;

    @Override
    public T getParent() 
    {
        return parent;
    }

    @Override
    public <G extends T> G getParentAs(Class<G> clazz) 
    {
        if(clazz.isAssignableFrom(parent.getClass()))
        {
            return clazz.cast(parent);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <TAncestor> TAncestor getAncestor(Class<TAncestor> clazz)
    {
        IHierarchyBase next = parent;
        while(next == null || !(clazz.isAssignableFrom(next.getClass())))
        {
            if(next instanceof IHierarchy<?>)
            {
                next = ((IHierarchy<?>) next).getParent();
            }
            else 
            {
                next = null;
            }

            if(next == null)
            {
                throw new IllegalArgumentException(this.getClass().getName() + " does not contain an ancestor or type " + clazz.getName());
            }
        }

        return (TAncestor) next;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <TAncestor> TAncestor getAncestorOrDefault(Class<TAncestor> clazz)
    {
        IHierarchyBase next = parent;
        while(next == null || !(clazz.isAssignableFrom(next.getClass())))
        {
            if(next instanceof IHierarchy<?>)
            {
                next = ((IHierarchy<?>) next).getParent();
            }
            else 
            {
                return null;
            }
        }

        return (TAncestor) next;
    }

    @Override
    public void initializeHierarchy(T parent) 
    {
        this.parent = parent;
        this.initializeHierarchy();
    }

}