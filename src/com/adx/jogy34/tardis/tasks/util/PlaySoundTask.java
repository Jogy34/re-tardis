package com.adx.jogy34.tardis.tasks.util;

import com.adx.jogy34.tardis.tasks.base.TardisTask;
import com.adx.jogy34.tardis.util.Timespan;

import org.bukkit.Location;
import org.bukkit.Sound;

public class PlaySoundTask extends TardisTask 
{
    protected Location playAt;
    protected Sound sound;
    protected float volume, pitch;

    public PlaySoundTask(Location playAt, Sound sound, float volume, float pitch)
    {
        this.playAt = playAt;
        this.sound = sound;
        this.volume = volume;
        this.pitch = pitch;
    }

    @Override
    public Timespan step() 
    {
        this.playAt.getWorld().playSound(playAt, sound, volume, pitch);
        this.finished = true;
        return Timespan.empty();
    }

    @Override
    public void finishRemainingWork() 
    {
        this.playAt.getWorld().playSound(playAt, sound, volume, pitch);
        this.finished = true;
    }
}