package com.adx.jogy34.tardis.tasks.base;

public abstract class TardisTask implements ITardisTask
{
    protected boolean finished = false;

    public boolean isDone()
    {
        return finished;
    }

    public void start() { }
    public void end() { }
}