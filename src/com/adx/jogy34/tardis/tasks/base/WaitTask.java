package com.adx.jogy34.tardis.tasks.base;

import com.adx.jogy34.tardis.util.Timespan;

public class WaitTask extends TardisTask
{
    protected Timespan end;
    protected Timespan timeout;

    public WaitTask(Timespan timeout)
    {
        super();
        this.timeout = timeout;
    }

    @Override
    public void start() {
        this.end = Timespan.now().add(timeout);
    }

    @Override
    public Timespan step() {
        Timespan remaining = this.end.subtract(Timespan.now());

        if(remaining.getTicks() <= 0)
        {
            this.finished = true;
        }

        return remaining;
    }

    @Override
    public void finishRemainingWork() {
        this.finished = true;
    }
}