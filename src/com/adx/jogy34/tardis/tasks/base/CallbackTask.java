package com.adx.jogy34.tardis.tasks.base;

import com.adx.jlinq.interfaces.Actions.IAction0;
import com.adx.jogy34.tardis.util.Timespan;

public class CallbackTask extends TardisTask {

    protected IAction0 action;
    public CallbackTask(IAction0 action)
    {
        this.action = action;
    }

    @Override
    public Timespan step() {
        this.action.invoke();
        this.finished = true;
        return Timespan.empty();
    }

    @Override
    public void finishRemainingWork() {
        this.action.invoke();
    }
}