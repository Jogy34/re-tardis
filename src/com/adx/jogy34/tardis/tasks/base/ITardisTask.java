package com.adx.jogy34.tardis.tasks.base;

import com.adx.jogy34.tardis.util.Timespan;

public interface ITardisTask
{
    void start();
    Timespan step();
    void end();
    void finishRemainingWork();
    boolean isDone();
}