package com.adx.jogy34.tardis.tasks.base;

import com.adx.jlinq.EList;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jogy34.tardis.util.Timespan;

public class ParallelTask extends TardisTask 
{
    protected EList<ITardisTask> tasks = new EList<ITardisTask>();
    protected boolean hasStarted = false;

    public ParallelTask(IEnumerable<ITardisTask> tasks)
    {
        this.tasks.AddAll(tasks);
    }

    public ParallelTask(ITardisTask... tasks)
    {
        this.tasks.AddAll(tasks);
    }

    public void add(ITardisTask task)
    {
        if(this.hasStarted) throw new IllegalArgumentException("Cannot add to a parallel task which has already started");

        this.tasks.add(task);
    }

    @Override
    public void start() 
    {
        if(tasks.isEmpty()) throw new IllegalArgumentException("Parallel tasks must contain at least one item");

        this.hasStarted = true;
        tasks.ForEach(t -> t.start());
    }

    @Override
    public Timespan step() 
    {
        EList<ITardisTask> tasksToPlay = tasks.Where(t -> !t.isDone()).ToList();
        EList<Timespan> times = tasksToPlay.Select(t -> t.step()).ToList();
        tasksToPlay.Where(t -> t.isDone()).ForEach(t -> t.end());

        if(tasks.All(t -> t.isDone()))
        {
            this.finished = true;
            return Timespan.empty();
        }

        return times.OrderBy(x -> x.getTicks()).First();

        
    }

    @Override
    public void end() 
    {
    }
    
    @Override
    public void finishRemainingWork() 
    {
        tasks.ForEach(t -> t.finishRemainingWork());
    }

}