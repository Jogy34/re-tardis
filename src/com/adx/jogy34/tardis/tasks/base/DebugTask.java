package com.adx.jogy34.tardis.tasks.base;

import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.Timespan;

public class DebugTask extends TardisTask
{
    private String message;
    public DebugTask(String message)
    {
        this.message = message;
    }

    @Override
    public Timespan step() {
        Logger.debug(this.message);
        this.finished = true;
        return Timespan.empty();
    }
    
    @Override
    public void finishRemainingWork() {
        this.finished = true;
    }
}