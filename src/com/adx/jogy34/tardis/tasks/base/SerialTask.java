package com.adx.jogy34.tardis.tasks.base;

import com.adx.jlinq.EList;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jogy34.tardis.util.Timespan;

public class SerialTask extends TardisTask
{
    protected EList<ITardisTask> tasks = new EList<ITardisTask>();
    protected int currentTask = 0;
    protected boolean hasStarted = false;

    public SerialTask(IEnumerable<ITardisTask> tasks)
    {
        this.tasks.AddAll(tasks);
    }

    public SerialTask(ITardisTask... tasks)
    {
        this.tasks.AddAll(tasks);
    }

    public void add(ITardisTask task)
    {
        if(this.hasStarted) throw new IllegalArgumentException("Cannot add to a serial task which has already started");

        this.tasks.add(task);
    }

    @Override
    public void start() {
        if(tasks.isEmpty()) throw new IllegalArgumentException("Serial tasks must contain at least one item");

        this.hasStarted = true;
        tasks.First().start();
    }

    @Override
    public Timespan step() 
    {
        ITardisTask current = tasks.get(currentTask);
        Timespan wait = current.step();
        
        if(current.isDone())
        {
            current.end();
            currentTask++;
            if(currentTask >= tasks.Count()) {
                this.finished = true;
                return Timespan.empty();
            }
            else
            {
                tasks.get(currentTask).start();
                return Timespan.fromTicks(1);
            }
        }
        
        return wait;
    }

    @Override
    public void finishRemainingWork() {
        this.tasks.ForEach(x -> x.finishRemainingWork());
        this.finished = true;
    }
    
}