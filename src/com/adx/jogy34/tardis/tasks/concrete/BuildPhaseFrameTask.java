package com.adx.jogy34.tardis.tasks.concrete;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.tasks.base.TardisTask;
import com.adx.jogy34.tardis.util.Timespan;
import com.adx.jogy34.tardis.util.helpers.IntHelper;

import org.bukkit.Material;

public class BuildPhaseFrameTask extends TardisTask 
{
    private EList<Material> primaryBlocks, secondaryBlocks;
    private TARDIS t;
    
    public BuildPhaseFrameTask(TARDIS t, EList<Material> primaryBlocks, EList<Material> secondaryBlocks)
    {
        this.t = t;
        this.primaryBlocks = primaryBlocks;
        this.secondaryBlocks = secondaryBlocks;
    }

    @Override
    public Timespan step() 
    {
        for(int x = -1; x <= 1; x++)
        {
            for(int y = 0; y <= TARDIS.BASE_HEIGHT; y++) 
            {
                for(int z = -1; z <= 1; z++)
                {
                    Material block = y == TARDIS.BASE_HEIGHT ? 
                        secondaryBlocks.get(IntHelper.random(secondaryBlocks.size())) :
                        primaryBlocks.get(IntHelper.random(primaryBlocks.size()));

                    t.getPosition().getBlockRelative(x, y, z).setType(block);
                }
            }
        }

        t.getPosition().getBlockRelative(0, TARDIS.TOTAL_HEIGHT, 0).setType(secondaryBlocks.get(IntHelper.random(secondaryBlocks.size())));

        this.finished = true;

        return Timespan.empty();
    }

    @Override
    public void finishRemainingWork() {

    }
}