package com.adx.jogy34.tardis.tasks.concrete;

import com.adx.jlinq.EList;
import com.adx.jlinq.interfaces.Actions.IAction0;
import com.adx.jlinq.tuples.Tuples.Tuple;
import com.adx.jogy34.tardis.data.TardisPhaseData;
import com.adx.jogy34.tardis.enums.TardisColor;
import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.tasks.base.CallbackTask;
import com.adx.jogy34.tardis.tasks.base.ParallelTask;
import com.adx.jogy34.tardis.tasks.base.SerialTask;
import com.adx.jogy34.tardis.tasks.base.TardisTask;
import com.adx.jogy34.tardis.tasks.base.WaitTask;
import com.adx.jogy34.tardis.tasks.util.PlaySoundTask;
import com.adx.jogy34.tardis.util.Timespan;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;

public class TardisPhaseTask extends TardisTask 
{
    protected IAction0 callback;
    protected TardisColor primary, secondary;
    protected TARDIS t;
    protected ParallelTask phasing;

    public TardisPhaseTask(TARDIS t, IAction0 callback)
    {
        this.t = t;
        this.callback = callback;

        Tuple<TardisColor, TardisColor> colors = TardisColor.from(t);
        primary = colors.item1;
        secondary = colors.item2;
    }

    @Override
    public void start() 
    {
        boolean doAnimation = ConfigProvider.getConfig().getBoolean(ConfigProvider.Constants.PHASING_ANIMATION);
        boolean doSound = ConfigProvider.getConfig().getBoolean(ConfigProvider.Constants.PHASING_SOUND);
        if(doAnimation || doSound)
        {
            if(t.getPosition() == null || t.getSettings().getInterior().getSpawnLocation() == null)
            {
                phasing = null;
                return;
            }
            
            t.remove(false);
            if(doAnimation && doSound)
            {
                phasing = new ParallelTask(
                    playAnimation(),
                    playSounds(t.getPosition().getBlock().getLocation()),
                    playSounds(t.getSettings().getInterior().getSpawnLocation())
                );
            }
            else if(doAnimation)
            {
                phasing = new ParallelTask(playAnimation());
            }
            else 
            {
                phasing = new ParallelTask(
                    playSounds(t.getPosition().getBlock().getLocation()),
                    playSounds(t.getSettings().getInterior().getSpawnLocation())
                );
            }

        }
        else 
        {
            phasing = null;
        }

    }

    @Override
    public Timespan step() 
    {
        if(phasing == null)
        {
            this.finished = true;
            return Timespan.empty();
        }
        Timespan next = phasing.step();
        if(phasing.isDone()) this.finished = true;
        return next;
    }

    @Override
    public void end() 
    {
        if(callback != null) callback.invoke();
    }

    @Override
    public void finishRemainingWork() 
    {
        phasing.finishRemainingWork();
    }

    private SerialTask playAnimation()
    {
        EList<Material> primaryBlocks = TardisPhaseData.PHASE_BLOCKS_MAP.get(primary);
        EList<Material> secondaryBlocks = TardisPhaseData.PHASE_BLOCKS_MAP.get(secondary);
        EList<Material> clearBlocks = TardisPhaseData.CLEAR_PHASE_BLOCKS;

        Timespan[] times = {
            Timespan.fromTicks(7), Timespan.fromTicks(5), Timespan.fromTicks(4), Timespan.fromTicks(6),
            Timespan.fromTicks(8), Timespan.fromTicks(9), Timespan.fromTicks(5), Timespan.fromTicks(1),
            Timespan.fromTicks(3), Timespan.fromTicks(5), Timespan.fromTicks(9), Timespan.fromTicks(2),
            Timespan.fromTicks(1), Timespan.fromTicks(4), Timespan.fromTicks(2), Timespan.fromTicks(2),
        };

        SerialTask animation = new SerialTask();
        for(int i = 0; i < times.length; i++)
        {
            animation.add(new BuildPhaseFrameTask(t, i % 2 == 0 ? primaryBlocks : clearBlocks, i % 2 == 0 ? secondaryBlocks : clearBlocks));
            animation.add(new WaitTask(times[i]));
        }
        
        animation.add(new CallbackTask(() -> t.remove(false)));

        return animation;
    }

    private SerialTask playSounds(Location playAt)
    {
        float volume = 2f;
        float minecartPitch = 1.5f;
        int waitTicks = 12;

        return new SerialTask(
            new ParallelTask(
                new PlaySoundTask(playAt, Sound.ENTITY_MINECART_RIDING, volume, minecartPitch),
                new PlaySoundTask(playAt, Sound.BLOCK_FIRE_EXTINGUISH, volume, 2)
            ),
            new WaitTask(Timespan.fromTicks(waitTicks)),
            new ParallelTask(
                new PlaySoundTask(playAt, Sound.ENTITY_MINECART_RIDING, volume, minecartPitch),
                new PlaySoundTask(playAt, Sound.BLOCK_ANVIL_DESTROY, volume, 2),
                new PlaySoundTask(playAt, Sound.BLOCK_PORTAL_TRAVEL, 0.25f, 1.25f)
            ),
            new WaitTask(Timespan.fromTicks(waitTicks)),
            new ParallelTask(
                new PlaySoundTask(playAt, Sound.ENTITY_MINECART_RIDING, volume, minecartPitch),
                new PlaySoundTask(playAt, Sound.BLOCK_FIRE_EXTINGUISH, volume, 2)
            ),
            new WaitTask(Timespan.fromTicks(waitTicks)),
            new ParallelTask(
                new PlaySoundTask(playAt, Sound.ENTITY_MINECART_RIDING, volume, minecartPitch),
                new PlaySoundTask(playAt, Sound.BLOCK_ANVIL_DESTROY, volume, 2)
            ),
            new WaitTask(Timespan.fromTicks(waitTicks)),
            new ParallelTask(
                new PlaySoundTask(playAt, Sound.ENTITY_MINECART_RIDING, volume, minecartPitch),
                new PlaySoundTask(playAt, Sound.BLOCK_FIRE_EXTINGUISH, volume, 2)
            ),
            new WaitTask(Timespan.fromTicks(waitTicks)),
            new ParallelTask(
                new PlaySoundTask(playAt, Sound.ENTITY_MINECART_RIDING, volume, minecartPitch),
                new PlaySoundTask(playAt, Sound.BLOCK_ANVIL_DESTROY, volume, 2)
            )
        );
    }
}