package com.adx.jogy34.tardis.tasks.concrete;

import com.adx.jlinq.EList;
import com.adx.jlinq.tuples.Tuples.Tuple;
import com.adx.jogy34.tardis.enums.TARDISItems;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.tasks.base.CallbackTask;
import com.adx.jogy34.tardis.tasks.base.SerialTask;
import com.adx.jogy34.tardis.tasks.base.TardisTask;
import com.adx.jogy34.tardis.tasks.base.WaitTask;
import com.adx.jogy34.tardis.tasks.util.PlaySoundTask;
import com.adx.jogy34.tardis.util.Timespan;
import com.adx.jogy34.tardis.util.spacial.Position;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ToastTask extends TardisTask
{
    protected static EList<Position> inProgressToasters = new EList<Position>();

    public static Tuple<Boolean, ToastTask> create(Location toaster, Player clicking)
    {
        Position pos = new Position(toaster);
        if(!inProgressToasters.Any(x -> x.equals(pos)))
        {
            ItemStack inHand = clicking.getInventory().getItemInMainHand();
            if(inHand.getType() != Material.BREAD)
            {
                inHand = clicking.getInventory().getItemInOffHand();
            }
            
            if(inHand.getType() == Material.BREAD)
            {
                TARDISItems type = ServiceProvider.itemService().getItem(inHand);
                int count = inHand.getAmount() == 1 ? 1 : 2;
                inHand.setAmount(inHand.getAmount() - count);
                if(type == TARDISItems.BURNT_TOAST) 
                {
                    return new Tuple<Boolean,ToastTask>(true, new ToastTask(toaster, count, true, true));
                }
                else if(type == TARDISItems.TOAST || Math.random() < 0.2)
                {
                    return new Tuple<Boolean,ToastTask>(true, new ToastTask(toaster, count, true, false));
                }
                else 
                {
                    return new Tuple<Boolean,ToastTask>(true, new ToastTask(toaster, count, false, false));
                }
            }
        }
        
        return new Tuple<Boolean, ToastTask>(false, null);
    }

    protected SerialTask toasting;
    protected final Location toaster;
    protected final int count;
    protected final boolean isBurnt, isDust;
    protected Position pos;

    private ToastTask(Location toaster, int count, boolean isBurnt, boolean isDust)
    {
        this.toaster = toaster;
        this.count = count;
        this.isBurnt = isBurnt;
        this.isDust = isDust;
        this.pos = new Position(toaster);
    }

    @Override
    public void start() 
    {
        inProgressToasters.add(pos);
        toasting = new SerialTask();
        for(int i = 0; i < 18; i++)
        {
            toasting.add(new PlaySoundTask(toaster, Sound.BLOCK_LEVER_CLICK, 0.5f, 10));
            toasting.add(new WaitTask(Timespan.fromTicks(6)));
        }
        toasting.add(new PlaySoundTask(toaster, Sound.BLOCK_BELL_USE, 0.5f, 8));
        toasting.add(new CallbackTask(() -> {
            if(!isDust) {
                Location top = toaster.getBlock().getRelative(0, 1, 0).getLocation().add(0.5, -0.5, 0.5);
                ItemStack item = isBurnt ? TARDISItems.BURNT_TOAST.get() : TARDISItems.TOAST.get();
                for(int i = 0; i < count; i++) toaster.getWorld().dropItem(top, item);
            }
            if(isDust || isBurnt) {
                Location top = toaster.getBlock().getRelative(0, 1, 0).getLocation();
                toaster.getWorld().playEffect(top, Effect.SMOKE, 0);
            }
        }));

        toasting.start();
    }

    @Override
    public Timespan step() 
    {
        Timespan next = toasting.step();
        this.finished = toasting.isDone();
        return next;
    }

    @Override
    public void end() 
    {
        inProgressToasters.remove(pos);
    }

    @Override
    public void finishRemainingWork() 
    {
        inProgressToasters.remove(pos);
        toasting.finishRemainingWork();
        this.finished = true;
    }
}