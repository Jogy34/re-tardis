package com.adx.jogy34.tardis.actions;

import java.text.MessageFormat;
import java.util.UUID;

import com.adx.jlinq.EList;
import com.adx.jlinq.tuples.Tuples.Tuple;
import com.adx.jogy34.tardis.actions.data.PerformCommandData;
import com.adx.jogy34.tardis.actions.data.SelectBlockActionData;
import com.adx.jogy34.tardis.actions.data.SetTextActionData;
import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;
import com.adx.jogy34.tardis.actions.enums.TardisActions;
import com.adx.jogy34.tardis.actions.interfaces.ITardisActionData;
import com.adx.jogy34.tardis.enums.TARDISItems;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.schematics.AuxillaryData;
import com.adx.jogy34.tardis.schematics.Cost;
import com.adx.jogy34.tardis.schematics.AccessPoint;
import com.adx.jogy34.tardis.schematics.enums.AccessPointType;
import com.adx.jogy34.tardis.schematics.enums.SchematicType;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.util.spacial.Point;
import com.adx.jogy34.tardis.util.spacial.Position;
import com.adx.jogy34.tardis.util.helpers.ArrayHelper;
import com.adx.jogy34.tardis.util.helpers.StringHelper;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.Bisected.Half;
import org.bukkit.block.data.type.Door;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CreateSchematicAction extends TardisAction
{
    protected Position first;
    protected Position second;
    protected EList<AccessPoint> accessPoints = new EList<AccessPoint>();
    protected String name;
    protected String description;
    protected SchematicType type;
    protected AccessPointType selectingType;
    protected Cost cost;
    protected EList<AuxillaryData> auxData;
    protected Material icon = Material.BEDROCK;
    protected int iteration = 0;
    protected boolean replace = false;

    public CreateSchematicAction(UUID player, SchematicType type)
    {
        super(player, TardisActions.SELECT_SCHEMATIC, TardisActionDataTypes.SELECT_BLOCK, TardisActionDataTypes.SET_TEXT);
        this.type = type;
    }

    @Override
    public void start() 
    {
        Player p = Bukkit.getPlayer(player);
        MessageService.say(p, Text.USE_TOOLS_TO_CREATE_SCHEMATIC);
        removeItemsFromPlayer(p);
        selectingType = AccessPointType.getFirst(type);
        
        if(selectingType == null) p.getInventory().addItem(TARDISItems.SCHEMATIC_SELECT_ANCHOR_POINTS.get());
        else {
            p.getInventory().addItem(
                        TARDISItems.SCHEMATIC_SELECT_ANCHOR_POINTS.get(), 
                        TARDISItems.SCHEMATIC_SELECT_ACCESS_POINTS.getWithName(MessageFormat.format(" {0}({1})", ChatColor.GRAY, selectingType.getName())));
        }
    }

    @Override
    public boolean update(ITardisActionData data) 
    {
        switch(data.getType())
        {
            case SELECT_BLOCK:
            {
                if(data instanceof SelectBlockActionData)
                {
                    SelectBlockActionData select = (SelectBlockActionData) data;
                    TARDISItems item = ServiceProvider.itemService().getItem(select.holding());
                    if(item == TARDISItems.SCHEMATIC_SELECT_ANCHOR_POINTS || item == TARDISItems.SCHEMATIC_SELECT_ACCESS_POINTS)
                    {
                        UUID w = getWorldUID();
                        if(select.clickedBlock() && !select.wasSneaking())
                        {
                            if(w != null && !w.equals(select.getBlock().getWorld().getUID()))
                            {
                                MessageService.say(this.player, Text.MUST_BE_IN_SAME_WORLD);
                                return true;
                            }
                        }

                        if(item == TARDISItems.SCHEMATIC_SELECT_ANCHOR_POINTS)
                        {
                            if(select.clickedBlock() && !select.wasSneaking())
                            {
                                Position selected = new Position(select.getBlock());
                                boolean wasRightClick = select.wasRightClick();
                                int anchor = wasRightClick ? 1 : 2;
                                if(wasRightClick) first = selected;
                                else second = selected;
                    
                                MessageService.say(this.player, Text.format("{SET_ANCHOR_POINT_TO}", anchor, selected));
                            }
                        }
                        else if(selectingType != null)
                        {
                            if(select.wasSneaking())
                            {
                                AccessPointType next = select.wasRightClick() ? selectingType.getNext(type) : selectingType.getPrevious(type);
                                selectingType = next;
                                String msg = Text.format("{NOW_SELECTING}", selectingType.getName());
                                MessageService.say(this.player, msg);
                                ServiceProvider.itemService().updateName(select.holding(), MessageFormat.format(" {0}({1})", ChatColor.GRAY, selectingType.getName()));
                            }
                            else if(select.clickedBlock())
                            {
                                Block block = select.getBlock();
                                Position selected = new Position(block);
                                if(select.wasRightClick())
                                {
                                    if(accessPoints.Any(x -> x.getOffset().equals(selected)))
                                    {
                                        MessageService.say(this.player, Text.ACCESS_POINT_EXISTS_AT_LOCATION);
                                    }
                                    else
                                    {
                                        if(selectingType.isValidMaterial(block.getType()))
                                        {
                                            if(selectingType == AccessPointType.SPAWN)
                                            {
                                                Position actual = selected.add(0, 1, 0);
                                                block = actual.getBlock();
                                                AccessPoint toAdd = new AccessPoint(actual.toPoint(), selectingType);
                                                
                                                if(block.getType() != Material.AIR || block.getRelative(0, 1, 0).getType() != Material.AIR)
                                                {
                                                    MessageService.say(this.player, Text.SPAWN_POINT_MUST_BE_CLEAR);
                                                }
                                                else
                                                {
                                                    accessPoints = accessPoints.Where(x -> x.getType() != AccessPointType.SPAWN).ToList();
                                                    accessPoints.add(toAdd);
                                                    MessageService.say(this.player, Text.format("{SPAWN_POINT_SET_TO}", toAdd.getOffset().toString()));
                                                }
                                            }
                                            else
                                            {
                                                AccessPoint toAdd = new AccessPoint(selected.toPoint(), selectingType);
                                                
                                                if(selectingType == AccessPointType.EXIT_DOOR)
                                                {
                                                    Door doorData = (Door) block.getBlockData();
                                                    if(doorData.getHalf() == Half.TOP) 
                                                    {
                                                        toAdd = new AccessPoint(selected.add(0, -1, 0).toPoint(), selectingType);
                                                    }
                                                }

                                                accessPoints.add(toAdd);
                                                MessageService.say(this.player, Text.format("{ADDED_ACCESS_POINT_AT}", selectingType.getName(), toAdd.getOffset()));
                                            }
                                        }
                                        else
                                        {
                                            MessageService.say(this.player, Text.format("{NOT_VALID_FOR_ACCESS_POINT}", selectingType.getName()));
                                        }
                                    }
                                }
                                else
                                {
                                    AccessPoint toRemove = accessPoints.FirstOrDefault(x -> x.getOffset().equals(selected));
                                    if(toRemove == null)
                                    {
                                        MessageService.say(this.player, Text.NO_ACCESS_POINT_AT_LOCATION);
                                    }
                                    else
                                    {
                                        accessPoints.remove(toRemove);
                                        MessageService.say(this.player, Text.format("{REMOVED_ACCESS_POINT_AT_LOCATION}", toRemove.getOffset().toString()));
                                    }
                                }
                            }
                        }
                        
                        return true;
                    }
        
                }
                break;
            }
            case SET_TEXT:
            {
                if(data instanceof SetTextActionData)
                {
                    this.name = ((SetTextActionData) data).getText();
                    return true;
                }
                break;
            }
            case PERFORM_COMMAND:
            {
                if(data instanceof PerformCommandData)
                {
                    PerformCommandData cmdData = ((PerformCommandData) data);
                    String cmd = cmdData.getCommand();
                    if(cmd.equalsIgnoreCase(Text.VALIDATE))
                    {
                        if(this.checkValid()) MessageService.say(player, Text.SCHEMATIC_IS_VALID);
                    }
                    else if(cmd.equalsIgnoreCase(Text.REPLACE))
                    {
                        this.replace = !this.replace;
                        MessageService.say(player, this.replace ? Text.SET_MODE_TO_REPLACE : Text.SET_MODE_TO_NEW);
                    }
                    else if(cmd.equalsIgnoreCase(Text.SET))
                    {
                        String[] args = cmdData.getArgs();
                        if(args.length > 1)
                        {
                            String arg = args[0];
                            if(arg.equals(Text.COST))
                            {
                                int amount = 1;
                                if(args.length > 2) amount = Math.max(StringHelper.toIntOrDefault(args[2], amount), amount);
                                
                                try
                                {
                                    Material mat = Material.valueOf(args[1].toUpperCase());
                                    cost = new Cost(amount, mat);
                                    MessageService.say(player, Text.format("{SET_COST_TO}", cost));
                                }
                                catch(Exception e)
                                {
                                    MessageService.say(player, Text.format("{NOT_VALID_MATERIAL}", args[1]));
                                }
                            }
                            else if(arg.equalsIgnoreCase(Text.DESCRIPTION))
                            {
                                this.description = ArrayHelper.joinRange(args, " ", 1);
                                MessageService.say(player, Text.DESCRIPTION_SET);
                            }
                            else if(arg.equalsIgnoreCase(Text.ICON))
                            {
                                try
                                {
                                    Material mat = Material.valueOf(args[1].toUpperCase());
                                    this.icon = mat;
                                    MessageService.say(player, Text.SET_ICON);
                                }
                                catch(Exception e)
                                {
                                    MessageService.say(player, Text.format("{NOT_VALID_MATERIAL}", args[1]));
                                }
                            }
                            else if(arg.equalsIgnoreCase(Text.ITERATION))
                            {
                                this.iteration = Math.max(StringHelper.toIntOrDefault(args[1]), 1);
                                MessageService.say(player, Text.format("{SCHEMATIC_ITERATION_SET_TO}", this.iteration));
                            }
                            else
                            {
                                if(this.auxData == null) this.auxData = new EList<AuxillaryData>();
                                AuxillaryData newData = new AuxillaryData(args[0], ArrayHelper.joinRange(args, " ", 1));
                                AuxillaryData existing = this.auxData.FirstOrDefault(x -> x.isSameType(newData));
                                if(existing != null) 
                                {
                                    existing.setValue(newData.getValue());
                                }
                                else
                                {
                                    this.auxData.add(newData);
                                }
                            }
                        }
                        else
                        {
                            MessageService.say(player, Text.SPECIFY_WHAT_TO_SET);
                        }
                    }
                }
                break;
            }
            default:
            {
                break;
            }
        }

        return false;
    }

    public boolean checkValid()
    {
        if(first == null || second == null)
        {
            MessageService.say(player, Text.MUST_SPECIFY_BOTH_ANCHOR_POINTS);
            return false;
        }

        Tuple<Position, Position> ordered = first.order(second);
        Position p1 = ordered.item1, p2 = ordered.item2;

        EList<AccessPoint> outsideBounds = this.accessPoints.Where(pt -> {
            Point off = pt.getOffset();
            return off.getX() < p1.getX() || off.getX() > p2.getX() ||
                   off.getY() < p1.getY() || off.getY() > p2.getY() ||
                   off.getZ() < p1.getZ() || off.getZ() > p2.getZ();
        }).ToList();

        if(outsideBounds.Any())
        {
            MessageService.say(player, Text.format("{SELECTED_ACCESS_POINTS_OUTSIDE_BOUNDS}", p1, p2, outsideBounds.ConcatenateToString(", ")));
            return false;
        }

        World w = first.getWorld();
        EList<AccessPoint> invalid = this.accessPoints.Where(pt -> pt.getType() != AccessPointType.SPAWN && !pt.getType().isValidMaterial(pt.getOffset().getBlock(w).getType())).ToList();
        EList<AccessPoint> spawnPoints = this.accessPoints.Where(x -> x.getType() == AccessPointType.SPAWN).ToList();
        if(spawnPoints.Any())
        {
            invalid.AddAll(spawnPoints.Where(sp -> {
                Block b = sp.getOffset().getBlock(w);
                return b.getType() != Material.AIR || b.getRelative(0, 1, 0).getType() != Material.AIR;
            }));
        }
        
        if(invalid.Any())
        {
            MessageService.say(player, Text.format("{SELECTED_ACCESS_POINTS_ARE_INVALID}", invalid.ConcatenateToString(", ")));
            this.accessPoints.removeAll(invalid);
            return false;
        }
        
        EList<AccessPointType> containing = this.accessPoints.Select(x -> x.getType()).ToList();
        EList<AccessPointType> missing = this.type.getRequiredAccessPoints().Where(x -> !containing.contains(x)).ToList();
        
        if(missing.Any())
        {
            MessageService.say(player, Text.format("{MISSING_REQUIRED_ACCESS_POINTS}", missing.Select(x -> x.getName()).ConcatenateToString(", ")));
            return false;
        }

        return true;
    }

    @Override
    public boolean complete() 
    {
        if(!checkValid()) return false;
        
        MessageService.say(player, Text.format("{SAVING_SCHEMATIC}", name, first, second));
        try
        {
            ServiceProvider.schematicService().createSchematic(first, second, name, this.getPlayer().getName(), description, type, cost, accessPoints, auxData, icon, iteration, replace);
        }
        catch(Exception e)
        {
            MessageService.say(player, Text.format("{UNABLE_TO_REPLACE_SCHEMATIC}", name));
            return false;
        }
        
        Player p = Bukkit.getPlayer(player);
        removeItemsFromPlayer(p);

        return true;
    }
    
    @Override
    public void cancel() 
    {
        Player p = Bukkit.getPlayer(player);
        if(p != null)
        {
            removeItemsFromPlayer(p);
        }
    }

    protected void removeItemsFromPlayer(Player p)
    {
        ItemStack[] items = p.getInventory().getContents();
        p.getInventory().removeItem(EList.from(items).Where(i -> {
            TARDISItems type = ServiceProvider.itemService().getItem(i);
            return type == TARDISItems.SCHEMATIC_SELECT_ACCESS_POINTS || 
                   type == TARDISItems.SCHEMATIC_SELECT_ANCHOR_POINTS;
        }).ToArray(ItemStack.class));
    }
    
    protected UUID getWorldUID()
    {
        if(first != null) return first.getWorldUID();
        if(second != null) return second.getWorldUID();

        return null;
    }
}