package com.adx.jogy34.tardis.actions;

import java.util.UUID;

import com.adx.jogy34.tardis.actions.data.SelectBlockActionData;
import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;
import com.adx.jogy34.tardis.actions.enums.TardisActions;
import com.adx.jogy34.tardis.actions.interfaces.ITardisActionData;
import com.adx.jogy34.tardis.providers.CostProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.schematics.AccessPoint;
import com.adx.jogy34.tardis.schematics.enums.AccessPointType;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.helpers.BlockHelper;
import com.adx.jogy34.tardis.util.spacial.Point;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class PlaceSignDisplayAction extends TardisAction
{
    protected AccessPointType placingType;

    public PlaceSignDisplayAction(UUID player, AccessPointType placingType)
    {
        super(player, TardisActions.PLACE_SIGN_DISPLAY, TardisActionDataTypes.SELECT_BLOCK);

        if(!placingType.isSignDisplay())
        {
            throw new IllegalArgumentException("Selected AccessPointType is not valid for this action");
        }

        this.placingType = placingType;
    }

    @Override
    public void start() 
    {
        String type = placingType == AccessPointType.CLOCK ? Text.CLOCK : Text.CONSOLE;
        MessageService.say(this.player, Text.format("{RIGHT_CLICK_TO_ADD}", type));
    }

    @Override
    public boolean update(ITardisActionData data) 
    {
        if(data.getType() == TardisActionDataTypes.SELECT_BLOCK)
        {
            SelectBlockActionData select = (SelectBlockActionData) data;
            if(select.clickedBlock() && select.wasRightClick())
            {
                Block selected = select.getBlock();
                Player p = getPlayer();
                TARDIS t = TARDISProvider.getTardisForPlayer(this.player);
                if(t == null) return true;
        
                if(!t.isWithinInteriorBounds(selected.getX()))
                {
                    MessageService.say(this.player, Text.MUST_BE_INSIDE_TARDIS);
                    shouldComplete = true;
                    return true;
                }

                if(selected.getType().isSolid())
                {
                    Block placeAt = selected.getRelative(select.getFace());
                    if(placeAt.getType() != Material.AIR)
                    {
                        MessageService.say(this.player, Text.SELECTED_LOCATION_OBSTRUCTED);
                        return false;
                    }
                    else
                    {
                        CostProvider.Costs cost =  placingType == AccessPointType.CLOCK ? CostProvider.Costs.CLOCKS : CostProvider.Costs.CONSOLES;
                        if(!cost.getCost().pay(p))
                        {
                            MessageService.say(this.player, Text.INSUFFICIENT_FUNDS);
                        }
                        else
                        {
                            if(select.getFace().getModY() != 0)
                            {
                                BlockHelper.createSignPost(placeAt, BlockHelper.toSignPostMaterial(p.getInventory().getItemInMainHand().getType()), BlockHelper.getRealDirection(p).getOppositeFace());
                            }
                            else
                            {
                                BlockHelper.createWallSign(placeAt, BlockHelper.toWallSignMaterial(p.getInventory().getItemInMainHand().getType()), select.getFace());
                            }
                            t.addAccessPoint(new AccessPoint(new Point(placeAt), placingType));
                        }
                    }
                }
                else
                {
                    MessageService.say(this.player, Text.MUST_SELECT_SOLID_BLOCK);
                    return false;
                }
                shouldComplete = true;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean complete() {
        return true;
    }

    @Override
    public void cancel() {

	}
}