package com.adx.jogy34.tardis.actions;

import java.util.UUID;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;
import com.adx.jogy34.tardis.actions.enums.TardisActions;
import com.adx.jogy34.tardis.actions.interfaces.ITardisAction;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public abstract class TardisAction implements ITardisAction
{
    protected final UUID player;
    protected final TardisActions type;
    protected final EList<TardisActionDataTypes> validTypes;
    protected boolean shouldComplete = false;
    
    public TardisAction(UUID player, TardisActions type, TardisActionDataTypes... validTypes)
    {
        this.player = player;
        this.type = type;
        this.validTypes = new EList<TardisActionDataTypes>(validTypes);
    }

    public Player getPlayer()
    {
        return Bukkit.getPlayer(player);
    }

    @Override
    public TardisActions getType()
    {
        return type;
    }

    @Override
    public boolean isValidDataType(TardisActionDataTypes dataType) 
    {
        return validTypes.contains(dataType);
    }

    @Override
    public boolean shouldComplete() {
        return this.shouldComplete;
    }
}