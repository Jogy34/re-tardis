package com.adx.jogy34.tardis.actions.interfaces;

import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;
import com.adx.jogy34.tardis.actions.enums.TardisActions;

public interface ITardisAction
{
    public void start();
    public boolean update(ITardisActionData data);
    public boolean complete();
    public void cancel();
    public TardisActions getType();
    public boolean isValidDataType(TardisActionDataTypes dataType);
    public boolean shouldComplete();
}