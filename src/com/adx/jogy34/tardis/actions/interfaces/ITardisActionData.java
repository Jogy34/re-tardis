package com.adx.jogy34.tardis.actions.interfaces;

import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;

public interface ITardisActionData
{
    public TardisActionDataTypes getType();
}