package com.adx.jogy34.tardis.actions.enums;

public enum TardisActions
{
    SELECT_SCHEMATIC,
    PLACE_SIGN_DISPLAY,
    BUILD_ROOM
    ;
}