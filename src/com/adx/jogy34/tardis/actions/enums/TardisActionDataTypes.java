package com.adx.jogy34.tardis.actions.enums;

public enum TardisActionDataTypes
{
    SELECT_BLOCK,
    PLACE_BLOCK,
    SET_TEXT,
    PERFORM_COMMAND
    ;
}