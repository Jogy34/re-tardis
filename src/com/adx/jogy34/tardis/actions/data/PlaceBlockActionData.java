package com.adx.jogy34.tardis.actions.data;

import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;

import org.bukkit.block.Block;

public class PlaceBlockActionData extends TardisActionDataBase
{
    protected Block placed;

    public PlaceBlockActionData(Block placed)
    {
        super(TardisActionDataTypes.PLACE_BLOCK);
        this.placed = placed;
    }

    public Block getPlaced()
    {
        return placed;
    }
}