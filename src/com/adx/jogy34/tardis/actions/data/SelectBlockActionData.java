package com.adx.jogy34.tardis.actions.data;

import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;

public class SelectBlockActionData extends TardisActionDataBase
{
    protected Block block;
    protected Action action;
    protected ItemStack holding;
    protected boolean sneaking;
    protected BlockFace face;

    public SelectBlockActionData(Block block, Action action, ItemStack holding, boolean sneaking, BlockFace face)
    {
        super(TardisActionDataTypes.SELECT_BLOCK);
        this.block = block;
        this.action = action;
        this.holding = holding;
        this.sneaking = sneaking;
        this.face = face;
    }

    public Block getBlock()
    {
        return block;
    }

    public Action getAction()
    {
        return action;
    }

    public ItemStack holding()
    {
        return holding;
    }

    public boolean wasSneaking()
    {
        return sneaking;
    }

    public boolean wasRightClick()
    {
        return action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK;
    }

    public boolean clickedBlock()
    {
        return action == Action.RIGHT_CLICK_BLOCK || action == Action.LEFT_CLICK_BLOCK;
    }

    public BlockFace getFace() 
    {
        return face;
    }
}