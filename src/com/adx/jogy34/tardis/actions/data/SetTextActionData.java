package com.adx.jogy34.tardis.actions.data;

import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;

public class SetTextActionData extends TardisActionDataBase
{
    protected String reason, text;
    public SetTextActionData(String reason, String text)
    {
        super(TardisActionDataTypes.SET_TEXT);
        this.reason = reason;
        this.text = text;
    }

    public String getReason()
    {
        return reason;
    }

    public String getText()
    {
        return text;
    }
}