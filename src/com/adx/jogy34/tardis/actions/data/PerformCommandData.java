package com.adx.jogy34.tardis.actions.data;

import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;

public class PerformCommandData extends TardisActionDataBase
{
    protected String cmd;
    protected String[] args;

    public PerformCommandData(String cmd, String[] args)
    {
        super(TardisActionDataTypes.PERFORM_COMMAND);
        this.cmd = cmd.toLowerCase();
        this.args = args;
    }

    public String getCommand()
    {
        return this.cmd;
    }

    public String[] getArgs()
    {
        return this.args;
    }
}