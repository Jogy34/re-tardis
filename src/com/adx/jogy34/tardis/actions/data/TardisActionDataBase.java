package com.adx.jogy34.tardis.actions.data;

import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;
import com.adx.jogy34.tardis.actions.interfaces.ITardisActionData;

public abstract class TardisActionDataBase implements ITardisActionData
{
    protected final TardisActionDataTypes _type;

    public TardisActionDataBase(TardisActionDataTypes type)
    {
        this._type = type;
    }

    @Override
    public TardisActionDataTypes getType() 
    {
        return _type;
    }
}