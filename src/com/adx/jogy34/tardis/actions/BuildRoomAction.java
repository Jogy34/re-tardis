package com.adx.jogy34.tardis.actions;

import java.util.UUID;

import com.adx.jogy34.tardis.actions.data.SetTextActionData;
import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;
import com.adx.jogy34.tardis.actions.enums.TardisActions;
import com.adx.jogy34.tardis.actions.interfaces.ITardisActionData;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.schematics.TardisSchematic;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.spacial.Point;

import org.bukkit.block.BlockFace;

public class BuildRoomAction extends TardisAction
{
    protected Point clickedBlock;
    protected BlockFace clickedFace;
    protected String chosenSchematic;
    
    public BuildRoomAction(UUID player, Point clickedBlock, BlockFace clickedFace)
    {
        super(player, TardisActions.BUILD_ROOM, TardisActionDataTypes.SET_TEXT);
        this.clickedBlock = clickedBlock;
        this.clickedFace = clickedFace;
    }

    @Override
    public void start() 
    {

    }

    @Override
    public boolean update(ITardisActionData data) 
    {
        if(data.getType() == TardisActionDataTypes.SET_TEXT)
        {
            chosenSchematic = ((SetTextActionData) data).getText();
            this.shouldComplete = true;
            return true;
        }
        return false;
    }

    @Override
    public boolean complete() 
    {
        TARDIS t = TARDISProvider.getTardisForPlayer(this.player);
        TardisSchematic schematic = ServiceProvider.schematicService().getSchematic(chosenSchematic);

        if(t.getSettings().getInterior().canBuildRoom(clickedBlock, clickedFace.getOppositeFace(), schematic))
        {
            if(schematic.getCost() == null || schematic.getCost().pay(this.getPlayer()))
            {
                t.getSettings().getInterior().buildRoom(clickedBlock, clickedFace.getOppositeFace(), schematic);
            }
            else
            {
                MessageService.say(this.player, Text.INSUFFICIENT_FUNDS);
            }
        }
        else
        {
            MessageService.say(this.player, Text.OBSTRUCTED_PATH);
        }

        return true;
    }

    @Override
    public void cancel() {
		
	}
}