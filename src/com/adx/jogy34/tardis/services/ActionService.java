package com.adx.jogy34.tardis.services;

import java.util.UUID;

import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;
import com.adx.jogy34.tardis.actions.enums.TardisActions;
import com.adx.jogy34.tardis.actions.interfaces.ITardisAction;
import com.adx.jogy34.tardis.actions.interfaces.ITardisActionData;
import com.adx.jogy34.tardis.services.interfaces.IActionService;

import org.bukkit.entity.Player;

public class ActionService implements IActionService 
{
    protected EMap<UUID, ITardisAction> actionData = new EMap<UUID, ITardisAction>();
    
    @Override
    public void startPerformingAction(UUID player, ITardisAction action)
    {
        if(actionData.containsKey(player))
        {
            actionData.get(player).cancel();
        }
        actionData.put(player, action);
        action.start();
    }

    @Override
    public boolean isPerformingAction(UUID player) 
    {
        return actionData.containsKey(player);
    }

    @Override
    public boolean isPerformingAction(UUID player, TardisActions type) 
    {
        return isPerformingAction(player) && actionData.get(player).getType() == type;
    }

    @Override
    public boolean isValidUpdateAction(UUID player, TardisActionDataTypes type) 
    {
        return isPerformingAction(player) && actionData.get(player).isValidDataType(type);
    }

    @Override
    public boolean performAction(UUID player, ITardisActionData data) 
    {
        if(isPerformingAction(player))
        {
            ITardisAction action = actionData.get(player);
            if(action.update(data))
            {
                if(action.shouldComplete()) completeAction(player);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean completeAction(UUID player) 
    {
        if(isPerformingAction(player) && actionData.get(player).complete())
        {
            actionData.remove(player);
            return true;
        }
        
        return false;
    }

    @Override
    public void stopAction(Player player) { stopAction(player.getUniqueId()); }
    @Override
    public void stopAction(UUID player) 
    {
        if(actionData.containsKey(player))
        {
            actionData.get(player).cancel();
            actionData.remove(player);
        }
    }

    @Override
    public void stopAction(Player player, TardisActions type) { stopAction(player.getUniqueId(), type); }
    @Override
    public void stopAction(UUID player, TardisActions type) 
    {
        if(actionData.containsKey(player))
        {
            if(actionData.get(player).getType() == type)
            {
                actionData.get(player).cancel();
                actionData.remove(player);
            }
        }
    }

    @Override
    public void stopAllActions() 
    {
        actionData.ForEach(x -> x.getValue().cancel());
        actionData.clear();
    }

    @Override
    public void startPerformingAction(Player player, ITardisAction action) {
        startPerformingAction(player.getUniqueId(), action);
    }

    @Override
    public boolean isPerformingAction(Player player) {
        return isPerformingAction(player.getUniqueId());
    }

    @Override
    public boolean isPerformingAction(Player player, TardisActions type) {
        return isPerformingAction(player.getUniqueId(), type);
    }

    @Override
    public boolean isValidUpdateAction(Player player, TardisActionDataTypes type) {
        return isValidUpdateAction(player.getUniqueId(), type);
    }

    @Override
    public boolean performAction(Player player, ITardisActionData data) {
        return performAction(player.getUniqueId(), data);
    }

    @Override
    public boolean completeAction(Player player) {
        return completeAction(player.getUniqueId());
    }
}