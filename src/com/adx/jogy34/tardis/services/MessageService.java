package com.adx.jogy34.tardis.services;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MessageService
{
    protected static final String TARDIS_PREFIX = String.format("%s[TARDIS]%s ", ChatColor.DARK_BLUE, ChatColor.RESET);

    public static void say(Player p, String message) 
    {
        p.sendMessage(TARDIS_PREFIX + message);
    }

    public static void say(UUID player, String message) 
    {
        Player p = Bukkit.getPlayer(player);
        if (p != null) say(p, message);
    }

    public static void say(CommandSender sender, String message) 
    {
        if(sender instanceof Player) say((Player) sender, message);
        else sender.sendMessage(message);
    }
    
    public static void send(String message)
    {
        say(Bukkit.getConsoleSender(), message);
    }
}