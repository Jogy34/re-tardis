package com.adx.jogy34.tardis.services;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.commands.interfaces.ITardisCommand;
import com.adx.jogy34.tardis.services.interfaces.ICommandService;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.helpers.ReflectionHelper;

import org.bukkit.command.CommandSender;

public class CommandService implements ICommandService 
{
    private EMap<String, ITardisCommand> _commands = new EMap<String, ITardisCommand>();
    private EList<ITardisCommand> _uniqueCommands = new EList<ITardisCommand>();

    @Override
    public void init() 
    {
        ReflectionHelper.getClasses("com.adx.jogy34.tardis.commands").ForEach(c -> {
            if(c.isAnnotationPresent(TardisCommandParam.class) && ITardisCommand.class.isAssignableFrom(c))
            {
                try 
                {
                    registerCommand((ITardisCommand) c.newInstance());
                } 
                catch (InstantiationException | IllegalAccessException e) 
                {
                    Logger.error(e);
                }
            }
        });
	}

    @Override
    public boolean handleCommand(String command, CommandSender sender, String[] args) 
    {
        if(_commands.containsKey(command.toLowerCase()))
        {
            ITardisCommand cmd = _commands.get(command.toLowerCase());
            String message = cmd.isValid(sender);
            if(message == null)
            {
                if(!cmd.execute(sender, args))
                {
                    MessageService.say(sender, String.format("%s Usage: /TARDS %s %s" , cmd.getCommandKey(), cmd.getCommandKey(), cmd.getUsage()));
                }
            }
            else
            {
                MessageService.say(sender, message);
            }
            return true;
        }

        return false;
    }

    @Override
    public EList<String> handleTabComplete(String command, CommandSender sender, String[] args) 
    {
        if(args.length == 0)
        {
            String start = command == null ? "" : command.toLowerCase();
            return _commands.Select(c -> c.getValue())
                            .Where(c -> c.isValid(sender) == null)
                            .Where(c -> c.getCommandKey().toLowerCase().startsWith(start))
                            .Select(c -> c.getCommandKey()).ToList();
        }
        else if(_commands.containsKey(command.toLowerCase()))
        {
            ITardisCommand cmd = _commands.get(command.toLowerCase());
            if(cmd.isValid(sender) == null)
            {
                String[] result = cmd.tabComplete(sender, args);
                if(result == null) return new EList<String>();
                String arg = args[args.length - 1].toLowerCase();
                return new EList<String>(result).Where(x -> x.toLowerCase().startsWith(arg)).ToList();
            }
        }

        return new EList<String>();
    }

    @Override
    public void registerCommand(ITardisCommand cmd) 
    {
        _commands.put(cmd.getCommandKey().toLowerCase(), cmd);
        cmd.getAliases().ForEach(a -> _commands.put(a.toLowerCase(), cmd));

        _uniqueCommands.add(cmd);
    }

    @Override
    public IEnumerable<ITardisCommand> getAllCommands()
    {
        return _uniqueCommands;
    }
}