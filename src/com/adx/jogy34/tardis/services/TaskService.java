package com.adx.jogy34.tardis.services;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.providers.UtilProvider;
import com.adx.jogy34.tardis.tasks.base.ITardisTask;
import com.adx.jogy34.tardis.util.Timespan;

import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public final class TaskService
{
    protected static EList<TardisTaskRunner> runningTasks = new EList<TardisTaskRunner>();

    private TaskService() {}

    public static void scheduleTask(ITardisTask t)
    {
        TardisTaskRunner runner = new TardisTaskRunner(t);
        runningTasks.add(runner);
        runner.start();
    }

    public static void onPluginDisable()
    {
        runningTasks.ForEach(t -> t.finishRemainingWork());
    }

    protected static class TardisTaskRunner
    {
        protected ITardisTask toRun;
        protected BukkitTask runningTask;
        public TardisTaskRunner(ITardisTask toRun)
        {
            this.toRun = toRun;
        }

        public void start()
        {
            toRun.start();
            this.scheduleNext(Timespan.fromTicks(1));
        }

        public void finishRemainingWork()
        {
            runningTask.cancel();
            toRun.finishRemainingWork();
            runningTasks.remove(this);
        }
        
        public void scheduleNext(Timespan timespan)
        {
            if(this.runningTask != null) this.runningTask.cancel();
            if(timespan == null || timespan.getTicks() <= 0) timespan = Timespan.fromTicks(1);
            TardisTaskRunner self = this;
            this.runningTask = new BukkitRunnable(){
                @Override
                public void run()
                {
                    Timespan waitTime = toRun.step();
                    if(toRun.isDone())
                    {
                        toRun.end();
                        runningTasks.remove(self);
                    }
                    else
                    {
                        scheduleNext(waitTime);
                    }
                }
            }.runTaskLater(UtilProvider.TARDISPlugin(), timespan.getTicks());
        }
    }
}