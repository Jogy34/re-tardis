package com.adx.jogy34.tardis.services;

import java.util.Arrays;
import java.util.UUID;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Server;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.enums.TARDISItems;
import com.adx.jogy34.tardis.providers.UtilProvider;
import com.adx.jogy34.tardis.services.interfaces.IItemService;
import com.adx.jogy34.tardis.util.Text;

public class ItemService implements IItemService
{
    public static final String NAME_DELIMETER = ChatColor.RESET.toString() + ChatColor.RESET.toString();

    private EMap<TARDISItems, ItemStack> items = new EMap<TARDISItems, ItemStack>();
    private EMap<TARDISItems, Recipe> recipes = new EMap<TARDISItems, Recipe>();
    private boolean initialized = false;
    
    @Override
    public void init()
    {
        if(!initialized) 
        {
            initialized = true;
            initItems();
            initRecipes();
            Server server = UtilProvider.TARDISPlugin().getServer();
            recipes.ForEach(x -> server.addRecipe(x.getValue()));
        }
    }

    public ItemStack getItem(TARDISItems item, String... addlLore)
    {
        if(item == TARDISItems.NONE) return null;

        ItemStack stack = items.get(item).clone();

        if(addlLore.length > 0)
        {
            ItemMeta meta = stack.getItemMeta();
            EList<String> lore = new EList<String>(meta.getLore());
            lore.AddAll(addlLore);
            meta.setLore(lore);
            stack.setItemMeta(meta);
        }

        return stack;
    }

    public ItemStack getItemWithName(TARDISItems item, String name)
    {
        if(item == TARDISItems.NONE) return null;

        ItemStack stack = items.get(item).clone();

        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(meta.getDisplayName() + NAME_DELIMETER + name);
        stack.setItemMeta(meta);

        return stack;
    }

    public ItemStack updateName(ItemStack item, String name)
    {
        ItemStack original = getOriginal(item);
        if(original != null) 
        {
            String originalName = original.getItemMeta().getDisplayName();
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(originalName + NAME_DELIMETER + name);
            item.setItemMeta(meta);
        }

        return item;
    }

    public ItemStack updateLore(ItemStack item, String... addlLore)
    {
        return this.updateLore(item, false, addlLore);
    }

    public ItemStack updateLore(ItemStack item, boolean maintainCurrent, String... addlLore)
    {
        ItemMeta meta = item.getItemMeta();
        ItemStack original = getOriginal(item);
        EList<String> lore;

        if(maintainCurrent || original == null) {
            lore = new EList<String>(meta.getLore());
        } else {
            lore = new EList<String>(original.getItemMeta().getLore());
        }

        lore.AddAll(addlLore);
        meta.setLore(lore);
        item.setItemMeta(meta);

        return item;
    }

    public ItemStack getOriginal(ItemStack item)
    {
        return getItem(getItem(item));
    }
    
    public TARDISItems getItem(ItemStack item) 
    {
        if(item == null || item.getType() == Material.AIR) return TARDISItems.NONE;

        int itemHash = hashItem(item, false);
        
        Entry<TARDISItems, ItemStack> result = items.FirstOrDefault(x -> x.getKey().getHashCode(false) == itemHash);
        
        return result == null ? TARDISItems.NONE : result.getKey();
    }

    public int hashItem(ItemStack item)
    {
        return hashItem(item, true);
    }

    /**
     * Generates a relatively unique hashcode for items. 
     * Allowed differences: lore, amounts (optional), display name after 2 reset codes
     */
    public int hashItem(ItemStack item, boolean includeAmount)
    {
        ItemMeta meta = item.getItemMeta();
        String name = (meta.hasDisplayName() ? meta.getDisplayName() : "").split(NAME_DELIMETER)[0];
        EList<ItemFlag> itemFlags = new EList<ItemFlag>(meta.getItemFlags());
        EMap<Enchantment, Integer> enchants = new EMap<Enchantment, Integer>(meta.getEnchants());

        String hashString = String.format("%s|%s|%s|%s|%s|%s", 
                                            item.getType(), 
                                            includeAmount ? item.getAmount() : -1, 
                                            name,
                                            meta.isUnbreakable(),
                                            itemFlags.ConcatenateToString(":"),
                                            enchants.ConcatenateToString(x -> x.getKey() + "-" + x.getValue(), ":"));

        return hashString.hashCode();
    }
    
    private void initItems()
    {
        if(!items.containsKey(TARDISItems.TARDIS_EGG))
        {
            ItemStack tardisEgg = new ItemStack(Material.DOLPHIN_SPAWN_EGG, 1);
            ItemMeta tardisEggMeta = tardisEgg.getItemMeta();
            tardisEggMeta.setDisplayName(Text.format("{0}{1}{SPAWN_TARDIS}", ChatColor.DARK_BLUE, ChatColor.BOLD));
            tardisEggMeta.setLore(Arrays.asList(Text.USE_THIS_TO_CREATE_A_TARDIS));
            tardisEggMeta.addEnchant(Enchantment.VANISHING_CURSE, 1, true);
            tardisEggMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            tardisEgg.setItemMeta(tardisEggMeta);
            items.put(TARDISItems.TARDIS_EGG, tardisEgg);
        }
        if(!items.containsKey(TARDISItems.SCHEMATIC_SELECT_ANCHOR_POINTS))
        {
            ItemStack item = new ItemStack(Material.STICK);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(Text.format("{0}{1}{SELECT_ANCHOR_POINTS}", ChatColor.DARK_BLUE, ChatColor.BOLD));
            itemMeta.setLore(Arrays.asList(
                    Text.SELECT_CORNERS_ANCHOR_POINT,
                    Text.RIGHT_CLICK_ANCHOR_POINT, 
                    Text.LEFT_CLICK_ANCHOR_POINT));
            itemMeta.addEnchant(Enchantment.VANISHING_CURSE, 1, true);
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(itemMeta);
            items.put(TARDISItems.SCHEMATIC_SELECT_ANCHOR_POINTS, item);
        }
        if(!items.containsKey(TARDISItems.SCHEMATIC_SELECT_ACCESS_POINTS))
        {
            ItemStack item = new ItemStack(Material.BLAZE_ROD);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(Text.format("{0}{1}{SELECT_ACCESS_POINTS}", ChatColor.DARK_BLUE, ChatColor.BOLD));
            itemMeta.setLore(Arrays.asList(
                Text.CYCLE_ACCESS_POINT_TYPES,
                Text.RIGHT_CLICK_ACCESS_POINT,
                Text.LEFT_CLICK_ACCESS_POINT
            ));
            itemMeta.addEnchant(Enchantment.VANISHING_CURSE, 1, true);
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(itemMeta);
            items.put(TARDISItems.SCHEMATIC_SELECT_ACCESS_POINTS, item);
        }
        if(!items.containsKey(TARDISItems.MEASURING_STICK))
        {
            ItemStack item = new ItemStack(Material.STICK);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(Text.format("{0}{1}", ChatColor.DARK_GREEN, "Measuring Stick [Debug]"));
            itemMeta.addEnchant(Enchantment.VANISHING_CURSE, 1, true);
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(itemMeta);
            items.put(TARDISItems.MEASURING_STICK, item);
        }
        if(!items.containsKey(TARDISItems.TELEPORT_STICK))
        {
            ItemStack item = new ItemStack(Material.BLAZE_ROD);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(Text.format("{0}{1}", ChatColor.DARK_GREEN, "Teleport Stick [Debug]"));
            itemMeta.addEnchant(Enchantment.VANISHING_CURSE, 1, true);
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(itemMeta);
            items.put(TARDISItems.TELEPORT_STICK, item);
        }
        if(!items.containsKey(TARDISItems.DIAMOND_PLATED_ELYTRA))
        {
            ItemStack item = new ItemStack(Material.ELYTRA);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.AQUA + "Diamond Plated Elytra");
            meta.addAttributeModifier(Attribute.GENERIC_ARMOR_TOUGHNESS, new AttributeModifier(UUID.randomUUID(), "generic.armorToughness", 2, Operation.ADD_NUMBER, EquipmentSlot.CHEST));
            meta.addAttributeModifier(Attribute.GENERIC_ARMOR, new AttributeModifier(UUID.randomUUID(), "generic.armor", 8, Operation.ADD_NUMBER, EquipmentSlot.CHEST));
            item.setItemMeta(meta);
            items.put(TARDISItems.DIAMOND_PLATED_ELYTRA, item);
        }
        if(!items.containsKey(TARDISItems.TOAST))
        {
            ItemStack item = new ItemStack(Material.BREAD);
            ItemMeta meta = item.getItemMeta();
            
            meta.setDisplayName(ChatColor.GOLD + Text.TOAST);

            item.setItemMeta(meta);
            items.put(TARDISItems.TOAST, item);
        }
        if(!items.containsKey(TARDISItems.BURNT_TOAST))
        {
            ItemStack item = new ItemStack(Material.BREAD);
            ItemMeta meta = item.getItemMeta();
            
            meta.setDisplayName(ChatColor.BLACK + Text.BURNT_TOAST);

            item.setItemMeta(meta);
            items.put(TARDISItems.BURNT_TOAST, item);
        }
    }
    
    private void initRecipes()
    {
        if(!recipes.containsKey(TARDISItems.TARDIS_EGG))
        {
            ShapedRecipe tardisEggRecipe = new ShapedRecipe(new NamespacedKey(UtilProvider.TARDISPlugin(), "TARDIS-Egg"), items.get(TARDISItems.TARDIS_EGG));
            tardisEggRecipe.shape("BEB", "GDG", "III");
            tardisEggRecipe.setIngredient('B', Material.BONE_MEAL);
            tardisEggRecipe.setIngredient('E', Material.EMERALD);
            tardisEggRecipe.setIngredient('G', Material.GOLD_INGOT);
            tardisEggRecipe.setIngredient('D', Material.DIAMOND);
            tardisEggRecipe.setIngredient('I', Material.IRON_INGOT);
            recipes.put(TARDISItems.TARDIS_EGG, tardisEggRecipe);
        }
    }
}
