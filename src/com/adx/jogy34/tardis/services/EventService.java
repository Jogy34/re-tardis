package com.adx.jogy34.tardis.services;

import com.adx.jogy34.tardis.eventlisteners.annotations.TardisEventListener;
import com.adx.jogy34.tardis.providers.UtilProvider;
import com.adx.jogy34.tardis.services.interfaces.IEventService;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.helpers.ReflectionHelper;

import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class EventService implements IEventService
{
    @Override
    public void init()
    {
        JavaPlugin plugin = UtilProvider.TARDISPlugin();
        PluginManager pluginManager = plugin.getServer().getPluginManager();
        ReflectionHelper.getClasses("com.adx.jogy34.tardis.eventlisteners").ForEach(c -> {
            if(c.isAnnotationPresent(TardisEventListener.class) && Listener.class.isAssignableFrom(c))
            {
                try
                {
                    pluginManager.registerEvents((Listener) c.newInstance(), plugin);
                } 
                catch (InstantiationException | IllegalAccessException e) 
                {
                    Logger.error(e);
                }
            }
        });
    }
}