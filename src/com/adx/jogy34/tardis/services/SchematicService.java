package com.adx.jogy34.tardis.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jogy34.tardis.menu.upgrade.BuildRoomDialogue;
import com.adx.jogy34.tardis.menu.upgrade.UpgradeTardisMenu;
import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.UtilProvider;
import com.adx.jogy34.tardis.schematics.AuxillaryData;
import com.adx.jogy34.tardis.schematics.Cost;
import com.adx.jogy34.tardis.schematics.HallwayTheme;
import com.adx.jogy34.tardis.schematics.TardisSchematic;
import com.adx.jogy34.tardis.schematics.AccessPoint;
import com.adx.jogy34.tardis.schematics.enums.AuxillaryDataType;
import com.adx.jogy34.tardis.schematics.enums.SchematicType;
import com.adx.jogy34.tardis.services.interfaces.ISchematicService;
import com.adx.jogy34.tardis.util.spacial.Position;

import org.bukkit.Material;
import org.bukkit.craftbukkit.libs.jline.internal.InputStreamReader;

import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.helpers.StringHelper;

public class SchematicService implements ISchematicService 
{
    protected EList<String> defaultSchematics = new EList<String>(Arrays.asList(
        //Control Rooms
        "control_room_1", "control_room_2", "control_room_3", "control_room_4", "control_room_5", "control_room_6",
        
        //Additional Rooms
        "storage_room", "workshop", "master_bedroom", "lounge", "kitchen", "theater", 
        "bedroom", "library", "pool"
    ));

    protected static final String ITERATION_PREFIX = "Iteration:";
    protected static final String CONSTANT_PREFIX = "Constant:";

    protected EMap<String, TardisSchematic> schematics = new EMap<String, TardisSchematic>();
    protected EList<HallwayTheme> hallwayThemes = new EList<HallwayTheme>();

    @Override
    public TardisSchematic createSchematic(Position p1, Position p2, String name, String createdBy, String description, SchematicType type, Cost cost,
                                           EList<AccessPoint> absoluteAccessPoints, EList<AuxillaryData> auxData, Material icon, int iteration, boolean replace)
    {
        TardisSchematic s = new TardisSchematic(p1, p2, name, createdBy, description, type, cost, absoluteAccessPoints, auxData, icon, iteration, !replace);
        if(replace)
        {
            schematics.First(x -> x.getValue().getConstant().equals(s.getConstant())).getValue().replaceData(s);
        }
        else 
        {
            schematics.put(s.getConstant(), s);
        }

        switch(type)
        {
            case CONTROL_ROOM:
            {
                ServiceProvider.inventoryMenuService().getMenu(UpgradeTardisMenu.class).reset();
                break;
            }
            case ADDITIONAL_ROOM:
            {
                ServiceProvider.inventoryMenuService().getMenu(BuildRoomDialogue.class).reset();
                break;
            }
            default:
            {
                break;
            }
        }

        return s;
    }
    
    @Override
    public void loadSchematics()
    {
        Logger.info("Loading TARDIS Schematics...");
        defaultSchematics.ForEach(s -> {
            if(ConfigProvider.saveDefaultConfig(TardisSchematic.SCHEMATIC_PATH + s + ".yml"))
            {
                Logger.info(Text.format("{ADDED_DEFAULT_SCHEMATIC}", s));
            }
        });

        File schematicFolder = new File(UtilProvider.TARDISPlugin().getDataFolder(), TardisSchematic.SCHEMATIC_PATH);
        if(schematicFolder.exists() && schematicFolder.isDirectory())
        {
            EMap<String, Integer> internalSchematics = new EMap<String, Integer>();
            try
            {
                JarFile jar = new JarFile(UtilProvider.TARDISPlugin().getJarFile()); 
                Enumeration<JarEntry> entry = jar.entries();
                String path = TardisSchematic.SCHEMATIC_PATH.replace(File.separator, "/");
                while (entry.hasMoreElements()) 
                { 
                    JarEntry jarEntry = entry.nextElement();
                    if(jarEntry.getName().startsWith(path))
                    {
                        InputStream stream = jar.getInputStream(jarEntry);
                        try(BufferedReader reader = new BufferedReader(new InputStreamReader(stream)))
                        {
                            String constant = null, line = null;
                            Integer iteration = null;
                            while((line = reader.readLine()) != null && (constant == null || iteration == null))
                            {
                                if(line.startsWith(CONSTANT_PREFIX))
                                {
                                    line = line.replace(CONSTANT_PREFIX, "").trim();
                                    constant = line;
                                }
                                else if(line.startsWith(ITERATION_PREFIX))
                                {
                                    line = line.replace(ITERATION_PREFIX, "").trim();
                                    iteration = StringHelper.toIntOrDefault(line);
                                }
                            }

                            if(constant != null)
                            {
                                if(iteration == null) iteration = 0;
                                internalSchematics.put(constant, iteration);
                            }
                        }
                        catch(Exception e)
                        {
                            Logger.error(e);
                        }
                    }
                }
                jar.close();
            } 
            catch(Exception e)
            {
                Logger.error(e);
            }

            File[] children = schematicFolder.listFiles();
            for(File f : children)
            {
                String name = f.getName();
                if(f.isFile() && name.endsWith(".yml"))
                {
                    try
                    {
                        name = name.substring(0, name.length() - 4);
                        String config = TardisSchematic.SCHEMATIC_PATH + name;
                        ConfigProvider.reloadConfig(config);
                        TardisSchematic schematic = ConfigProvider.createFromConfigParameters(TardisSchematic.class, ConfigProvider.getAllConfigProperties(config));
                        if(schematic.getIteration() >= 0 && internalSchematics.containsKey(schematic.getConstant()) && schematic.getIteration() < internalSchematics.get(schematic.getConstant()))
                        {
                            Logger.info(Text.format("{REPLACING_WITH_UPDATED_SCHEMATIC}", schematic.getName()));

                            ConfigProvider.reloadConfig(config);
                            ConfigProvider.saveDefaultConfig(TardisSchematic.SCHEMATIC_PATH + name + ".yml", true);
                            schematic = ConfigProvider.createFromConfigParameters(TardisSchematic.class, ConfigProvider.getAllConfigProperties(config));
                        }
                        schematics.put(schematic.getConstant(), schematic);
                    }
                    catch(Exception e)
                    {
                        Logger.error(e);
                    }
                }
            }
        }
        Logger.info("...Loaded " + schematics.size() + " Schematic" + (schematics.size() == 1 ? "" : "s"));
        this.loadHallwayThemes();
    }

    @Override
    public void reload()
    {
        this.schematics.clear();
        this.hallwayThemes.clear();
        this.loadSchematics();
        
        ServiceProvider.inventoryMenuService().getMenu(BuildRoomDialogue.class).reset();
        ServiceProvider.inventoryMenuService().getMenu(UpgradeTardisMenu.class).reset();
    }

    @Override
    public TardisSchematic getSchematic(String constant)
    {
        if(schematics.containsKey(constant)) return schematics.get(constant);
        return null;
    }

    @Override
    public TardisSchematic firstControlRoom() 
    {
        TardisSchematic first = null;
        int lowestOrder = 0;
        for(TardisSchematic s : schematics.Select(x -> x.getValue()).Where(s -> s.isActive() && s.getType() == SchematicType.CONTROL_ROOM))
        {
            AuxillaryData data = s.getAuxillaryData(AuxillaryDataType.CONTROL_ROOM_ORDER);
            if(data == null) 
            {
                Logger.warning(Text.format("{MISSING_CONTROL_ROOM_ORDER}", s.getConstant()));
            }
            else
            {
                int order = data.getInt();
                if(order < lowestOrder || first == null)
                {
                    lowestOrder = order;
                    first = s;
                }
            }
        }
        return first;
    }

    @Override
    public TardisSchematic findNextControlRoom(int order) 
    {
        TardisSchematic next = null;
        int lowestNextOrder = 0;
        for(TardisSchematic s : schematics.Select(x -> x.getValue()).Where(s -> s.isActive() && s.getType() == SchematicType.CONTROL_ROOM))
        {
            AuxillaryData data = s.getAuxillaryData(AuxillaryDataType.CONTROL_ROOM_ORDER);
            if(data == null) 
            {
                Logger.warning(Text.format("{MISSING_CONTROL_ROOM_ORDER}", s.getConstant()));
            }
            else
            {
                int o = data.getInt();
                if(o > order && (o < lowestNextOrder || next == null))
                {
                    lowestNextOrder = o;
                    next = s;
                    
                    //No need to look at the rest if this comes directly after
                    if(o == order + 1) return next;
                }
            }
        }
        return next;
    }

    @Override
    public IEnumerable<TardisSchematic> getSchematics() { return getSchematics(false); }
    @Override
    public IEnumerable<TardisSchematic> getSchematics(boolean includeInatvive) {
        return schematics.Select(x -> x.getValue()).Where(x -> includeInatvive || x.isActive());
    }
    
    @Override
    public IEnumerable<TardisSchematic> getSchematics(SchematicType type) { return getSchematics(type, false); }
    @Override
    public IEnumerable<TardisSchematic> getSchematics(SchematicType type, boolean includeInatvive) {
        return getSchematics().Where(x -> (includeInatvive || x.isActive()) &&  x.getType() == type);
    }

    public HallwayTheme getRandomHallwayTheme()
    {
        return hallwayThemes.get((int) Math.floor(Math.random() * hallwayThemes.size()));
    }

    protected void loadHallwayThemes()
    {
        //region Stone Theme

        HallwayTheme theme = new HallwayTheme(Material.SEA_LANTERN);

        theme.addFloor(Material.DARK_PRISMARINE, 5);
        theme.addFloor(Material.PRISMARINE_BRICKS, 2);
        theme.addFloor(Material.OBSIDIAN, 0.5);
        
        theme.addStep(Material.DARK_PRISMARINE_SLAB, 5);
        theme.addStep(Material.PRISMARINE_BRICK_SLAB, 5);

        theme.addAngle(Material.COBBLESTONE_STAIRS, 1);
        theme.addAngle(Material.STONE_BRICK_STAIRS, 1);
        theme.addAngle(Material.STONE_STAIRS, 1);
        theme.addAngle(Material.POLISHED_ANDESITE_STAIRS, 1);

        theme.addWall(Material.POLISHED_ANDESITE, 2);
        theme.addWall(Material.SMOOTH_STONE, 2);
        theme.addWall(Material.STONE, 1);

        this.hallwayThemes.add(theme);

        //endregion Stone Theme

        //region Wood Theme

        theme = new HallwayTheme(Material.GLOWSTONE);

        theme.addFloor(Material.DARK_OAK_PLANKS, 4);
        theme.addFloor(Material.SPRUCE_PLANKS, 3);
        theme.addFloor(Material.NETHER_BRICKS, 0.8);
        
        theme.addStep(Material.DARK_OAK_SLAB, 4);
        theme.addStep(Material.SPRUCE_SLAB, 3);
        theme.addStep(Material.NETHER_BRICK_SLAB, 0.8);

        theme.addAngle(Material.OAK_STAIRS, 4);
        theme.addAngle(Material.BIRCH_STAIRS, 3);
        theme.addAngle(Material.SANDSTONE_STAIRS, 2);

        theme.addWall(Material.STRIPPED_OAK_LOG, 2);
        theme.addWall(Material.STRIPPED_SPRUCE_LOG, 2);
        theme.addWall(Material.STRIPPED_BIRCH_LOG, 1);

        this.hallwayThemes.add(theme);

        //endregion Wool Theme

        //region Light Theme

        theme = new HallwayTheme(Material.SEA_LANTERN);

        theme.addFloor(Material.WHITE_CONCRETE, 4);
        theme.addFloor(Material.SMOOTH_QUARTZ, 5);
        theme.addFloor(Material.END_STONE_BRICKS, 0.8);
        theme.addFloor(Material.MUSHROOM_STEM, 0.6);
        
        theme.addStep(Material.QUARTZ_SLAB, 4);
        theme.addStep(Material.SANDSTONE_SLAB, 1);
        theme.addStep(Material.SMOOTH_QUARTZ_SLAB, 4);
        theme.addStep(Material.DIORITE_SLAB, 4);
        theme.addStep(Material.POLISHED_DIORITE_SLAB, 4);

        theme.addAngle(Material.QUARTZ_STAIRS, 4);
        theme.addAngle(Material.SANDSTONE_STAIRS, 1);
        theme.addAngle(Material.SMOOTH_QUARTZ_STAIRS, 4);
        theme.addAngle(Material.DIORITE_STAIRS, 4);
        theme.addAngle(Material.POLISHED_DIORITE_STAIRS, 4);

        theme.addWall(Material.POLISHED_DIORITE, 5);
        theme.addWall(Material.BIRCH_LOG, 3);
        theme.addWall(Material.IRON_BLOCK, 1);

        this.hallwayThemes.add(theme);

        //endregion Light Theme

        //region Dark Theme

        theme = new HallwayTheme(Material.GLOWSTONE);

        theme.addFloor(Material.OBSIDIAN, 3);
        theme.addFloor(Material.COAL_BLOCK, 3);
        theme.addFloor(Material.BLACK_CONCRETE, 2);
        
        theme.addStep(Material.NETHER_BRICK_SLAB, 4);
        theme.addStep(Material.DARK_PRISMARINE_SLAB, 3);
        theme.addStep(Material.RED_NETHER_BRICK_SLAB, 4);

        theme.addAngle(Material.NETHER_BRICK_STAIRS, 4);
        theme.addAngle(Material.DARK_PRISMARINE_STAIRS, 3);
        theme.addAngle(Material.RED_NETHER_BRICK_STAIRS, 4);

        theme.addWall(Material.OBSIDIAN, 2);
        theme.addWall(Material.COAL_BLOCK, 2);
        theme.addWall(Material.BLACK_CONCRETE, 1);
        theme.addWall(Material.BLACK_WOOL, 1);

        this.hallwayThemes.add(theme);

        //endregion Dark Theme

        //region Old Stone Cottage Theme

        theme = new HallwayTheme(Material.LANTERN);

        theme.addFloor(Material.DARK_OAK_PLANKS, 5);
        theme.addFloor(Material.OAK_PLANKS, 5);
        theme.addFloor(Material.SPRUCE_PLANKS, 5);
        theme.addFloor(Material.STRIPPED_DARK_OAK_LOG, 2);
        theme.addFloor(Material.STRIPPED_SPRUCE_LOG, 2);

        theme.addAngle(Material.MOSSY_COBBLESTONE_STAIRS, 5);
        theme.addAngle(Material.MOSSY_STONE_BRICK_STAIRS, 5);
        theme.addAngle(Material.STONE_STAIRS, 2);
        theme.addAngle(Material.STONE_BRICK_STAIRS, 2);

        theme.addWall(Material.MOSSY_COBBLESTONE, 5);
        theme.addWall(Material.MOSSY_STONE_BRICKS, 5);
        theme.addWall(Material.CRACKED_STONE_BRICKS, 4);
        theme.addWall(Material.STONE, 2);
        theme.addWall(Material.COBBLESTONE, 2);
        theme.addWall(Material.STONE_BRICKS, 2);

        theme.addStep(Material.MOSSY_STONE_BRICK_SLAB, 5);
        theme.addStep(Material.MOSSY_COBBLESTONE_SLAB, 5);
        theme.addStep(Material.STONE_SLAB, 3);

        this.hallwayThemes.add(theme);

        //endregion Old Stone Cottage Theme
    }
}