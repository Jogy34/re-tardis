package com.adx.jogy34.tardis.services;

import java.util.Collection;
import java.util.UUID;

import com.adx.jlinq.EMap;
import com.adx.jlinq.interfaces.Functions.IFunction0;
import com.adx.jogy34.tardis.menu.base.InventoryId;
import com.adx.jogy34.tardis.menu.base.interfaces.IInventoryMenu;
import com.adx.jogy34.tardis.menu.customize.CustomizeTARDISMenu;
import com.adx.jogy34.tardis.services.interfaces.IInventoryMenuService;
import com.adx.jogy34.tardis.util.Logger;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryView;

public class InventoryMenuService implements IInventoryMenuService
{
    private EMap<String, IInventoryMenu> _menus = new EMap<String, IInventoryMenu>();
    private EMap<String, EMap<UUID, IInventoryMenu>> _playerSpecificMenus = new EMap<String, EMap<UUID, IInventoryMenu>>();
    
    public void registerMenu(IInventoryMenu menu)
    {
        if(!_menus.containsKey(menu.getClass().getName()))
        {
            _menus.put(menu.getClass().getName(), menu);
        }
    }

    @SuppressWarnings("unchecked")
    public <T extends IInventoryMenu> T getMenu(Class<T> menuClass)
    {
        if(!_menus.containsKey(menuClass.getName()))
        {
            try
            {
                _menus.put(menuClass.getName(), menuClass.newInstance());
            }
            catch(Exception e) {
                Logger.error(e);
            }
        }

        return (T) _menus.getOrDefault(menuClass.getName(), null);
    }

    public CustomizeTARDISMenu getCustomizeTardisMenu(UUID playerId)
    {
        CustomizeTARDISMenu menu = (CustomizeTARDISMenu) getPlayerSpecificMenu(playerId, CustomizeTARDISMenu.class.getName(), () -> new CustomizeTARDISMenu());
        return menu;
    }
    
    private IInventoryMenu getPlayerSpecificMenu(UUID playerId, String key, IFunction0<IInventoryMenu> getDefault)
    {
        if(!_playerSpecificMenus.containsKey(key))
        {
            _playerSpecificMenus.put(key, new EMap<UUID, IInventoryMenu>());
        }
        EMap<UUID, IInventoryMenu> menus = _playerSpecificMenus.get(key);
        
        if(!menus.containsKey(playerId))
        {
            if(getDefault == null) return null;
            menus.put(playerId, getDefault.invoke());
        }
        
        return menus.get(playerId);
    }

    @Override
    public void onPluginDisable() 
    {
        Collection<? extends Player> players = Bukkit.getOnlinePlayers();
        for(Player p : players)
        {
            InventoryView open = p.getOpenInventory();
            if(open != null && open.getTopInventory().getHolder() instanceof InventoryId)
            {
                // p.closeInventory();
            }
        }
    }
}
