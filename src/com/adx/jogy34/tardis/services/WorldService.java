package com.adx.jogy34.tardis.services;

import java.util.UUID;
import java.util.Map.Entry;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.enums.TARDISWorlds;
import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.services.interfaces.IWorldService;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;
import com.adx.jogy34.tardis.util.spacial.NoFlyChunk;
import com.adx.jogy34.tardis.util.spacial.Position;
import com.adx.jogy34.tardis.worlds.EmptyGenerator;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.World.Environment;

public class WorldService implements IWorldService
{
    protected EMap<TARDISWorlds, UUID> customWorlds = new EMap<TARDISWorlds, UUID>();
    protected EList<String> worldPositions;
    protected NoFlyChunkWrapper noFlyChunks;

    @Override
    public void init() 
    {
        String worldName = ConfigProvider.getConfig().getString(ConfigProvider.Constants.TARDIS_WORLD);
        World tardisWorld = new WorldCreator(worldName)
                                        .environment(Environment.NORMAL)
                                        .generator(new EmptyGenerator())
                                        .createWorld();
        tardisWorld.setStorm(false);
        tardisWorld.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        tardisWorld.setTime(6000);
        
        customWorlds.put(TARDISWorlds.TARDIS_WORLD, tardisWorld.getUID());

        worldPositions = new EList<String>(ConfigProvider.getConfig().getStringList(ConfigProvider.Constants.WORLD_POSITIONS));
        worldPositions.remove(tardisWorld.getName()); // Make sure you can't fly into the TARDIS world

        this.noFlyChunks = ConfigProvider.createFromConfigParameters(NoFlyChunkWrapper.class, ConfigProvider.getAllConfigProperties(ConfigProvider.getDataConfig()));
    }

    @Override
    public World getWorld(TARDISWorlds world) { return Bukkit.getWorld(customWorlds.get(world)); }

    @Override
    public TARDISWorlds getTypeForWorld(World world) 
    {
        Entry<TARDISWorlds, UUID> w = customWorlds.FirstOrDefault(x -> x.getValue().equals(world.getUID()));
        return w == null ? TARDISWorlds.NONE : w.getKey();
    }

    @Override
    public World getMainWorld()
    {
        return Bukkit.getWorlds().get(0);
    }

    @Override
    public World getHigherWorld(World w)
    {
        int currentIndex = worldPositions.indexOf(w.getName());
        if(currentIndex < 0)
        {
            return Bukkit.getWorld(worldPositions.First());
        }
        else if(currentIndex >= worldPositions.size() - 1)
        {
            return null;
        }
        else
        {
            return Bukkit.getWorld(worldPositions.get(currentIndex + 1));
        }
    }

    @Override
    public World getLowerWorld(World w)
    {
        int currentIndex = worldPositions.indexOf(w.getName());
        if(currentIndex < 0)
        {
            return Bukkit.getWorld(worldPositions.Last());
        }
        else if(currentIndex == 0)
        {
            return null;
        }
        else
        {
            return Bukkit.getWorld(worldPositions.get(currentIndex - 1));
        }
    }

    @Override
    public int getWorldHeight(World w)
    {
        if(ConfigProvider.getConfig().contains(ConfigProvider.Constants.MAX_WORLD_HEIGHT + "." + w.getName()))
        {
            int maxHeight = ConfigProvider.getConfig().getInt(ConfigProvider.Constants.MAX_WORLD_HEIGHT + "." + w.getName());
            if(maxHeight > 0) return maxHeight;
        }

        return w.getMaxHeight();
    }

    private static class NoFlyChunkWrapper implements IConfigSerializable
    {
        @ConfigProperty(ConfigProvider.Constants.Data.NO_FLY_CHUNKS)
        protected EList<NoFlyChunk> noFlyChunks;

        public void add(Chunk c)
        {
            this.noFlyChunks.add(new NoFlyChunk(c));
            this.save();
        }

        public void remove(Chunk c)
        {
            noFlyChunks = noFlyChunks.Where(nfc -> !nfc.isThisChunk(c)).ToList();
            this.save();
        }

        public void save()
        {
            ConfigProvider.saveToConfig(ConfigProvider.getDataConfig(), this);
            ConfigProvider.saveDataConfig();
        }

        public boolean canLand(Position p)
        {
            return noFlyChunks.All(c -> c.canLand(p));
        }

        public boolean areValidLandingChunks(EList<Chunk> chunks)
        {
            return chunks.All(c -> !this.contains(c));
        }

        public boolean contains(Chunk c)
        {
            return noFlyChunks.Any(nfc -> nfc.isThisChunk(c));
        }
    }

    @Override
    public boolean addNoFlyChunk(Chunk c)
    {
        if(!this.noFlyChunks.contains(c))
        {
            this.noFlyChunks.add(c);
            return true;
        }
        return false;
    }

    @Override
    public boolean removeNoFlyChunk(Chunk c)
    {
        if(this.noFlyChunks.contains(c))
        {
            this.noFlyChunks.remove(c);
            return true;
        }
        return false;
    }

    @Override
    public boolean canLand(Position p)
    {
        return this.noFlyChunks.canLand(p);
    }

    @Override
    public boolean canLand(Chunk c)
    {
        return !this.noFlyChunks.contains(c);
    }

    @Override
    public boolean areValidLandingChunks(EList<Chunk> chunks)
    {
        return this.noFlyChunks.areValidLandingChunks(chunks);
    }
}