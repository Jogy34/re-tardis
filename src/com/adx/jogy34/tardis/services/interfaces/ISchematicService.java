package com.adx.jogy34.tardis.services.interfaces;

import com.adx.jlinq.EList;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jogy34.tardis.schematics.AuxillaryData;
import com.adx.jogy34.tardis.schematics.Cost;
import com.adx.jogy34.tardis.schematics.HallwayTheme;
import com.adx.jogy34.tardis.schematics.TardisSchematic;
import com.adx.jogy34.tardis.schematics.AccessPoint;
import com.adx.jogy34.tardis.schematics.enums.SchematicType;
import com.adx.jogy34.tardis.util.spacial.Position;

import org.bukkit.Material;

public interface ISchematicService
{
    public TardisSchematic createSchematic(Position p1, Position p2, String name, String createdBy, String description, SchematicType type, Cost cost,
                                           EList<AccessPoint> absoluteAccessPoints, EList<AuxillaryData> auxData, Material icon, int iteration, boolean replace);
    
    public void loadSchematics();
    public void reload();
    
    public TardisSchematic getSchematic(String constant);
    public TardisSchematic firstControlRoom();
    public TardisSchematic findNextControlRoom(int order);

    public IEnumerable<TardisSchematic> getSchematics();
    public IEnumerable<TardisSchematic> getSchematics(SchematicType type);
    public IEnumerable<TardisSchematic> getSchematics(boolean includeInactive);
    public IEnumerable<TardisSchematic> getSchematics(SchematicType type, boolean includeInatcive);

    public HallwayTheme getRandomHallwayTheme();
}