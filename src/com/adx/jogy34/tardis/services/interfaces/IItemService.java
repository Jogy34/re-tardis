package com.adx.jogy34.tardis.services.interfaces;

import org.bukkit.inventory.ItemStack;

import com.adx.jogy34.tardis.enums.TARDISItems;

public interface IItemService
{
    void init();
    ItemStack getItem(TARDISItems item, String... addlLore);
    TARDISItems getItem(ItemStack item);
    int hashItem(ItemStack item);
    int hashItem(ItemStack item, boolean includeAmount);
    ItemStack updateLore(ItemStack item, String... addlLore);
    ItemStack updateLore(ItemStack item, boolean maintainCurrent, String... addlLore);
    ItemStack getOriginal(ItemStack item);
    ItemStack getItemWithName(TARDISItems item, String name);
    ItemStack updateName(ItemStack item, String name);
}
