package com.adx.jogy34.tardis.services.interfaces;

import com.adx.jlinq.EList;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jogy34.tardis.commands.interfaces.ITardisCommand;

import org.bukkit.command.CommandSender;

public interface ICommandService
{
    boolean handleCommand(String command, CommandSender sender, String[] args);
    EList<String> handleTabComplete(String command, CommandSender sender, String[] args);
    void registerCommand(ITardisCommand cmd);
    void init();
    IEnumerable<ITardisCommand> getAllCommands();
}