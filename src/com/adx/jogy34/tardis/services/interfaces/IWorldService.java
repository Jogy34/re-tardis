package com.adx.jogy34.tardis.services.interfaces;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.enums.TARDISWorlds;
import com.adx.jogy34.tardis.util.spacial.Position;

import org.bukkit.Chunk;
import org.bukkit.World;

public interface IWorldService
{
    public void init();
    public World getWorld(TARDISWorlds world);
    public TARDISWorlds getTypeForWorld(World world);
    World getMainWorld();
    World getHigherWorld(World w);
    World getLowerWorld(World w);
    int getWorldHeight(World w);
    boolean addNoFlyChunk(Chunk c);
    boolean removeNoFlyChunk(Chunk c);
    boolean canLand(Position p);
    boolean canLand(Chunk c);
    boolean areValidLandingChunks(EList<Chunk> chunks);
}