package com.adx.jogy34.tardis.services.interfaces;

import java.util.UUID;

import com.adx.jogy34.tardis.actions.enums.TardisActionDataTypes;
import com.adx.jogy34.tardis.actions.enums.TardisActions;
import com.adx.jogy34.tardis.actions.interfaces.ITardisAction;
import com.adx.jogy34.tardis.actions.interfaces.ITardisActionData;

import org.bukkit.entity.Player;

public interface IActionService
{
    public void startPerformingAction(Player player, ITardisAction action);
    public void startPerformingAction(UUID player, ITardisAction action);
    public boolean isPerformingAction(Player player);
    public boolean isPerformingAction(UUID player);
    public boolean isPerformingAction(Player player, TardisActions type);
    public boolean isPerformingAction(UUID player, TardisActions type);
    public boolean isValidUpdateAction(Player player, TardisActionDataTypes type);
    public boolean isValidUpdateAction(UUID player, TardisActionDataTypes type);
    public boolean performAction(Player player, ITardisActionData data);
    public boolean performAction(UUID player, ITardisActionData data);
    public boolean completeAction(Player player);
    public boolean completeAction(UUID player);
    public void stopAction(Player player);
    public void stopAction(UUID player);
    public void stopAction(Player player, TardisActions type);
    public void stopAction(UUID player, TardisActions type);
    public void stopAllActions();
}