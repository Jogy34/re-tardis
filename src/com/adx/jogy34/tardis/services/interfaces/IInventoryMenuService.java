package com.adx.jogy34.tardis.services.interfaces;

import java.util.UUID;

import com.adx.jogy34.tardis.menu.base.interfaces.IInventoryMenu;
import com.adx.jogy34.tardis.menu.customize.CustomizeTARDISMenu;

public interface IInventoryMenuService
{
    CustomizeTARDISMenu getCustomizeTardisMenu(UUID playerId);
    void registerMenu(IInventoryMenu menu);
    <T extends IInventoryMenu> T getMenu(Class<T> menuClass);
	void onPluginDisable();
}
