package com.adx.jogy34.tardis.schematics;

import java.util.Map.Entry;

import com.adx.jlinq.EMap;

import org.bukkit.Material;

public class HallwayTheme
{
    protected EMap<Material, Double> floorWeights = new EMap<Material, Double>();
    protected EMap<Material, Double> angleWeights = new EMap<Material, Double>();
    protected EMap<Material, Double> wallWeights  = new EMap<Material, Double>();
    protected EMap<Material, Double> stepWeights  = new EMap<Material, Double>();
    protected Material light;

    public HallwayTheme(Material light)
    {
        this.light = light;
    }

    public void addFloor(Material mat, double weight)
    {
        this.floorWeights.put(mat, weight);
    }

    public void addAngle(Material mat, double weight)
    {
        this.angleWeights.put(mat, weight);
    }

    public void addWall(Material mat, double weight)
    {
        this.wallWeights.put(mat, weight);
    }

    public void addStep(Material mat, double weight)
    {
        this.stepWeights.put(mat, weight);
    }

    public Material[] getFloor(int count)
    {
        return getWeightedRandom(floorWeights, count);
    }

    public Material[] getAngle(int count)
    {
        return getWeightedRandom(angleWeights, count);
    }

    public Material[] getWall(int count)
    {
        return getWeightedRandom(wallWeights, count);
    }

    public Material[] getStep(int count)
    {
        return getWeightedRandom(stepWeights, count);
    }
    
    public Material getLight() 
    {
        return light;
    }

    protected Material[] getWeightedRandom(EMap<Material, Double> from, int count)
    {
        Material[] result = new Material[count];

        double total = 0;
        for(Double d : from.Select(x -> x.getValue()))
        {
            total += d;
        }

        for(int i = 0; i < count; i++)
        {
            double val = Math.random() * total;
    
            for(Entry<Material, Double> weight : from)
            {
                val -= weight.getValue();
                if(val <= 0) 
                {
                    result[i] = weight.getKey();
                    break;
                }
            }
    
            //This should never get hit but who knows with rounding errors and whatnot
            if(result[i] == null) result[i] = from.First().getKey();
        }

        return result;
    }
}