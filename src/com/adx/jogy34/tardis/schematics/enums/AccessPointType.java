package com.adx.jogy34.tardis.schematics.enums;

import java.text.MessageFormat;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.data.MiscMaterialData;
import com.adx.jogy34.tardis.data.TARDISAppearanceData;
import com.adx.jogy34.tardis.enums.PhaseState;
import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.helpers.StringHelper;
import com.adx.jogy34.tardis.util.spacial.Position;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;

public enum AccessPointType
{
    HALLWAY_ACCESS(Text.HALLWAY_ACCESS_POINT, SchematicType.CONTROL_ROOM, SchematicType.ADDITIONAL_ROOM),
    EXIT_DOOR(Text.EXIT_DOOR, SchematicType.CONTROL_ROOM, SchematicType.ADDITIONAL_ROOM),
    CONTROL_X_PLUS(Text.CONTROL_X_PLUS, SchematicType.CONTROL_ROOM),
    CONTROL_X_MINUS(Text.CONTROL_X_MINUS, SchematicType.CONTROL_ROOM),
    CONTROL_Y_PLUS(Text.CONTROL_Y_PLUS, SchematicType.CONTROL_ROOM),
    CONTROL_Y_MINUS(Text.CONTROL_Y_MINUS, SchematicType.CONTROL_ROOM),
    CONTROL_Z_PLUS(Text.CONTROL_Z_PLUS, SchematicType.CONTROL_ROOM),
    CONTROL_Z_MINUS(Text.CONTROL_Z_MINUS, SchematicType.CONTROL_ROOM),
    PARKING_BRAKE(Text.PARKING_BRAKE, SchematicType.CONTROL_ROOM),
    SPAWN(Text.SPAWN, SchematicType.CONTROL_ROOM),
    CONSOLE(Text.CONSOLE),
    CLOCK(Text.CLOCK),
    ;

    private EList<SchematicType> schematicTypes;
    private String name;

    private AccessPointType(String name, SchematicType... schematicTypes)
    {
        this.name = name;
        this.schematicTypes = new EList<SchematicType>(schematicTypes);
    }

    public AccessPointType getNext(SchematicType forType)
    {
        int startIndex = this.ordinal();
        AccessPointType[] values = values();

        for(int i = startIndex + 1; i < values.length; i++)
        {
            if(values[i].schematicTypes.contains(forType))
            {
                return values[i];
            }
        }
        
        for(int i = 0; i < startIndex; i++)
        {
            if(values[i].schematicTypes.contains(forType))
            {
                return values[i];
            }
        }

        return this;
    }

    public AccessPointType getPrevious(SchematicType forType)
    {
        int startIndex = this.ordinal();
        AccessPointType[] values = values();

        for(int i = startIndex - 1; i >= 0; i--)
        {
            if(values[i].schematicTypes.contains(forType))
            {
                return values[i];
            }
        }

        for(int i = values.length - 1; i > startIndex; i--)
        {
            if(values[i].schematicTypes.contains(forType))
            {
                return values[i];
            }
        }

        return this;
    }

    public static AccessPointType getFirst(SchematicType forType)
    {
        return new EList<>(values()).FirstOrDefault(x -> x.schematicTypes.contains(forType));
    }

    public String getName() { return name; }

    public boolean isValidMaterial(Material mat)
    {
        switch(this)
        {
            case HALLWAY_ACCESS:
            {
                return TARDISAppearanceData.ALLOWED_HALLWAY_ACCESS_MATERIALS.contains(mat);
            }
            case EXIT_DOOR:
            {
                return TARDISAppearanceData.ALLOWED_DOOR_MATERIALS.contains(mat);
            }
            case CONTROL_X_MINUS:
            case CONTROL_X_PLUS:
            case CONTROL_Y_MINUS:
            case CONTROL_Y_PLUS:
            case CONTROL_Z_MINUS:
            case CONTROL_Z_PLUS:
            {
                return TARDISAppearanceData.ALLOWED_CONTROL_MATERIALS.contains(mat);
            }
            case SPAWN:
            {
                return mat.isSolid();
            }
            case PARKING_BRAKE:
            case CLOCK:
            case CONSOLE:
            {
                return MiscMaterialData.ALL_SIGNS.contains(mat);
            }
        }
        return false;
    }

    public boolean isValidForTardisAccessPoint()
    {
        return this != SPAWN && this != EXIT_DOOR;
    }

    public int getControlDirection()
    {
        switch(this)
        {
            case CONTROL_X_PLUS:
            case CONTROL_Y_PLUS:
            case CONTROL_Z_PLUS:
            {
                return 1;
            }
            case CONTROL_X_MINUS:
            case CONTROL_Y_MINUS:
            case CONTROL_Z_MINUS:
            {
                return -1;
            }
            default:
            {
                return 0;
            }
        }
    }

    public boolean isControl()
    {
        switch(this)
        {
            case CONTROL_X_PLUS:
            case CONTROL_Y_PLUS:
            case CONTROL_Z_PLUS:
            case CONTROL_X_MINUS:
            case CONTROL_Y_MINUS:
            case CONTROL_Z_MINUS:
            {
                return true;
            }
            default:
            {
                return false;
            }
        }
    }

    public int getModX()
    {
        if(this == CONTROL_X_MINUS || this == CONTROL_X_PLUS)
        {
            return getControlDirection();
        }

        return 0;
    }

    public int getModY()
    {
        if(this == CONTROL_Y_MINUS || this == CONTROL_Y_PLUS)
        {
            return getControlDirection();
        }

        return 0;
    }

    public int getModZ()
    {
        if(this == CONTROL_Z_MINUS || this == CONTROL_Z_PLUS)
        {
            return getControlDirection();
        }

        return 0;
    }

    public boolean isSignDisplay()
    {
        return this == CLOCK || this == CONSOLE || this == AccessPointType.PARKING_BRAKE;
    }

    public String[] getSignDisplay(TARDIS t)
    {
        Position pos = t.getPosition();
        World w = pos.getWorld();
        if(this == CLOCK)
        {
            long time = w.getTime();
            int hours = (int) (Math.floor(time / 1000.0) + 6) % 24;
            int minutes = (int) Math.floor(((time % 1000) / 1000.0) * 60);
            String formatted = null;
            if(use24HourFormat)
            {
                formatted = StringHelper.leftPad(hours + "", '0', 2) + ":" + StringHelper.leftPad(minutes + "", '0', 2);
            }
            else
            {
                String side = hours < 12 ? " AM" : " PM";
                if(hours == 0) hours = 12;
                if(hours > 12) hours -= 12;

                formatted = StringHelper.leftPad(hours + "", '0', 2) + ":" + StringHelper.leftPad(minutes + "", '0', 2) + side;
            }

            return new String[] {w.getName(), formatted};
        }
        else if(this == CONSOLE)
        {
            return new String[] {
                Text.format("{X}: {0}", pos.getX()),
                Text.format("{Y}: {0}", pos.getY()),
                Text.format("{Z}: {0}", pos.getZ()),
                Text.format("{0}", w.getName()),
            };
        }
        else if(this == PARKING_BRAKE)
        {
            PhaseState phase = t.getPhaseState();
            return new String[] {
                Text.PARKING_BRAKE,
                MessageFormat.format("{0}{1}[{2}]", ChatColor.BOLD, phase.getColor(), phase.getText())
            };
        }

        return null;
    }

    private static boolean use24HourFormat = true;
    static {
        use24HourFormat = ConfigProvider.getConfig().getBoolean(ConfigProvider.Constants.CLOCK_24HR);
    }
}