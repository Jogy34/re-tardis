package com.adx.jogy34.tardis.schematics.enums;

import java.util.Arrays;

import com.adx.jlinq.EList;

import org.bukkit.Material;

public enum SchematicType
{
    CONTROL_ROOM("Control Room", Material.BEACON),
    ADDITIONAL_ROOM("Additional Room", Material.OAK_LOG),
    OTHER("Other", Material.WHITE_CONCRETE),
    ;

    public final String NAME;
    public final Material ICON;

    private SchematicType(String name, Material icon)
    {
        this.NAME = name;
        this.ICON = icon;
    }

    public EList<AccessPointType> getRequiredAccessPoints()
    {
        EList<AccessPointType> results = new EList<AccessPointType>();

        switch(this)
        {
            case CONTROL_ROOM:
            {
                results.addAll(Arrays.asList(
                    AccessPointType.HALLWAY_ACCESS,
                    AccessPointType.SPAWN,
                    AccessPointType.EXIT_DOOR,
                    AccessPointType.CONTROL_Z_PLUS,
                    AccessPointType.CONTROL_Z_MINUS,
                    AccessPointType.CONTROL_Y_PLUS,
                    AccessPointType.CONTROL_Y_MINUS,
                    AccessPointType.CONTROL_X_PLUS,
                    AccessPointType.CONTROL_X_MINUS,
                    AccessPointType.PARKING_BRAKE
                ));
                break;
            }
            case ADDITIONAL_ROOM:
            {
                results.add(AccessPointType.HALLWAY_ACCESS);
                break;
            }
            case OTHER:
            {
                break;
            }
        }

        return results;
    }
}