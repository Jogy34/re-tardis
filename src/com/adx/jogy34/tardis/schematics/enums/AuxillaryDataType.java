package com.adx.jogy34.tardis.schematics.enums;

import com.adx.jlinq.EList;

public enum AuxillaryDataType
{
    CONTROL_ROOM_ORDER("order"),
    ICON("icon"),
    GENERIC,
    ;

    private EList<String> names;
    private AuxillaryDataType(String... names)
    {
        this.names = new EList<String>(names);
    }

    public static AuxillaryDataType getFromName(String name)
    {
        name = name.toLowerCase();
        for(AuxillaryDataType type : values())
        {
            if(type.names.contains(name)) return type;
        }

        return GENERIC;
    }
}