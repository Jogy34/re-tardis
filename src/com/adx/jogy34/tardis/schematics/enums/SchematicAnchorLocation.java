package com.adx.jogy34.tardis.schematics.enums;

import com.adx.jogy34.tardis.schematics.TardisSchematicSize;
import com.adx.jogy34.tardis.util.spacial.Position;

public enum SchematicAnchorLocation
{
    FRONT_LEFT_BOTTOM_CORNER,
    CENTER_BOTTOM,
    ;

    public Position transform(Position anchor, TardisSchematicSize size)
    {
        switch(this)
        {
            case CENTER_BOTTOM:
            {
                return anchor.add(-size.getX() / 2, 0, -size.getZ() / 2);
            }
            case FRONT_LEFT_BOTTOM_CORNER:
            default:
            {
                return anchor;
            }
        }
    }
}