package com.adx.jogy34.tardis.schematics;

import java.text.MessageFormat;

import com.adx.jogy34.tardis.schematics.enums.AccessPointType;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;
import com.adx.jogy34.tardis.util.spacial.Point;

public class AccessPoint implements IConfigSerializable
{
    @ConfigProperty("Offset")
    protected Point offset;
    @ConfigProperty("Type")
    protected AccessPointType type;

    @SuppressWarnings("unused")
    private AccessPoint() {}

    public AccessPoint(Point offset, AccessPointType type)
    {
        this.offset = offset;
        this.type = type;
    }

    public AccessPointType getType() { return type; }
    public Point getOffset() { return offset; }

    public AccessPoint offsetFrom(Point p)
    {
        return new AccessPoint(offset.add(p), type);
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} => ({1})", type.getName(), offset);
    }

}