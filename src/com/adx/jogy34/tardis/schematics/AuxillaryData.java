package com.adx.jogy34.tardis.schematics;

import com.adx.jogy34.tardis.schematics.enums.AuxillaryDataType;
import com.adx.jogy34.tardis.util.helpers.StringHelper;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Material;

public class AuxillaryData implements IConfigSerializable
{
    @ConfigProperty("Type")
    protected AuxillaryDataType type;
    @ConfigProperty("Value")
    protected String value;
    @ConfigProperty("Constant")
    protected String constant;

    @SuppressWarnings("unused")
    private AuxillaryData() {}

    public AuxillaryData(String constant, String value)
    {
        this(AuxillaryDataType.getFromName(constant), constant, value);
    }

    public AuxillaryData(AuxillaryDataType type, String value)
    {
        this(type, null, value);
    }

    public AuxillaryData(AuxillaryDataType type, String constant, String value)
    {
        this.type = type;
        this.value = value;
        this.constant = constant;
    }

    public AuxillaryDataType getType() { return type; }
    public String getConstant() { return constant; }
    
    public String getValue() { return value; }
    public void setValue(String value) { this.value = value; }

    public int getInt() { return StringHelper.toIntOrDefault(value); }
    public double getDouble() { return StringHelper.toDoubleOrDefault(value); }
    public boolean getBoolean() { return StringHelper.toBooleanOrDefault(value); }
    public Material getMaterial() { return StringHelper.toMaterialOrDefault(value, type == AuxillaryDataType.ICON ? Material.BONE_BLOCK : Material.AIR); }

    public boolean isSameType(AuxillaryData other) {
        if(other == null) return false;

        if(this.type != other.type) return false;
        if(this.constant == null) return other.constant == null;
        else return this.constant.equals(other.constant);
    }
}