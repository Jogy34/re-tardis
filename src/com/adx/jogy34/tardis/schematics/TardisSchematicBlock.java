package com.adx.jogy34.tardis.schematics;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.schematics.blockstate.SchematicBlockState;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.inventory.BlockInventoryHolder;
import org.bukkit.inventory.Inventory;

public class TardisSchematicBlock implements IConfigSerializable
{
    @ConfigProperty("Key")
    protected String key;
    @ConfigProperty("Type")
    protected Material type;
    @ConfigProperty("Data")
    protected BlockData blockData;
    @ConfigProperty("BlockState")
    protected SchematicBlockState blockState;
    @ConfigProperty("Inventory")
    protected EList<InventoryItem> inventory;

    public TardisSchematicBlock()
    {
    }

    public TardisSchematicBlock(String key, Block block)
    {
        this.key = key;
        this.type = block.getType();
        this.blockData = block.getBlockData();
        this.blockState = SchematicBlockState.from(block);
        this.inventory = InventoryItem.from(block);
    }

    public boolean matches(Block other) 
    {
        if(other == null) return false;
        
        if(other.getType() != type) return false;

        BlockData otherData = other.getBlockData();
        if(blockData == null || otherData == null) return blockData == null && otherData == null;
        if(!blockData.matches(otherData) || !otherData.matches(blockData)) return false;

        SchematicBlockState otherState = SchematicBlockState.from(other);
        if(this.blockState != null || otherState != null)
        {
            if(this.blockState == null || otherState == null || !this.blockState.matches(otherState)) return false;
        }

        EList<InventoryItem> otherInventory = InventoryItem.from(other);
        if(inventory != null || otherInventory != null) 
        {
            if(inventory == null || otherInventory == null || inventory.size() != otherInventory.size()) return false;
            if(!inventory.All(i -> otherInventory.Any(ii -> i.matches(ii)))) return false;
        }

        return true;
    }

    public boolean matches(TardisSchematicBlock other)
    {
        if(other == null) return false;

        if(other.key.equals(key) && other.type == type)
        {
            if(this.blockData != null || other.blockData != null)
            {
                if(this.blockData == null || other.blockData == null || !this.blockData.matches(other.blockData) || !other.blockData.matches(blockData)) 
                {
                    return false;
                }
            }

            if(this.blockState != null || other.blockState != null)
            {
                if(this.blockState == null || other.blockState == null || !this.blockState.matches(other.blockState)) return false;
            }

            if(inventory != null || other.inventory != null) 
            {
                if(inventory == null || other.inventory == null || inventory.size() != other.inventory.size()) return false;
                if(!inventory.All(i -> other.inventory.Any(ii -> i.matches(ii)))) return false;
            }

            return true;
        }

        return false;
    }

    public String getKey()
    {
        return key;
    }

    public void apply(Block b) { this.apply(b, false); }
    public void apply(Block b, boolean includeAir)
    {
        if(includeAir || this.type != Material.AIR)
        {
            b.setType(type, false);
            if(blockData != null) b.setBlockData(blockData.clone(), false);
            if(blockState != null) blockState.apply(b);
            if(inventory != null) 
            {
                BlockState state = b.getState();
                if(state instanceof BlockInventoryHolder) 
                {
                    Inventory inven = ((BlockInventoryHolder) state).getInventory();
                    inventory.ForEach(x -> x.apply(inven));
                }
            }
        }
    }
}