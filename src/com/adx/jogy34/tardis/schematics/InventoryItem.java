package com.adx.jogy34.tardis.schematics;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.inventory.BlockInventoryHolder;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryItem implements IConfigSerializable
{
    @ConfigProperty("Type")
    protected Material type;
    @ConfigProperty("Amount")
    protected int amount;
    @ConfigProperty("Slots")
    protected EList<Integer> slots;

    @SuppressWarnings("unused")
    private InventoryItem() {}

    public InventoryItem(ItemStack stack, int slot) 
    {
        this.type = stack.getType();
        this.amount = stack.getAmount();
        this.slots = new EList<Integer>();
        this.slots.add(slot);
    }

    public boolean addIfMatches(ItemStack stack, int slot) 
    {
        if(matches(stack))
        {
            this.slots.add(slot);
            return true;
        }
        
        return false;
    }

    public boolean matches(ItemStack stack)
    {
        return type == stack.getType() && amount == stack.getAmount();
    }

    public boolean matches(InventoryItem other)
    {
        if(type == other.type && amount == other.amount && slots.size() == other.slots.size())
        {
            return slots.All(s -> other.slots.contains(s));
        }

        return false;
    }

    public void apply(Inventory inven)
    {
        ItemStack stack = new ItemStack(type, amount);
        if(slots != null)
        {
            slots.ForEach(x -> inven.setItem(x, stack));
        }
    }

    public static EList<InventoryItem> from(Block b)
    {
        BlockState state = b.getState();
        if(state instanceof BlockInventoryHolder)
        {
            EList<InventoryItem> result = new EList<InventoryItem>();
            BlockInventoryHolder invenHolder = (BlockInventoryHolder) state;
            Inventory inven = invenHolder.getInventory();
            ItemStack[] contents = inven.getContents();
            
            for(int i = 0; i < contents.length; i++)
            {
                if(contents[i] != null) 
                {
                    final int index = i;
                    InventoryItem addedTo = result.FirstOrDefault(x -> x.addIfMatches(contents[index], index));
                    if(addedTo == null) 
                    {
                        result.add(new InventoryItem(contents[i], i));
                    }
                }
            }

            if(result.isEmpty()) result = null;

            return result;
        }
        
        return null;
    }
}