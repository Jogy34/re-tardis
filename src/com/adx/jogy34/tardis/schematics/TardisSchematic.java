package com.adx.jogy34.tardis.schematics;

import java.io.File;

import com.adx.jlinq.EList;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jlinq.tuples.Tuples.Tuple;
import com.adx.jogy34.tardis.enums.TARDISSchematicVersion;
import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.schematics.enums.AccessPointType;
import com.adx.jogy34.tardis.schematics.enums.AuxillaryDataType;
import com.adx.jogy34.tardis.schematics.enums.SchematicAnchorLocation;
import com.adx.jogy34.tardis.schematics.enums.SchematicType;
import com.adx.jogy34.tardis.util.spacial.Point;
import com.adx.jogy34.tardis.util.spacial.Position;
import com.adx.jogy34.tardis.util.Encode;
import com.adx.jogy34.tardis.util.helpers.StringHelper;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

public class TardisSchematic implements IConfigSerializable
{
    public static final String SCHEMATIC_PATH = "Schematics" + File.separator;
    protected static final String COUNT_SEPARATOR = ".";
    protected static final String X_SEPARATOR = ",";
    protected static final String Z_SEPARATOR = ";";
    protected static final String Y_SEPARATOR = "\n";

    @ConfigProperty("Constant")
    protected String constant;
    @ConfigProperty("Name")
    protected String name;
    @ConfigProperty("Author")
    protected String author;
    @ConfigProperty("Description")
    protected String description;
    @ConfigProperty("Size")
    protected TardisSchematicSize size;
    @ConfigProperty("Blocks")
    protected EList<TardisSchematicBlock> blocks;
    @ConfigProperty("Structure")
    protected String structure;
    @ConfigProperty("Version")
    protected TARDISSchematicVersion version = TARDISSchematicVersion.CURRENT;
    @ConfigProperty("Iteration")
    protected int iteration = 1;
    @ConfigProperty("Type")
    protected SchematicType type;
    @ConfigProperty("AccessPoints")
    protected EList<AccessPoint> accessPoints;
    @ConfigProperty("Active")
    protected boolean active = true;
    @ConfigProperty("Cost")
    protected Cost cost;
    @ConfigProperty("AuxillaryData")
    protected EList<AuxillaryData> auxData;
    @ConfigProperty("Icon")
    protected Material icon;

    @SuppressWarnings("unused")
    private TardisSchematic() { }
    
    public TardisSchematic(Position p1, Position p2, String constant, String author, String description, SchematicType type, Cost cost,
                           EList<AccessPoint> absoluteAccessPoints, EList<AuxillaryData> auxData, Material icon, int iteration)
    {
        this(p1, p2, constant, author, description, type, cost, absoluteAccessPoints, auxData, icon, iteration, true);
    }

    public TardisSchematic(Position p1, Position p2, String constant, String author, String description, SchematicType type, Cost cost,
                           EList<AccessPoint> absoluteAccessPoints, EList<AuxillaryData> auxData, Material icon, int iteration, boolean save)
    {
        this.constant = constant.toLowerCase().replace(' ', '_');
        this.name = StringHelper.fromSnakeCase(this.constant);
        this.author = author;
        this.description = description;
        this.size = new TardisSchematicSize(p1, p2);
        this.blocks = new EList<TardisSchematicBlock>();
        this.structure = "";
        this.type = type;
        this.cost = cost;
        this.auxData = auxData;
        this.icon = icon;
        this.iteration = iteration;

        Tuple<Position, Position> ordered = p1.order(p2);

        this.accessPoints = absoluteAccessPoints.Select(ap -> new AccessPoint(ap.getOffset().subtract(ordered.item1), ap.getType())).ToList();

        fillSchematicData(ordered.item1, ordered.item2);
        
        if(save) saveToConfig();
    }

    public void replaceData(TardisSchematic other)
    {
        this.accessPoints = other.accessPoints;
        this.blocks = other.blocks;
        this.structure = other.structure;
        saveToConfig();
    }

    protected void fillSchematicData(Position from, Position to)
    {
        World w = from.getWorld();

        String lastKey = null;
        int lastCount = 0;
        
        for(int y = from.getY(); y <= to.getY(); y++)
        {
            for(int z = from.getZ(); z <= to.getZ(); z++)
            {
                for(int x = from.getX(); x <= to.getX(); x++)
                {
                    Block b = w.getBlockAt(x, y, z);
                    TardisSchematicBlock thisBlock = blocks.FirstOrDefault(sb -> sb.matches(b));
                    if(thisBlock == null)
                    {
                        thisBlock = new TardisSchematicBlock(Encode.base80Encode(blocks.size()), b);
                        blocks.add(thisBlock);
                    }

                    if(thisBlock.getKey().equals(lastKey)) 
                    {
                        lastCount++;
                    }
                    else 
                    {
                        if(lastCount > 0)
                        {
                            this.structure += lastKey + COUNT_SEPARATOR + lastCount + X_SEPARATOR;
                        }

                        lastKey = thisBlock.getKey();
                        lastCount = 1;
                    }
                }

                this.structure += lastKey + COUNT_SEPARATOR + lastCount + Z_SEPARATOR;
                lastKey = null;
                lastCount = 0;
            }
            this.structure += Y_SEPARATOR;
        }
    }

    protected void saveToConfig()
    {
        String configName = SCHEMATIC_PATH + this.constant;
        ConfigProvider.saveToConfig(ConfigProvider.getConfig(configName), this);
        ConfigProvider.saveConfig(configName);
    }

    //#region Getters/Setters

    public String getConstant()
    {
        return this.constant;
    }

    public String getName()
    {
        return this.name;
    }

    public TardisSchematicSize getSize()
    {
        return this.size.clone();
    }

    public SchematicType getType() 
    {
        return type;
    }

    public Material getIcon() 
    {
        return icon;
    }

    public String getDescription() 
    {
        return description;
    }

    public void setDescription(String description) 
    {
        this.description = description;
    }

    public Cost getCost() 
    {
        return cost == null ? new Cost() : cost;
    }

    public int getIteration()
    {
        return this.iteration;
    }

    public void setAuxillaryData(AuxillaryData data) 
    {
        if(this.auxData == null) this.auxData = new EList<AuxillaryData>();

        AuxillaryData existing = this.auxData.FirstOrDefault(x -> x.isSameType(data));

        if(existing == null) this.auxData.add(data);
        else existing.setValue(data.getValue());
    }

    public Point getFirstAccessPointOffset(AccessPointType type)
    {
        AccessPoint ap = accessPoints.FirstOrDefault(x -> x.getType() == type);
        return ap == null ? new Point(0, 0, 0) : ap.getOffset();
    }

    public Point getFirstAccessPointOffsetFrom(AccessPointType type, Point start)
    {
        return start.add(getFirstAccessPointOffset(type));
    }

    public IEnumerable<Point> getAccessPointOffsets(AccessPointType type)
    {
        return accessPoints.Where(ap -> ap.getType() == type).Select(x -> x.getOffset());
    }

    public IEnumerable<Point> getAccessPointOffsetsFrom(AccessPointType type, Point start)
    {
        return accessPoints.Where(ap -> ap.getType() == type).Select(x -> start.add(x.getOffset()));
    }

    public IEnumerable<AccessPoint> getAccessPointsOffsetFrom(Point start)
    {
        return accessPoints.Select(x -> x.offsetFrom(start));
    }

    public IEnumerable<AccessPoint> getFilteredAccessPointsOffsetFrom(Point start)
    {
        return getAccessPointsOffsetFrom(start).Where(x -> x.getType().isValidForTardisAccessPoint());
    }

    public AuxillaryData getAuxillaryData(AuxillaryDataType type)
    {
        return auxData.FirstOrDefault(d -> d.getType() == type);
    }

    public AuxillaryData getAuxillaryData(String constant)
    {
        return auxData.FirstOrDefault(d -> d.getConstant() == constant);
    }
    
    public boolean isActive() 
    {
		return this.active;
	}

    //#endregion Getters/Setters

    //#region Construction

    public void build(Position anchor) { this.build(anchor, false); }
    public void build(Position anchor, boolean includeAir) { this.build(anchor, SchematicAnchorLocation.FRONT_LEFT_BOTTOM_CORNER, includeAir); }
    public void build(Position anchor, SchematicAnchorLocation anchorType, boolean includeAir)
    {
        Position start = anchorType.transform(anchor, size);
        World w = start.getWorld();
        String[] layers = structure.split(Y_SEPARATOR);
        for(int y = 0; y < layers.length; y++)
        {
            String layer = layers[y];
            String[] slices = layer.split(Z_SEPARATOR);
            for(int z = 0; z < slices.length; z++)
            {
                String slice = slices[z];
                String[] chunks = slice.split(X_SEPARATOR);
                int x = 0;
                for(int i = 0; i < chunks.length; i++)
                {
                    String[] chunkData = chunks[i].split("\\" + COUNT_SEPARATOR);
                    if(chunkData.length == 2)
                    {
                        TardisSchematicBlock type = blocks.FirstOrDefault(b -> b.getKey().equals(chunkData[0]));
                        int length = Integer.parseInt(chunkData[1]);
                        for(int j = 0; j < length; j++, x++)
                        {
                            type.apply(w.getBlockAt(x + start.getX(), y + start.getY(), z + start.getZ()), includeAir);
                        }
                    }
                }
            }
        }
    }

    public void destroy(Position anchor) { this.destroy(anchor, SchematicAnchorLocation.FRONT_LEFT_BOTTOM_CORNER); }
    public void destroy(Position anchor, SchematicAnchorLocation anchorType)
    {
        Position start = anchorType.transform(anchor, size);
        for(int x = 0; x < size.getX(); x++)
        {
            for(int y = 0; y < size.getY(); y++)
            {
                for(int z = 0; z < size.getZ(); z++)
                {
                    start.getBlockRelative(x, y, z).setType(Material.AIR, false);
                }
            }
        }
    }
    
    //#endregion Construction
}