package com.adx.jogy34.tardis.schematics;

import com.adx.jogy34.tardis.util.spacial.Position;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

public class TardisSchematicSize implements IConfigSerializable
{
    @ConfigProperty("X")
    protected int _x;
    @ConfigProperty("Y")
    protected int _y;
    @ConfigProperty("Z")
    protected int _z;

    public TardisSchematicSize()
    {
    }

    public TardisSchematicSize(Position p1, Position p2)
    {
        this(Math.abs(p1.getX() - p2.getX()) + 1,  Math.abs(p1.getY() - p2.getY()) + 1, Math.abs(p1.getZ() - p2.getZ()) + 1);
    }

    public TardisSchematicSize(int x, int y, int z)
    {
        this._x = x;
        this._y = y;
        this._z = z;
    }

    public TardisSchematicSize clone()
    {
        return new TardisSchematicSize(_x, _y, _z);
    }

    public int getX() { return _x; }
    public int getY() { return _y; }
    public int getZ() { return _z; }
}