package com.adx.jogy34.tardis.schematics;

import java.text.MessageFormat;

import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.helpers.StringHelper;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class Cost implements IConfigSerializable
{
    @ConfigProperty("Amount")
    protected int amount;
    @ConfigProperty("Type")
    protected Material type;

    public Cost() {
        this(0, Material.AIR);
    }

    public Cost(int amount, Material type)
    {
        this.amount = amount;
        this.type = type;
    }

    public int getAmount() { return amount; }
    public void setAmount(int amount) { this.amount = amount; }

    public Material getType() { return type; }
    public void setType(Material type) { this.type = type; }

    public boolean isFree()
    {
        return amount <= 0 || type == null || type == Material.AIR;
    }

    @Override
    public String toString() {
        return isFree() ? Text.FREE : MessageFormat.format("{0}x {1}", amount, StringHelper.getName(type));
    }

    public String toDisplayString() {
        return Text.format("{0}{COST}: {1}", ChatColor.GOLD, this);
    }

    public boolean pay(Player p)
    {
        if(p.getGameMode() == GameMode.CREATIVE || isFree()) return true;
        PlayerInventory pi = p.getInventory();
        ItemStack[] items = pi.getStorageContents();
        int leftToTake = amount;

        for(int i = 0; i < items.length && leftToTake > 0; i++)
        {
            ItemStack item = items[i];
            if(item != null && item.getType() == type)
            {
                if(item.getAmount() <= leftToTake)
                {
                    leftToTake -= item.getAmount();
                    items[i] = null;
                }
                else
                {
                    item.setAmount(item.getAmount() - leftToTake);
                    leftToTake = 0;
                }
            }
        }

        if(leftToTake == 0)
        {
            pi.setStorageContents(items);
            return true;
        }

        return false;
    }
}