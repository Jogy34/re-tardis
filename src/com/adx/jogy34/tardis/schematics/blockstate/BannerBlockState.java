package com.adx.jogy34.tardis.schematics.blockstate;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.DyeColor;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;

public class BannerBlockState extends SchematicBlockState 
{
    @ConfigProperty("Base")
    protected DyeColor base;
    @ConfigProperty("Patterns")
    protected EList<BannerPattern> patterns = new EList<BannerPattern>();

    @SuppressWarnings("unused")
    private BannerBlockState() {}

    public BannerBlockState(Banner banner)
    {
        this.base = banner.getBaseColor();
        this.patterns = new EList<>(banner.getPatterns()).Select(p -> new BannerPattern(p)).ToList();
    }

    @Override
    public void apply(Block b) 
    {
        if(canApply(b))
        {
            Banner state = (Banner) b.getState();
            state.setBaseColor(base);
            state.setPatterns(patterns.Select(p -> p.toPattern()).ToList());
            state.update(false, false);
        }
    }

    @Override
    public boolean matches(SchematicBlockState other) 
    {
        if(!(other instanceof SkullBlockState)) return false;
        
        BannerBlockState s = (BannerBlockState) other;

        if(s.base != base || patterns.Count() != s.patterns.Count()) return false;

        for(int i = 0; i < patterns.Count(); i++)
        {
            if(!patterns.get(i).matches(s.patterns.get(i))) return false;
        }

        return true;
    }
    
    private static class BannerPattern implements IConfigSerializable
    {
        @ConfigProperty("Type")
        public PatternType type;
        @ConfigProperty("Color")
        public DyeColor color;

        @SuppressWarnings("unused")
        private BannerPattern() {}

        public BannerPattern(Pattern p)
        {
            type = p.getPattern();
            color = p.getColor();
        }

        public boolean matches(BannerPattern p)
        {
            return p.type == type && p.color == color;
        }

        public Pattern toPattern()
        {
            return new Pattern(color, type);
        }
    }
}