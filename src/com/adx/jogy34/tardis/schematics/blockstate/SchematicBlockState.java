package com.adx.jogy34.tardis.schematics.blockstate;

import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.util.annotations.KnownTypes;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;

@KnownTypes({SignBlockState.class, SkullBlockState.class, BannerBlockState.class})
public abstract class SchematicBlockState implements IConfigSerializable
{
    public abstract void apply(Block b);
    public abstract boolean matches(SchematicBlockState other);

    public boolean canApply(Block b)
    {
        SchematicBlockState from = from(b);
        return from != null && from.getClass() == this.getClass();
    }

    public static SchematicBlockState from(Block b)
    {
        BlockState state = b.getState();
        if(state instanceof Sign)
        {
            return new SignBlockState((Sign) state);
        }
        else if(state instanceof Skull)
        {
            return new SkullBlockState((Skull) state);
        }
        else if(state instanceof Banner)
        {
            return new BannerBlockState((Banner) state);
        }

        return null;
    }

    @Override
    public String toString() 
    {
        return ConfigProvider.getConfigProperties(this).ConcatenateToString(x -> "{" + x.getKey() + ": " + x.getValue() + "}", ", ");
    }
}