package com.adx.jogy34.tardis.schematics.blockstate;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;

public class SignBlockState extends SchematicBlockState 
{
    @ConfigProperty("Lines")
    protected EList<String> lines;

    @SuppressWarnings("unused")
    private SignBlockState() {}

    public SignBlockState(Sign state)
    {
        this.lines = new EList<String>(state.getLines());
    }

    @Override
    public void apply(Block b) 
    {
        if(this.canApply(b))
        {
            Sign state = (Sign) b.getState();
            for(int i = 0; i < lines.size() && i < 4; i++)
            {
                state.setLine(i, lines.get(i));
            }
            state.update(false, false);
        }
	}

    @Override
    public boolean matches(SchematicBlockState other) 
    {
        if(!(other instanceof SignBlockState)) return false;

        SignBlockState s = (SignBlockState) other;

        if(s.lines.size() != lines.size()) return false;

        for(int i = 0; i < lines.size(); i++)
        {
            if(!lines.get(i).equals(s.lines.get(i))) return false;
        }

        return true;
    }
}