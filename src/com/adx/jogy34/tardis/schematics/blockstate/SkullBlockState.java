package com.adx.jogy34.tardis.schematics.blockstate;

import java.lang.reflect.Method;
import java.util.AbstractList;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.helpers.VersionHelper;

import org.bukkit.block.Block;
import org.bukkit.block.Skull;

public class SkullBlockState extends SchematicBlockState {

    private static Class<?> nbtTagCompoundClass, nbtTagListClass, tileEntityClass, craftBlockEntityStateClass;
    private static Method getTileEntityFromCraftSkull, getSnapshotNBTMethod, getCompoundMethod, hasKeyMethod;
    private static Method getStringMethod, getMethod, getCompoundListMethod, sizeListMethod, setStringMethod;
    private static Method setMethod, addMethod, loadMethod, updateMethod;

    static {
        try 
        {
            craftBlockEntityStateClass = VersionHelper.getOBCClass("block.CraftBlockEntityState");
            nbtTagCompoundClass = VersionHelper.getNMSClass("NBTTagCompound");
            nbtTagListClass = VersionHelper.getNMSClass("NBTTagList");
            Class<?> nbtBase = VersionHelper.getNMSClass("NBTBase");
            tileEntityClass = VersionHelper.getNMSClass("TileEntity");
            
            getTileEntityFromCraftSkull = craftBlockEntityStateClass.getDeclaredMethod("getTileEntity");
            getSnapshotNBTMethod = craftBlockEntityStateClass.getDeclaredMethod("getSnapshotNBT");
            getCompoundMethod = nbtTagCompoundClass.getDeclaredMethod("getCompound", String.class);
            hasKeyMethod = nbtTagCompoundClass.getDeclaredMethod("hasKey", String.class);
            getStringMethod = nbtTagCompoundClass.getDeclaredMethod("getString", String.class);
            getMethod = nbtTagCompoundClass.getDeclaredMethod("get", String.class);
            getCompoundListMethod = nbtTagListClass.getDeclaredMethod("getCompound", int.class);
            sizeListMethod = nbtTagListClass.getDeclaredMethod("size");

            setStringMethod = nbtTagCompoundClass.getDeclaredMethod("setString", String.class, String.class);
            setMethod = nbtTagCompoundClass.getDeclaredMethod("set", String.class, nbtBase);
            addMethod = AbstractList.class.getDeclaredMethod("add", Object.class);
            loadMethod = tileEntityClass.getDeclaredMethod("load", nbtTagCompoundClass);
            updateMethod = tileEntityClass.getDeclaredMethod("update");

        } 
        catch (Exception e) 
        {
            Logger.error(e);
        }
    }

    @ConfigProperty("Player")
    protected String player;

    @ConfigProperty("textures")
    protected EList<String> textures;

    @SuppressWarnings("unused")
    private SkullBlockState() {
    }

    @SuppressWarnings("deprecation")
    public SkullBlockState(Skull skull) {
        this.player = skull.getOwner();

        if(this.player == null) 
        {
            fillWithObcNms(skull);
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public void apply(Block b) {
        if (this.player != null && canApply(b)) {
            Skull state = (Skull) b.getState();

            if (textures != null && textures.Any()) {
                applyWithObcNms(state);
            }
            else {
                state.setOwner(player);
                state.update(false, false);
            }
        }
    }

    private void fillWithObcNms(Skull skull)
    {
        try
        {
            Object tag = getSnapshotNBTMethod.invoke(skull);
            if ((boolean) hasKeyMethod.invoke(tag, "Owner")) {
                Object owner = getCompoundMethod.invoke(tag, "Owner");
                if ((boolean) hasKeyMethod.invoke(owner, "Properties") && (boolean) hasKeyMethod.invoke(owner, "Id")) {
                    this.player = (String) getStringMethod.invoke(owner, "Id");
                    Object props = getCompoundMethod.invoke(owner, "Properties");
                    if ((boolean) hasKeyMethod.invoke(props, "textures")) {
                        this.textures = new EList<String>();
                        Object textures = getMethod.invoke(props, "textures");
                        int size = (int) sizeListMethod.invoke(textures);
                        for (int i = 0; i < size; i++) {
                            Object val = getCompoundListMethod.invoke(textures, i);
                            this.textures.add((String) getStringMethod.invoke(val, "Value"));
                        }
                    }
                }
            }
        }
        catch(Exception e)
        {
            Logger.error(e);
        }
    }

    private void applyWithObcNms(Skull state)
    {
        try
        {
            Object textures = nbtTagListClass.newInstance();
            this.textures.ForEach(x -> {
                try
                {
                    Object val = nbtTagCompoundClass.newInstance();
                    setStringMethod.invoke(val, "Value", x);
                    addMethod.invoke(textures, val);
                }
                catch(Exception e)
                {
                    Logger.error(e);
                }
            });

            Object props = nbtTagCompoundClass.newInstance();
            setMethod.invoke(props, "textures", textures);

            Object owner = nbtTagCompoundClass.newInstance();
            setMethod.invoke(owner, "Properties", props);
            setStringMethod.invoke(owner, "Id", this.player);

            Object tag = getSnapshotNBTMethod.invoke(state);
            setMethod.invoke(tag, "Owner", owner);

            getTileEntityFromCraftSkull.setAccessible(true);

            Object te = getTileEntityFromCraftSkull.invoke(state);
            loadMethod.invoke(te, tag);
            updateMethod.invoke(te);

            getTileEntityFromCraftSkull.setAccessible(false);
        }
        catch(Exception e)
        {
            Logger.error(e);
        }
    }

    @Override
    public boolean matches(SchematicBlockState other) 
    {
        if(!(other instanceof SkullBlockState)) return false;
        
        SkullBlockState s = (SkullBlockState) other;

        if(!s.player.equals(player)) return false;

        return true;
    }
    
    public String getOwner()
    {
        return this.player;
    }
}