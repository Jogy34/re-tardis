package com.adx.jogy34.tardis.tardis;

import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;
import com.adx.jogy34.tardis.util.spacial.Point;

import org.bukkit.block.BlockFace;

public class HallwayFix implements IConfigSerializable
{
    @ConfigProperty("To")
    protected Point to;
    @ConfigProperty("Direction")
    protected BlockFace direction;

    @SuppressWarnings("unused")
    private HallwayFix() {}

    public HallwayFix(Point to, BlockFace direction)
    {
        this.to = to;
        this.direction = direction;
    }

    public BlockFace getDirection() {
        return direction;
    }

    public Point getTo() {
        return to;
    }
}