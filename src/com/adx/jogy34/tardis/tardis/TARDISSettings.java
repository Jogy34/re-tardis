package com.adx.jogy34.tardis.tardis;

import com.adx.jogy34.tardis.hierarchy.Hierarchy;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

public class TARDISSettings extends Hierarchy<TARDIS> implements IConfigSerializable
{
    @ConfigProperty("Appearance")
    protected TARDISAppearance appearance;
    @ConfigProperty("Interior")
    protected TARDISInterior interior;

    public TARDISSettings()
    {
        this(new TARDISAppearance(), new TARDISInterior());
    }

    public TARDISSettings(TARDISAppearance appearance)
    {
        this(appearance, new TARDISInterior());
    }

    public TARDISSettings(TARDISAppearance appearance, TARDISInterior interior)
    {
        this.appearance = appearance;
        this.interior = interior;
    }

    public TARDISAppearance getAppearance() {
        return appearance;
    }

    public TARDISInterior getInterior() {
        return interior;
    }

    public void update(TARDISAppearance newAppearance)
    {
        newAppearance.initializeHierarchy(this);
        this.appearance = newAppearance;
        this.getParent().rebuild();
        interior.updateExitDoorTypes();
    }

    @Override
    public void initializeHierarchy() {
        this.appearance.initializeHierarchy(this);
        this.interior.initializeHierarchy(this);
    }
}
