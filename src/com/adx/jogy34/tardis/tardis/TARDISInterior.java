package com.adx.jogy34.tardis.tardis;

import java.util.UUID;

import com.adx.jlinq.EList;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jlinq.tuples.Tuples.Tuple;
import com.adx.jogy34.tardis.enums.HallwayType;
import com.adx.jogy34.tardis.enums.TARDISWorlds;
import com.adx.jogy34.tardis.hierarchy.Hierarchy;
import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.schematics.AccessPoint;
import com.adx.jogy34.tardis.schematics.HallwayTheme;
import com.adx.jogy34.tardis.schematics.TardisSchematic;
import com.adx.jogy34.tardis.schematics.enums.AccessPointType;
import com.adx.jogy34.tardis.schematics.enums.SchematicType;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.util.helpers.BlockHelper;
import com.adx.jogy34.tardis.util.helpers.IntHelper;
import com.adx.jogy34.tardis.util.helpers.StringHelper;
import com.adx.jogy34.tardis.util.helpers.VersionHelper;
import com.adx.jogy34.tardis.util.spacial.Bound;
import com.adx.jogy34.tardis.util.spacial.BoundCube;
import com.adx.jogy34.tardis.util.spacial.Point;
import com.adx.jogy34.tardis.util.spacial.Position;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Axis;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Orientable;
import org.bukkit.block.data.Bisected.Half;
import org.bukkit.block.data.type.Door;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.entity.Player;

public class TARDISInterior extends Hierarchy<TARDISSettings> implements IConfigSerializable
{
    public static final int WORLD_BLOCK_SIZE = 2048;
 
    @ConfigProperty("Exits")
    protected EList<Point> exitLocations;
    @ConfigProperty("Spawn")
    protected Point spawnLocation;
    @ConfigProperty("XBounds")
    protected Bound<Integer> xBound;
    @ConfigProperty("Anchor")
    protected Position anchor;
    @ConfigProperty("WorldBlock")
    protected int worldBlock = -1;
    @ConfigProperty("ControlRoom")
    protected String controlRoomConstant;
    @ConfigProperty("FixLighting")
    protected boolean fixLighting = false;
    @ConfigProperty("AccessPoints")
    protected EList<AccessPoint> accessPoints = new EList<AccessPoint>();
    @ConfigProperty("HallwayType")
    protected HallwayType hallwayType = null;
    @ConfigProperty("ControlRoomConnections")
    protected EList<HallwayFix> controlRoomConnections = new EList<HallwayFix>();
    @ConfigProperty("GottaGoFast")
    protected boolean gottaGoFast = false;

    public TARDISInterior()
    {
    }

    public TARDISInterior(EList<Point> exitLocations, Point spawnLocation)
    {
        this.exitLocations = exitLocations;
        this.spawnLocation = spawnLocation;
    }

    public boolean isExitDoor(Block b) { return isExitDoor(b.getX(), b.getY(), b.getZ(), b.getWorld().getUID()); }
    public boolean isExitDoor(Position pos) { return isExitDoor(pos.getX(), pos.getY(), pos.getZ(), pos.getWorldUID()); }
    public boolean isExitDoor(Location loc) { return isExitDoor(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), loc.getWorld().getUID()); }
    public boolean isExitDoor(int x, int y, int z, UUID world)
    {
        if(!anchor.getWorldUID().equals(world)) return false;
        return exitLocations.Any(exitLocation -> {
            if(x != exitLocation.getX() || z != exitLocation.getZ()) return false;
        
            BlockData blockData = exitLocation.getBlock(anchor.getWorld()).getBlockData();
            if(!(blockData instanceof Door))
            {
                this.setSafetyPrecautions();
                blockData = exitLocation.getBlock(anchor.getWorld()).getBlockData();
            }

            Door door  = (Door) blockData;

            return y == exitLocation.getY() || y == exitLocation.getY() + (door.getHalf() == Half.BOTTOM ? 1 : -1);
        });
    }

    public Location getSpawnLocation()
    {
        if(this.spawnLocation == null) return null;
        return this.spawnLocation.toLocation(anchor.getWorld(), true).add(0, 0.5, 0).setDirection(BlockFace.EAST.getDirection());
    }

    public String getControlRoomConstant() {
        return controlRoomConstant;
    }
    
    public int getWorldBlockStartX()
    {
        return this.worldBlock * WORLD_BLOCK_SIZE;
    }

    public int getWorldBlock()
    {
        return this.worldBlock;
    }

    public World getWorld()
    {
        return this.anchor.getWorld();
    }

    public boolean isWithinBounds(int x)
    {
        return xBound.isWithinBound(x);
    }

    public boolean isFast()
    {
        return gottaGoFast;
    }

    public void setFast(boolean fast)
    {
        this.gottaGoFast = fast;
    }

    public AccessPoint getAccessPoint(Block b) { 
        if(!b.getWorld().getUID().equals(anchor.getWorldUID()) || !isWithinBounds(b.getX()))
        {
            return null;
        }
        return getAccessPoint(new Point(b)); 
    }

    public AccessPoint getAccessPoint(Point p)
    {
        return accessPoints.FirstOrDefault(ap -> ap.getOffset().equals(p));
    }

    public void init()
    {
        TARDIS t = this.getAncestor(TARDIS.class);
        if(!t.isTemporary())
        {
            World tardisWorld = ServiceProvider.worldService().getWorld(TARDISWorlds.TARDIS_WORLD);

            if(this.worldBlock < 0)
            {
                //Creating world markers in case the world block in the config gets whiped out
                this.worldBlock = ConfigProvider.getDataConfig().getInt(ConfigProvider.Constants.Data.NEXT_WORLD_BLOCK);
                while(tardisWorld.getBlockAt(this.worldBlock * WORLD_BLOCK_SIZE, 0, 0).getType() != Material.AIR)
                {
                    this.worldBlock++;
                }
    
                tardisWorld.getBlockAt(this.worldBlock * WORLD_BLOCK_SIZE, 0, 0).setType(Material.BEDROCK);
                ConfigProvider.getDataConfig().set(ConfigProvider.Constants.Data.NEXT_WORLD_BLOCK, this.worldBlock + 1);
                ConfigProvider.saveDataConfig();
            }

            if(xBound == null)
            {
                int start = getWorldBlockStartX();
                int end = WORLD_BLOCK_SIZE + start;
                xBound = new Bound<Integer>(start, end);
            }

            if(anchor == null)
            {
                int center = (xBound.getMin() + xBound.getMax()) / 2;
                anchor = new Position(center, tardisWorld.getMaxHeight() / 4, 0, tardisWorld);
            }

            if(exitLocations == null) exitLocations = new EList<Point>();
            
            if(this.controlRoomConstant == null)
            {
                changeControlRoom(ServiceProvider.schematicService().firstControlRoom());
            }

            if(!exitLocations.Any())
            {
                exitLocations.add(anchor.clone());
            }

            if(spawnLocation == null)
            {
                spawnLocation = new Position(exitLocations.First().getBlock(anchor.getWorld()).getRelative(3, 0, 0));
            }
        }

        setSafetyPrecautions();

        this.updateSignDisplays();
    }

    public void changeControlRoom(String constant)
    {
        TardisSchematic schematic = ServiceProvider.schematicService().getSchematic(constant);
        if(schematic != null && schematic.getType() == SchematicType.CONTROL_ROOM)
        {
            this.changeControlRoom(schematic);
        }
    }

    public void changeControlRoom(TardisSchematic schematic)
    {
        if(schematic.getType() != SchematicType.CONTROL_ROOM)
        {
            throw new IllegalArgumentException("Schematic to replace control room must be of type CONTROL_ROOM");
        }

        TARDIS t = this.getParent().getParent();
        World w = anchor.getWorld();

        kickAllPlayers(Text.TARDIS_UNDER_CONSTRUCTION);
        
        EList<AccessPoint> fixHallways = new EList<AccessPoint>();

        BoundCube controlRoomBoundary = null;
        if(this.controlRoomConstant != null)
        {
            TardisSchematic current = ServiceProvider.schematicService().getSchematic(controlRoomConstant);
            if(current != null)
            {
                Position startPos = anchor.subtract(current.getFirstAccessPointOffset(AccessPointType.EXIT_DOOR));
                controlRoomBoundary = new BoundCube(startPos, startPos.add(current.getSize()));
                BoundCube bounds = controlRoomBoundary;

                fixHallways.AddAll(accessPoints.Where(ap -> ap.getType() == AccessPointType.HALLWAY_ACCESS && 
                                                            ap.getOffset().getBlock(w).getType() == Material.AIR &&
                                                            bounds.isWithinBounds(ap.getOffset())));

                current.destroy(startPos);
            }
        }
        
        Position startPos = anchor.subtract(schematic.getFirstAccessPointOffset(AccessPointType.EXIT_DOOR));
        schematic.build(startPos, true);
        
        if(controlRoomBoundary == null) 
        {
            controlRoomBoundary = new BoundCube(startPos, startPos.add(schematic.getSize()));
        }

        //Lambdas need effectively final variables
        BoundCube bounds = controlRoomBoundary;

        if(spawnLocation == null || bounds.isWithinBounds(spawnLocation)) 
        {
            spawnLocation = schematic.getFirstAccessPointOffsetFrom(AccessPointType.SPAWN, startPos);
        }

        this.exitLocations = this.exitLocations.Where(el -> !bounds.isWithinBounds(el)).ToList();
        this.exitLocations.AddAll(schematic.getAccessPointOffsetsFrom(AccessPointType.EXIT_DOOR, startPos));

        this.controlRoomConstant = schematic.getConstant();

        accessPoints = accessPoints.Where(x -> !bounds.isWithinBounds(x.getOffset())).Union(schematic.getFilteredAccessPointsOffsetFrom(startPos)).ToList();

        updateExitDoorTypes();
        updateSignDisplays();
        fixLighting = true;

        IEnumerable<Point> hallwayEntrances = schematic.getAccessPointOffsets(AccessPointType.HALLWAY_ACCESS);

        controlRoomConnections.ForEach(x -> {
            BlockFace dir = x.getDirection();
            int modX = dir.getModX();
            int modZ = dir.getModZ();

            IEnumerable<Point> entrances = hallwayEntrances.OrderBy(p -> modX != 0 ? p.getX() : p.getZ());
            Point apOffset = startPos.add((modX + modZ) < 0 ? entrances.FirstOrDefault() : entrances.LastOrDefault());

            connectHallwayBetween(apOffset, x.getTo(), dir, true);
        });

        t.saveToConfig();
    }

    public void kickAllPlayers(String message)
    {
        TARDIS t = getParent().getParent();
        t.land(t.getPosition(), true);
        new EList<>(anchor.getWorld().getPlayers()).Where(p -> this.isWithinBounds(p.getLocation().getBlockX())).ForEach(p -> {
            t.leave(p);
            MessageService.say(p, message);
        });
    }

    public void updateExitDoorTypes()
    {
        Material doorType = this.getParent().getAppearance().getDoorMaterial();

        for(Point p : this.exitLocations)
        {
            Block b = p.getBlock(anchor.getWorld());
            BlockData bd = b.getBlockData();
            if(b.getType() == Material.AIR)
            {
                if(b.getRelative(0, 1, 0).getType() == Material.AIR)
                {
                    if(b.getRelative(0, -1, 0).isPassable()) 
                    {
                        b.getRelative(0, -1, 0).setType(Material.BEDROCK, false);
                    }

                    BlockHelper.createDoor(b, doorType, BlockFace.EAST);
                }
                else
                {
                    Logger.warning("{EXPECTED_DOOR_AT}", p.add(0, 1, 0), p.getBlockRelative(anchor.getWorld(), 0, 1, 0).getType());
                    continue;
                }
            }

            if(bd instanceof Door)
            {
                Door door = (Door) bd;
                
                Block bottom = door.getHalf() == Half.TOP ? b.getRelative(0, -1, 0) : b;
                BlockHelper.createDoor(bottom, doorType, door.getFacing());
            }
            else
            {
                Logger.warning("{EXPECTED_DOOR_AT}", p, b.getType());
            }
        }
    }

    public HallwayType getHallwayType() {
        return hallwayType;
    }

    public void setHallwayType(HallwayType hallwayType) {
        this.hallwayType = hallwayType;
    }

    @Override
    public void initializeHierarchy() 
    {
    }

    protected void setSafetyPrecautions()
    {
        exitLocations.ForEach(exitLocation -> {
            Block doorBlock = exitLocation.getBlock(anchor.getWorld());

            Block underDoor = doorBlock.getRelative(0, -1, 0);
            if(underDoor.isPassable()) underDoor.setType(Material.BEDROCK);

            BlockData blockData = doorBlock.getBlockData();
            if(!(blockData instanceof Door))
            {
                BlockHelper.createDoor(doorBlock, this.getParent().getAppearance().getDoorMaterial(), BlockFace.EAST);
            }
        });

        Block underSpawn = spawnLocation.getBlock(anchor.getWorld()).getRelative(0, -1, 0);
        if(underSpawn.isPassable()) underSpawn.setType(Material.BEDROCK);
    }

    public void enter(Player p)
    {
        p.teleport(spawnLocation.toLocation(anchor.getWorld()).setDirection(BlockFace.EAST.getDirection()));

        if(fixLighting && this.controlRoomConstant != null)
        {
            TardisSchematic current = ServiceProvider.schematicService().getSchematic(controlRoomConstant);
            if(current != null)
            {
                Position startPos = anchor.subtract(current.getFirstAccessPointOffset(AccessPointType.EXIT_DOOR));
                new BoundCube(startPos, startPos.add(current.getSize())).updateLightingInBounds(anchor.getWorld());
                fixLighting = false;
            }
        }
    }

    public void addAccessPoint(AccessPoint ap)
    {
        if(!this.isWithinBounds(ap.getOffset().getX()))
        {
            throw new IllegalArgumentException("When adding an access point it must be within the selected TARDIS");
        }

        this.accessPoints.add(ap);
        if(ap.getType().isSignDisplay())
        {
            updateSignDisplays();
        }
    }

    public boolean removeAccessPoint(Block b) { return removeAccessPoint(getAccessPoint(b)); }
    public boolean removeAccessPoint(Point p) { return removeAccessPoint(getAccessPoint(p)); }
    public boolean removeAccessPoint(AccessPoint ap)
    {
        if(this.accessPoints.remove(ap))
        {
            if(ap.getType().isControl() || ap.getType().isSignDisplay())
            {
                ap.getOffset().getBlock(anchor.getWorld()).setType(Material.AIR);
            }
            return true;
        }

        return false;
    }

    public void updateSignDisplays()
    {
        this.updateConsoles();
        this.updateClocks();
        this.updateParkingBrakes();
    }

    public void updateConsoles()
    {
        updateSignDisplays(AccessPointType.CONSOLE);
    }

    public void updateClocks()
    {
        updateSignDisplays(AccessPointType.CLOCK);
    }

    public void updateParkingBrakes()
    {
        updateSignDisplays(AccessPointType.PARKING_BRAKE);
    }

    protected void updateSignDisplays(AccessPointType type)
    {
        if(type.isSignDisplay())
        {
            TARDIS t = this.getParent().getParent();
            World w = anchor.getWorld();
            this.accessPoints.Where(ap -> ap.getType() == type).ForEach(ap -> {
                Block b = ap.getOffset().getBlock(w);
                if(!BlockHelper.setSignText(b, ap.getType().getSignDisplay(t)))
                {
                    //This should be fine because the 'Where' (above) is creating a new collection
                    this.accessPoints = this.accessPoints.Where(x -> !x.getOffset().equals(ap.getOffset())).ToList();
                    Logger.warning(Text.format("{EXPECTED_SIGN_AT}", ap.getOffset(), type.getName(), StringHelper.getName(b.getType())));
                }
            });
        }
    }

    //region Building

    public static final int AVG_SECTION_LENGTH = 11;
    public static final int SECTION_VARIANCE = 3;
    public static final int NUM_SECTIONS = 4;
    public static final int MIN_CHANGE_DIST = 3;
    public static final int CUT_AMT = 5;
    public static final int LIGHT_DISTANCE = 5;

    public boolean tryConnect(Point fromAccessPoint, BlockFace dir, Player who)
    {
        World w = anchor.getWorld();
        int modX = dir.getModX();
        int modZ = dir.getModZ();
        boolean xDir = modX != 0;

        AccessPoint connectTo = this.accessPoints.Where(ap -> ap.getType() == AccessPointType.HALLWAY_ACCESS).Where(ap -> {
            Point test = ap.getOffset();
            for(int i = -1; i <= 1; i++)
            {
                for(int j = -1; j <= 1; j++)
                {
                    if(test.getBlockRelative(w, i * modZ, j, i * modX).getType() == Material.AIR) return false;
                }
            }

            Point dist = test.subtract(fromAccessPoint).abs();

            int yOff = dist.getY(), sideOff = xDir ? dist.getZ() : dist.getX(), len = xDir ? dist.getX() : dist.getZ();

            if(modX < 0 && test.getX() >= fromAccessPoint.getX() ||
               modX > 0 && test.getX() <= fromAccessPoint.getX() ||
               modZ > 0 && test.getZ() <= fromAccessPoint.getZ() ||
               modZ < 0 && test.getZ() >= fromAccessPoint.getZ()) return false;

            if(len < ((AVG_SECTION_LENGTH + SECTION_VARIANCE) * (NUM_SECTIONS + 1)) && (yOff + sideOff - 1) < (len + MIN_CHANGE_DIST) / MIN_CHANGE_DIST)
            {
                Tuple<Point, Point> ordered = test.order(fromAccessPoint);
    
                BoundCube hallway = new BoundCube(ordered.item1.add(CUT_AMT *  Math.abs(modX), -5, CUT_AMT *  Math.abs(modZ)),
                                                  ordered.item2.add(CUT_AMT * -Math.abs(modX),  5, CUT_AMT * -Math.abs(modZ)));

                return hallway.isClear(w);
            }

            return false;
        }).OrderBy(ap -> {
            Point test = ap.getOffset();
            Point dist = test.subtract(fromAccessPoint).abs();
            return xDir ? dist.getX() : dist.getZ();
        }).LastOrDefault();

        if(connectTo == null) return false;

        if(who != null) MessageService.say(who, Text.CONNECTING_ROOMS);

        connectHallwayBetween(fromAccessPoint, connectTo.getOffset(), dir);

        if(isInsideControlRoom(fromAccessPoint))
        {
            controlRoomConnections.add(new HallwayFix(connectTo.getOffset(), dir));
        }
        else if(isInsideControlRoom(connectTo.getOffset()))
        {
            controlRoomConnections.add(new HallwayFix(fromAccessPoint, dir.getOppositeFace()));
        }

        this.accessPoints = this.accessPoints.Where(pt -> !pt.getOffset().equals(fromAccessPoint) && !pt.getOffset().equals(connectTo.getOffset())).ToList();
        return true;
    }

    protected void connectHallwayBetween(Point start, Point end, BlockFace dir) { this.connectHallwayBetween(start, end, dir, false); }
    protected void connectHallwayBetween(Point start, Point end, BlockFace dir, boolean forceEndFloor)
    {
        World w = anchor.getWorld();
        int modX = dir.getModX();
        int modZ = dir.getModZ();
        boolean xDir = modX != 0;

        HallwayTheme theme = ServiceProvider.schematicService().getRandomHallwayTheme();

        Point dist = end.subtract(start);
        int yOff = dist.getY(), sideOff = xDir ? dist.getZ() : dist.getX(), len = xDir ? dist.getX() : dist.getZ();

        start = start.add(modX, 0, modZ);
        int nextLight = 3;

        if(yOff == 0 || sideOff == 0)
        {
            buildHallway(start, end, dir, theme, nextLight);
        }
        else
        {
            double yPercent = (double) Math.abs(yOff) / (Math.abs(yOff) + Math.abs(sideOff));
            if(yPercent > 0.7) yPercent = 0.7;
            else if(yPercent < 0.3) yPercent = 0.3;

            int sideStepOffset = ((int) (len * (1 - yPercent))) / 2;
            int frontStepOffset = (int) (len * yPercent);
    
            Point p1 = start.add(xDir ? sideStepOffset : sideOff / 2, 0, xDir ? sideOff / 2 : sideStepOffset);
            Point p2 = p1.add(xDir ? frontStepOffset : 0, yOff, xDir ? 0 : frontStepOffset);

            nextLight = buildHallway(start, p1, dir, theme, nextLight);
            nextLight = buildHallway(p1, p2, dir, theme, nextLight);
            nextLight = buildHallway(p2, end, dir, theme, nextLight);
        }

        ensureHallwayEntrance(start.toPosition(w).subtract(modX, 0, modZ), dir, theme);
        ensureHallwayEntrance(end.toPosition(w), dir, theme, forceEndFloor);
    }

    public boolean canBuildRoom(Point fromAccessPoint, BlockFace dir, TardisSchematic schematic)
    {
        World w = anchor.getWorld();
        int modX = dir.getModX();
        int modZ = dir.getModZ();
        boolean xDir = modX != 0;

        int maxSections = (AVG_SECTION_LENGTH + SECTION_VARIANCE) * NUM_SECTIONS;
        int maxYOffset = ((maxSections + MIN_CHANGE_DIST) / MIN_CHANGE_DIST) + 2;
        
        Point apOffset = null;
        
        IEnumerable<Point> nextAccessPoints = schematic.getAccessPointOffsets(AccessPointType.HALLWAY_ACCESS).OrderBy(p -> xDir ? p.getX() : p.getZ());
        apOffset = (modX + modZ) < 0 ? nextAccessPoints.LastOrDefault() : nextAccessPoints.FirstOrDefault();

        Point maxEnd = fromAccessPoint.add(maxSections * modX, -maxYOffset, maxSections * modZ);
        Point lowestStart = maxEnd.subtract(apOffset).subtract(modX * AVG_SECTION_LENGTH, 0, modZ * AVG_SECTION_LENGTH);

        BoundCube corridorArea = new BoundCube(fromAccessPoint.add(modX * 5, maxYOffset, modZ * 5), maxEnd);
        BoundCube roomArea = new BoundCube(lowestStart, lowestStart.add(schematic.getSize()).add(modX * AVG_SECTION_LENGTH, 2 * maxYOffset, modZ * AVG_SECTION_LENGTH));

        return this.isWithinBounds(roomArea.getLower().getX()) && this.isWithinBounds(roomArea.getUpper().getX()) && corridorArea.isClear(w) && roomArea.isClear(w);
    }

    public boolean isInsideControlRoom(Point p)
    {
        if(controlRoomConstant != null)
        {
            TardisSchematic controlRoom = ServiceProvider.schematicService().getSchematic(controlRoomConstant);
            Point start = anchor.subtract(controlRoom.getAccessPointOffsets(AccessPointType.EXIT_DOOR).First());
            BoundCube controlRoomBounds = new BoundCube(start, start.add(controlRoom.getSize()));
            return controlRoomBounds.isWithinBounds(p);
        }

        return false;
    }

    public boolean buildRoom(Point fromAccessPoint, BlockFace dir, TardisSchematic schematic)
    {
        boolean insideControlRoom = isInsideControlRoom(fromAccessPoint);

        World w = anchor.getWorld();
        int modX = dir.getModX();
        int modZ = dir.getModZ();
        boolean xDir = modX != 0;

        Point apOffset = null;
        
        IEnumerable<Point> nextAccessPoints = schematic.getAccessPointOffsets(AccessPointType.HALLWAY_ACCESS).OrderBy(p -> xDir ? p.getX() : p.getZ());
        apOffset = (modX + modZ) < 0 ? nextAccessPoints.LastOrDefault() : nextAccessPoints.FirstOrDefault();

        HallwayTheme theme = ServiceProvider.schematicService().getRandomHallwayTheme();

        Point from = fromAccessPoint.add(modX, 0, modZ);
        Point last = from.clone();
        Point to = from.clone();
        int nextLight = 3;

        switch(hallwayType)
        {
            case DIRECT:
            {
                int length = AVG_SECTION_LENGTH * NUM_SECTIONS;
                to = from.add(modX * length, 0, modZ * length);
                buildHallway(from, to, dir, theme, nextLight);
                break;
            }
            case DYNAMIC:
            {
                for(int i = 0; i < NUM_SECTIONS + (insideControlRoom ? -1 : 0); i++)
                {
                    int length = ((i == 0 && insideControlRoom? 2 : 1) * AVG_SECTION_LENGTH) + (IntHelper.random(SECTION_VARIANCE * 2 + 1) - SECTION_VARIANCE);
                    int maxOffset = length / (i == 0 && insideControlRoom ? (2 * MIN_CHANGE_DIST) - 1 : MIN_CHANGE_DIST);
                    int offset = 0;
                    if(IntHelper.random(8) < 6)
                    {
                        offset = IntHelper.random(2 * maxOffset + 1) - maxOffset;
                    }
                    boolean sidewayOffset = IntHelper.random(2) == 0;
                    to = last.add(
                        (modX * length) + (sidewayOffset && !xDir ? offset : 0), 
                        sidewayOffset ? 0 : offset, 
                        (modZ * length) + (sidewayOffset && xDir ? offset : 0));
                    
                    nextLight = buildHallway(last, to, dir, theme, nextLight);

                    last = to;
                }
                break;
            }
        }

        if(insideControlRoom) controlRoomConnections.add(new HallwayFix(to, dir));

        Position ap = fromAccessPoint.toPosition(w);
        Position schematicBuild = to.subtract(apOffset).toPosition(w);

        schematic.build(schematicBuild);

        ensureHallwayEntrance(ap, dir, theme);
        ensureHallwayEntrance(to.toPosition(w), dir, theme);

        Point finalPt = to;

        this.accessPoints.AddAll(schematic.getFilteredAccessPointsOffsetFrom(schematicBuild.toPoint()));
        this.accessPoints = this.accessPoints.Where(pt -> !pt.getOffset().equals(finalPt) && !pt.getOffset().equals(fromAccessPoint)).ToList();

        return true;
    }

    protected void ensureHallwayEntrance(Position ap, BlockFace dir, HallwayTheme theme) { this.ensureHallwayEntrance(ap, dir, theme, false); }
    protected void ensureHallwayEntrance(Position ap, BlockFace dir, HallwayTheme theme, boolean forceFloor)
    {
        int modX = dir.getModX();
        int modZ = dir.getModZ();

        Material[] floor = theme.getFloor(3);
        Material[] wall = theme.getFloor(9);
        int floorIndex = 0, wallndex = 0;

        for(int i = -1; i <= 1; i++)
        {
            for(int j = -1; j <= 1; j++)
            {
                ap.getBlockRelative(i * modZ, j, i * modX).setType(Material.AIR);
            }
            
            BlockHelper.setIfAir(ap.getBlockRelative( i * modZ, -2,  i * modX), floor[floorIndex++], forceFloor);
            BlockHelper.setIfAir(ap.getBlockRelative( i * modZ,  2,  i * modX), wall[wallndex++]);
            BlockHelper.setIfAir(ap.getBlockRelative( 2 * modZ,  i,  2 * modX), wall[wallndex++]);
            BlockHelper.setIfAir(ap.getBlockRelative(-2 * modZ,  i, -2 * modX), wall[wallndex++]);
        }
    }

    protected int buildHallway(Point from, Point to, BlockFace dir, HallwayTheme theme, int nextLight)
    {
        int modX = dir.getModX();
        int modZ = dir.getModZ();

        Point dist = to.subtract(from);
        int sections = Math.abs(dist.getX() * modX + dist.getZ() * modZ);

        boolean xDir = modX != 0;
        Axis orientation = xDir ? Axis.X : Axis.Z;

        Material[] floor = theme.getFloor(sections * 5);
        Material[] steps = theme.getStep(Math.abs(dist.getY()) * 3);
        Material[] angles = theme.getAngle(sections * 6);
        Material[] walls = theme.getWall(sections * 14);
        Material light = theme.getLight();
        int floorIndex = 0, stepIndex = 0, angleIndex = 0, wallIndex = 0;

        double x = from.getX(), y = from.getY(), z = from.getZ();
        double dx = xDir ? dir.getModX() : ((double) dist.getX()) / sections;
        double dy = ((double) dist.getY()) / sections;
        double dz = xDir ? ((double) dist.getZ()) / sections : dir.getModZ();
        World w = anchor.getWorld();
        boolean includeStep = false;

        for(int i = 0; i < sections; i++)
        {
            int bx = (int) Math.round(x);
            int by = (int) Math.round(y);
            int bz = (int) Math.round(z);
            x += dx;
            y += dy;
            z += dz;
            nextLight--;

            Position center = new Position(bx, by, bz, w);

            boolean nextIsStep = false;

            if(i < sections - 1)
            {
                int nextY = (int) Math.round(y);
                if(nextY != by)
                {
                    boolean up = nextY > by;
                    if(up) includeStep = true;
                    else nextIsStep = true;
                }
            }

            //Floor
            for(int fl = -2; fl <= 2; fl++)
            {
                //Clear out the inside just in case 
                for(int cl = -2; cl <= 2; cl++)
                {
                    center.getBlockRelative(xDir ? 0 : fl, cl, xDir ? fl : 0).setType(Material.AIR, false);
                }
                center.getBlockRelative(xDir ? 0 : fl, -2, xDir ? fl : 0).setType(floor[floorIndex++]);
                if(includeStep && Math.abs(fl) < 2) center.getBlockRelative(xDir ? 0 : fl, -1, xDir ? fl : 0).setType(steps[stepIndex++]);
                
            }

            //Walls
            for(int wl = -1; wl <= 1; wl++)
            {
                setTypeAndAxis(center.getBlockRelative(modZ *  3, wl, modX *  3), walls[wallIndex++], orientation);
                setTypeAndAxis(center.getBlockRelative(modZ * -3, wl, modX * -3), walls[wallIndex++], orientation);
                setTypeAndAxis(center.getBlockRelative(modZ * wl, 3, modX * wl) , walls[wallIndex++], orientation);

                if(wl == 0)
                {
                    setTypeAndAxis(center.getBlockRelative(modZ *  4, wl, modX *  4), walls[wallIndex++], orientation);
                    setTypeAndAxis(center.getBlockRelative(modZ * -4, wl, modX * -4), walls[wallIndex++], orientation);
                    setTypeAndAxis(center.getBlockRelative(modZ * wl, 4, modX * wl) , walls[wallIndex++], orientation);
                }
            }
            setTypeAndAxis(center.getBlockRelative(modZ * -2, 2, modX * -2), walls[wallIndex++], orientation);
            setTypeAndAxis(center.getBlockRelative(modZ *  2, 2, modX *  2), walls[wallIndex++], orientation);
            
            //Lights
            if(nextLight <= 0)
            {
                setLight(center.getBlockRelative(modZ *  3, 0, modX *  3), light, false);
                setLight(center.getBlockRelative(modZ * -3, 0, modX * -3), light, false);
                setLight(center.getBlockRelative(0, 3, 0), light, true);
            }

            //Angles
            BlockFace side1 = xDir ? BlockFace.SOUTH : BlockFace.EAST;
            if(modX + modZ < 0) side1 = side1.getOppositeFace(); //negatives are switched
            BlockFace side2 = side1.getOppositeFace();

            setStairs(center.getBlockRelative(modZ *   2, -1, modX *   2), angles[angleIndex++], side1, Half.BOTTOM);
            setStairs(center.getBlockRelative(modZ *   2,  1, modX *   2), angles[angleIndex++], side1, Half.TOP);
            setStairs(center.getBlockRelative(modZ *   1,  2, modX *   1), angles[angleIndex++], side1, Half.TOP);
            
            setStairs(center.getBlockRelative(modZ *  -2, -1, modX *  -2), angles[angleIndex++], side2, Half.BOTTOM);
            setStairs(center.getBlockRelative(modZ *  -2,  1, modX *  -2), angles[angleIndex++], side2, Half.TOP);
            setStairs(center.getBlockRelative(modZ *  -1,  2, modX *  -1), angles[angleIndex++], side2, Half.TOP);
            
            includeStep = nextIsStep;
            if(nextLight <= 0) nextLight = LIGHT_DISTANCE;
        }

        return nextLight;
    }

    protected void setTypeAndAxis(Block b, Material type, Axis orientation)
    {
        b.setType(type, false);
        BlockData data = b.getBlockData();
        if(data instanceof Orientable)
        {
            Orientable dirData = (Orientable) data;
            dirData.setAxis(orientation);
            b.setBlockData(dirData);
        }
    }

    protected void setStairs(Block b, Material type, BlockFace facing, Half half)
    {
        b.setType(type, false);
        BlockData data = b.getBlockData();
        if(data instanceof Stairs)
        {
            Stairs stairs = (Stairs) data;
            stairs.setFacing(facing);
            stairs.setHalf(half);
            b.setBlockData(stairs);
        }
    }

    protected void setLight(Block b, Material type, boolean onCeiling)
    {
        b.setType(type, false);

        if(onCeiling)
        {
            Class<?> craftLanternClass = VersionHelper.getOBCClass("block.impl.CraftLantern");
            BlockData data = b.getBlockData();
            if(craftLanternClass.isInstance(data))
            {
                try
                {
                    craftLanternClass.getMethod("setHanging", boolean.class).invoke(data, true);
                    b.setBlockData(data);
                }
                catch(Exception e)
                {
                    Logger.error(e);
                }
            }
        }
    }

    //endregion Building
}