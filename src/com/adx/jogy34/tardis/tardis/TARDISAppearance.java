package com.adx.jogy34.tardis.tardis;

import java.text.MessageFormat;

import com.adx.jogy34.tardis.hierarchy.Hierarchy;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Material;

public class TARDISAppearance extends Hierarchy<TARDISSettings> implements IConfigSerializable
{
    @ConfigProperty("Primary")
    private Material primary = Material.BLUE_CONCRETE;
    @ConfigProperty("Secondary")
    private Material secondary = Material.SMOOTH_QUARTZ;
    @ConfigProperty("Door")
    private Material door = Material.DARK_OAK_DOOR;
    @ConfigProperty("Lantern")
    private Material lantern = Material.BEACON;
    @ConfigProperty("PoliceSignType")
    private Material policeSignType = Material.OAK_WALL_SIGN;
    @ConfigProperty("PoliceSigns")
    private boolean policeSigns = true;
    @ConfigProperty("OwnerSignType")
    private Material ownerSignType = Material.OAK_WALL_SIGN;
    @ConfigProperty("OwnerSign")
    private boolean ownerSign = true;

    public TARDISAppearance() {}

    public TARDISAppearance(TARDISAppearance copy)
    {
        this.primary = copy.primary;
        this.secondary = copy.secondary;
        this.door = copy.door;
        this.lantern = copy.lantern;
        this.policeSignType = copy.policeSignType;
        this.policeSigns = copy.policeSigns;
        this.ownerSignType = copy.ownerSignType;
        this.ownerSign = copy.ownerSign;
    }
    
    public void setPrimaryMaterial(Material mat) { primary = mat; }
    public Material getPrimaryMaterial() { return primary; }

    public void setSecondaryMaterial(Material mat) { secondary = mat; }
    public Material getSecondaryMaterial() { return secondary; }

    public void setDoorMaterial(Material mat) { door = mat; }
    public Material getDoorMaterial() { return door; }

    public void setLanternMaterial(Material mat) { lantern = mat; }
    public Material getLanternMaterial() { return lantern; }

    public void setPoliceSignMaterial(Material mat) { policeSignType = mat; }
    public Material getPoliceSignMaterial() { return policeSignType; }
    
    public void setUsePoliceSigns(boolean use) { policeSigns = use; }
    public boolean usesPoliceSigns() { return policeSigns; }

    public void setOwnerSignMaterial(Material mat) { ownerSignType = mat; }
    public Material getOwnerSignMaterial() { return ownerSignType; }
    
    public void setUseOwnerSign(boolean use) { ownerSign = use; }
    public boolean usesOwnerSign() { return ownerSign; }

    @Override
    public TARDISAppearance clone() {
        return new TARDISAppearance(this);
    }

    @Override
    public void initializeHierarchy() { }

    @Override
    public String toString() {
        return MessageFormat.format("{0}, {1}, {2}, {3}, {4}: {5}, {6}: {7}", primary, secondary, door, lantern, policeSigns, policeSignType, ownerSign, ownerSignType);
    }
}
