package com.adx.jogy34.tardis.tardis;

import java.io.File;
import java.util.Date;
import java.util.UUID;

import com.adx.jlinq.EList;
import com.adx.jlinq.tuples.Tuples.Tuple;
import com.adx.jogy34.tardis.actions.BuildRoomAction;
import com.adx.jogy34.tardis.data.Constants;
import com.adx.jogy34.tardis.data.MiscMaterialData;
import com.adx.jogy34.tardis.enums.PhaseState;
import com.adx.jogy34.tardis.enums.TARDISVersion;
import com.adx.jogy34.tardis.enums.TARDISWorlds;
import com.adx.jogy34.tardis.enums.TardisCantLandReason;
import com.adx.jogy34.tardis.hierarchy.HierarchyBase;
import com.adx.jogy34.tardis.menu.upgrade.BuildRoomDialogue;
import com.adx.jogy34.tardis.menu.upgrade.ChangeHallwayTypeMenu;
import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.providers.QuoteProvider;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.schematics.AccessPoint;
import com.adx.jogy34.tardis.schematics.blockstate.SkullBlockState;
import com.adx.jogy34.tardis.schematics.enums.AccessPointType;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.services.TaskService;
import com.adx.jogy34.tardis.tardis.TardisPermission.TardisPermissionLevel;
import com.adx.jogy34.tardis.tasks.concrete.TardisPhaseTask;
import com.adx.jogy34.tardis.tasks.concrete.ToastTask;
import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.helpers.BlockHelper;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;
import com.adx.jogy34.tardis.util.spacial.OrientedPosition;
import com.adx.jogy34.tardis.util.spacial.Point;
import com.adx.jogy34.tardis.util.spacial.Position;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Skull;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.Lightable;
import org.bukkit.block.data.type.Bed;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class TARDIS extends HierarchyBase implements IConfigSerializable {
    public static final String CONFIG_PATH = "Tardises" + File.separator;

    //Height above the position
    public static final int TOTAL_HEIGHT = 5;
    public static final int BASE_HEIGHT = TOTAL_HEIGHT - 1;

    @ConfigProperty("Version")
    protected TARDISVersion version = TARDISVersion.CURRENT;
    @ConfigProperty("Settings")
    protected TARDISSettings settings;
    @ConfigProperty("Player")
    protected UUID player;
    @ConfigProperty("Position")
    protected OrientedPosition position;
    @ConfigProperty("Home")
    protected Position home;
    @ConfigProperty("AllowedPlayers")
    protected EList<TardisPermission> allowedPlayers = new EList<TardisPermission>();

    @ConfigProperty("PlayerName")
    protected String playerName;
    protected boolean foundPlayersName = false;

    protected PhaseState phase = PhaseState.SOLID;
    protected boolean initialized = false;
    protected EList<Position> signs = new EList<Position>();
    protected Tuple<Point, Long> removeDisplaySign = null;

    @SuppressWarnings("unused")
    private TARDIS() { }

    public TARDIS(UUID player, TARDISAppearance appearance, int x, int y, int z, World world, BlockFace facing)
    {
        this.settings = new TARDISSettings(appearance);
        this.player = player;
        this.position = new OrientedPosition(x, y, z, world.getUID(), facing);
        this.home = new Position(x, y, z, world);

        init();
    }

    public TARDIS(String playerName, TARDISAppearance appearance, int x, int y, int z, World world, BlockFace facing)
    {
        this.settings = new TARDISSettings(appearance);
        this.playerName = playerName;
        this.position = new OrientedPosition(x, y, z, world.getUID(), facing);
        this.home = new Position(x, y, z, world);

        init();
    }

    public void init()
    {
        if(initialized) return;
        initialized = true;
        
        this.initializeHierarchy();

        if(player != null) 
        {
            TARDISProvider.setTardisForPlayer(player, this);
            this.settings.getInterior().init();

            Player p = Bukkit.getPlayer(player);
            if(p != null) 
            {
                this.playerName = p.getName();
                foundPlayersName = true;
            }
            this.saveToConfig();
        }

        this.land(this.position, true);
    }

    //region physical

    public void build()
    {
        this.build(true);
    }

    public void build(boolean animation)
    {
        loadChunks();

        if(animation && phase != PhaseState.SOLID)
        {
            if(phase == PhaseState.IN_PHASE)
            {
                phase = PhaseState.PHASING;
                this.settings.getInterior().updateParkingBrakes();
                TaskService.scheduleTask(new TardisPhaseTask(this, () -> this.build(false)));
            }

            return;
        }

        Material p = this.settings.getAppearance().getPrimaryMaterial(),
                 s = this.settings.getAppearance().getSecondaryMaterial(),
                 d = this.settings.getAppearance().getDoorMaterial(),
                 l = this.settings.getAppearance().getLanternMaterial();

        BlockData pd = Bukkit.createBlockData(p),
                  sd = Bukkit.createBlockData(s),
                  ld = Bukkit.createBlockData(l);

        if(pd instanceof Directional) ((Directional) pd).setFacing(position.getFacing());
        if(sd instanceof Directional) ((Directional) sd).setFacing(position.getFacing());
        if(ld instanceof Directional) ((Directional) ld).setFacing(position.getFacing());
        if(ld instanceof Lightable) ((Lightable) ld).setLit(true);

        World world = position.getWorld();

        for(int x = -1; x <= 1; x++)
        {
            for(int y = 0; y <= TOTAL_HEIGHT; y++)
            {
                for(int z = -1; z <= 1; z++)
                {
                    Block block = position.getBlockRelative(x, y, z);
                    if(!block.getChunk().isLoaded()) block.getChunk().load();
                    if(x == 0 && z == 0 && y > 0 && y < (l == Material.REDSTONE_LAMP ? BASE_HEIGHT : TOTAL_HEIGHT)) continue;
                    else if(y == TOTAL_HEIGHT && x == 0 && z == 0) {
                        block.setType(l, false);
                        block.setBlockData(ld, false);
                    }
                    else if(y == BASE_HEIGHT && x == 0 && z == 0 && l == Material.REDSTONE_LAMP) block.setType(Material.REDSTONE_BLOCK, false);
                    else if(y == BASE_HEIGHT) {
                        block.setType(s, false);
                        block.setBlockData(sd, false);
                    }
                    else if(y < BASE_HEIGHT) {
                        block.setType(p, false);
                        block.setBlockData(pd, false);
                    }
                }
            }
        }

        Block doorBottom = world.getBlockAt(position.getX() + position.getFacing().getModX(), position.getY() + 1, position.getZ() + position.getFacing().getModZ());
        
        //The way the doors "face" is backwards from what I would have expected but modX/Z work as I expected
        BlockHelper.createDoor(doorBottom, d, position.getFacing().getOppositeFace());
        
        if(this.settings.getAppearance().usesPoliceSigns())
        {
            Material policeSignMat = this.settings.getAppearance().getPoliceSignMaterial();
            for(int x = -2; x <= 2; x++)
            {
                for(int z = -2; z <= 2; z++)
                {
                    int absZ = Math.abs(z);
                    int absX = Math.abs(x);
                    if((absX == 2 || absZ == 2) && absX != absZ)
                    {
                        BlockFace dir = absZ == 2 ? BlockFace.SOUTH : BlockFace.EAST;
                        if(z == -2 || x == -2) dir = dir.getOppositeFace();
                        int signNum = absZ == 2 ? (x * dir.getModZ() + 1) : (-z * dir.getModX() + 1);
    
                        String[] text = getPoliceSignText(signNum);
                        
                        Position signPos = position.add(x, BASE_HEIGHT, z);
                        Block b = signPos.getBlock();
                        if(!b.getChunk().isLoaded()) b.getChunk().load();
                        if(b.getType() == Material.AIR)
                        {
                            BlockHelper.createWallSign(signPos.getBlock(), policeSignMat, dir, text);
                            signs.add(signPos);
                        }
                    }
                }
            }
        }

        if(this.settings.getAppearance().usesOwnerSign())
        {
            BlockFace facing = position.getFacing();
            Position ownerSign = position.add(facing.getModX() * 2 - facing.getModZ(), 2, facing.getModZ() * 2 + facing.getModX());
            if(ownerSign.getBlock().getType() == Material.AIR)
            {
                signs.add(ownerSign);
                BlockHelper.createWallSign(ownerSign.getBlock(), this.settings.getAppearance().getOwnerSignMaterial(), facing, "", Text.format("{OWNERS}", getPlayerName()), "TARDIS");
            }
        }

        phase = PhaseState.SOLID;
        if(!this.isTemporary()) this.settings.getInterior().updateParkingBrakes();
    }

    public void rebuild()
    {
        if(this.phase == PhaseState.SOLID)
        {
            this.remove(false, false);
            this.build(false);
        }
    }
    
    public void remove()
    {
        this.remove(true, true);
    }

    public void remove(boolean setPhase)
    {
        this.remove(setPhase, false);
    }

    public void remove(boolean setPhase, boolean animation)
    {
        loadChunks();

        if(this.player == null) TARDISProvider.removeTemporaryTardis(this);
        if(setPhase && animation)
        {
            if(this.phase != PhaseState.SOLID)
            {
                return;
            }
            
            if(animation)
            {
                this.phase = PhaseState.PHASING;
                this.settings.getInterior().updateParkingBrakes();
                TaskService.scheduleTask(new TardisPhaseTask(this, () -> this.remove(true, false)));
                return;
            }
        }


        World world = position.getWorld();

        Block doorBottom = world.getBlockAt(position.getX() + position.getFacing().getModX(), position.getY() + 1, position.getZ() + position.getFacing().getModZ());
        Block doorTop = doorBottom.getRelative(0, 1, 0);

        doorBottom.setType(Material.AIR, false);
        doorTop.setType(Material.AIR, false);

        signs.ForEach(x -> x.getBlock().setType(Material.AIR, false));
        signs.clear();

        for(int x = -1; x <= 1; x++)
        {
            for(int y = 0; y <= BASE_HEIGHT; y++)
            {
                for(int z = -1; z <= 1; z++)
                {
                    Block block = position.getBlockRelative(x, y, z);
                    if(!block.getChunk().isLoaded()) block.getChunk().load();
                    block.setType(Material.AIR);
                }
            }
        }

        Block block = position.getBlockRelative(0, TOTAL_HEIGHT, 0);
        block.setType(Material.AIR);

        if(setPhase) this.phase = PhaseState.IN_PHASE;
        if(!this.isTemporary()) this.settings.getInterior().updateParkingBrakes();
    }

    public void loadChunks()
    {
        for(int i = -1; i <= 1; i++)
        {
            for(int j = -1; j <= 1; j++)
            {
                Chunk c = position.getBlockRelative(2 * i, 0, 2 * j).getChunk();
                if(!c.isLoaded()) c.load(true);
            }
        }
    }

    public boolean isInsideTardis(Position pos) { return isInsideTardis(pos.getX(), pos.getY(), pos.getZ(), pos.getWorld()); };
    public boolean isInsideTardis(Location loc) { return isInsideTardis(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), loc.getWorld()); };
    public boolean isInsideTardis(Block block) { return isInsideTardis(block.getX(), block.getY(), block.getZ(), block.getWorld()); };

    public boolean isInsideTardis(int x, int y, int z, World world)
    {
        Position testPos = new Position(x, y, z, world);
        if(this.phase == PhaseState.IN_PHASE || !position.getWorldUID().equals(world.getUID())) return false;
        
        if(x >= position.getX() - 1 && x <= position.getX() + 1 && 
           y >= position.getY() && y <= position.getY() + BASE_HEIGHT && 
           z >= position.getZ() - 1 && z <= position.getZ() + 1) {
            return true;
        }

        if(x == position.getX() && z == position.getZ() && y == position.getY() + TOTAL_HEIGHT) return true;
        
        return signs.Any(s -> s.equals(testPos));
    }

    public void faceDirection(BlockFace face)
    {
        boolean shouldBuild = this.phase == PhaseState.SOLID;

        if(shouldBuild) this.remove(false, false);

        this.position.setFacing(face);
        
        if(shouldBuild) this.build(false);
    }

    public boolean isTemporary()
    {
        return this.player == null;
    }

    private String[] getPoliceSignText(int signNum)
    {
        if(signNum == 0) return new String[] {"", Text.POLICE};
        if(signNum == 1) return new String[] {"", Text.PUBLIC, Text.CALL};
        if(signNum == 2) return new String[] {"", Text.BOX};
        return new String[0];
    }

    //endregion physical


    //region Config

    public void saveToConfig()
    {
        if(this.isTemporary()) return;
        
        Player p = Bukkit.getPlayer(player); //This only works if the player in online
        
        String configName = CONFIG_PATH + player.toString();
        FileConfiguration config = ConfigProvider.getConfig(configName);

        if(p != null) config.options().header(p.getName() + "'s TARDIS");

        ConfigProvider.saveToConfig(config, this);
        ConfigProvider.saveConfig(configName);
    }

    public boolean delete()
    {
        if(this.isTemporary()) return false;

        String configName = CONFIG_PATH + player.toString();
        if(ConfigProvider.deleteConfig(configName))
        {
            this.settings.getInterior().kickAllPlayers(Text.TARDIS_IS_BEING_DELETED);
            TARDISProvider.removeTardis(player);
            this.remove(false, false);
            return true;
        }

        return false;
    }

    //endregion Config

    //region Moving

    public TardisCantLandReason moveTo(int x, int y, int z, World w){ return this.moveTo(new Position(x, y, z, w)); }
    public TardisCantLandReason moveTo(int x, int y, int z, UUID w){ return this.moveTo(new Position(x, y, z, w)); }
    public TardisCantLandReason moveTo(Block b) { return this.moveTo(new Position(b)); }
    public TardisCantLandReason moveTo(Location l) { return this.moveTo(new Position(l)); }
    public TardisCantLandReason moveTo(Position p) { return this.moveTo(p, false); }
    public TardisCantLandReason moveTo(Position p, boolean ignoreAnimation)
    {
        if(this.phase == PhaseState.PHASING)
        {
            return TardisCantLandReason.PHASING;
        }
        TardisCantLandReason reason = this.canMoveTo(p);
        if(reason == TardisCantLandReason.NONE)
        {
            if(this.phase != PhaseState.IN_PHASE) this.remove(true, false);
            this.position.set(p);
            this.build(!ignoreAnimation);
            if(!this.isTemporary()) this.settings.getInterior().updateConsoles();
        }
        return reason;
    }

    public TardisCantLandReason canMoveTo(int x, int y, int z, World w) { return this.canMoveTo(new Position(x, y, z, w)); }
    public TardisCantLandReason canMoveTo(int x, int y, int z, UUID w) { return this.canMoveTo(new Position(x, y, z, w)); }
    public TardisCantLandReason canMoveTo(Block b) { return this.canMoveTo(new Position(b)); }
    public TardisCantLandReason canMoveTo(Location l) { return this.canMoveTo(new Position(l)); }

    public TardisCantLandReason canMoveTo(Position p)
    {
        if(p.getY() < 0) return TardisCantLandReason.TO_LOW;
        if(p.getY() > ServiceProvider.worldService().getWorldHeight(p.getWorld()) - TOTAL_HEIGHT) return TardisCantLandReason.TO_HIGH;

        EList<Chunk> inChunks = new EList<Chunk>();

        Block lantern = p.getBlockRelative(0, TOTAL_HEIGHT, 0);
        if(!canOverrite(lantern)) return TardisCantLandReason.BLOCKED_5;
        
        for(int x = -1; x <= 1; x++)
        {
            for(int y = BASE_HEIGHT; y >= 0 ; y--)
            {
                for(int z = -1; z <= 1; z++)
                {
                    Block b = p.getBlockRelative(x, y, z);
                    if(!b.getChunk().isLoaded()) b.getChunk().load(true);
                    if(!canOverrite(b)) return TardisCantLandReason.blocked(y);

                    if(y == 0)
                    {
                        Chunk c = b.getChunk();
                        if(!inChunks.contains(c)) inChunks.add(c);
                    }
                }
            }
        }

        return ServiceProvider.worldService().areValidLandingChunks(inChunks) ? TardisCantLandReason.NONE : TardisCantLandReason.NO_FLY_CHUNK;
    }
    
    public void land() { this.land(this.position); }
    public void land(int x, int y, int z, World w) { this.land(new Position(x, y, z, w)); }
    public void land(int x, int y, int z, UUID w) { this.land(new Position(x, y, z, w)); }
    public void land(Block b) { this.land(new Position(b)); }
    public void land(Location l) { this.land(new Position(l)); }
    public void land(Position p) { this.land(p, false); }

    public void land(Position p, boolean ignoreAnimation)
    {
        //Can't fly into the TARDIS world
        if(ServiceProvider.worldService().getTypeForWorld(p.getWorld()) == TARDISWorlds.TARDIS_WORLD)
        {
            p = p.clone();
            p.setWorld(ServiceProvider.worldService().getMainWorld());
        }

        int maxHeight = ServiceProvider.worldService().getWorldHeight(p.getWorld()) - TOTAL_HEIGHT;

        if(p.getY() <= 0) p.setY(1);
        
        boolean floating = true;

        while(floating && p.getY() > 1) 
        {
            for(int x = -1; x <= 1 && floating; x++)
            {
                for(int z = -1; z <= 1 && floating; z++)
                {
                    if(!canOverrite(p.getBlockRelative(x, -1, z)))
                    {
                        floating = false;
                    }
                }
            }

            if(floating) p = p.add(0, -1, 0);
        }

        TardisCantLandReason cantLand = moveTo(p, ignoreAnimation);
        while(!cantLand.isValid())
        {
            p = cantLand.adjust(p, this, maxHeight);
            cantLand = moveTo(p, ignoreAnimation);
        }
    }

    public boolean canOverrite(Block b)
    {
        return MiscMaterialData.TRANSPARENT_MATERIALS.contains(b.getType()) || this.isInsideTardis(b);
    }

    public TardisCantLandReason goHome()
    {
        if(home == null) return null;
        return moveTo(home);
    }

    public void setHome()
    {
        this.home = new Position(position);
    }

    public Position getHome()
    {
        return home == null ? null : home.clone(); 
    }

    //endregion Moving

    //region Inside

    public boolean isExitDoor(Block b)
    {
        return this.settings.getInterior().isExitDoor(b);
    }

    public void enter(Player p)
    {
        this.settings.getInterior().enter(p);
    }

    public boolean leave(Player p)
    {
        if(this.phase == PhaseState.IN_PHASE) this.land();
        if(this.phase == PhaseState.SOLID)
        {
            p.teleport(this.position.toLocation().add(0, 1.5, 0).setDirection(this.position.getFacing().getDirection()));
        }
        return true;
    }

    public boolean isWithinInteriorBounds(int x) 
    {
		return this.settings.getInterior().isWithinBounds(x);
    }
    
    /**
     * Attempts to interact with a given block
     * @param p The player interacting within the TARDIS
     * @param b The block that was interacted with
     * @param clicked The face of the block that was clicked on 
     * @return If the event should be cancelled
     */
    public boolean interactWith(Player p, Block b, BlockFace clicked)
    {
        TardisPermissionLevel permission = this.getPermissionLevel(p);
        if(!permission.canEnter()) return true; //Shouldn't really have gotten here in the first place

        if(MiscMaterialData.BED_MATERIALS.contains(b.getType()))
        {
            //Prevent marking half beds as bed spawn loactions (see control room 6)
            Bed bed = ((Bed) b.getBlockData());
            Block other = b.getRelative(bed.getFacing());
            if(other.getType() != b.getType() || ((Bed) other.getBlockData()).getPart() == bed.getPart()) return true;

            if(!permission.canFly()) return true;
            p.setBedSpawnLocation(b.getLocation());
            MessageService.say(p, Text.BED_SPAWN_SET);
            return true;
        }
        else if(b.getType() == Material.BOOKSHELF && QuoteProvider.canUseQuotes())
        {
            MessageService.say(p, QuoteProvider.getQuote());
            return false;
        }
        else if(b.getType() == Material.PLAYER_HEAD || b.getType() == Material.PLAYER_WALL_HEAD)
        {
            SkullBlockState state = new SkullBlockState(((Skull) b.getState()));
            if(state.getOwner() != null)
            {
                switch(state.getOwner())
                {
                    case Constants.SkullOwners.PileOfBooks:
                    {
                        if(QuoteProvider.canUseQuotes()) MessageService.say(p, QuoteProvider.getQuote());
                        break;
                    }
                    case Constants.SkullOwners.CoffeeMachine:
                    {
                        p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20 * 20, 5));
                        break;
                    }
                    case Constants.SkullOwners.Toaster:
                    {
                        Tuple<Boolean, ToastTask> task = ToastTask.create(b.getLocation(), p);
                        if(task.item1) TaskService.scheduleTask(task.item2);
                        break;
                    }
                    case Constants.SkullOwners.WaterJug:
                    {
                        ItemStack inHand = p.getInventory().getItemInMainHand();
                        if(inHand.getType() != Material.BUCKET)
                        {
                            inHand = p.getInventory().getItemInOffHand();
                        }
                        if(inHand.getType() == Material.BUCKET)
                        {
                            inHand.setAmount(inHand.getAmount() - 1);
                            b.getWorld().dropItem(b.getRelative(0, 1, 0).getLocation(), new ItemStack(Material.WATER_BUCKET));
                        }
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
            }
        }

        AccessPoint interacting = this.settings.getInterior().getAccessPoint(b);
        if(interacting == null) return false;

        AccessPointType type = interacting.getType();        
        if(type.isControl())
        {
            if(!permission.canFly()) return false;
            if(this.phase == PhaseState.SOLID) {
                this.remove();
                return true;
            }
            if(this.phase == PhaseState.PHASING) {
                return true;
            }

            int amt = 10;
            if(p.isSneaking()) amt = 1;

            this.position = this.position.add(type.getModX() * amt, type.getModY() * amt, type.getModZ() * amt);
            World w = this.position.getWorld();
            int maxHeight = ServiceProvider.worldService().getWorldHeight(w);
            if(this.position.getY() > maxHeight)
            {
                World next = ServiceProvider.worldService().getHigherWorld(w);
                if(next == null)
                {
                    this.position.setY(maxHeight);
                }
                else
                {
                    this.position.setWorld(next);
                    this.position.setY(this.position.getY() % maxHeight);
                }
            }
            else if(this.position.getY() < 0)
            {
                World next = ServiceProvider.worldService().getLowerWorld(w);
                if(next == null)
                {
                    this.position.setY(0);
                }
                else
                {
                    this.position.setWorld(next);
                    this.position.setY(ServiceProvider.worldService().getWorldHeight(next) + this.position.getY());
                }
            }

            this.settings.getInterior().updateConsoles();
            return false;
        }
        else if(type.isSignDisplay() && type != AccessPointType.PARKING_BRAKE)
        {
            if(!permission.canBuild()) return false;

            if(p.isSneaking())
            {
                long millis = new Date().getTime();
                Point point = new Point(b);
                // Giving them 3 seconds from the last click to confirm
                if(removeDisplaySign == null || !removeDisplaySign.item1.equals(point) || removeDisplaySign.item2 < millis - (1000 * 3))
                {
                    MessageService.say(p, Text.format("{CLICK_AGAIN_TO_COMFIRM}", type.getName()));
                    removeDisplaySign = new Tuple<Point,Long>(point, millis);
                }
                else
                {
                    MessageService.say(p, Text.format("{REMOVING_AT}", type.getName(), point));
                    this.settings.getInterior().removeAccessPoint(interacting);
                }

                return true;
            }
        }
        else if(type == AccessPointType.PARKING_BRAKE)
        {
            if(!permission.canFly()) return false;

            if(this.phase == PhaseState.IN_PHASE) this.land();
            else this.remove();
        }
        else if(type == AccessPointType.HALLWAY_ACCESS)
        {
            if(!permission.canBuild()) return true;

            if(this.getSettings().getInterior().getHallwayType() == null)
            {
                ServiceProvider.inventoryMenuService().getMenu(ChangeHallwayTypeMenu.class).open(p);
                MessageService.say(p, Text.MUST_SET_HALLWAY_TYPE);
            }
            else
            {
                if(clicked.getModY() == 0)
                {
                    if(interacting.getOffset().getBlockRelative(this.getSettings().getInterior().getWorld(), clicked.getModX(), 0, clicked.getModZ()).getType() == Material.AIR)
                    {
                        if(!this.getSettings().getInterior().tryConnect(interacting.getOffset(), clicked.getOppositeFace(), p))
                        {
                            ServiceProvider.actionService().startPerformingAction(p, new BuildRoomAction(p.getUniqueId(), interacting.getOffset(), clicked));
                            ServiceProvider.inventoryMenuService().getMenu(BuildRoomDialogue.class).open(p);
                        }
                    }
                }
            }
            return true;
        }

        return false;
    }

    public void addAccessPoint(AccessPoint ap)
    {
        this.settings.getInterior().addAccessPoint(ap);
    }

    //endregion Inside

    //region Getters/Setters

    public String getPlayerName()
    {
        if(!foundPlayersName || playerName == null)
        {
            Player p = Bukkit.getPlayer(this.player);
            if(p != null) 
            {
                playerName = p.getName();
                foundPlayersName = true;
            }
        }

        return playerName == null ? "" : playerName;
    }

    public UUID getPlayerId()
    {
        return this.player;
    }

    public TARDISSettings getSettings() {
        return settings;
    }

    public OrientedPosition getPosition() {
        return position.clone();
    }

    public boolean isPhased() {
        return this.phase == PhaseState.IN_PHASE;
    }

    public PhaseState getPhaseState() 
    {
        return this.phase;
    }

    //endregion Getters/Setters

    //region Permissions

    public TardisPermissionLevel getPermissionLevel(Player p)
    {
        UUID pId = p.getUniqueId();
        TardisPermissionLevel permissionLevel = this.player.equals(pId) ? TardisPermissionLevel.OWNER : (p.isOp() ? TardisPermissionLevel.CAN_FLY : null);
        if(permissionLevel == null)
        {
            TardisPermission perm = allowedPlayers.FirstOrDefault(ap -> ap.getPlayer().equals(pId));
            permissionLevel = perm == null ? TardisPermissionLevel.NONE : perm.getLevel();
        }

        return permissionLevel;
    }

    public void setPermissionLevel(Player p, TardisPermissionLevel level)
    {
        UUID pId = p.getUniqueId();

        if(this.player.equals(pId) || p.isOp()) return;

        TardisPermission perm = allowedPlayers.FirstOrDefault(ap -> ap.getPlayer().equals(pId));
        if(perm == null)
        {
            if(level != TardisPermissionLevel.NONE)
            {
                allowedPlayers.add(new TardisPermission(p, level));
            }
        }
        else
        {
            if(level != TardisPermissionLevel.NONE)
            {
                perm.setLevel(level);
            }
            else
            {
                allowedPlayers.remove(perm);
            }
        }
    }

    public EList<String> getAllowedPlayersDisplay()
    {
        return this.allowedPlayers.Select(ap -> ap.toString()).ToList();
    }

    public boolean isAllowed(Player p)
    {
        return this.getPermissionLevel(p).canEnter();
    }

    //endregion Permissions

    //region Debug

    public String info()
    {
        return String.format("Position {%s}, Phase: %s", position.toString(), phase + "");
    }

    //endregion Debug

    //region Hierarchy

    @Override
    public void initializeHierarchy() 
    {
        this.settings.initializeHierarchy(this);
    }

    //endregion Hierarchy
}