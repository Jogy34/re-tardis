package com.adx.jogy34.tardis.tardis;

import java.text.MessageFormat;
import java.util.UUID;

import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class TardisPermission implements IConfigSerializable
{
    public static enum TardisPermissionLevel
    {
        NONE(false, false, false, "N/A"),
        CAN_ENTER(true, false, false, "Enter"),
        CAN_FLY(true, true, false, "Fly"),
        OWNER(true, true, true, "Owner")
        ;

        protected final boolean canEnter, canFly, canBuild;
        protected final String displayName;
        private TardisPermissionLevel(boolean canEnter, boolean canFly, boolean canBuild, String displayName)
        {
            this.canEnter = canEnter;
            this.canFly = canFly;
            this.canBuild = canBuild;
            this.displayName = displayName;
        }

        public boolean canEnter()
        {
            return this.canEnter;
        }

        public boolean canFly()
        {
            return this.canFly;
        }

        public boolean canBuild()
        {
            return this.canBuild;
        }

        public String getDisplayName()
        {
            return this.displayName;
        }

        public static TardisPermissionLevel fromCommandString(String str)
        {
            str = str.toLowerCase();
            switch(str)
            {
                case "none": return NONE;
                case "can_enter":
                case "can enter":
                case "enter":
                {
                    return CAN_ENTER;
                }
                case "can fly":
                case "can_fly":
                case "fly":
                {
                    return CAN_FLY;
                }
                default: return null;
            }
        }
    }

    @ConfigProperty("Player")
    protected UUID player;
    @ConfigProperty("PlayerName")
    protected String playerName;
    @ConfigProperty("Level")
    protected TardisPermissionLevel level;

    protected boolean updatedName = false;

    @SuppressWarnings("unused")
    private TardisPermission() { }

    public TardisPermission(Player player, TardisPermissionLevel level)
    {
        this.player = player.getUniqueId();
        this.playerName = player.getName();
        this.level = level;
        this.updatedName = true;
    }

    public UUID getPlayer()
    {
        return player;
    }

    public TardisPermissionLevel getLevel() 
    {
        return level;
    }

    public void setLevel(TardisPermissionLevel level)
    {
        this.level = level;
    }

    public String getPlayerName()
    {
        if(!this.updatedName)
        {
            Player p = Bukkit.getPlayer(this.player);
            if(p != null)
            {
                this.playerName = p.getName();
                this.updatedName = true;
            }
        }

        return this.playerName;
    }

    @Override
    public String toString() 
    {
        return MessageFormat.format("{0}: {1}", this.getPlayerName(), this.level.getDisplayName());
    }
}