package com.adx.jogy34.tardis.menu.shared.materialselect;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.menu.base.InventoryMenu;
import com.adx.jogy34.tardis.menu.base.InventoryPage;
import com.adx.jogy34.tardis.menu.shared.materialselect.interfaces.IMaterialSelectListener;

public class MaterialSelectDialog<T> extends InventoryMenu<Integer>
{
    protected EList<Material> options;
    protected IMaterialSelectListener<T> listener;
    protected String name;
    protected T key;
    protected boolean exit;
    
    public MaterialSelectDialog(String name, EList<Material> options, IMaterialSelectListener<T> listener, T key, boolean exit)
    {
        super(0, false);
        this.options = options;
        this.listener = listener;
        this.name = name;
        this.key = key;
        this.exit = exit;
        this.init();
    }

    @Override
    protected void createMenu()
    {
        if(this.options.size() <= InventoryPage.MAX_INVENTORY_PAGE_SIZE)
        {
            this.dialogues.put(0, new MaterialSelectPage(this.name, this.options, false, false, this.exit, this, 0));
        }
        else 
        {
            int max = InventoryPage.MAX_INVENTORY_PAGE_SIZE - 9;
            int page = 0;
            int index = 0;
            int opCount = this.options.Count();
            int totalPages = (opCount / max) + (opCount % max > 0 ? 1 : 0);
            EList<Material> nextOptions = new EList<Material>();
            while(index < this.options.size())
            {
                if(nextOptions.size() >= max)
                {
                    this.dialogues.put(page, new MaterialSelectPage(String.format("%s %s(%d/%d)", name, ChatColor.DARK_GRAY, page + 1, totalPages), nextOptions, page > 0, true, this.exit, this, page));
                    page++;
                    nextOptions = new EList<Material>();
                }
                nextOptions.add(options.get(index));
                index++;
            }

            if(nextOptions.Any())
            {
                this.dialogues.put(page, new MaterialSelectPage(String.format("%s %s(%d/%d)", name, ChatColor.DARK_GRAY, page + 1, totalPages), nextOptions, true, false, this.exit, this, page));
            }
        }
    }
    
    public void next(Player who, int index)
    {
        if(index < this.dialogues.size() - 1)
        {
            who.closeInventory();
            this.dialogues.get(index + 1).open(who);
        }
    }
    
    public void end(Player who)
    {
        who.closeInventory();
        this.dialogues.get(dialogues.Count() - 1).open(who);
    }
    
    public void previous(Player who, int index)
    {
        if(index > 0)
        {
            who.closeInventory();
            this.dialogues.get(index - 1).open(who);
        }
    }
    
    public void beginning(Player who)
    {
        who.closeInventory();
        this.dialogues.get(0).open(who);
    }
    
    public void selectMaterial(Player who, Material mat)
    {
        this.listener.onSelect(who, mat, this.key);
    }
    
    public void exit(Player who)
    {
        this.listener.onExitMaterialSelect(who, this.key);
    }
}
