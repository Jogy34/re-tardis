package com.adx.jogy34.tardis.menu.shared.materialselect.interfaces;

import org.bukkit.Material;
import org.bukkit.entity.Player;

public interface IMaterialSelectListener<T>
{
    public void onSelect(Player who, Material mat, T key);
    public void onExitMaterialSelect(Player who, T key);
}
