package com.adx.jogy34.tardis.menu.shared.materialselect;

import org.bukkit.Material;

public class MaterialSelectMenuAction
{
    public static enum MaterialSelectMenuActionType
    {
        SELECT, PREVIOUS, BEGINNING, NEXT, END, EXIT
    }
    
    protected Material what;
    protected MaterialSelectMenuActionType action;
    
    public MaterialSelectMenuAction(Material mat)
    {
        this.what = mat;
        this.action = MaterialSelectMenuActionType.SELECT;
    }
    
    public MaterialSelectMenuAction(MaterialSelectMenuActionType action)
    {
        this.action = action;
        this.what = Material.AIR;
    }
}
