package com.adx.jogy34.tardis.menu.shared.materialselect;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.menu.base.InventoryPage;
import com.adx.jogy34.tardis.menu.shared.materialselect.MaterialSelectMenuAction.MaterialSelectMenuActionType;

public class MaterialSelectPage extends InventoryPage<MaterialSelectMenuAction>
{
    protected EList<Material> options;
    protected boolean prev, next, exit;
    protected MaterialSelectDialog<?> parent;
    protected int index;
    
    public MaterialSelectPage(String name, EList<Material> options, boolean prev, boolean next, boolean exit, MaterialSelectDialog<?> parent, int index)
    {
        super(options.size() + (prev || next || exit ? 9 : 0), name);
        this.options = options;
        this.prev = prev;
        this.next = next;
        this.parent = parent;
        this.index = index;
        this.exit = exit;
    }

    @Override
    protected void createPage()
    {
        this.options.ForEach(x -> this.addItem(x, new MaterialSelectMenuAction(x)));
        if(this.prev || this.next || this.exit)
        {
            int lastRow = (this.inventory.getSize() / 9) - 1;
            if(this.prev)
            {
                ItemStack beginning = new ItemStack(Material.OAK_SIGN);
                ItemMeta beginningMeta = beginning.getItemMeta();
                beginningMeta.setDisplayName("<<< Beginning");
                beginning.setItemMeta(beginningMeta);
                
                this.setItem(lastRow, 0, beginning, new MaterialSelectMenuAction(MaterialSelectMenuActionType.BEGINNING));

                ItemStack prev = new ItemStack(Material.OAK_SIGN);
                ItemMeta prevMeta = prev.getItemMeta();
                prevMeta.setDisplayName("< Previous");
                prev.setItemMeta(prevMeta);
                
                this.setItem(lastRow, 1, prev, new MaterialSelectMenuAction(MaterialSelectMenuActionType.PREVIOUS));
            }
            
            if(this.next)
            {
                ItemStack end = new ItemStack(Material.OAK_SIGN);
                ItemMeta endMeta = end.getItemMeta();
                endMeta.setDisplayName("End >>>");
                end.setItemMeta(endMeta);

                this.setItem(lastRow, 8, end, new MaterialSelectMenuAction(MaterialSelectMenuActionType.END));

                ItemStack next = new ItemStack(Material.OAK_SIGN);
                ItemMeta nextMeta = next.getItemMeta();
                nextMeta.setDisplayName("Next >");
                next.setItemMeta(nextMeta);

                this.setItem(lastRow, 7, next, new MaterialSelectMenuAction(MaterialSelectMenuActionType.NEXT));
            }
            
            if(this.exit)
            {
                ItemStack exit = new ItemStack(Material.BARRIER);
                ItemMeta exitMeta = exit.getItemMeta();
                exitMeta.setDisplayName(ChatColor.RED + "Exit");
                exit.setItemMeta(exitMeta);

                this.setItem(lastRow, 4, exit, new MaterialSelectMenuAction(MaterialSelectMenuActionType.EXIT));
            }
        }
        
        this.fillEmpty();
    }

    @Override
    protected void onInventoryClick(Player who, MaterialSelectMenuAction what)
    {
        switch(what.action)
        {
            case NEXT: 
            {
                this.parent.next(who, this.index);
                break;
            }
            case END:
            {
                this.parent.end(who);
                break;
            }
            case PREVIOUS: 
            {
                this.parent.previous(who, this.index);
                break;
            }
            case BEGINNING:
            {
                this.parent.beginning(who);
                break;
            }
            case SELECT: 
            {
                this.parent.selectMaterial(who, what.what);;
                break;
            }
            case EXIT:
            {
                this.parent.exit(who);
                break;
            }
        }
    }
}
