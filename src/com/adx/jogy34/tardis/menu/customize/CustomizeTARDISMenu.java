package com.adx.jogy34.tardis.menu.customize;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.data.MiscMaterialData;
import com.adx.jogy34.tardis.data.TARDISAppearanceData;
import com.adx.jogy34.tardis.enums.TARDISItems;
import com.adx.jogy34.tardis.menu.base.interfaces.IInventoryMenuDialog;
import com.adx.jogy34.tardis.menu.base.interfaces.IInventoryPage;
import com.adx.jogy34.tardis.menu.actions.TARDISCustomizeActions;
import com.adx.jogy34.tardis.menu.actions.TARDISCustomizeActions.UseSpawnEggAction;
import com.adx.jogy34.tardis.menu.base.InventoryMenu;
import com.adx.jogy34.tardis.menu.shared.materialselect.interfaces.IMaterialSelectListener;
import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.menu.shared.materialselect.MaterialSelectDialog;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.tardis.TARDISAppearance;
import com.adx.jogy34.tardis.util.helpers.BlockHelper;
import com.adx.jogy34.tardis.util.helpers.EMapHelper;

public class CustomizeTARDISMenu extends InventoryMenu<CustomizeTARDISDialogues> implements IMaterialSelectListener<CustomizeTARDISDialogues>
{
    protected TARDISAppearance tardisAppearance;
    protected boolean isNew = false;
    protected TARDISCustomizeActions.UseSpawnEggAction useSpawnEggAction = UseSpawnEggAction.NONE;
    
    public CustomizeTARDISMenu()
    {
        super(CustomizeTARDISDialogues.MAIN);
    }

    @Override
    public void init() 
    {
        tardisAppearance = new TARDISAppearance();
        super.init();
    }
    
    @Override
    protected void createMenu()
    {
        addPage(CustomizeTARDISDialogues.MAIN, new CustomizeTARDISMainPage(this));
        addPage(CustomizeTARDISDialogues.SELECT_LANTERN, new MaterialSelectDialog<CustomizeTARDISDialogues>("Select Lantern", TARDISAppearanceData.ALLOWED_LANTERN_MATERIALS, this, CustomizeTARDISDialogues.SELECT_LANTERN, true));
        addPage(CustomizeTARDISDialogues.SELECT_PRIMARY, new MaterialSelectDialog<CustomizeTARDISDialogues>("Select Primary", TARDISAppearanceData.ALLOWED_OUTER_MATERIALS, this, CustomizeTARDISDialogues.SELECT_PRIMARY, true));
        addPage(CustomizeTARDISDialogues.SELECT_SECONDARY, new MaterialSelectDialog<CustomizeTARDISDialogues>("Select Secondary", TARDISAppearanceData.ALLOWED_OUTER_MATERIALS, this, CustomizeTARDISDialogues.SELECT_SECONDARY, true));
        addPage(CustomizeTARDISDialogues.SELECT_DOOR, new MaterialSelectDialog<CustomizeTARDISDialogues>("Select Door", TARDISAppearanceData.ALLOWED_DOOR_MATERIALS, this, CustomizeTARDISDialogues.SELECT_DOOR, true));
        addPage(CustomizeTARDISDialogues.SELECT_OWNER_SIGN, new MaterialSelectDialog<CustomizeTARDISDialogues>("Select Owner Sign", MiscMaterialData.SIGN_POSTS, this, CustomizeTARDISDialogues.SELECT_OWNER_SIGN, true));
        addPage(CustomizeTARDISDialogues.SELECT_POLICE_SIGNS, new MaterialSelectDialog<CustomizeTARDISDialogues>("Select Police Signs", MiscMaterialData.SIGN_POSTS, this, CustomizeTARDISDialogues.SELECT_POLICE_SIGNS, true));
    }
    
    public TARDISAppearance getAppearance()
    {
        return this.tardisAppearance;
    }

    public void performAction(Player who, TARDISCustomizeActions.General action)
    {
        switch(action)
        {
            case TOGGLE_READY:
            {
                if(isNew)
                {
                    switch(useSpawnEggAction)
                    {
                        case NONE: useSpawnEggAction = TARDISCustomizeActions.UseSpawnEggAction.PREVIEW; break;
                        case PREVIEW: useSpawnEggAction = TARDISCustomizeActions.UseSpawnEggAction.CREATE; break;
                        case CREATE: useSpawnEggAction = TARDISCustomizeActions.UseSpawnEggAction.NONE; break;
                    }
                }
                else
                {
                    IInventoryMenuDialog parent = this.getParent();
                    if(parent != null)
                    {
                        parent.setData(who, tardisAppearance, "confirm", this);
                    }
                }
                break;
            }
            case CANCEL:
            {
                IInventoryMenuDialog parent = this.getParent();
                if(parent != null)
                {
                    parent.setData(who, null, "cancel", this);
                }
                break;
            }
            case TOGGLE_POLICE_SIGNS:
            {
                this.tardisAppearance.setUsePoliceSigns(!this.tardisAppearance.usesPoliceSigns());
                break;
            }
            case TOGGLE_OWNER_SIGN:
            {
                this.tardisAppearance.setUseOwnerSign(!this.tardisAppearance.usesOwnerSign());
                break;
            }
        }
    }

    public TARDISCustomizeActions.UseSpawnEggAction getUseSpawnEggAction() { return this.useSpawnEggAction; }
    public boolean isNew() { return this.isNew; }

    @Override
    public void onSelect(Player who, Material mat, CustomizeTARDISDialogues dialogue)
    {
        boolean resetMain = true;
        switch(dialogue)
        {
            case SELECT_LANTERN:
            {
                this.tardisAppearance.setLanternMaterial(mat);
                break;
            }
            case SELECT_PRIMARY:
            {
                this.tardisAppearance.setPrimaryMaterial(mat);
                break;
            }
            case SELECT_SECONDARY:
            {
                this.tardisAppearance.setSecondaryMaterial(mat);
                break;
            }
            case SELECT_DOOR:
            {
                this.tardisAppearance.setDoorMaterial(mat);
                break;
            }
            case SELECT_POLICE_SIGNS:
            {
                this.tardisAppearance.setPoliceSignMaterial(BlockHelper.toWallSignMaterial(mat));
                break;
            }
            case SELECT_OWNER_SIGN:
            {
                this.tardisAppearance.setOwnerSignMaterial(BlockHelper.toWallSignMaterial(mat) );
                break;
            }
            default:
            {
                resetMain = false;
                break;
            }
        }
        
        if(resetMain) 
        {
            ((IInventoryPage) dialogues.get(CustomizeTARDISDialogues.MAIN)).reset();
            navigateTo(who, CustomizeTARDISDialogues.MAIN);
        }
    }

    @Override
    public void onExitMaterialSelect(Player who, CustomizeTARDISDialogues key)
    {
        navigateTo(who, CustomizeTARDISDialogues.MAIN);
    }

    public void update(ItemStack spawnEgg)
    {
        isNew = true;
        List<String> defLore = TARDISItems.TARDIS_EGG.get().getItemMeta().getLore();
        EList<String> lore = new EList<String>(spawnEgg.getItemMeta().getLore());
        EMap<String, String> data = lore.Where(x -> !defLore.contains(x))
                                        .Select(x -> ChatColor.stripColor(x))
                                        .ToMap(x -> x.split(":")[0], x -> x.substring(x.indexOf(':') + 1).trim());

        this.useSpawnEggAction = TARDISCustomizeActions.UseSpawnEggAction.valueOf((String) data.getOrDefault("Action", TARDISCustomizeActions.UseSpawnEggAction.NONE.toString()));
        tardisAppearance = data.Any() ? ConfigProvider.createFromConfigParameters(TARDISAppearance.class, data) : new TARDISAppearance();
    }

    public void update(Player p)
    {
        TARDIS t = TARDISProvider.getTardisForPlayer(p);
        if(t == null) 
        {
            p.closeInventory();
            return;
        }

        update(t);
    }

    public void update(TARDIS t)
    {
        isNew = false;
        this.tardisAppearance = t.getSettings().getAppearance().clone();
    }
    
    @Override
    public void onClose(Player p) 
    {
        if(isNew)
        {
            ItemStack item = p.getInventory().getItemInMainHand();
            TARDISItems type = ServiceProvider.itemService().getItem(item);
            if(type == TARDISItems.TARDIS_EGG) 
            {
                EList<String> lore = new EList<String>(TARDISItems.TARDIS_EGG.get().getItemMeta().getLore());
                ChatColor actionColor = ChatColor.RED;
                switch(useSpawnEggAction)
                {
                    case NONE: actionColor = ChatColor.RED; break;
                    case PREVIEW: actionColor = ChatColor.YELLOW; break;
                    case CREATE: actionColor = ChatColor.GREEN; break;
                }
                lore.add(actionColor + "Action: " + useSpawnEggAction);
                lore.addAll(EMapHelper.toKeyValueStrings(ConfigProvider.getConfigProperties(tardisAppearance))
                            .Select(x -> ChatColor.DARK_GRAY + x).ToList());
                

                ItemMeta meta = item.getItemMeta();
                meta.setLore(lore);
                item.setItemMeta(meta);
            }
        }
    }
}
