package com.adx.jogy34.tardis.menu.customize;

public enum CustomizeTARDISDialogues
{
    MAIN, SELECT_LANTERN, SELECT_PRIMARY, SELECT_SECONDARY, SELECT_DOOR, SELECT_POLICE_SIGNS, SELECT_OWNER_SIGN
}
