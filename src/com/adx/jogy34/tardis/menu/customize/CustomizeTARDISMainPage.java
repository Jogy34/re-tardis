package com.adx.jogy34.tardis.menu.customize;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.menu.actions.TARDISCustomizeActions;
import com.adx.jogy34.tardis.menu.actions.TARDISCustomizeActions.UseSpawnEggAction;
import com.adx.jogy34.tardis.menu.base.InventoryPage;
import com.adx.jogy34.tardis.tardis.TARDISAppearance;
import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.helpers.BlockHelper;
import com.adx.jogy34.tardis.util.helpers.StringHelper;

public class CustomizeTARDISMainPage extends InventoryPage<TARDISCustomizeActions.MainPage>
{
    public CustomizeTARDISMainPage(CustomizeTARDISMenu parent)
    {
        super(InventoryPage.MAX_INVENTORY_PAGE_SIZE, "Appearance");
    }
    
    @Override
    protected void createPage()
    {
        CustomizeTARDISMenu parent = this.getParentAs(CustomizeTARDISMenu.class);
        TARDISAppearance appearance = parent.getAppearance();
        
        ItemStack lantern = new ItemStack(appearance.getLanternMaterial()),
                  primary = new ItemStack(appearance.getPrimaryMaterial()),
                  secondary = new ItemStack(appearance.getSecondaryMaterial()),
                  door = new ItemStack(appearance.getDoorMaterial());
        
        ItemMeta lanternMeta = lantern.getItemMeta(),
                 primaryMeta = primary.getItemMeta(),
                 secondaryMeta = secondary.getItemMeta(),
                 doorMeta = door.getItemMeta();
        
        String tagColor = String.format("%s%s", ChatColor.GOLD, ChatColor.BOLD);
        
        lanternMeta.setDisplayName(String.format("%s[Lantern] %s%s", tagColor, ChatColor.RESET, StringHelper.getName(lantern.getType())));
        primaryMeta.setDisplayName(String.format("%s[Primary] %s%s", tagColor, ChatColor.RESET, StringHelper.getName(primary.getType())));
        secondaryMeta.setDisplayName(String.format("%s[Secondary] %s%s", tagColor, ChatColor.RESET, StringHelper.getName(secondary.getType())));
        doorMeta.setDisplayName(String.format("%s[Door] %s%s", tagColor, ChatColor.RESET, StringHelper.getName(door.getType())));

        lantern.setItemMeta(lanternMeta);
        primary.setItemMeta(primaryMeta);
        secondary.setItemMeta(secondaryMeta);
        door.setItemMeta(doorMeta);
        
        this.setItem(1, 1, lantern, TARDISCustomizeActions.MainPage.SET_LANTERN);
        this.fillSection(2, 0, 3, 1, secondary, TARDISCustomizeActions.MainPage.SET_SECONDARY);
        this.fillSection(3, 0, 3, 3, primary, TARDISCustomizeActions.MainPage.SET_PRIMARY);
        this.setItem(4, 1, door, TARDISCustomizeActions.MainPage.SET_DOOR);

        this.setReadyItem(parent);
        this.setPoliceSignsItem(appearance);
        this.setOwnerSignItem(appearance);

        this.fillEmpty();
    }

    protected void setReadyItem(CustomizeTARDISMenu parent)
    {
        if(parent.isNew())
        {
            UseSpawnEggAction action = parent.getUseSpawnEggAction();
            Material readyMat = Material.RED_CONCRETE;
            String displayName = "", primaryLore = "";
            EList<String> lore = new EList<String>();
            boolean addCrouchNotification = true;


            switch(action)
            {
                case NONE: 
                {
                    readyMat = Material.RED_CONCRETE; 
                    displayName = ChatColor.RED + "Not Ready";
                    primaryLore = "This menu will appear";
                    addCrouchNotification = false;
                    break;
                }
                case PREVIEW: 
                {
                    readyMat = Material.YELLOW_CONCRETE; 
                    displayName = ChatColor.YELLOW + "Preview";
                    primaryLore = "A TARDIS Preview will appear for a few seconds";
                    break;
                }
                case CREATE: 
                {
                    readyMat = Material.GREEN_CONCRETE; 
                    displayName = ChatColor.GREEN + "Create TARDIS";
                    primaryLore = "A TARDIS will be created";
                    break;
                }
            }

            lore.add(String.format("%s%s%s", ChatColor.DARK_PURPLE, ChatColor.ITALIC, primaryLore));

            if(addCrouchNotification)
            {
                lore.add(String.format("%s%sCrouch right click to bring up this menu again", ChatColor.DARK_GRAY, ChatColor.ITALIC));
            }

            ItemStack ready = new ItemStack(readyMat);
            ItemMeta readyMeta = ready.getItemMeta();
            
            readyMeta.setDisplayName(String.format("%s[Spawn Egg]%s %s", ChatColor.GOLD, ChatColor.RESET, displayName));
            readyMeta.setLore(lore);
            
            ready.setItemMeta(readyMeta);
            this.setItem(5, 8, ready, TARDISCustomizeActions.MainPage.TOGGLE_READY);
        }
        else
        {
            this.setItem(5, 8, TARDISCustomizeActions.MainPage.TOGGLE_READY, Material.GREEN_CONCRETE, Text.format("{0}{SAVE}", ChatColor.GREEN));
            this.setItem(5, 7, TARDISCustomizeActions.MainPage.CANCEL, Material.BARRIER, Text.format("{0}{CANCEL}", ChatColor.RED));
        }
    }

    protected void setPoliceSignsItem(TARDISAppearance appearance)
    {
        Material type = BlockHelper.toSignPostMaterial(appearance.getPoliceSignMaterial());

        ItemStack policeSigns = new ItemStack(type);
        ItemMeta policeSignMeta = policeSigns.getItemMeta();

        policeSignMeta.setDisplayName((appearance.usesPoliceSigns() ? ChatColor.GREEN : ChatColor.RED) + "Police Box Signs: " + (appearance.usesPoliceSigns() ? "ON" : "OFF"));
        policeSigns.setItemMeta(policeSignMeta);

        this.setItem(2, 4, policeSigns, TARDISCustomizeActions.MainPage.TOGGLE_POLICE_SIGNS);

        ItemStack policeSignsType = new ItemStack(type);
        ItemMeta policeSignsTypeMeta = policeSignsType.getItemMeta();

        policeSignsTypeMeta.setDisplayName(Text.format("{0}{1}[{POLICE_SIGNS}] {2}{3}", ChatColor.GOLD, ChatColor.BOLD, ChatColor.RESET, StringHelper.getName(type)));
        policeSignsType.setItemMeta(policeSignsTypeMeta);
        
        this.setItem(2, 5, policeSignsType, TARDISCustomizeActions.MainPage.CHANGE_POLICE_SIGNS);
    }

    protected void setOwnerSignItem(TARDISAppearance appearance)
    {
        Material type = BlockHelper.toSignPostMaterial(appearance.getOwnerSignMaterial());

        ItemStack ownerSign = new ItemStack(type);
        ItemMeta ownerSignMeta = ownerSign.getItemMeta();

        ownerSignMeta.setDisplayName((appearance.usesOwnerSign() ? ChatColor.GREEN : ChatColor.RED) + "Owner Sign: " + (appearance.usesOwnerSign() ? "ON" : "OFF"));
        ownerSign.setItemMeta(ownerSignMeta);

        this.setItem(4, 4, ownerSign, TARDISCustomizeActions.MainPage.TOGGLE_OWNER_SIGN);

        ItemStack ownerSignType = new ItemStack(type);
        ItemMeta ownerSignTypeMeta = ownerSignType.getItemMeta();

        ownerSignTypeMeta.setDisplayName(Text.format("{0}{1}[{OWNER_SIGN}] {2}{3}", ChatColor.GOLD, ChatColor.BOLD, ChatColor.RESET, StringHelper.getName(type)));
        ownerSignType.setItemMeta(ownerSignTypeMeta);
        
        this.setItem(4, 5, ownerSignType, TARDISCustomizeActions.MainPage.CHANGE_OWNER_SIGN);
    }

    @Override
    protected void onInventoryClick(Player who, TARDISCustomizeActions.MainPage what)
    {
        CustomizeTARDISMenu parent = this.getParentAs(CustomizeTARDISMenu.class);
        switch(what)
        {
            case SET_DOOR:
            {
                parent.navigateTo(who, CustomizeTARDISDialogues.SELECT_DOOR);
                break;
            }
            case SET_LANTERN:
            {
                parent.navigateTo(who, CustomizeTARDISDialogues.SELECT_LANTERN);
                break;
            }
            case SET_PRIMARY:
            {
                parent.navigateTo(who, CustomizeTARDISDialogues.SELECT_PRIMARY);
                break;
            }
            case SET_SECONDARY:
            {
                parent.navigateTo(who, CustomizeTARDISDialogues.SELECT_SECONDARY);
                break;
            }
            case TOGGLE_READY:
            {
                parent.performAction(who, TARDISCustomizeActions.General.TOGGLE_READY);
                this.setReadyItem(parent);
                break;
            }
            case CANCEL:
            {
                parent.performAction(who, TARDISCustomizeActions.General.CANCEL);
                break;
            }
            case CHANGE_POLICE_SIGNS:
            {
                parent.navigateTo(who, CustomizeTARDISDialogues.SELECT_POLICE_SIGNS);
                break;
            }
            case TOGGLE_POLICE_SIGNS:
            {
                parent.performAction(who, TARDISCustomizeActions.General.TOGGLE_POLICE_SIGNS);
                this.setPoliceSignsItem(parent.getAppearance());
                break;
            }
            case CHANGE_OWNER_SIGN:
            {
                parent.navigateTo(who, CustomizeTARDISDialogues.SELECT_OWNER_SIGN);
                break;
            }
            case TOGGLE_OWNER_SIGN:
            {
                parent.performAction(who, TARDISCustomizeActions.General.TOGGLE_OWNER_SIGN);
                this.setOwnerSignItem(parent.getAppearance());
                break;
            }
        }
    }

    @Override
    public void open(Player p) 
    {
        this.reset();
        super.open(p);
    }

    @Override
    public void onClose(Player who) 
    {
        this.getParentAs(CustomizeTARDISMenu.class).onClose(who);
    }
}
