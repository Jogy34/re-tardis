package com.adx.jogy34.tardis.menu.upgrade;

import com.adx.jogy34.tardis.menu.base.InventoryPage;
import com.adx.jogy34.tardis.menu.upgrade.UpgradeTardisActions.MainPage;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.entity.Player;

public class UpgradeTardisMainPage extends InventoryPage<UpgradeTardisActions.MainPage> 
{
    public UpgradeTardisMainPage()
    {
        super(InventoryPage.MIN_INVENTORY_PAGE_SIZE, Text.UPGRADE_TARDIS);
    }

    @Override
    protected void createPage() 
    {
        for(UpgradeTardisActions.MainPage pg : UpgradeTardisActions.MainPage.values())
        {
            addItem(pg.ICON, pg, pg.TEXT, pg.DESCRIPTION);
        }
    }

    @Override
    protected void onInventoryClick(Player who, MainPage what) 
    {
        UpgradeTardisMenu menu = this.getParentAs(UpgradeTardisMenu.class);
        if(menu == null) return;

        switch(what)
        {
            case CHANGE_APPEARANCE:
            {
                menu.navigateTo(who, UpgradeTardisDialogues.CHANGE_APPEARANCE);
                break;
            }
            case CONTROL_ROOM:
            {
                menu.navigateTo(who, UpgradeTardisDialogues.CHANGE_CONTROL_ROOM);
                break;
            }
            case MISCELLANEOUS:
            {
                menu.navigateTo(who, UpgradeTardisDialogues.MISCELANEOUS);
                break;
            }
            case CHANGE_HALLWAY_TYPE:
            {
                menu.navigateTo(who, UpgradeTardisDialogues.SET_HALLWAY_TYPE);
                break;
            }
        }
	}
}