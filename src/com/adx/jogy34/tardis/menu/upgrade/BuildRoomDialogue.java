package com.adx.jogy34.tardis.menu.upgrade;

import com.adx.jogy34.tardis.menu.base.InventoryMenu;

public class BuildRoomDialogue extends InventoryMenu<Integer>
{
    public BuildRoomDialogue()
    {
        super(0);
    }

    @Override
    protected void createMenu() 
    {
        addPage(0, new SelectRoomPage());
    }
}