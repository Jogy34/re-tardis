package com.adx.jogy34.tardis.menu.upgrade;

import com.adx.jogy34.tardis.enums.HallwayType;
import com.adx.jogy34.tardis.menu.base.InventoryPage;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.entity.Player;

public class ChangeHallwayTypePage extends InventoryPage<HallwayType> {

    public ChangeHallwayTypePage()
    {
        super(HallwayType.values().length + 1, Text.HALLWAY_STYLE);
    }

    @Override
    protected void createPage() 
    {
        for(HallwayType type : HallwayType.values())
        {
            addItem(type.ICON, type, type.NAME, type.DESCRIPTION);
        }

        setItem(this.size - 1, CANCEL, null);
    }

    @Override
    protected void onInventoryClick(Player who, HallwayType what) 
    {
        if(what == null)
        {
            if(this.getAncestorOrDefault(UpgradeTardisMenu.class) == null)
            {
                who.closeInventory();
            }
            else
            {
                this.getAncestor(UpgradeTardisMenu.class).navigateTo(who, UpgradeTardisDialogues.MAIN);
            }
        }
        else
        {
            TARDIS t = TARDISProvider.getTardisForPlayer(who);
            if(t != null)
            {
                t.getSettings().getInterior().setHallwayType(what);
            }
            else
            {
                MessageService.say(who, Text.NEED_TARDIS_FOR_THAT);
            }
            who.closeInventory();
        }
	}

}