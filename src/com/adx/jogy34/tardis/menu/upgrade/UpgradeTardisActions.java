package com.adx.jogy34.tardis.menu.upgrade;

import org.bukkit.Material;

import com.adx.jogy34.tardis.providers.CostProvider;
import com.adx.jogy34.tardis.util.Text;

public class UpgradeTardisActions
{
    public static enum MainPage
    {
        CONTROL_ROOM(Material.BEACON, Text.UPGRADE_CONTROL_ROOM, Text.UPGRADE_CONTROL_ROOM_DESC_1),
        CHANGE_APPEARANCE(Material.BLUE_WOOL, Text.CHANGE_APPEARANCE, Text.CHANGE_APPEARANCE_DESC_1),
        CHANGE_HALLWAY_TYPE(Material.STICK, Text.CHANGE_HALLWAY_TYPE, Text.CHANGE_HALLWAY_TYPE_DESC),
        MISCELLANEOUS(Material.SEA_PICKLE, Text.MISCELLANEOUS, Text.MISCELLANEOUS_DESC),
        ;

        public final Material ICON;
        public final String TEXT;
        public final String[] DESCRIPTION;

        private MainPage(Material icon, String text, String... description)
        {
            ICON = icon;
            TEXT = text;
            DESCRIPTION = description;
        }
    }

    public static enum MiscellaneousPage
    {
        ADD_CONSOLE(Material.OAK_SIGN, Text.ADD_CONSOLE, Text.CONSOLE_DESC, CostProvider.Costs.CONSOLES.getCost().toDisplayString()),
        ADD_CLOCK(Material.OAK_SIGN, Text.ADD_CLOCK, Text.CLOCK_DESC, CostProvider.Costs.CLOCKS.getCost().toDisplayString()),
        SPEED(Material.POTION, Text.SPEED, Text.SPEED_DESC, CostProvider.Costs.SPEED.getCost().toDisplayString()),
        ;

        public final Material ICON;
        public final String TEXT;
        public final String[] DESCRIPTION;

        private MiscellaneousPage(Material icon, String text, String... description)
        {
            ICON = icon;
            TEXT = text;
            DESCRIPTION = description;
        }
    }
}