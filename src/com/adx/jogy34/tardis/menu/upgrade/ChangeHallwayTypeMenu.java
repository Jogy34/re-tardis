package com.adx.jogy34.tardis.menu.upgrade;

import com.adx.jogy34.tardis.menu.base.InventoryMenu;

public class ChangeHallwayTypeMenu extends InventoryMenu<Integer>
{
    public ChangeHallwayTypeMenu()
    {
        super(1);
    }

    @Override
    protected void createMenu() 
    {
        addPage(1, new ChangeHallwayTypePage());
    }
}