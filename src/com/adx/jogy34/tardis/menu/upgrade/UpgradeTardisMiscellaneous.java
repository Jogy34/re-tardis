package com.adx.jogy34.tardis.menu.upgrade;

import com.adx.jogy34.tardis.actions.PlaceSignDisplayAction;
import com.adx.jogy34.tardis.menu.base.InventoryPage;
import com.adx.jogy34.tardis.menu.upgrade.UpgradeTardisActions.MiscellaneousPage;
import com.adx.jogy34.tardis.providers.CostProvider;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.schematics.enums.AccessPointType;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.entity.Player;

public class UpgradeTardisMiscellaneous extends InventoryPage<MiscellaneousPage>
{
    public UpgradeTardisMiscellaneous()
    {
        super(MiscellaneousPage.values().length + 1, Text.MISCELLANEOUS);
    }

    @Override
    protected void createPage() 
    {
        for(MiscellaneousPage misc : MiscellaneousPage.values())
        {
            addItem(misc.ICON, misc, misc.TEXT, misc.DESCRIPTION);
        }

        setItem(this.size - 1, CANCEL, null);
    }

    @Override
    protected void onInventoryClick(Player who, MiscellaneousPage what) 
    {
        if(what == null)
        {
            this.getParentAs(UpgradeTardisMenu.class).navigateTo(who, UpgradeTardisDialogues.MAIN);
        }
        else
        {
            TARDIS t = TARDISProvider.getTardisForPlayer(who);
            switch(what)
            {
                case ADD_CONSOLE:
                case ADD_CLOCK:
                {
                    if(t != null && t.isWithinInteriorBounds(who.getLocation().getBlockX()))
                    {
                        ServiceProvider.actionService().startPerformingAction(who, new PlaceSignDisplayAction(who.getUniqueId(), what == MiscellaneousPage.ADD_CONSOLE ? AccessPointType.CONSOLE : AccessPointType.CLOCK));
                        who.closeInventory();
                    }
                    else
                    {
                        MessageService.say(who, Text.MUST_BE_INSIDE_TARDIS);
                    }
                    break;
                }
                case SPEED:
                {
                    if(t.getSettings().getInterior().isFast())
                    {
                        MessageService.say(who, Text.YOU_HAVE_ALREADY_UPGRADED_THAT);
                    }
                    else if(CostProvider.Costs.SPEED.getCost().pay(who))
                    {
                        t.getSettings().getInterior().setFast(true);
                        who.closeInventory();
                    }
                    else
                    {
                        MessageService.say(who, Text.INSUFFICIENT_FUNDS);
                    }
                }
            }
        }
	}
}