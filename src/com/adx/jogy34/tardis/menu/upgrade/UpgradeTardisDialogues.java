package com.adx.jogy34.tardis.menu.upgrade;

public enum UpgradeTardisDialogues
{
    MAIN, CHANGE_APPEARANCE, CHANGE_CONTROL_ROOM, MISCELANEOUS, SET_HALLWAY_TYPE
}