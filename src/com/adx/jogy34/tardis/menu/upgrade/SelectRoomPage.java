package com.adx.jogy34.tardis.menu.upgrade;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.actions.data.SetTextActionData;
import com.adx.jogy34.tardis.actions.enums.TardisActions;
import com.adx.jogy34.tardis.menu.base.InventoryPage;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.schematics.enums.SchematicType;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class SelectRoomPage extends InventoryPage<String>
{
    public SelectRoomPage()
    {
        super(ServiceProvider.schematicService().getSchematics(SchematicType.ADDITIONAL_ROOM).Count() + 1, Text.SELECT_ROOM_TO_BUILD);
    }

    @Override
    protected void createPage()
    {
        ServiceProvider.schematicService().getSchematics(SchematicType.ADDITIONAL_ROOM).OrderBy(s -> s.getName()).ForEach(s -> {
            addItem(s.getIcon(), s.getConstant(), s.getName(), s.getDescription(), Text.format("{0}{COST}: {1}", ChatColor.GOLD, s.getCost()));
        });

        setItem(this.size - 1, CANCEL, null);
    }

    @Override
    public void reset()
    {
        new EList<>(this.inventory.getViewers()).ForEach(x -> x.closeInventory());
        this.initInventory(ServiceProvider.schematicService().getSchematics(SchematicType.ADDITIONAL_ROOM).Count() + 1);
        super.reset();
    }

    @Override
    protected void onInventoryClick(Player who, String what) 
    {
        if(what == null || what.isEmpty()) 
        {
            ServiceProvider.actionService().stopAction(who, TardisActions.BUILD_ROOM);
            who.closeInventory();
        }
        else
        {
            who.closeInventory();
            if(ServiceProvider.actionService().isPerformingAction(who, TardisActions.BUILD_ROOM))
            {
                ServiceProvider.actionService().performAction(who, new SetTextActionData("choose_room", what));
            }
        }
	}
}