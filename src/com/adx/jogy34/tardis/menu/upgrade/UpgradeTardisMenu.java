package com.adx.jogy34.tardis.menu.upgrade;

import com.adx.jogy34.tardis.menu.base.InventoryMenu;
import com.adx.jogy34.tardis.menu.base.interfaces.IInventoryMenuDialog;
import com.adx.jogy34.tardis.menu.customize.CustomizeTARDISMenu;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.tardis.TARDISAppearance;

import org.bukkit.entity.Player;

public class UpgradeTardisMenu extends InventoryMenu<UpgradeTardisDialogues>
{
    public UpgradeTardisMenu()
    {
        super(UpgradeTardisDialogues.MAIN);
    }

    @Override
    protected void createMenu() 
    {
        addPage(UpgradeTardisDialogues.MAIN, new UpgradeTardisMainPage());
        addPage(UpgradeTardisDialogues.CHANGE_CONTROL_ROOM, new UpgradeControlRoom());
        addPage(UpgradeTardisDialogues.MISCELANEOUS, new UpgradeTardisMiscellaneous());
        addPage(UpgradeTardisDialogues.SET_HALLWAY_TYPE, new ChangeHallwayTypeMenu());
    }

    @Override
    public void navigateTo(Player who, UpgradeTardisDialogues dialogue) 
    {
        if(dialogue == UpgradeTardisDialogues.CHANGE_APPEARANCE) 
        {
            CustomizeTARDISMenu appearance = ServiceProvider.inventoryMenuService().getCustomizeTardisMenu(who.getUniqueId());
            appearance.initializeHierarchy(this);
            appearance.update(who);
            appearance.open(who);
        }
        super.navigateTo(who, dialogue);
    }

    @Override
    public void setData(Player who, Object data, String reason, IInventoryMenuDialog from) 
    {
        if(from instanceof CustomizeTARDISMenu)
        {
            switch(reason)
            {
                case "confirm":
                {
                    if(data instanceof TARDISAppearance)
                    {
                        TARDIS t = TARDISProvider.getTardisForPlayer(who);
                        if(t != null)
                        {
                            t.getSettings().update((TARDISAppearance) data);
                        }
                    }
                }
                case "cancel":
                default:
                {
                    this.open(who);
                    break;
                }
            }
        }
    }
}