package com.adx.jogy34.tardis.menu.upgrade;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.menu.base.InventoryPage;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.schematics.TardisSchematic;
import com.adx.jogy34.tardis.schematics.enums.AuxillaryDataType;
import com.adx.jogy34.tardis.schematics.enums.SchematicType;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class UpgradeControlRoom extends InventoryPage<String>
{
    public UpgradeControlRoom() 
    {
        super(ServiceProvider.schematicService().getSchematics(SchematicType.CONTROL_ROOM).Count() + 1, Text.UPGRADE_CONTROL_ROOM);
    }

    @Override
    protected void createPage() 
    {
        ServiceProvider.schematicService()
            .getSchematics(SchematicType.CONTROL_ROOM)
            .OrderBy(s -> s.getAuxillaryData(AuxillaryDataType.CONTROL_ROOM_ORDER).getInt())
            .ForEach(s -> 
            {
                addItem(s.getIcon(), s.getConstant(), s.getName(), s.getDescription(), Text.format("{0}{COST}: {1}", ChatColor.GOLD, s.getCost()));
            });

            setItem(this.size - 1, CANCEL, null);
    }

    @Override
    protected void onInventoryClick(Player who, String what) 
    {
        if(what == null)
        {
            this.getParentAs(UpgradeTardisMenu.class).navigateTo(who, UpgradeTardisDialogues.MAIN);
        }
        else
        {
            TARDIS t = TARDISProvider.getTardisForPlayer(who);
            if(t != null)
            {
                if(what.equals(t.getSettings().getInterior().getControlRoomConstant()))
                {
                    MessageService.say(who, Text.SELECTED_CURRENT_CONTROL_ROOM);
                }
                else
                {
                    TardisSchematic s = ServiceProvider.schematicService().getSchematic(what);
                    if(s != null)
                    {
                        if(s.getCost().pay(who))
                        {
                            who.closeInventory();
                            t.getSettings().getInterior().changeControlRoom(s);
                            MessageService.say(who, Text.UPGRADING_CONTROL_ROOM);
                        }
                        else
                        {
                            MessageService.say(who, Text.INSUFFICIENT_FUNDS);
                        }
                    }
                    else
                    {
                        Logger.error("Player selected control room upgrade to '" + what + "' but a schematic with that constant does not exist");
                    }
                }
            }
            else
            {
                MessageService.say(who, Text.NEED_TARDIS_FOR_THAT);
            }
        }
    }

    @Override
    public void reset()
    {
        new EList<>(this.inventory.getViewers()).ForEach(x -> x.closeInventory());
        this.initInventory(ServiceProvider.schematicService().getSchematics(SchematicType.CONTROL_ROOM).Count() + 1);
        super.reset();
    }
}