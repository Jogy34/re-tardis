package com.adx.jogy34.tardis.menu.actions;

public class TARDISCustomizeActions
{
    public static enum Navigation
    {
        SET_LANTERN, SET_PRIMARY, SET_SECONDARY, SET_DOOR,
        ;
    }
    
    public static enum MainPage
    {
        SET_LANTERN,
        SET_PRIMARY,
        SET_SECONDARY,
        SET_DOOR,
        TOGGLE_READY,
        CHANGE_POLICE_SIGNS,
        TOGGLE_POLICE_SIGNS,
        CHANGE_OWNER_SIGN,
        TOGGLE_OWNER_SIGN,
        CANCEL,
        ;
    }

    public static enum General
    {
        TOGGLE_READY,
        TOGGLE_POLICE_SIGNS,
        TOGGLE_OWNER_SIGN,
        CANCEL
        ;
    }

    public static enum UseSpawnEggAction
    {
        NONE,
        PREVIEW,
        CREATE
    }
}
