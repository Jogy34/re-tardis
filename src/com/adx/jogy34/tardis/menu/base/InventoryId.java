package com.adx.jogy34.tardis.menu.base;

import java.util.UUID;

import com.adx.jogy34.tardis.menu.base.interfaces.IInventoryPage;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class InventoryId implements InventoryHolder
{
    protected final UUID id;
    protected IInventoryPage page;
    
    public InventoryId(IInventoryPage page)
    {
        this(page, UUID.randomUUID());
    }
    
    public InventoryId(IInventoryPage page, UUID id)
    {
        this.id = id;
        this.page = page;
    }
    
    @Override
    public Inventory getInventory()
    {
        return page.getInventory();
    }
    
    public UUID getId()
    {
        return this.id;
    }
    
    public void notifyUse(Player who, ItemStack what)
    {
        this.page.onUse(who, what);
    }

    public void notifyClose(Player who)
    {
        this.page.onClose(who);
    }
}
