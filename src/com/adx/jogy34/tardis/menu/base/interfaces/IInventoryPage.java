package com.adx.jogy34.tardis.menu.base.interfaces;

import com.adx.jogy34.tardis.hierarchy.interfaces.IHierarchy;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public interface IInventoryPage extends IInventoryMenuDialog, IHierarchy<IInventoryMenuDialog>
{
    Inventory getInventory();
    void onUse(Player who, ItemStack what);
    void onClose(Player who);
    void reset();
}
