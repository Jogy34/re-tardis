package com.adx.jogy34.tardis.menu.base.interfaces;

import com.adx.jogy34.tardis.hierarchy.interfaces.IHierarchy;

import org.bukkit.entity.Player;

public interface IInventoryMenuDialog extends IHierarchy<IInventoryMenuDialog>
{
    void open(Player p);
    void setData(Player who, Object data, IInventoryMenuDialog from);
    void setData(Player who, Object data, String reason, IInventoryMenuDialog from);
    void init();
    void reset();
}
