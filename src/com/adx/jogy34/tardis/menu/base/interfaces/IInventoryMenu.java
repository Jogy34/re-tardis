package com.adx.jogy34.tardis.menu.base.interfaces;

import com.adx.jogy34.tardis.hierarchy.interfaces.IHierarchy;

import org.bukkit.entity.Player;

public interface IInventoryMenu extends IInventoryMenuDialog, IHierarchy<IInventoryMenuDialog>
{
    void open(Player p);
    void onClose(Player p);
}
