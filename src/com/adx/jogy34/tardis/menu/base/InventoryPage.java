package com.adx.jogy34.tardis.menu.base;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.hierarchy.Hierarchy;
import com.adx.jogy34.tardis.menu.base.interfaces.IInventoryMenuDialog;
import com.adx.jogy34.tardis.menu.base.interfaces.IInventoryPage;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.util.helpers.IntHelper;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.Text;

public abstract class InventoryPage<Indicator> extends Hierarchy<IInventoryMenuDialog> implements IInventoryPage
{
    public static final int MAX_INVENTORY_PAGE_SIZE = 9 * 6;
    public static final int MIN_INVENTORY_PAGE_SIZE = 9;
    public static final String TARDIS_MENU_PREFIX =  String.format("%s[TARDIS]%s%s ", ChatColor.DARK_BLUE, ChatColor.RESET, ChatColor.BLACK);
    protected static final ItemStack EMPTY;
    protected static final ItemStack CANCEL;
    
    protected Inventory inventory;
    protected String inventoryName;
    protected InventoryId id;
    protected int size;
    protected EMap<Integer, Indicator> onItemClick = new EMap<Integer, Indicator>();

    public InventoryPage(int size, String name)
    {
        inventoryName = String.format("%s%s", TARDIS_MENU_PREFIX, name);
        this.initInventory(size);
    }
    
    protected abstract void createPage();
    protected abstract void onInventoryClick(Player who, Indicator what);
    
    protected void fillSection(int row, int column, int width, int height, ItemStack item, Indicator onClick)
    {
        for(int i = row; i < row + height; i++)
        {
            for(int j = column; j < column + width; j++)
            {
                this.setItem(i, j, item);
            }
        }
        this.onItemClick.put(ServiceProvider.itemService().hashItem(item), onClick);
    }

    public void init()
    {
        this.createPage();
        this.fillEmpty();
    }

    protected void initInventory(int size)
    {
        if(this.inventory != null) this.inventory.clear();

        this.size = IntHelper.toNineDenom(size);
        this.id = new InventoryId(this);
        this.inventory = Bukkit.createInventory(id, Math.min(Math.max(this.size, MIN_INVENTORY_PAGE_SIZE), MAX_INVENTORY_PAGE_SIZE), inventoryName);
    }
    
    protected void setItem(int index, ItemStack item)
    {
        inventory.setItem(index, item);
    }

    protected void setItem(int index, ItemStack item, Indicator onClick)
    {
        inventory.setItem(index, item);
        this.onItemClick.put(ServiceProvider.itemService().hashItem(item), onClick);
    }

    private void setItem(int row, int column, ItemStack item)
    {
        inventory.setItem((row * 9) + column, item);
    }
    
    protected void setItem(int row, int column, ItemStack item, Indicator onClick)
    {
        inventory.setItem((row * 9) + column, item);
        this.onItemClick.put(ServiceProvider.itemService().hashItem(item), onClick);
    }

    protected void setItem(int row, int column, Indicator onClick, Material type, String displayName, String... lore)
    {
        this.setItem(row, column, createItem(type, displayName, lore), onClick);
    }
    
    protected void addItem(Material item, Indicator onClick)
    {
        this.addItem(new ItemStack(item), onClick);
    }
    
    protected void addItem(Material item, Indicator onClick, String display, String... lore)
    {
        this.addItem(createItem(item, display, lore), onClick);
    }
    
    protected void addItem(ItemStack item, Indicator onClick)
    {
        inventory.setItem(inventory.firstEmpty(), item);
        this.onItemClick.put(ServiceProvider.itemService().hashItem(item), onClick);
    }

    protected ItemStack createItem(Material type, String displayName, String... lore)
    {
        ItemStack i = new ItemStack(type);
        ItemMeta meta = i.getItemMeta();
        if(displayName != null) meta.setDisplayName(ChatColor.RESET + displayName);
        if(lore.length > 0) meta.setLore(Arrays.asList(lore));
        i.setItemMeta(meta);
        return i;
    }
    
    public void onUse(Player who, ItemStack what)
    {
        if(what != null && what.getType() != Material.AIR)
        {
            int hash = ServiceProvider.itemService().hashItem(what);
            if(this.onItemClick.containsKey(hash))
            {
                this.onInventoryClick(who, this.onItemClick.get(hash));
            }
        }
    }

    protected void fillEmpty() 
    {
        int firstEmpty = -1;
        while((firstEmpty = inventory.firstEmpty()) >= 0)
        {
            inventory.setItem(firstEmpty, EMPTY);
        }
    }
    
    public void open(Player p)
    {
        p.closeInventory();
        p.openInventory(inventory);
    }
    
    public Inventory getInventory()
    {
        return this.inventory;
    }
    
    public void reset()
    {
        try
        {
            new EList<>(this.inventory.getViewers()).ForEach(x -> x.closeInventory());
            this.inventory.clear();
            this.onItemClick.clear();
            this.createPage();
            this.fillEmpty();
        }
        catch(Exception e)
        {
            Logger.error(e);
        }
    }

    @Override
    public void setData(Player who, Object data, IInventoryMenuDialog from) { this.setData(who, data, null, from); }
    @Override
    public void setData(Player who, Object data, String reason, IInventoryMenuDialog from) {}
    
    @Override
    public void onClose(Player who) { }

    @Override
    public void initializeHierarchy() { }

    static 
    {
        EMPTY = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
        ItemMeta emptyMeta = EMPTY.getItemMeta();
        emptyMeta.setDisplayName(ChatColor.BLACK + " ");
        EMPTY.setItemMeta(emptyMeta);

        CANCEL = new ItemStack(Material.BARRIER);
        ItemMeta cancelMeta = CANCEL.getItemMeta();
        cancelMeta.setDisplayName(Text.format("{0}{CANCEL}", ChatColor.RED));
        CANCEL.setItemMeta(cancelMeta);
    }
}
