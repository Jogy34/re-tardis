package com.adx.jogy34.tardis.menu.base;

import org.bukkit.entity.Player;

import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.hierarchy.Hierarchy;
import com.adx.jogy34.tardis.menu.base.interfaces.IInventoryMenu;
import com.adx.jogy34.tardis.menu.base.interfaces.IInventoryMenuDialog;

public abstract class InventoryMenu<T> extends Hierarchy<IInventoryMenuDialog> implements IInventoryMenu
{
    protected final EMap<T, IInventoryMenuDialog> dialogues = new EMap<T, IInventoryMenuDialog>();
    protected T first;
    
    public InventoryMenu(T first)
    {
        this(first, true);
    }

    public InventoryMenu(T first, boolean initImmediately)
    {
        this.first = first;
        if(initImmediately) init();
    }

    public void init()
    {
        createMenu();
        this.initializeHierarchy();
        this.dialogues.ForEach(x -> x.getValue().init());
    }
    
    protected abstract void createMenu();
    
    public void open(Player p)
    {
        dialogues.get(this.first).open(p);
    }

    protected void addPage(T key, IInventoryMenuDialog dialog)
    {
        dialogues.put(key, dialog);
    }

    @Override
    public void onClose(Player p) { }

    public void navigateTo(Player who, T dialogue)
    {
        if(dialogue == null)
        {
            who.closeInventory();
        }
        else if(dialogues.containsKey(dialogue))
        {
            who.closeInventory();
            dialogues.get(dialogue).open(who);
        }
    }

    @Override
    public void initializeHierarchy() 
    {
        this.dialogues.ForEach(x -> x.getValue().initializeHierarchy(this));
    }

    @Override
    public void reset()
    {
        this.dialogues.ForEach(x -> x.getValue().reset());
    }

    @Override
    public void setData(Player who, Object data, IInventoryMenuDialog from) { this.setData(who, data, null, from); }
    @Override
    public void setData(Player who, Object data, String reason, IInventoryMenuDialog from) {}
}
