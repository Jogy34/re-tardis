package com.adx.jogy34.tardis.menu.schematic;

import com.adx.jogy34.tardis.menu.actions.SelectSchematicTypeActions;
import com.adx.jogy34.tardis.menu.actions.SelectSchematicTypeActions.Pages;
import com.adx.jogy34.tardis.menu.base.InventoryMenu;

public class SelectSchematicTypeMenu extends InventoryMenu<SelectSchematicTypeActions.Pages>
{
    public SelectSchematicTypeMenu()
    {
        super(Pages.MAIN);
    }

    @Override
    protected void createMenu() 
    {
        addPage(Pages.MAIN, new SelectSchematicTypeMainPage());
    }
}