package com.adx.jogy34.tardis.menu.schematic;

import com.adx.jogy34.tardis.actions.CreateSchematicAction;
import com.adx.jogy34.tardis.menu.base.InventoryPage;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.schematics.enums.SchematicType;

import org.bukkit.entity.Player;

public class SelectSchematicTypeMainPage extends InventoryPage<SchematicType> 
{
    public SelectSchematicTypeMainPage()
    {
        super(InventoryPage.MIN_INVENTORY_PAGE_SIZE, "Schematic Type");
    }

    @Override
    protected void createPage() 
    {
        for(SchematicType type : SchematicType.values())
        {
            addItem(type.ICON, type, type.NAME);
        }
    }

    @Override
    protected void onInventoryClick(Player who, SchematicType what) 
    {
        System.out.println(what);
        if(!ServiceProvider.actionService().isPerformingAction(who))
        {
            ServiceProvider.actionService().startPerformingAction(who, new CreateSchematicAction(who.getUniqueId(), what));
        }
        who.closeInventory();
	}
}