package com.adx.jogy34.tardis.enums;

import org.bukkit.inventory.ItemStack;

import com.adx.jogy34.tardis.providers.ServiceProvider;

public enum TARDISItems
{
    TARDIS_EGG,
    
    // Schematic Selection Items
    SCHEMATIC_SELECT_ANCHOR_POINTS,
    SCHEMATIC_SELECT_ACCESS_POINTS,

    // Debug Items
    MEASURING_STICK,
    TELEPORT_STICK,

    // Loot Items,
    DIAMOND_PLATED_ELYTRA,
    TOAST,
    BURNT_TOAST,

    NONE
    ;
    
    private Integer hash = null, hashWithoutAmount = null;

    public ItemStack get(String... addlLore)
    {
        return ServiceProvider.itemService().getItem(this, addlLore);
    }

    public ItemStack getWithName(String name)
    {
        return ServiceProvider.itemService().getItemWithName(this, name);
    }

    public int getHashCode()
    {
        return getHashCode(true);
    }

    public int getHashCode(boolean includeAmount)
    {
        if(hash == null || hashWithoutAmount == null) 
        {
            ItemStack item = get();
            hash = ServiceProvider.itemService().hashItem(item, true);
            hashWithoutAmount = ServiceProvider.itemService().hashItem(item, false);
        }
        return includeAmount ? hash : hashWithoutAmount;
    }
}