package com.adx.jogy34.tardis.enums;

import com.adx.jogy34.tardis.util.Text;

import org.bukkit.Material;

public enum HallwayType
{
    DIRECT(Text.DIRECT, Material.END_ROD, Text.DIRECT_DESC),
    DYNAMIC(Text.DYNAMIC, Material.STRING, Text.DYNAMIC_DESC),
    ;

    public final String NAME;
    public final Material ICON;
    public final String[] DESCRIPTION;

    private HallwayType(String name, Material icon, String... description)
    {
        this.NAME = name;
        this.ICON = icon;
        this.DESCRIPTION = description;
    }
}