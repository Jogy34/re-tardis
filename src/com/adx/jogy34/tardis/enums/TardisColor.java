package com.adx.jogy34.tardis.enums;

import java.util.Map.Entry;

import com.adx.jlinq.EList;
import com.adx.jlinq.tuples.Tuples.Tuple;
import com.adx.jogy34.tardis.data.TardisPhaseData;
import com.adx.jogy34.tardis.tardis.TARDIS;

import org.bukkit.Material;

public enum TardisColor 
{
    YELLOW, BROWN, BLACK, BLUE, PURPLE, WHITE, RED, GREEN;

    public static Tuple<TardisColor, TardisColor> from(TARDIS t) 
    {
        TardisColor primary = null, secondary = null;
        Material primaryMat = t.getSettings().getAppearance().getPrimaryMaterial();
        Material secondaryMat = t.getSettings().getAppearance().getSecondaryMaterial();

        for (Entry<TardisColor, EList<Material>> x : TardisPhaseData.COLOR_BLOCK_MAP)
        {
            if(primary == null && x.getValue().contains(primaryMat)) primary = x.getKey();
            if(secondary == null && x.getValue().contains(secondaryMat)) secondary = x.getKey();

            if(primary != null && secondary != null) break;
        }

        if(primary == null) primary = BLUE;
        if(secondary == null) secondary = WHITE;

        return new Tuple<TardisColor, TardisColor>(primary, secondary);
    }
}