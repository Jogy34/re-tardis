package com.adx.jogy34.tardis.enums;

import com.adx.jogy34.tardis.util.Text;

import org.bukkit.ChatColor;

public enum PhaseState
{
    IN_PHASE(ChatColor.RED, Text.OFF),
    PHASING(ChatColor.YELLOW, "Phasing"),
    SOLID(ChatColor.GREEN, Text.ON);

    private final ChatColor color;
    private final String text;

    private PhaseState(ChatColor color, String text)
    {
        this.color = color;
        this.text = text;
    }

    public ChatColor getColor()
    {
        return color;
    }

    public String getText()
    {
        return text;
    }
}