package com.adx.jogy34.tardis.enums;

public enum TARDISSchematicVersion
{
    V_1,
    ;

    public static final TARDISSchematicVersion CURRENT = V_1; 
}