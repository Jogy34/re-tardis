package com.adx.jogy34.tardis.enums;

import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.spacial.Position;

public enum TardisCantLandReason
{
    NONE,
    TO_LOW,
    TO_HIGH,
    BLOCKED_0,
    BLOCKED_1,
    BLOCKED_2,
    BLOCKED_3,
    BLOCKED_4,
    BLOCKED_5,
    NO_FLY_CHUNK,
    PHASING
    ;

    public static TardisCantLandReason blocked(int height)
    {
        switch(height)
        {
            case 0: return BLOCKED_0; 
            case 1: return BLOCKED_1; 
            case 2: return BLOCKED_2; 
            case 3: return BLOCKED_3; 
            case 4: return BLOCKED_4; 
            default: 
            case 5: return BLOCKED_5; 
        }
    }

    public Position adjust(Position p, TARDIS t, int maxHeight)
    {
        p = p.clone();
        switch(this)
        {
            case TO_LOW:
            {
                p.setY(0);
                break;
            }
            case NO_FLY_CHUNK:
            {
                int cx = (Math.abs(p.getX()) - 1) % 16;
                if(cx <= 0) cx = 15;
                p = p.add(cx + 1, 0, 0);
                boolean floating = true;

                while(floating && p.getY() > 1) 
                {
                    for(int x = -1; x <= 1 && floating; x++)
                    {
                        for(int z = -1; z <= 1 && floating; z++)
                        {
                            if(!t.canOverrite(p.getBlockRelative(x, -1, z)))
                            {
                                floating = false;
                            }
                        }
                    }
        
                    if(floating) p = p.add(0, -1, 0);
                }
                //Fall through
            }
            case BLOCKED_0:
            case BLOCKED_1:
            case BLOCKED_2:
            case BLOCKED_3:
            case BLOCKED_4:
            case BLOCKED_5:
            {
                if(this == BLOCKED_0) p = p.add(0, 1, 0);
                if(this == BLOCKED_1) p = p.add(0, 2, 0);
                if(this == BLOCKED_2) p = p.add(0, 3, 0);
                if(this == BLOCKED_3) p = p.add(0, 4, 0);
                if(this == BLOCKED_4) p = p.add(0, 5, 0);
                if(this == BLOCKED_5) p = p.add(0, 6, 0);
                //Fall through... again
            }
            case TO_HIGH:
            {
                while(p.getY() > maxHeight || !t.canOverrite(p.getBlock())) 
                {
                    p = p.add(0, 1, 0);
                    if(p.getY() > maxHeight) {
                        p.setY(1);
                        p.add(3, 0, 0);
                    }
                }
                break;
            }
            default:
            case NONE: 
            case PHASING: 
            {
                break;
            }
        }

        return p;
    }

    public boolean isBlocked()
    {
        return this == BLOCKED_0 || this == BLOCKED_1 || this == BLOCKED_2 ||
               this == BLOCKED_3 || this == BLOCKED_4 || this == BLOCKED_5;
    }

    public boolean isValid()
    {
        return this == NONE || this == PHASING;
    }
}