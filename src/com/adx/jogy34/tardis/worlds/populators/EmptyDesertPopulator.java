package com.adx.jogy34.tardis.worlds.populators;

import java.util.Random;

import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.generator.BlockPopulator;

public class EmptyDesertPopulator extends BlockPopulator {

	@Override
    public void populate(World world, Random random, Chunk source) 
    {
        for(int i = 0; i < 16; i++)
        {
            for(int j = 0; j < 16; j++)
            {
                for(int k = 0; k < world.getMaxHeight(); k++)
                {
                    world.setBiome(source.getX() * 16 + i, k, source.getZ() * 16 + j, Biome.DESERT);
                }
            }
        }
	}
}