package com.adx.jogy34.tardis.worlds;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.worlds.populators.EmptyDesertPopulator;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

public class EmptyGenerator extends ChunkGenerator
{
    protected EList<BlockPopulator> _populators = new EList<BlockPopulator>(Arrays.asList(new EmptyDesertPopulator()));
    protected Location _spawnLocation = null;

    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) { return _populators; }

    @Override
    public boolean canSpawn(World world, int x, int z) { return false; }

    @Override
    public ChunkData generateChunkData(World world, Random random, int x, int z, BiomeGrid biome) 
    {
        return createChunkData(world);
    }



    @Override
    public Location getFixedSpawnLocation(World world, Random random) 
    {
        if(_spawnLocation == null) _spawnLocation = new Location(world, 0, 64, 0);
        return _spawnLocation;
    }

    
}