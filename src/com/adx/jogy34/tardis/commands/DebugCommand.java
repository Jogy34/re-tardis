package com.adx.jogy34.tardis.commands;

import java.text.MessageFormat;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.enums.TARDISItems;
import com.adx.jogy34.tardis.enums.TARDISWorlds;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.schematics.AccessPoint;
import com.adx.jogy34.tardis.schematics.TardisSchematic;
import com.adx.jogy34.tardis.schematics.enums.AccessPointType;
import com.adx.jogy34.tardis.schematics.enums.SchematicAnchorLocation;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.services.TaskService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.tasks.base.DebugTask;
import com.adx.jogy34.tardis.tasks.base.SerialTask;
import com.adx.jogy34.tardis.tasks.base.WaitTask;
import com.adx.jogy34.tardis.tasks.concrete.TardisPhaseTask;
import com.adx.jogy34.tardis.util.spacial.Point;
import com.adx.jogy34.tardis.util.spacial.Position;
import com.adx.jogy34.tardis.util.Encode;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.Timespan;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class DebugCommand extends TardisCommand 
{
    public DebugCommand()
    {
        super("Debug", "tardis.admin", AllowedCommandSender.BOTH);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        if(args.length == 0) return false;
        if(sender instanceof Player)
        {
            Player p = (Player) sender;
            switch(args[0].toLowerCase())
            {
                case "world":
                {
                    TARDISWorlds world = TARDISWorlds.TARDIS_WORLD;
                    if(args.length > 1)
                    {
                        try 
                        {
                            world = TARDISWorlds.valueOf(args[1].toUpperCase());
                        } 
                        catch(Exception e) 
                        {
                            MessageService.say(p, "Specified world is not correct");
                            return true;
                        }
                    }

                    World w = world == TARDISWorlds.NONE ? Bukkit.getWorlds().get(0) : ServiceProvider.worldService().getWorld(world);
                    p.teleport(w.getSpawnLocation());
                }
                case "schematic":
                {
                    if(args.length > 1)
                    {
                        TardisSchematic schematic = ServiceProvider.schematicService().getSchematic(args[1]);
                        if(schematic != null)
                        {
                            MessageService.say(p, "Building " + schematic.getName());
                            schematic.build(new Position(p.getLocation()), SchematicAnchorLocation.CENTER_BOTTOM, true);
                        }
                        else
                        {
                            MessageService.say(p, "Specified schematic can not be found");
                        }
                    }
                    else
                    {
                        MessageService.say(p, "Please specify a schematic to build");
                    }
                    break;
                }
                case "tardis-data":
                {
                    Logger.debug(TARDISProvider.getTardisForPlayer(p.getUniqueId()));
                    break;
                }
                case "items":
                {
                    p.getInventory().addItem(TARDISItems.MEASURING_STICK.get());
                    p.getInventory().addItem(TARDISItems.TELEPORT_STICK.get());
                    break;
                }
                case "sethallway":
                {
                    Block target = p.getTargetBlock(null, 5);
                    if(target != null && AccessPointType.HALLWAY_ACCESS.isValidMaterial(target.getType()))
                    {
                        TARDIS t = TARDISProvider.getTardisForPlayer(p);
                        t.addAccessPoint(new AccessPoint(new Point(target), AccessPointType.HALLWAY_ACCESS));
                        MessageService.say(p, "Set target to a hallway");
                    }
                    else
                    {
                        MessageService.say(p, "Target block not valid");
                    }
                    break;
                }
                case "test":
                {
                    //Testing some functionality
                    p.getInventory().addItem(TARDISItems.DIAMOND_PLATED_ELYTRA.get());
                    break;
                }
                case "phase-test":
                {
                    TARDIS t = TARDISProvider.getTardisForPlayer(p.getUniqueId());
                    TaskService.scheduleTask(new TardisPhaseTask(t, () -> t.build()));
                    break;
                }
            }
        }

        switch(args[0].toLowerCase())
        {
            case "encode80":
            {
                try
                {
                    MessageService.say(sender, "Encoded Integer: " + Encode.base80Encode(Integer.parseInt(args[1])));
                }
                catch(Exception e) 
                {
                    Logger.debug(e);
                }
                break;
            }
            case "log":
            {
                int logAmount = 5;
                try
                {
                    logAmount = Integer.parseInt(args[1]);
                }
                catch(Exception e) {}
                
                Object[] messages = new String[logAmount > 0 ? logAmount : 5];
                for(int i = 0; i < messages.length; i++) messages[i] = "Debug Test Message " + i;
                Logger.debug(messages);
                break;
            }
            case "ordertext":
            {
                Text.setLocale(null, true);
                break;
            }
            case "test":
            {
                //Testing some functionality
                
                break;
            }
            case "permissions":
            {
                if(args.length > 2)
                {
                    Player p = Bukkit.getPlayer(args[2]);
                    if(p == null) MessageService.say(sender, args[2] + " does not exist");
                    else Logger.debug(MessageFormat.format("{0} {1}: {2}", p.getName(), args[1], p.hasPermission(args[1])));
                }
                else
                {
                    MessageService.say(sender, "permissions [permission] [player]");
                }
                break;
            }
            case "task":
            {
                TaskService.scheduleTask(new SerialTask(
                    new DebugTask("Task Starting"),
                    new WaitTask(Timespan.fromSeconds(2)),
                    new DebugTask("2 Seconds passed"),
                    new WaitTask(Timespan.fromSeconds(5)),
                    new DebugTask("5 more Seconds passed"),
                    new WaitTask(Timespan.fromSeconds(10)),
                    new DebugTask("10 more Seconds passed")
                ));
                break;
            }
        }

        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) 
    {
        switch(args.length)
        {
            case 1:
            {
                return new String[] {"World", "Encode80", "Log", "Schematic", "Tardis-Data", "Sign", 
                    "OrderText", "Test", "Items", "sethallway", "Permissions", "Task", "Phase-Test"};
            }
            case 2:
            {
                switch(args[0].toLowerCase())
                {
                    case "world":
                    {
                        return new EList<TARDISWorlds>(TARDISWorlds.values()).Select(x -> x.toString()).ToArray(String.class);
                    }
                    case "schematic":
                    {
                        return ServiceProvider.schematicService().getSchematics().Select(x -> x.getConstant()).ToArray(String.class);
                    } 
                }
                break;
            }
        }

        return null;
	}

    @Override
    public String getUsage() 
    {
        return "<debugCommand>";
    }

    @Override
    public String getDescription() 
    {
        return "You should not be able to see this, if you can please report it to the plugin developer";
    }
}