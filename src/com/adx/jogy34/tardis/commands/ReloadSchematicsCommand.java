package com.adx.jogy34.tardis.commands;

import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.providers.ServiceProvider;

import org.bukkit.command.CommandSender;

@TardisCommandParam
public class ReloadSchematicsCommand extends TardisCommand
{
    public ReloadSchematicsCommand()
    {
        super("ReloadSchematics", "tardis.schematics", AllowedCommandSender.BOTH);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        ServiceProvider.schematicService().reload();
        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) {
        return null;
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public String getDescription() {
		return "Reloads all schematics in the Schematics folder";
	}
}