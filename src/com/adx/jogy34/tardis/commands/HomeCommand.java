package com.adx.jogy34.tardis.commands;

import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.enums.TardisCantLandReason;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class HomeCommand extends TardisCommand
{
    public HomeCommand()
    {
        super(Text.HOME, AllowedCommandSender.PLAYER_WITH_TARDIS);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        TARDIS t = TARDISProvider.getTardisForPlayer((Player) sender);
        switch(args.length)
        {
            case 0:
            {
                TardisCantLandReason goHomeReason = t.goHome();
    
                if(!goHomeReason.isValid()) MessageService.say(sender, "TARDISes home location appears to be blocked");
                else if(goHomeReason == TardisCantLandReason.PHASING) MessageService.say(sender, "Tardis is busy");
                else MessageService.say(sender, "Tardis sent home");
                return true;
            }
            case 1:
            {
                if(args[0].equalsIgnoreCase(Text.SET))
                {
                    t.setHome();
                    MessageService.say(sender, Text.format("{SET_HOME}", t.getHome()));
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) 
    {
        switch(args.length)
        {
            case 0:
            case 1:
            {
                return new String[] { Text.SET };
            }
        }

        return null;
    }

    @Override
    public String getUsage() 
    {
        return Text.format("<{SET}>");
    }

    @Override
    public String getDescription() 
    {
		return Text.HOME_DESC;
	}
} 