package com.adx.jogy34.tardis.commands;

import java.util.UUID;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.actions.data.PerformCommandData;
import com.adx.jogy34.tardis.actions.data.SetTextActionData;
import com.adx.jogy34.tardis.actions.enums.TardisActions;
import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.menu.schematic.SelectSchematicTypeMenu;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.helpers.ArrayHelper;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class SchematicCommand extends TardisCommand 
{
    public SchematicCommand()
    {
        super(Text.SCHEMATIC, "tardis.schematics", AllowedCommandSender.PLAYER);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        if(sender instanceof Player)
        {
            Player p = (Player) sender;
            UUID playerId = p.getUniqueId();
            if(args.length == 0)
            {
                if(ServiceProvider.actionService().isPerformingAction(playerId, TardisActions.SELECT_SCHEMATIC))
                {
                    MessageService.say(p, Text.ALREAY_CREATING_SCHEMATIC);
                }
                else
                {
                    ServiceProvider.inventoryMenuService().getMenu(SelectSchematicTypeMenu.class).open(p);
                }
                return true;
            }
            else
            {
                if(!ServiceProvider.actionService().isPerformingAction(playerId, TardisActions.SELECT_SCHEMATIC))
                {
                    MessageService.say(p,Text.NOT_CREATING_SCHEMATIC);
                    return true;
                }
                else
                {
                    String arg = args[0].toLowerCase();
                    if(arg.equals(Text.CANCEL.toLowerCase()))
                    {
                        ServiceProvider.actionService().stopAction(playerId);
                        MessageService.say(p, Text.HALTING_SCHEMATIC);
                        return true;
                    }
                    else if(arg.equals(Text.SAVE.toLowerCase()))
                    {
                        if(args.length > 1)
                        {
                            ServiceProvider.actionService().performAction(playerId, new SetTextActionData("name", args[1]));
                            ServiceProvider.actionService().completeAction(playerId);
                        }
                        else 
                        {
                            MessageService.say(p, "Please supply a name for the schematic");
                        }
                        return true;
                    }
                    else if(arg.equals(Text.VALIDATE.toLowerCase()) || arg.equals(Text.SET.toLowerCase()) || arg.equals(Text.REPLACE.toLowerCase()))
                    {
                        ServiceProvider.actionService().performAction(playerId, new PerformCommandData(args[0], ArrayHelper.subArray(args, 1)));
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    @SuppressWarnings("deprecation")
    public String[] tabComplete(CommandSender sender, String[] args) 
    {
        Player p = (Player) sender;
        if(ServiceProvider.actionService().isPerformingAction(p.getUniqueId(), TardisActions.SELECT_SCHEMATIC))
        {
            switch(args.length)
            {
                case 0:
                case 1:
                {
                    return new String[] {Text.SAVE, Text.VALIDATE, Text.SET, Text.CANCEL, Text.REPLACE};
                }
                case 2:
                {
                    String arg = args[0].toLowerCase();
                    if(arg.equals(Text.SAVE.toLowerCase()))
                    {
                        return new String[] { Text.format("[{CONSTANT}]") };
                    }
                    else if(arg.equals(Text.SET.toLowerCase()))
                    {
                        return new String[] { Text.COST, Text.DESCRIPTION, Text.ICON, Text.ITERATION, "[...]" };
                    }
                }
                case 3:
                {
                    String arg = args[0].toLowerCase();
                    if(arg.equals(Text.SET.toLowerCase())) 
                    {
                        String toSet = args[1].toLowerCase();
                        if(toSet.equals(Text.COST.toLowerCase()) || toSet.equals(Text.ICON.toLowerCase())) 
                        {
                            return new EList<>(Material.values()).Where(x -> !x.isLegacy()).Select(x -> x.toString()).ToArray(String.class);
                        }
                        else
                        {
                            return new String[] { Text.format("[{VALUE}]")};
                        }
                    }
                }
            }
        }

        return null;
    }

    @Override
    public String getUsage() 
    {
        return "<Save|Validate|Set|Cancel> <Name|Data>";
    }

	@Override
    public String getDescription() 
    {
		return "Create a schematic for a TARDIS room";
	}
}