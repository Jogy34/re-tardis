package com.adx.jogy34.tardis.commands.interfaces;

import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public interface ITardisCommand
{
    public static enum AllowedCommandSender
    {
        PLAYER(true, false, false),
        PLAYER_WITH_TARDIS(true, false, true),
        CONSOLE(false, true, false),
        BOTH(true, true, false)
        ;

        private boolean allowsPlayer, allowsConsole, needsTardis;
        private AllowedCommandSender(boolean allowsPlayer, boolean allowsConsole, boolean needsTardis)
        {
            this.allowsPlayer = allowsPlayer;
            this.allowsConsole = allowsConsole;
            this.needsTardis = needsTardis;
        }

        public boolean allowsPlayer() { return this.allowsPlayer; }
        public boolean allowsConsole() { return this.allowsConsole; }
        public boolean needsTardis() { return this.needsTardis; }

        public String isValid(CommandSender sender)
        {
            boolean isPlayer = sender instanceof Player;
            if(isPlayer)
            {
                if(!this.allowsPlayer) return Text.CANT_BE_PLAYER_FOR_COMMAND;
                if(this.needsTardis && TARDISProvider.getTardisForPlayer((Player) sender) == null) return Text.NEED_TARDIS_FOR_COMMAND;
                return null;
            }
            else
            {
                return this.allowsConsole ? null : Text.MUST_BE_PLAYER_FOR_COMMAND;
            }
        }
    }

    String getCommandKey();
    AllowedCommandSender getAllowedCommandSender();
    boolean execute(CommandSender sender, String[] args);
    String[] tabComplete(CommandSender sender, String[] args);
    IEnumerable<String> getAliases();
    String getUsage();
    String getDescription();
    String getPermission();
    String isValid(CommandSender sender);
}