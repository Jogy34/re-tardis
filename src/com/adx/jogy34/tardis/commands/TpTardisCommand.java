package com.adx.jogy34.tardis.commands;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class TpTardisCommand extends TardisCommand
{
    public TpTardisCommand()
    {
        super(Text.TP, AllowedCommandSender.PLAYER_WITH_TARDIS);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        if(args.length > 0)
        {
            Player to = Bukkit.getPlayer(args[0]);
            if(to != null) 
            {
                TARDIS t = TARDISProvider.getTardisForPlayer((Player) sender);
                t.land(to.getLocation().add(5, 0, 5));
                return true;
            }
        }

        MessageService.say(sender, Text.SPECIFY_VALID_PLAYER);

        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) 
    {
        switch(args.length)
        {
            case 0:
            case 1:
            {
                return new EList<>(Bukkit.getOnlinePlayers()).Select(x -> x.getName()).ToArray(String.class);
            }
        }

        return null;
    }

    @Override
    public String getUsage() {
        return Text.format("[{PLAYER}]");
    }

    @Override
    public String getDescription() {
		return Text.TP_DESC;
	}
}