package com.adx.jogy34.tardis.commands;

import java.text.MessageFormat;

import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.spacial.Position;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class FindCommand extends TardisCommand
{
    public FindCommand()
    {
        super("Find", AllowedCommandSender.PLAYER_WITH_TARDIS);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        Player p = (Player) sender;
        TARDIS t = TARDISProvider.getTardisForPlayer(p);

        Position pos = t.getPosition();

        MessageService.say(p, MessageFormat.format("{0}: ({1}, {2}, {3})", pos.getWorld().getName(), pos.getX(), pos.getY(), pos.getZ()));

        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) {
        return null;
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public String getDescription() {
		return "Tells you where your TARDIS currently is";
	}


}