package com.adx.jogy34.tardis.commands;

import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class CancelCommand extends TardisCommand
{
    public CancelCommand()
    {
        super(Text.CANCEL, AllowedCommandSender.PLAYER);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        Player p = (Player) sender;
        if(ServiceProvider.actionService().isPerformingAction(p))
        {
            ServiceProvider.actionService().stopAction(p);
            MessageService.say(p, Text.CANCELLED_ACTIONS);
        }
        else
        {
            MessageService.say(p, Text.NOT_PERFORMING_ACTION);
        }
        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) {
        return null;
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public String getDescription() {
		return Text.CANCEL_DESC;
	}
}