package com.adx.jogy34.tardis.commands;

import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.tardis.TARDIS;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class RebuildCommand extends TardisCommand 
{

    public RebuildCommand()
    {
        super("Rebuild", AllowedCommandSender.PLAYER_WITH_TARDIS);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        Player p = (Player) sender;
        TARDIS t = TARDISProvider.getTardisForPlayer(p.getUniqueId());
        if(t != null)
        {
            t.rebuild();
        }

        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) 
    {
        return null;
    }

    @Override
    public String getUsage() {
        return "";
    }

	@Override
	public String getDescription() {
		return "Rebuilds your TARDIS";
	}

}