package com.adx.jogy34.tardis.commands;

import com.adx.jlinq.EList;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jogy34.tardis.commands.interfaces.ITardisCommand;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class TardisCommand implements ITardisCommand
{
    protected String key;
    protected AllowedCommandSender allowed;
    protected EList<String> aliases;
    protected String permission;
    
    public TardisCommand(String key, AllowedCommandSender allowedCommandSender, String... aliases)
    {
        this(key, null, allowedCommandSender, aliases);
    }
    
    public TardisCommand(String key, String permission, AllowedCommandSender allowedCommandSender, String... aliases)
    {
        this.key = key;
        this.allowed = allowedCommandSender;   
        this.aliases = new EList<String>(aliases);
        this.permission = permission;
    }

    @Override
    public AllowedCommandSender getAllowedCommandSender() 
    {
        return this.allowed;
    }

    @Override
    public String getCommandKey() 
    {
        return this.key;
    }

    @Override
    public IEnumerable<String> getAliases() 
    {
        return this.aliases;
    }

    @Override
    public String getPermission()
    {
        return this.permission;
    }
    
    @Override
    public String isValid(CommandSender sender)
    {
        String msg = this.getAllowedCommandSender().isValid(sender);
        if(msg == null)
        {
            if(sender instanceof Player && this.getPermission() != null)
            {
                if(!((Player) sender).hasPermission(this.getPermission()))
                {
                    msg = "You do not have permission to use that command";
                }
            }
        }

        return msg;
    }
}