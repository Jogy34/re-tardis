package com.adx.jogy34.tardis.commands;

import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class BackCommand extends TardisCommand
{
    public BackCommand()
    {
        super(Text.BACK, AllowedCommandSender.PLAYER);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        Player p = (Player) sender;

        TARDIS in = TARDISProvider.getTardisForInterior(p.getLocation());
        if(in == null)
        {
            MessageService.say(p, Text.MUST_BE_INSIDE_TARDIS);
        }
        else
        {
            p.teleport(in.getSettings().getInterior().getSpawnLocation());
        }

        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) {
        return null;
    }

    @Override
    public String getUsage() {
        return "";
    }

    @Override
    public String getDescription() {
		return Text.BACK_DESC;
	}
}