package com.adx.jogy34.tardis.commands;

import java.util.UUID;

import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.enums.TardisCantLandReason;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.services.TaskService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.tasks.base.CallbackTask;
import com.adx.jogy34.tardis.tasks.base.SerialTask;
import com.adx.jogy34.tardis.tasks.base.WaitTask;
import com.adx.jogy34.tardis.util.Text;
import com.adx.jogy34.tardis.util.Timespan;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class AdminCommand extends TardisCommand {

    private EMap<UUID, UUID> deletingTardis = new EMap<UUID, UUID>();
    private UUID consoleId = new UUID(0, 0);

    public AdminCommand() {
        super(Text.ADMIN, "tardis.admin", AllowedCommandSender.BOTH);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length < 2)
            return false;

        TARDIS t = TARDISProvider.getTardisForPlayer(args[0]);
        if (t == null) {
            MessageService.say(sender, Text.PLAYER_DOES_NOT_HAVE_TARDIS);
            return true;
        }

        if (args[1].equalsIgnoreCase(Text.CALL)) {
            if (!(sender instanceof Player)) {
                MessageService.say(sender, Text.MUST_BE_PLAYER_FOR_COMMAND);
                return true;
            }

            Player p = (Player) sender;
            CallTardisCommand.performCall(p, t);
            return true;
        } else if (args[1].equalsIgnoreCase(Text.HOME)) {
            if (args.length > 2) {
                if (args[2].equalsIgnoreCase(Text.SET)) {
                    t.setHome();
                    MessageService.say(sender, Text.format("{TARDIS_HOME_SET_TO}", t.getHome()));
                    return true;
                }
                return false;
            }

            TardisCantLandReason goHomeReason = t.goHome();

            if(!goHomeReason.isValid()) MessageService.say(sender, Text.TARDIS_HOME_BLOCKED);
            else if(goHomeReason == TardisCantLandReason.PHASING) MessageService.say(sender, Text.TARDIS_IS_BUSY);
            else MessageService.say(sender, Text.TARDIS_SENT_HOME);

            return true;
        }
        else if(args[1].equalsIgnoreCase(Text.DELETE))
        {
            final UUID fromId = sender instanceof Player ? ((Player) sender).getUniqueId() : consoleId;
            final UUID toId = t.getPlayerId();
            if(deletingTardis.containsKey(fromId) && deletingTardis.get(fromId).equals(toId)) 
            {
                if(t.delete()) MessageService.say(sender, Text.format("{TARDIS_HAS_BEEN_DELETED}", t.getPlayerName()));
                else MessageService.say(sender, Text.format("{TARDIS_COULD_NOT_BE_DELETED}", t.getPlayerName()));
                
                deletingTardis.remove(fromId);
            }
            else 
            {
                MessageService.say(sender, Text.format("{CONFIRM_DELETE}", t.getPlayerName()));
                deletingTardis.put(fromId, toId);
                TaskService.scheduleTask(new SerialTask(
                    new WaitTask(Timespan.fromSeconds(3)),
                    new CallbackTask(() -> {
                        if(deletingTardis.containsKey(fromId) && deletingTardis.get(fromId).equals(toId))
                        {
                            deletingTardis.remove(fromId);
                        }
                    })
                ));
            }
            return true;
        }

        return false;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) {
        if(args.length < 2) return TARDISProvider.getAllTardises().Select(t -> t.getPlayerName()).ToArray(String.class);
        else if(args.length < 3 && sender instanceof Player) return new String[] {Text.CALL, Text.HOME, Text.DELETE};
        else if(args.length < 3) return new String[] {Text.HOME, Text.DELETE};
        else if(args[1].toLowerCase().equals(Text.HOME.toLowerCase())) return new String[] {Text.SET};
        return null;
    }

    @Override
    public String getUsage() {
        return Text.format("[{PLAYER}] [{CALL}/{HOME}/{DELETE}] <{SET}>");
    }

    @Override
    public String getDescription() {
		return Text.ADMIN_DESC;
	}
}