package com.adx.jogy34.tardis.commands;

import java.text.MessageFormat;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.commands.interfaces.ITardisCommand;
import com.adx.jogy34.tardis.providers.ServiceProvider;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

@TardisCommandParam
public class HelpCommand extends TardisCommand
{
    private final int PAGE_SIZE = 5;

    public HelpCommand()
    {
        super("Help", AllowedCommandSender.BOTH, "?");
    }
    
    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        EMap<Integer, EList<String>> pages = this.createPages(sender);
        int pageIndex = 1;
        if(args.length > 0)
        {
            try 
            {
                pageIndex = Integer.parseInt(args[0]);
                if(pageIndex <= 0) pageIndex = 1;
                else if(pageIndex > pages.Count()) pageIndex = pages.Count();
            } 
            catch(Exception e) {}
        }

        EList<String> cmds = pages.get(pageIndex);

        sender.sendMessage(MessageFormat.format("{0}==== [TARDIS Help {1}(Page {2}/{3}){0}] ====", ChatColor.GOLD, ChatColor.GRAY, pageIndex, pages.Count()));
        cmds.ForEach(c -> sender.sendMessage(c));
		return true;
	}

    @Override
    public String getUsage() 
    {
        return "<page>";
    }

    @Override
    public String getDescription() 
    {
        return "Displays available commands";
    }

    private EMap<Integer, EList<String>> createPages(CommandSender sender)
    {
        EMap<Integer, EList<String>> pages = new EMap<Integer, EList<String>>();
        EList<ITardisCommand> commands = ServiceProvider.commandService().getAllCommands().Where(c -> c.isValid(sender) == null).ToList();
        int page = 1;
        for(ITardisCommand cmd : commands)
        {
            if(!pages.containsKey(page)) pages.put(page, new EList<String>());
            String title = ChatColor.GRAY + "[" + ChatColor.YELLOW + cmd.getCommandKey();

            if(cmd.getAliases().Any())
            {
                title += ChatColor.GRAY + "|" + ChatColor.YELLOW;
                title += cmd.getAliases().ConcatenateToString(ChatColor.GRAY + "|" + ChatColor.YELLOW);
            }

            title += ChatColor.GRAY + "] " + ChatColor.YELLOW;
            pages.get(page).add( title + " " + cmd.getUsage() + ChatColor.BLUE + " - " + cmd.getDescription());

            if(pages.get(page).Count() >= PAGE_SIZE) page++;
        }

        return pages;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) 
    {
        EMap<Integer, EList<String>> pages = this.createPages(sender);
        if(args.length < 2) {
            String[] results = new String[pages.Count()];
            for(int i = 0; i < results.length; i++) results[i] = (i + 1) + "";
            return results;
        }

        return null;
    }
}