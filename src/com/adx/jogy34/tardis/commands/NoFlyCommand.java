package com.adx.jogy34.tardis.commands;

import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.services.MessageService;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class NoFlyCommand extends TardisCommand
{
    public NoFlyCommand()
    {
        super("NoFly", "tardis.noFly", AllowedCommandSender.PLAYER);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        if(args.length == 0) return false;

        Player p = (Player) sender;
        String msg = "";
        switch(args[0].toLowerCase())
        {
            case "add":
            {
                msg = ServiceProvider.worldService().addNoFlyChunk(p.getLocation().getChunk()) ? "TARDISes can no longer land here" : "This is already a no-fly zone";
                break;
            }
            case "remove":
            {
                msg = ServiceProvider.worldService().removeNoFlyChunk(p.getLocation().getChunk()) ? "TARDISes can now land here" : "TARDISes can already land here";
                break;
            }
            case "check":
            {
                msg = ServiceProvider.worldService().canLand(p.getLocation().getChunk()) ? "TARDISes can land here" : "TARDISes can not land here";
                break;
            }
            default:
            {
                return false;
            }
        }
        MessageService.say(p, msg);
        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) {
        if(args.length < 2) return new String[] { "Add", "Remove", "Check" };
        return null;
    }

    @Override
    public String getUsage() {
        return "[Add/Remove/Check]";
    }

    @Override
    public String getDescription() {
		return "Set a chunk to prevent TARDISes from landing in it (f3 + g to show chunk boundaries)";
	}
}