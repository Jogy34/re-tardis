package com.adx.jogy34.tardis.commands;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.tardis.TardisPermission.TardisPermissionLevel;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class AllowCommand extends TardisCommand
{
    public AllowCommand()
    {
        super(Text.ALLOW, AllowedCommandSender.PLAYER_WITH_TARDIS);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        if(args.length < 1) return false;

        Player p = (Player) sender;
        TARDIS t = TARDISProvider.getTardisForPlayer(p);

        if(args[0].equalsIgnoreCase(Text.LIST))
        {
            EList<String> players = t.getAllowedPlayersDisplay();
            if(!players.Any())
            {
                MessageService.say(p, Text.NO_PLAYERS_ALLOWED);
            }
            else
            {
                players.Batch(3).ForEach(pb -> {
                    MessageService.say(p, pb.ConcatenateToString(" | "));
                });
            }
            return true;
        }
        else if(args.length < 2) return false;
        
        TardisPermissionLevel nextLevel = TardisPermissionLevel.fromCommandString(args[1]);
        if(nextLevel == null) return false;
        
        Player allowing = Bukkit.getPlayer(args[0]);
        
        if(allowing == null) MessageService.say(p, Text.format("{CANNOT_FIND_PLAYER}", args[0]));
        
        TardisPermissionLevel existing = t.getPermissionLevel(allowing);

        if(existing == nextLevel)
        {
            if(nextLevel == TardisPermissionLevel.NONE) MessageService.say(p, Text.PLAYER_CANNOT_ENTER_TARDIS);
            else MessageService.say(p, Text.format("{PLAYER_CAN_ALREADY_USE_YOUR_TARDIS}", allowing.getName(), nextLevel.getDisplayName()));
        }
        else
        {
            t.setPermissionLevel(allowing, nextLevel);
            if(nextLevel == TardisPermissionLevel.NONE) MessageService.say(p, Text.format("{PLAYER_IS_NO_LONGER_ALLOWED}", allowing.getName()));
            else MessageService.say(p, Text.format("{PLAYER_CAN_USE_YOUR_TARDIS}", allowing.getName(), nextLevel.getDisplayName()));
        }

        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) 
    {
        if(args.length <= 1) 
        {
            EList<String> result = new EList<>(Bukkit.getOnlinePlayers()).Select(p -> p.getName()).ToList();
            result.add(Text.LIST);
            return result.ToArray(String.class);
        }
        else if(args.length == 2) return new String[] { Text.ENTER, Text.FLY, Text.NONE };
        return null;
    }

    @Override
    public String getUsage()
    {
        return Text.format("[{PLAYER}|{LIST}}] <{ENTER}|{FLY}|{NONE}>");
    }

    @Override
    public String getDescription() 
    {
		return Text.ALLOW_DESC;
	}
}