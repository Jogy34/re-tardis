package com.adx.jogy34.tardis.commands;

import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.data.MiscMaterialData;
import com.adx.jogy34.tardis.enums.TardisCantLandReason;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;

import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class CallTardisCommand extends TardisCommand 
{
    public CallTardisCommand()
    {
        super("Call", AllowedCommandSender.PLAYER_WITH_TARDIS);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        Player p = (Player) sender;
        TARDIS t = TARDISProvider.getTardisForPlayer(p.getUniqueId());
        performCall(p, t);

        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) 
    {
        return null;
    }

    @Override
    public String getUsage() {
        return "";
    }

	@Override
	public String getDescription() {
		return "Call your TARDIS where you are looking";
	}

    public static void performCall(Player p, TARDIS t)
    {
        Block target = p.getTargetBlock(MiscMaterialData.TRANSPARENT_MATERIAL_SET, 5);
        if(MiscMaterialData.TRANSPARENT_MATERIALS.contains(target.getType()) || t.isInsideTardis(target))
        {
            MessageService.say(p, "TARDIS cannot land on a non-solid block");
        }
        else
        {
            Block moveTo = target.getRelative(0, 1, 0);
            TardisCantLandReason cantLand = t.moveTo(moveTo);
            if(cantLand.isBlocked()) MessageService.say(p, "Not sufficient space to call a TARDIS to that location");
            else if(cantLand == TardisCantLandReason.NO_FLY_CHUNK) MessageService.say(p, "TARDISes are not allowed to land here");
            else if(cantLand == TardisCantLandReason.TO_HIGH) MessageService.say(p, "This is to high up for a TARDIS");
            else if(cantLand == TardisCantLandReason.TO_LOW) MessageService.say(p, "This is to low down for a TARDIS");
        }
    }
}