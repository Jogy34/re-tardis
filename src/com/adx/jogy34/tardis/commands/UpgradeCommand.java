package com.adx.jogy34.tardis.commands;

import com.adx.jogy34.tardis.commands.annotations.TardisCommandParam;
import com.adx.jogy34.tardis.menu.upgrade.UpgradeTardisMenu;
import com.adx.jogy34.tardis.providers.ServiceProvider;
import com.adx.jogy34.tardis.providers.TARDISProvider;
import com.adx.jogy34.tardis.services.MessageService;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@TardisCommandParam
public class UpgradeCommand extends TardisCommand
{
    public UpgradeCommand() {
        super(Text.UPGRADE, AllowedCommandSender.PLAYER_WITH_TARDIS);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) 
    {
        Player p = (Player) sender;
        TARDIS t = TARDISProvider.getTardisForPlayer(p);

        if(t == null)
        {
            MessageService.say(p, Text.NEED_TARDIS_FOR_COMMAND);
            return true;
        }

        ServiceProvider.inventoryMenuService().getMenu(UpgradeTardisMenu.class).open(p);

        return true;
    }

    @Override
    public String[] tabComplete(CommandSender sender, String[] args) 
    {
        return null;
    }

    @Override
    public String getUsage() 
    {
        return "";
    }

	@Override
    public String getDescription() 
    {
		return Text.UPGRADE_YOUR_TARDIS;
	}
}