package com.adx.jogy34.tardis.data;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.enums.TardisColor;

import org.bukkit.Material;

public final class TardisPhaseData
{
    public static final EList<Material> YELLOW_BLOCKS = new EList<>(new Material[] {
        Material.GLOWSTONE, Material.YELLOW_WOOL, Material.YELLOW_CONCRETE, Material.HORN_CORAL_BLOCK,
        Material.YELLOW_GLAZED_TERRACOTTA, Material.GOLD_BLOCK, Material.YELLOW_STAINED_GLASS, Material.HAY_BLOCK,
        Material.YELLOW_TERRACOTTA
    });

    public static final EList<Material> BROWN_BLOCKS = new EList<>(new Material[] {
        Material.BROWN_STAINED_GLASS,Material.CHISELED_RED_SANDSTONE,Material.CUT_RED_SANDSTONE,
        Material.BROWN_TERRACOTTA,Material.COARSE_DIRT,Material.DARK_OAK_LOG,Material.GRANITE,
        Material.JACK_O_LANTERN,Material.CARVED_PUMPKIN,Material.BOOKSHELF,Material.BROWN_WOOL,
        Material.BLACK_TERRACOTTA,Material.BROWN_MUSHROOM_BLOCK,Material.GRAY_TERRACOTTA,
        Material.JUNGLE_PLANKS,Material.DARK_OAK_PLANKS,Material.BROWN_GLAZED_TERRACOTTA,
        Material.BROWN_CONCRETE,Material.CRAFTING_TABLE,Material.ACACIA_LOG,Material.ACACIA_PLANKS,
        Material.DIRT,Material.DARK_OAK_WOOD,Material.ORANGE_GLAZED_TERRACOTTA,Material.ORANGE_WOOL,
        Material.ORANGE_STAINED_GLASS,Material.OAK_WOOD,Material.ORANGE_TERRACOTTA,Material.OAK_LOG,
        Material.ORANGE_CONCRETE,Material.PUMPKIN,Material.OAK_PLANKS,Material.STRIPPED_ACACIA_WOOD,
        Material.SPRUCE_LOG,Material.STRIPPED_SPRUCE_WOOD,Material.RED_SANDSTONE,Material.SOUL_SAND,
        Material.STRIPPED_OAK_LOG,Material.STRIPPED_ACACIA_LOG,Material.REDSTONE_LAMP,
        Material.STRIPPED_SPRUCE_LOG,Material.STRIPPED_OAK_WOOD,Material.STRIPPED_JUNGLE_WOOD,
        Material.SMOOTH_RED_SANDSTONE,Material.STRIPPED_DARK_OAK_LOG,Material.STRIPPED_DARK_OAK_WOOD,
        Material.SPRUCE_PLANKS,Material.SPRUCE_WOOD,Material.TERRACOTTA,Material.STRIPPED_JUNGLE_LOG
    });

    public static final EList<Material> BLACK_BLOCKS = new EList<>(new Material[] {
        Material.DEAD_BUBBLE_CORAL_BLOCK,Material.GRAY_GLAZED_TERRACOTTA,Material.DEAD_HORN_CORAL_BLOCK,
        Material.BLACK_STAINED_GLASS,Material.CHISELED_STONE_BRICKS,Material.ACACIA_WOOD,Material.CYAN_TERRACOTTA,
        Material.BEDROCK,Material.OBSIDIAN,Material.GRAY_STAINED_GLASS,Material.DEAD_BRAIN_CORAL_BLOCK,
        Material.COAL_BLOCK,Material.BLACK_GLAZED_TERRACOTTA,Material.ANDESITE,Material.CRACKED_STONE_BRICKS,
        Material.COBBLESTONE,Material.GRAY_WOOL,Material.GRAY_CONCRETE,Material.DEAD_TUBE_CORAL_BLOCK,
        Material.LIGHT_GRAY_CONCRETE,Material.DEAD_FIRE_CORAL_BLOCK,Material.BLACK_CONCRETE,Material.BLACK_WOOL,
        Material.STONE_BRICKS,Material.STONE,Material.POLISHED_ANDESITE
    });

    public static final EList<Material> BLUE_BLOCKS = new EList<>(new Material[] {
        Material.LIGHT_BLUE_WOOL, Material.BLUE_CONCRETE, Material.CYAN_STAINED_GLASS, Material.BLUE_WOOL, 
        Material.CYAN_CONCRETE, Material.CYAN_GLAZED_TERRACOTTA, Material.LIGHT_BLUE_GLAZED_TERRACOTTA, 
        Material.LIGHT_BLUE_STAINED_GLASS, Material.LAPIS_BLOCK, Material.CYAN_WOOL, Material.BLUE_GLAZED_TERRACOTTA, 
        Material.BLUE_STAINED_GLASS, Material.PRISMARINE_BRICKS, Material.PACKED_ICE, Material.LIGHT_BLUE_TERRACOTTA, 
        Material.DARK_PRISMARINE, Material.DIAMOND_BLOCK, Material.LIGHT_BLUE_CONCRETE, Material.TUBE_CORAL_BLOCK, 
        Material.PRISMARINE
    });
    
    public static final EList<Material> PURPLE_BLOCKS = new EList<>(new Material[] {
        Material.PURPLE_STAINED_GLASS, Material.PURPLE_CONCRETE, Material.PURPLE_GLAZED_TERRACOTTA, 
        Material.BRAIN_CORAL_BLOCK, Material.BLUE_TERRACOTTA, Material.PURPUR_PILLAR, Material.PURPUR_BLOCK, 
        Material.PURPLE_TERRACOTTA, Material.MAGENTA_TERRACOTTA, Material.MAGENTA_STAINED_GLASS, Material.PURPLE_WOOL
    });

    public static final EList<Material> WHITE_BLOCKS = new EList<>(new Material[] {
        Material.LIGHT_GRAY_GLAZED_TERRACOTTA, Material.POLISHED_DIORITE, Material.CHISELED_SANDSTONE, 
        Material.BIRCH_LOG, Material.BIRCH_PLANKS, Material.BONE_BLOCK, Material.CHISELED_QUARTZ_BLOCK, 
        Material.WHITE_GLAZED_TERRACOTTA, Material.LIGHT_GRAY_WOOL, Material.IRON_BLOCK, Material.END_STONE_BRICKS, 
        Material.CLAY, Material.BIRCH_WOOD, Material.CUT_SANDSTONE, Material.WHITE_CONCRETE, Material.MUSHROOM_STEM,
        Material.LIGHT_GRAY_TERRACOTTA, Material.LIGHT_GRAY_STAINED_GLASS, Material.DIORITE, Material.END_STONE, 
        Material.GLASS, Material.SMOOTH_QUARTZ, Material.STRIPPED_BIRCH_LOG, Material.SANDSTONE, 
        Material.STRIPPED_BIRCH_WOOD, Material.SMOOTH_SANDSTONE, Material.QUARTZ_PILLAR, Material.QUARTZ_BLOCK, 
        Material.WHITE_WOOL, Material.WHITE_TERRACOTTA, Material.WHITE_STAINED_GLASS, Material.SEA_LANTERN, Material.SMOOTH_STONE
    });
    
    public static final EList<Material> RED_BLOCKS = new EList<>(new Material[] {
        Material.FIRE_CORAL_BLOCK, Material.NETHERRACK, Material.PINK_CONCRETE, Material.PINK_WOOL, 
        Material.PINK_STAINED_GLASS, Material.RED_NETHER_BRICKS, Material.RED_WOOL, Material.PINK_GLAZED_TERRACOTTA, 
        Material.MAGENTA_CONCRETE, Material.PINK_TERRACOTTA, Material.NETHER_BRICKS, Material.NETHER_WART_BLOCK, 
        Material.RED_STAINED_GLASS, Material.RED_TERRACOTTA, Material.MAGENTA_WOOL, Material.MAGENTA_GLAZED_TERRACOTTA, 
        Material.POLISHED_GRANITE, Material.RED_GLAZED_TERRACOTTA, Material.RED_CONCRETE, Material.REDSTONE_BLOCK, 
        Material.RED_MUSHROOM_BLOCK
    });
    
    public static final EList<Material> GREEN_BLOCKS = new EList<>(new Material[] {
        Material.EMERALD_BLOCK, Material.GREEN_CONCRETE, Material.GREEN_GLAZED_TERRACOTTA, 
        Material.GREEN_STAINED_GLASS, Material.JUNGLE_WOOD, Material.MELON, Material.DRIED_KELP_BLOCK, 
        Material.JUNGLE_LOG, Material.GREEN_TERRACOTTA, Material.GREEN_WOOL, Material.LIME_CONCRETE, 
        Material.SLIME_BLOCK, Material.LIME_STAINED_GLASS, Material.LIME_GLAZED_TERRACOTTA, 
        Material.LIME_TERRACOTTA, Material.LIME_WOOL, Material.MOSSY_STONE_BRICKS, Material.MOSSY_COBBLESTONE
    });

    public static final EMap<TardisColor, EList<Material>> COLOR_BLOCK_MAP = new EMap<TardisColor, EList<Material>>() {
        private static final long serialVersionUID = 1L;
        {
            put(TardisColor.BLACK, BLACK_BLOCKS);
            put(TardisColor.BLUE, BLUE_BLOCKS);
            put(TardisColor.BROWN, BROWN_BLOCKS);
            put(TardisColor.GREEN, GREEN_BLOCKS);
            put(TardisColor.PURPLE, PURPLE_BLOCKS);
            put(TardisColor.RED, RED_BLOCKS);
            put(TardisColor.WHITE, WHITE_BLOCKS);
            put(TardisColor.YELLOW, YELLOW_BLOCKS);
        }
    };

    public static final EList<Material> BLACK_PHASE_BLOCKS = new EList<Material>(new Material[] {
        Material.BLACK_STAINED_GLASS, Material.GRAY_STAINED_GLASS, 
        Material.BLACK_STAINED_GLASS, Material.GRAY_STAINED_GLASS,
        Material.BLACK_STAINED_GLASS, Material.GRAY_STAINED_GLASS,
        Material.BLACK_STAINED_GLASS, Material.GRAY_STAINED_GLASS,
        Material.CYAN_TERRACOTTA, Material.GRAY_TERRACOTTA
    });

    public static final EList<Material> BLUE_PHASE_BLOCKS = new EList<Material>(new Material[] {
        Material.CYAN_STAINED_GLASS, Material.BLUE_STAINED_GLASS, Material.LIGHT_BLUE_STAINED_GLASS,
        Material.CYAN_STAINED_GLASS, Material.BLUE_STAINED_GLASS, Material.LIGHT_BLUE_STAINED_GLASS,
        Material.CYAN_STAINED_GLASS, Material.BLUE_STAINED_GLASS, Material.LIGHT_BLUE_STAINED_GLASS,
        Material.CYAN_STAINED_GLASS, Material.BLUE_STAINED_GLASS, Material.LIGHT_BLUE_STAINED_GLASS,
        Material.CYAN_CONCRETE, Material.BLUE_CONCRETE, Material.LIGHT_BLUE_CONCRETE
    });

    public static final EList<Material> BROWN_PHASE_BLOCKS = new EList<Material>(new Material[] {
        Material.ORANGE_STAINED_GLASS, Material.BROWN_STAINED_GLASS,
        Material.ORANGE_STAINED_GLASS, Material.BROWN_STAINED_GLASS,
        Material.ORANGE_STAINED_GLASS, Material.BROWN_STAINED_GLASS,
        Material.ORANGE_STAINED_GLASS, Material.BROWN_STAINED_GLASS,
        Material.ORANGE_TERRACOTTA, Material.BROWN_TERRACOTTA
    });

    public static final EList<Material> GREEN_PHASE_BLOCKS = new EList<Material>(new Material[] {
        Material.GREEN_STAINED_GLASS, Material.LIME_STAINED_GLASS, 
        Material.GREEN_STAINED_GLASS, Material.LIME_STAINED_GLASS, 
        Material.GREEN_STAINED_GLASS, Material.LIME_STAINED_GLASS, 
        Material.GREEN_STAINED_GLASS, Material.LIME_STAINED_GLASS, 
        Material.GREEN_TERRACOTTA, Material.GREEN_CONCRETE
    });

    public static final EList<Material> PURPLE_PHASE_BLOCKS = new EList<Material>(new Material[] {
        Material.PURPLE_STAINED_GLASS, Material.MAGENTA_STAINED_GLASS, 
        Material.PURPLE_STAINED_GLASS, Material.MAGENTA_STAINED_GLASS, 
        Material.PURPLE_STAINED_GLASS, Material.MAGENTA_STAINED_GLASS, 
        Material.PURPLE_STAINED_GLASS, Material.MAGENTA_STAINED_GLASS, 
        Material.BLUE_TERRACOTTA, Material.PURPLE_TERRACOTTA
    });

    public static final EList<Material> RED_PHASE_BLOCKS = new EList<Material>(new Material[] {
        Material.RED_STAINED_GLASS, Material.RED_STAINED_GLASS,
        Material.RED_STAINED_GLASS, Material.PINK_STAINED_GLASS,
        Material.RED_STAINED_GLASS, Material.PINK_STAINED_GLASS,
        Material.RED_STAINED_GLASS, Material.PINK_STAINED_GLASS,
        Material.RED_CONCRETE, Material.PINK_TERRACOTTA
    });

    public static final EList<Material> WHITE_PHASE_BLOCKS = new EList<Material>(new Material[] {
        Material.WHITE_STAINED_GLASS, Material.LIGHT_GRAY_STAINED_GLASS, 
        Material.WHITE_STAINED_GLASS, Material.LIGHT_GRAY_STAINED_GLASS, 
        Material.WHITE_STAINED_GLASS, Material.LIGHT_GRAY_STAINED_GLASS, 
        Material.WHITE_STAINED_GLASS, Material.LIGHT_GRAY_STAINED_GLASS, 
        Material.WHITE_CONCRETE, Material.MUSHROOM_STEM
    });

    public static final EList<Material> YELLOW_PHASE_BLOCKS = new EList<Material>(new Material[] {
        Material.YELLOW_STAINED_GLASS, Material.YELLOW_STAINED_GLASS, 
        Material.YELLOW_STAINED_GLASS, Material.YELLOW_STAINED_GLASS, 
        Material.YELLOW_STAINED_GLASS, Material.YELLOW_STAINED_GLASS,
        Material.YELLOW_STAINED_GLASS, Material.YELLOW_STAINED_GLASS,
        Material.YELLOW_TERRACOTTA, Material.YELLOW_CONCRETE
    });

    public static final EList<Material> CLEAR_PHASE_BLOCKS = new EList<Material>(new Material[] {
        Material.GLASS, Material.GLASS, Material.GLASS, Material.GLASS, Material.ICE
    });

    public static EMap<TardisColor, EList<Material>> PHASE_BLOCKS_MAP = new EMap<TardisColor, EList<Material>>()
    {
        private static final long serialVersionUID = 1L;
        {
            put(TardisColor.BLACK, BLACK_PHASE_BLOCKS);
            put(TardisColor.BLUE, BLUE_PHASE_BLOCKS);
            put(TardisColor.BROWN, BROWN_PHASE_BLOCKS);
            put(TardisColor.GREEN, GREEN_PHASE_BLOCKS);
            put(TardisColor.PURPLE, PURPLE_PHASE_BLOCKS);
            put(TardisColor.RED, RED_PHASE_BLOCKS);
            put(TardisColor.WHITE, WHITE_PHASE_BLOCKS);
            put(TardisColor.YELLOW, YELLOW_PHASE_BLOCKS);
        }
    };
}