package com.adx.jogy34.tardis.data;

import org.bukkit.Material;

import com.adx.jlinq.EList;

public final class TARDISAppearanceData
{
    public static final EList<Material> ALLOWED_OUTER_MATERIALS = new EList<Material>(new Material[] {
        Material.ACACIA_LOG, Material.ACACIA_PLANKS, Material.ACACIA_WOOD, Material.ANDESITE, Material.BEDROCK, 
        Material.BIRCH_LOG, Material.BIRCH_PLANKS, Material.BIRCH_WOOD, Material.BLACK_CONCRETE, Material.BLACK_GLAZED_TERRACOTTA, 
        Material.BLACK_STAINED_GLASS, Material.BLACK_TERRACOTTA, Material.BLACK_WOOL, Material.BLUE_CONCRETE, Material.BLUE_GLAZED_TERRACOTTA, 
        Material.BLUE_STAINED_GLASS, Material.BLUE_TERRACOTTA, Material.BLUE_WOOL, Material.BONE_BLOCK, Material.BOOKSHELF, 
        Material.BRAIN_CORAL_BLOCK, Material.BROWN_CONCRETE, Material.BROWN_GLAZED_TERRACOTTA, Material.BROWN_MUSHROOM_BLOCK, Material.BROWN_STAINED_GLASS, 
        Material.BROWN_TERRACOTTA, Material.BROWN_WOOL, Material.CARVED_PUMPKIN, Material.CHISELED_QUARTZ_BLOCK, Material.CHISELED_RED_SANDSTONE, Material.CHISELED_SANDSTONE, 
        Material.CHISELED_STONE_BRICKS, Material.CLAY, Material.COAL_BLOCK, Material.COARSE_DIRT, Material.COBBLESTONE, 
        Material.CRACKED_STONE_BRICKS, Material.CRAFTING_TABLE, Material.CUT_RED_SANDSTONE, Material.CUT_SANDSTONE, Material.CYAN_CONCRETE, 
        Material.CYAN_GLAZED_TERRACOTTA, Material.CYAN_STAINED_GLASS, Material.CYAN_TERRACOTTA, Material.CYAN_WOOL, Material.DARK_OAK_LOG, 
        Material.DARK_OAK_PLANKS, Material.DARK_OAK_WOOD, Material.DARK_PRISMARINE, Material.DEAD_BRAIN_CORAL_BLOCK, Material.DEAD_BUBBLE_CORAL_BLOCK, 
        Material.DEAD_FIRE_CORAL_BLOCK, Material.DEAD_HORN_CORAL_BLOCK, Material.DEAD_TUBE_CORAL_BLOCK, Material.DIAMOND_BLOCK, Material.DIORITE, 
        Material.DIRT, Material.DRIED_KELP_BLOCK, Material.EMERALD_BLOCK, Material.END_STONE, Material.END_STONE_BRICKS, 
        Material.FIRE_CORAL_BLOCK, Material.GLASS, Material.GLOWSTONE, Material.GOLD_BLOCK, Material.GRANITE, 
        Material.GRAY_CONCRETE, Material.GRAY_GLAZED_TERRACOTTA, Material.GRAY_STAINED_GLASS, Material.GRAY_TERRACOTTA, Material.GRAY_WOOL, 
        Material.GREEN_CONCRETE, Material.GREEN_GLAZED_TERRACOTTA, Material.GREEN_STAINED_GLASS, Material.GREEN_TERRACOTTA, Material.GREEN_WOOL, 
        Material.HAY_BLOCK, Material.HORN_CORAL_BLOCK, Material.IRON_BLOCK, Material.JACK_O_LANTERN, Material.JUNGLE_LOG, 
        Material.JUNGLE_PLANKS, Material.JUNGLE_WOOD, Material.LAPIS_BLOCK, Material.LIGHT_BLUE_CONCRETE, Material.LIGHT_BLUE_GLAZED_TERRACOTTA, 
        Material.LIGHT_BLUE_STAINED_GLASS, Material.LIGHT_BLUE_TERRACOTTA, Material.LIGHT_BLUE_WOOL, Material.LIGHT_GRAY_CONCRETE, Material.LIGHT_GRAY_GLAZED_TERRACOTTA, 
        Material.LIGHT_GRAY_STAINED_GLASS, Material.LIGHT_GRAY_TERRACOTTA, Material.LIGHT_GRAY_WOOL, Material.LIME_CONCRETE, Material.LIME_GLAZED_TERRACOTTA, 
        Material.LIME_STAINED_GLASS, Material.LIME_TERRACOTTA, Material.LIME_WOOL, Material.MAGENTA_CONCRETE, Material.MAGENTA_GLAZED_TERRACOTTA, 
        Material.MAGENTA_STAINED_GLASS, Material.MAGENTA_TERRACOTTA, Material.MAGENTA_WOOL, Material.MELON, Material.MOSSY_COBBLESTONE, 
        Material.MOSSY_STONE_BRICKS, Material.MUSHROOM_STEM, Material.NETHER_BRICKS, Material.NETHER_WART_BLOCK, Material.NETHERRACK, 
        Material.OAK_LOG, Material.OAK_PLANKS, Material.OAK_WOOD, Material.OBSIDIAN, Material.ORANGE_CONCRETE, 
        Material.ORANGE_GLAZED_TERRACOTTA, Material.ORANGE_STAINED_GLASS, Material.ORANGE_TERRACOTTA, Material.ORANGE_WOOL, Material.PACKED_ICE, 
        Material.PINK_CONCRETE, Material.PINK_GLAZED_TERRACOTTA, Material.PINK_STAINED_GLASS, Material.PINK_TERRACOTTA, Material.PINK_WOOL, 
        Material.POLISHED_ANDESITE, Material.POLISHED_DIORITE, Material.POLISHED_GRANITE, Material.PRISMARINE, 
        Material.PRISMARINE_BRICKS, Material.PUMPKIN, Material.PURPLE_CONCRETE, Material.PURPLE_GLAZED_TERRACOTTA, Material.PURPLE_STAINED_GLASS, 
        Material.PURPLE_TERRACOTTA, Material.PURPLE_WOOL, Material.PURPUR_BLOCK, Material.PURPUR_PILLAR, Material.QUARTZ_BLOCK, 
        Material.QUARTZ_PILLAR, Material.RED_CONCRETE, Material.RED_GLAZED_TERRACOTTA, Material.RED_MUSHROOM_BLOCK, Material.RED_NETHER_BRICKS, 
        Material.RED_SANDSTONE, Material.RED_STAINED_GLASS, Material.RED_TERRACOTTA, Material.RED_WOOL, Material.REDSTONE_BLOCK, 
        Material.REDSTONE_LAMP, Material.SANDSTONE, Material.SEA_LANTERN, Material.SLIME_BLOCK, Material.SMOOTH_QUARTZ, 
        Material.SMOOTH_RED_SANDSTONE, Material.SMOOTH_SANDSTONE, Material.SMOOTH_STONE, Material.SOUL_SAND, 
        Material.SPRUCE_LOG, Material.SPRUCE_PLANKS, Material.SPRUCE_WOOD, Material.STONE, Material.STONE_BRICKS, 
        Material.STRIPPED_ACACIA_LOG, Material.STRIPPED_ACACIA_WOOD, Material.STRIPPED_BIRCH_LOG, Material.STRIPPED_BIRCH_WOOD, Material.STRIPPED_DARK_OAK_LOG, 
        Material.STRIPPED_DARK_OAK_WOOD, Material.STRIPPED_JUNGLE_LOG, Material.STRIPPED_JUNGLE_WOOD, Material.STRIPPED_OAK_LOG, Material.STRIPPED_OAK_WOOD, 
        Material.STRIPPED_SPRUCE_LOG, Material.STRIPPED_SPRUCE_WOOD, Material.TERRACOTTA, Material.TUBE_CORAL_BLOCK, 
        Material.WHITE_CONCRETE, Material.WHITE_GLAZED_TERRACOTTA, Material.WHITE_STAINED_GLASS, Material.WHITE_TERRACOTTA, Material.WHITE_WOOL, 
        Material.YELLOW_CONCRETE, Material.YELLOW_GLAZED_TERRACOTTA, Material.YELLOW_STAINED_GLASS, Material.YELLOW_TERRACOTTA, Material.YELLOW_WOOL
    });

    public static final EList<Material> ALLOWED_DOOR_MATERIALS = new EList<Material>(new Material[] {
        Material.ACACIA_DOOR, Material.BIRCH_DOOR, Material.JUNGLE_DOOR, Material.OAK_DOOR,
        Material.SPRUCE_DOOR, Material.DARK_OAK_DOOR
    });

    public static final EList<Material> ALLOWED_LANTERN_MATERIALS = new EList<Material>(new Material[] {
        Material.SEA_LANTERN, Material.REDSTONE_LAMP, Material.GLOWSTONE, Material.BEACON
    });

    public static final EList<Material> ALLOWED_CONTROL_MATERIALS = new EList<Material>(new Material[] {
        Material.LEVER, Material.BIRCH_BUTTON, Material.ACACIA_BUTTON, Material.DARK_OAK_BUTTON,
        Material.JUNGLE_BUTTON, Material.OAK_BUTTON, Material.SPRUCE_BUTTON, Material.STONE_BUTTON,
        Material.REPEATER, Material.COMPARATOR
    });

    public static final EList<Material> ALLOWED_HALLWAY_ACCESS_MATERIALS = new EList<Material>(new Material[] {
        Material.OBSERVER
    });
}
