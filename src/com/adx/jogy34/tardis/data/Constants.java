package com.adx.jogy34.tardis.data;

public final class Constants
{
    public static final class SkullOwners
    {
        public static final String PileOfBooks = "PileOfBooks";
        public static final String WaterJug = "52bedbc1-f414-4e95-8015-eff7ecaa9296";
        public static final String CoffeeMachine = "bb0c0b47-aec7-4b84-a713-902033200475";
        public static final String Toaster = "fc74b782-2269-44fb-b8f6-f47da7eabbf3";
    } 
}