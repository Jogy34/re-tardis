package com.adx.jogy34.tardis.data;

import java.util.HashSet;
import java.util.Set;

import com.adx.jlinq.EList;

import org.bukkit.Material;

public final class MiscMaterialData
{
    public static final EList<Material> TRANSPARENT_MATERIALS = new EList<Material>(new Material[] {
        Material.AIR, Material.CAVE_AIR, Material.GRASS, Material.POPPY, Material.DANDELION,
        Material.ALLIUM, Material.ACACIA_SAPLING, Material.AZURE_BLUET,Material.BIRCH_SAPLING,
        Material.BLUE_ORCHID, Material.BROWN_MUSHROOM, Material.DARK_OAK_SAPLING, Material.DEAD_BUSH,
        Material.FERN, Material.JUNGLE_SAPLING, Material.OAK_SAPLING, Material.ORANGE_TULIP,
        Material.OXEYE_DAISY, Material.PINK_TULIP, Material.RED_MUSHROOM, Material.RED_TULIP,
        Material.SPRUCE_SAPLING, Material.WHITE_TULIP, Material.FIRE
    });

    public static final Set<Material> TRANSPARENT_MATERIAL_SET = new HashSet<Material>(TRANSPARENT_MATERIALS);

    public static final EList<Material> BED_MATERIALS = new EList<Material>(new Material[] {
        Material.BLACK_BED, Material.BLUE_BED, Material.BROWN_BED, Material.CYAN_BED,
        Material.GRAY_BED, Material.GREEN_BED, Material.LIGHT_BLUE_BED, Material.LIGHT_GRAY_BED,
        Material.LIME_BED, Material.MAGENTA_BED, Material.ORANGE_BED, Material.PINK_BED,
        Material.PURPLE_BED, Material.RED_BED, Material.WHITE_BED, Material.YELLOW_BED
    });

    public static final EList<Material> WALL_SIGNS = new EList<Material>(new Material[] {
        Material.ACACIA_WALL_SIGN, Material.BIRCH_WALL_SIGN, Material.DARK_OAK_WALL_SIGN, 
        Material.JUNGLE_WALL_SIGN, Material.OAK_WALL_SIGN, Material.SPRUCE_WALL_SIGN
    });

    public static final EList<Material> SIGN_POSTS = new EList<Material>(new Material[] {
        Material.ACACIA_SIGN, Material.BIRCH_SIGN, Material.DARK_OAK_SIGN, 
        Material.JUNGLE_SIGN, Material.OAK_SIGN, Material.SPRUCE_SIGN
    });
    
    public static final EList<Material> ALL_SIGNS = WALL_SIGNS.Union(SIGN_POSTS).ToList();
}