package com.adx.jogy34.tardis.util;

public final class Encode
{
    protected final static String ENCODE_CHARACTERS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_=+<>/|*!@#$%^&`~";

    //Why? Because.
    public static String base80Encode(int i)
    {
        String result = "";
        do
        {
            result = ENCODE_CHARACTERS.charAt(i % 80) + result;
            i /= ENCODE_CHARACTERS.length();
        } while(Math.abs(i) > 0);

        return result;
    }
}