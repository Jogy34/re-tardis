package com.adx.jogy34.tardis.util;

public class Timespan
{
    public static final class Constants
    {
        public static final long TICKS_PER_SECOND = 20;
        public static final long SECONDS_PER_MINUTE = 60;
        public static final long MINUTES_PER_HOUR = 60;
        public static final long HOURS_PER_DAY = 24;
        
        public static final long MILISECONDS_PER_SECOND = 1000;
        public static final long MILISECONDS_PER_TICK = MILISECONDS_PER_SECOND / TICKS_PER_SECOND;
        public static final long MILISECONDS_PER_MINUTE = MILISECONDS_PER_SECOND * SECONDS_PER_MINUTE;
        public static final long MILISECONDS_PER_HOUR = MILISECONDS_PER_MINUTE * MINUTES_PER_HOUR;
        public static final long MILISECONDS_PER_DAY = MILISECONDS_PER_HOUR * HOURS_PER_DAY;
    }

    protected final long miliseconds;

    public Timespan(long miliseconds)
    {
        this.miliseconds = miliseconds;
    }

    public Timespan add(Timespan other)
    {
        return new Timespan(miliseconds + other.miliseconds);
    }

    public Timespan subtract(Timespan other)
    {
        return new Timespan(miliseconds - other.miliseconds);
    }

    //region Get Unit of Time
    public long getMilis()
    {
        return miliseconds;
    }

    public long getSeconds()
    {
        return miliseconds / Constants.MILISECONDS_PER_SECOND;
    }

    public double getSecondsPartial()
    {
        return ((double) miliseconds) / Constants.MILISECONDS_PER_SECOND;
    }

    public long getTicks()
    {
        return miliseconds / Constants.MILISECONDS_PER_TICK;
    }

    public double getTicksPartial()
    {
        return ((double) miliseconds) / Constants.MILISECONDS_PER_TICK;
    }

    public long getMinutes()
    {
        return miliseconds / Constants.MILISECONDS_PER_MINUTE;
    }

    public double getMinutesPartial()
    {
        return ((double) miliseconds) / Constants.MILISECONDS_PER_MINUTE;
    }

    public long getHours()
    {
        return miliseconds / Constants.MILISECONDS_PER_HOUR;
    }

    public double getHoursPartial()
    {
        return ((double) miliseconds) / Constants.MILISECONDS_PER_HOUR;
    }

    public long getDays()
    {
        return miliseconds / Constants.MILISECONDS_PER_DAY;
    }

    public double getDaysPartial()
    {
        return ((double) miliseconds) / Constants.MILISECONDS_PER_DAY;
    }
    
    //endregion Get Unit of Time

    //region Static Factory Methods

    public static Timespan fromMilis(int milis)
    {
        return new Timespan(milis);
    }

    public static Timespan fromTicks(int ticks)
    {
        return new Timespan(ticks * Constants.MILISECONDS_PER_TICK);
    }

    public static Timespan fromSeconds(int seconds)
    {
        return new Timespan(seconds * Constants.MILISECONDS_PER_SECOND);
    }

    public static Timespan fromMinutes(int minutes)
    {
        return new Timespan(minutes * Constants.MILISECONDS_PER_MINUTE);
    }

    public static Timespan fromHours(int hours)
    {
        return new Timespan(hours * Constants.MILISECONDS_PER_HOUR);
    }

    public static Timespan fromDays(int days)
    {
        return new Timespan(days * Constants.MILISECONDS_PER_DAY);
    }

    public static Timespan now()
    {
        return new Timespan(System.currentTimeMillis());
    }

    public static Timespan empty()
    {
        return new Timespan(0);
    }

    //endregion Static Factory Methods
}