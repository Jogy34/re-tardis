package com.adx.jogy34.tardis.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Map.Entry;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.providers.UtilProvider;

public final class Text
{
    private static EMap<String, String> _translations = new EMap<String, String>();
    private static String lastInitializedFile = null;

    //region Constants

    //Set all these like this so it's obvious if I forgot one or if something goes wrong
    /////////////////////////
    //        Items        //
    /////////////////////////

    // TARDIS Spawn Egg
    public static final String SPAWN_TARDIS                                 = $("SPAWN_TARDIS");
    public static final String USE_THIS_TO_CREATE_A_TARDIS                  = $("USE_THIS_TO_CREATE_A_TARDIS");
    public static final String TOAST                                        = $("TOAST");
    public static final String BURNT_TOAST                                  = $("BURNT_TOAST");

    /////////////////////////
    //     Schematics      //
    /////////////////////////

    // Anchor Points
    public static final String SELECT_ANCHOR_POINTS                         = $("SELECT_ANCHOR_POINTS");
    public static final String SELECT_CORNERS_ANCHOR_POINT                  = $("SELECT_CORNERS_ANCHOR_POINT");
    public static final String RIGHT_CLICK_ANCHOR_POINT                     = $("RIGHT_CLICK_ANCHOR_POINT");
    public static final String LEFT_CLICK_ANCHOR_POINT                      = $("LEFT_CLICK_ANCHOR_POINT");
	public static final String SET_ANCHOR_POINT_TO                          = $("SET_ANCHOR_POINT_TO");

    // Access Points
    public static final String SELECT_ACCESS_POINTS                         = $("SELECT_ACCESS_POINTS");
    public static final String RIGHT_CLICK_ACCESS_POINT                     = $("RIGHT_CLICK_ACCESS_POINT");
    public static final String LEFT_CLICK_ACCESS_POINT                      = $("LEFT_CLICK_ACCESS_POINT");
    public static final String CYCLE_ACCESS_POINT_TYPES                     = $("CYCLE_ACCESS_POINT_TYPES");
    public static final String NO_ACCESS_POINT_AT_LOCATION                  = $("NO_ACCESS_POINT_AT_LOCATION");
    public static final String ADDED_ACCESS_POINT_AT                        = $("ADDED_ACCESS_POINT_AT");
    public static final String REMOVED_ACCESS_POINT_AT_LOCATION             = $("REMOVED_ACCESS_POINT_AT_LOCATION");
    public static final String ACCESS_POINT_EXISTS_AT_LOCATION              = $("ACCESS_POINT_EXISTS_AT_LOCATION");
    public static final String NOT_VALID_FOR_ACCESS_POINT                   = $("NOT_VALID_FOR_ACCESS_POINT");
    public static final String SPAWN_POINT_MUST_BE_CLEAR                    = $("SPAWN_POINT_MUST_BE_CLEAR");

    // Access Point Types
    public static final String HALLWAY_ACCESS_POINT                         = $("HALLWAY_ACCESS_POINT");
    public static final String EXIT_DOOR                                    = $("EXIT_DOOR");
    public static final String CONTROL_X_PLUS                               = $("CONTROL_X_PLUS");
    public static final String CONTROL_X_MINUS                              = $("CONTROL_X_MINUS");
    public static final String CONTROL_Y_PLUS                               = $("CONTROL_Y_PLUS");
    public static final String CONTROL_Y_MINUS                              = $("CONTROL_Y_MINUS");
    public static final String CONTROL_Z_PLUS                               = $("CONTROL_Z_PLUS");
    public static final String CONTROL_Z_MINUS                              = $("CONTROL_Z_MINUS");
    public static final String PARKING_BRAKE                                = $("PARKING_BRAKE");

    // Set
    public static final String SET_COST_TO                                  = $("SET_COST_TO");
    public static final String DESCRIPTION_SET                              = $("DESCRIPTION_SET");

    // Modes
    public static final String SET_MODE_TO_REPLACE                          = $("SET_MODE_TO_REPLACE");
    public static final String SET_MODE_TO_NEW                              = $("SET_MODE_TO_NEW");

    // Other
    public static final String MUST_SPECIFY_BOTH_ANCHOR_POINTS              = $("MUST_SPECIFY_BOTH_ANCHOR_POINTS");
    public static final String MISSING_REQUIRED_ACCESS_POINTS               = $("MISSING_REQUIRED_ACCESS_POINTS");
    public static final String SELECTED_ACCESS_POINTS_ARE_INVALID           = $("SELECTED_ACCESS_POINTS_ARE_INVALID");
    public static final String SCHEMATIC_IS_VALID                           = $("SCHEMATIC_IS_VALID");
    public static final String SELECTED_ACCESS_POINTS_OUTSIDE_BOUNDS        = $("SELECTED_ACCESS_POINTS_OUTSIDE_BOUNDS");
    public static final String SET_ICON                                     = $("SET_ICON");
    public static final String SET_ADDITIONAL_DATA                          = $("SET_ADDITIONAL_DATA");
    public static final String ADDED_DEFAULT_SCHEMATIC                      = $("ADDED_DEFAULT_SCHEMATIC");
    public static final String REPLACING_WITH_UPDATED_SCHEMATIC             = $("REPLACING_WITH_UPDATED_SCHEMATIC");
    public static final String UNABLE_TO_REPLACE_SCHEMATIC                  = $("UNABLE_TO_REPLACE_SCHEMATIC");
    public static final String USE_TOOLS_TO_CREATE_SCHEMATIC                = $("USE_TOOLS_TO_CREATE_SCHEMATIC");
    public static final String MUST_BE_IN_SAME_WORLD                        = $("MUST_BE_IN_SAME_WORLD");
    public static final String SAVING_SCHEMATIC                        		= $("SAVING_SCHEMATIC");

    /////////////////////////
    //       TARDIS        //
    /////////////////////////

    public static final String POLICE                                       = $("POLICE");
    public static final String PUBLIC                                       = $("PUBLIC");
    public static final String CALL                                         = $("CALL");
    public static final String BOX                                          = $("BOX");
    public static final String OWNERS                                       = $("OWNERS");
    public static final String HALLWAY_STYLE                                = $("HALLWAY_STYLE");
    public static final String DIRECT                                       = $("DIRECT");
    public static final String DYNAMIC                                      = $("DYNAMIC");
    public static final String DIRECT_DESC                                  = $("DIRECT_DESC");
    public static final String DYNAMIC_DESC                                 = $("DYNAMIC_DESC");
    public static final String NEED_TARDIS_FOR_THAT                         = $("NEED_TARDIS_FOR_THAT");
    public static final String MUST_SET_HALLWAY_TYPE                        = $("MUST_SET_HALLWAY_TYPE");

    /////////////////////////
    //       Upgrade       //
    /////////////////////////

    // Titles
    public static final String UPGRADE_TARDIS                               = $("UPGRADE_TARDIS");
    public static final String UPGRADE_CONTROL_ROOM                         = $("UPGRADE_CONTROL_ROOM");
    public static final String CHANGE_APPEARANCE                            = $("CHANGE_APPEARANCE");
    public static final String MISCELLANEOUS                                = $("MISCELLANEOUS");
    public static final String CHANGE_HALLWAY_TYPE                          = $("CHANGE_HALLWAY_TYPE");
    public static final String SELECT_ROOM_TO_BUILD                         = $("SELECT_ROOM_TO_BUILD");
    
    // Descriptions
    public static final String UPGRADE_CONTROL_ROOM_DESC_1                  = $("UPGRADE_CONTROL_ROOM_DESC_1");
    public static final String CHANGE_APPEARANCE_DESC_1                     = $("CHANGE_APPEARANCE_DESC_1");
    public static final String MISCELLANEOUS_DESC                           = $("MISCELLANEOUS_DESC");
    public static final String CONSOLE_DESC                                 = $("CONSOLE_DESC");
    public static final String CLOCK_DESC                                   = $("CLOCK_DESC");
    public static final String CHANGE_HALLWAY_TYPE_DESC                     = $("CHANGE_HALLWAY_TYPE_DESC");
    public static final String SPEED_DESC                                   = $("SPEED_DESC");

    // Messages
    public static final String INSUFFICIENT_FUNDS                           = $("INSUFFICIENT_FUNDS");
    public static final String UPGRADING_CONTROL_ROOM                       = $("UPGRADING_CONTROL_ROOM");
    public static final String TARDIS_UNDER_CONSTRUCTION                    = $("TARDIS_UNDER_CONSTRUCTION");
    public static final String SELECTED_CURRENT_CONTROL_ROOM                = $("SELECTED_CURRENT_CONTROL_ROOM");
    public static final String RIGHT_CLICK_TO_ADD                           = $("RIGHT_CLICK_TO_ADD");
    public static final String MUST_SELECT_SOLID_BLOCK                      = $("MUST_SELECT_SOLID_BLOCK");
    public static final String SELECTED_LOCATION_OBSTRUCTED                 = $("SELECTED_LOCATION_OBSTRUCTED");
    public static final String MUST_BE_INSIDE_TARDIS                        = $("MUST_BE_INSIDE_TARDIS");
    public static final String REMOVING_AT                                  = $("REMOVING_AT");
    public static final String CLICK_AGAIN_TO_COMFIRM                       = $("CLICK_AGAIN_TO_COMFIRM");
    public static final String OBSTRUCTED_PATH                              = $("OBSTRUCTED_PATH");
    public static final String CONNECTING_ROOMS                             = $("CONNECTING_ROOMS");
    public static final String YOU_HAVE_ALREADY_UPGRADED_THAT               = $("YOU_HAVE_ALREADY_UPGRADED_THAT");

    // Appearance
    public static final String OWNER_SIGN                                   = $("OWNER_SIGN");
    public static final String POLICE_SIGNS                                 = $("POLICE_SIGNS");
    
    // Miscellaneous
    public static final String ADD_CONSOLE                                  = $("ADD_CONSOLE");
    public static final String ADD_CLOCK                                    = $("ADD_CLOCK");
    public static final String ADD_SIGN                                     = $("ADD_SIGN");
    public static final String CONSOLE                                      = $("CONSOLE");
    public static final String CLOCK                                        = $("CLOCK");
    public static final String SPEED                                        = $("SPEED");

    /////////////////////////
    //      Commands       //
    /////////////////////////

    // Command
    public static final String UPGRADE                                      = $("UPGRADE");
    public static final String SCHEMATIC                                    = $("SCHEMATIC");
    public static final String TP                                           = $("TP");
    public static final String HOME                                         = $("HOME");
    public static final String BACK                                         = $("BACK");
    public static final String ADMIN                                        = $("ADMIN");
    public static final String ALLOW                                       	= $("ALLOW");
    public static final String LIST                                       	= $("LIST");
    public static final String ENTER                                       	= $("ENTER");
    public static final String FLY                                       	= $("FLY");
    public static final String NONE                                       	= $("NONE");
    public static final String DELETE                                       = $("DELETE");

    // Schematic Command
    public static final String VALIDATE                                     = $("VALIDATE");
    public static final String ICON                                         = $("ICON");
    public static final String ITERATION                                    = $("ITERATION");
    public static final String REPLACE                                      = $("REPLACE");

    // Usage

    // Description
    public static final String UPGRADE_YOUR_TARDIS                          = $("UPGRADE_YOUR_TARDIS");
    public static final String TP_DESC                                      = $("TP_DESC");
    public static final String HOME_DESC                                    = $("HOME_DESC");
    public static final String CANCEL_DESC                                  = $("CANCEL_DESC");
    public static final String BACK_DESC                                    = $("BACK_DESC");
    public static final String ADMIN_DESC                                   = $("ADMIN_DESC");
    public static final String ALLOW_DESC                                   = $("ALLOW_DESC");

    // Messages
    public static final String CANT_BE_PLAYER_FOR_COMMAND                   = $("CANT_BE_PLAYER_FOR_COMMAND");
    public static final String MUST_BE_PLAYER_FOR_COMMAND                   = $("MUST_BE_PLAYER_FOR_COMMAND");
    public static final String NEED_TARDIS_FOR_COMMAND                      = $("NEED_TARDIS_FOR_COMMAND");
    public static final String ALREAY_CREATING_SCHEMATIC                    = $("ALREAY_CREATING_SCHEMATIC");
    public static final String NOT_CREATING_SCHEMATIC                       = $("NOT_CREATING_SCHEMATIC");
    public static final String HALTING_SCHEMATIC                            = $("HALTING_SCHEMATIC");
    public static final String SPECIFY_WHAT_TO_SET                          = $("SPECIFY_WHAT_TO_SET");
    public static final String SPECIFY_VALID_PLAYER                         = $("SPECIFY_VALID_PLAYER");
    public static final String HOME_IS_BLOCKED                              = $("HOME_IS_BLOCKED");
    public static final String SET_HOME                                     = $("SET_HOME");
    public static final String CANCELLED_ACTIONS                            = $("CANCELLED_ACTIONS");
    public static final String NOT_PERFORMING_ACTION                        = $("NOT_PERFORMING_ACTION");
    public static final String SCHEMATIC_ITERATION_SET_TO                   = $("SCHEMATIC_ITERATION_SET_TO");
    public static final String PLAYER_DOES_NOT_HAVE_TARDIS                  = $("PLAYER_DOES_NOT_HAVE_TARDIS");
    public static final String TARDIS_HOME_SET_TO                  			= $("TARDIS_HOME_SET_TO");
    public static final String TARDIS_HOME_BLOCKED                  		= $("TARDIS_HOME_BLOCKED");
    public static final String TARDIS_SENT_HOME                  			= $("TARDIS_SENT_HOME");
    public static final String NO_PLAYERS_ALLOWED                  			= $("NO_PLAYERS_ALLOWED");
    public static final String PLAYER_CANNOT_ENTER_TARDIS                  	= $("PLAYER_CANNOT_ENTER_TARDIS");
    public static final String PLAYER_CAN_USE_YOUR_TARDIS                  	= $("PLAYER_CAN_USE_YOUR_TARDIS");
    public static final String PLAYER_CAN_ALREADY_USE_YOUR_TARDIS           = $("PLAYER_CAN_ALREADY_USE_YOUR_TARDIS");
    public static final String PLAYER_IS_NO_LONGER_ALLOWED           		= $("PLAYER_IS_NO_LONGER_ALLOWED");
    public static final String TARDIS_HAS_BEEN_DELETED           		    = $("TARDIS_HAS_BEEN_DELETED");
    public static final String TARDIS_COULD_NOT_BE_DELETED           		= $("TARDIS_COULD_NOT_BE_DELETED");
    public static final String CONFIRM_DELETE           		            = $("CONFIRM_DELETE");
    public static final String TARDIS_IS_BEING_DELETED           		    = $("TARDIS_IS_BEING_DELETED");


    /////////////////////////
    //    Miscellaneous    //
    /////////////////////////

    public static final String NOW_SELECTING                                = $("NOW_SELECTING");
    public static final String SPAWN                                        = $("SPAWN");
    public static final String SPAWN_POINT_SET_TO                           = $("SPAWN_POINT_SET_TO");
    public static final String USAGE                                        = $("USAGE");
    public static final String NOT_VALID_MATERIAL                           = $("NOT_VALID_MATERIAL");
    public static final String SAVE                                         = $("SAVE");
    public static final String CANCEL                                       = $("CANCEL");
    public static final String COST                                         = $("COST");
    public static final String FREE                                         = $("FREE");
    public static final String CONSTANT                                     = $("CONSTANT");
    public static final String DESCRIPTION                                  = $("DESCRIPTION");
    public static final String VALUE                                        = $("VALUE");
    public static final String PLAYER                                       = $("PLAYER");
    public static final String SET                                          = $("SET");
    public static final String ON                                           = $("ON");
    public static final String OFF                                          = $("OFF");
    public static final String BED_SPAWN_SET                                = $("BED_SPAWN_SET");
    public static final String TARDIS_IS_BUSY                               = $("TARDIS_IS_BUSY");
    public static final String CANNOT_FIND_PLAYER                           = $("CANNOT_FIND_PLAYER");

    // Coordinates
    public static final String X                                            = $("X");
    public static final String Y                                            = $("Y");
    public static final String Z                                            = $("Z");
    public static final String WORLD                                        = $("WORLD");

    /////////////////////////
    //       Errors        //
    /////////////////////////

    // Schematics
    public static final String MISSING_CONTROL_ROOM_ORDER                   = $("MISSING_CONTROL_ROOM_ORDER");

    // Tardis
    public static final String EXPECTED_DOOR_AT                             = $("EXPECTED_DOOR_AT");
    public static final String EXPECTED_SIGN_AT                             = $("EXPECTED_SIGN_AT");

    //Quotes
    public static final String DISABLING_QUOTES                             = $("DISABLING_QUOTES");

    //Other
    public static final String NOT_A_VALID_WALL_SIGN                        = $("NOT_A_VALID_WALL_SIGN");
    public static final String NOT_A_VALID_SIGN_POST                        = $("NOT_A_VALID_SIGN_POST");


    //This is mostly used to prevent compiler inlining the static final strings
    private static String $(String name)
    {
        return "**ERROR: Translation not set -> " + name + "**";
    }
    
    //endregion Constants

    //region Everything Else

    static 
    {
        Field[] fields = Text.class.getDeclaredFields();
        for(Field f : fields)
        {
            if(f.getType() == String.class)
            {
                try 
                {
                    _translations.put(f.getName(), (String) f.get(null));
                } 
                catch (IllegalArgumentException | IllegalAccessException e) 
                {
                    Logger.error(e);
                }
            }
        }

        //Make sure everything is filled in with default values
        setLocale(null);
    }

    public static void setLocale(String localeTag)
    {
        setLocale(localeTag, false);
    }

    public static void setLocale(String localeTag, boolean organize)
    {
        Locale locale = localeTag == null ? Locale.ROOT : Locale.forLanguageTag(localeTag);

        InputStream propertyFile = null;
        String suffix = locale.toString();
        if(suffix == null) suffix = "";
        String fileName = null;
        
        while(suffix != null && propertyFile == null)
        {
            fileName = MessageFormat.format("com/adx/jogy34/tardis/util/I18N/I18N{0}.properties", suffix);
            propertyFile = UtilProvider.TARDISPlugin().getResource(fileName);
            if(suffix.isEmpty()) suffix = null;
            else if(suffix.lastIndexOf("_") < 0) suffix = "";
            else suffix = suffix.substring(0, suffix.lastIndexOf("_"));
        }

        if(propertyFile == null)
        {
            //One last try just in case
            fileName = "com/adx/jogy34/tardis/util/I18N/I18N.properties";
            propertyFile = UtilProvider.TARDISPlugin().getResource(fileName);
            if(propertyFile == null)
            {
                Logger.error("could not load default translations file");
                return;
            }
        }

        if(organize)
        {
            //Parse properties file and create all of the constants for the top of this file
            try(BufferedReader reader = new BufferedReader(new InputStreamReader(propertyFile)))
            {
                EList<String> lines = new EList<String>();
                String outer =  "\t/////////////////////////";
                String spacing = "                                             ";
                String line;
                boolean major = false;
                while((line = reader.readLine()) != null)
                {
                    if(line.startsWith("#"))
                    {
                        if(line.startsWith("##"))
                        {
                            major = !major;
                        }
                        else
                        {
                            line = line.replaceAll("#", "").trim();
                            if(major) 
                            {
                                String center = "\t//                     //";
                                for(int i = 0; i < line.length() && center.contains(""); i++)
                                {
                                    center = center.replaceFirst(" ", "");
                                }
                                center = center.substring(0, center.length() / 2) + line + center.substring(center.length() / 2);
                                lines.add(outer);
                                lines.add(center);
                                lines.add(outer);
                                
                            }
                            else
                            {
                                lines.add("\t// " + line);
                            }
                        }
                    }
                    else 
                    {
                        if(line.contains("=")) 
                        {
                            String title = line.split("=")[0].trim();
                            lines.add("\tpublic static final String " + title + spacing.substring(title.length()) + "= $(\"" + title + "\");");
                        }
                        else 
                        {
                            lines.add("");
                        }
                    }
                }
                Logger.debug(lines.toArray());
                return;
            }
            catch(Exception e)
            {
                Logger.debug(e);
            }
        }

        if(fileName.equals(lastInitializedFile))
        {
            return;
        }
        
        lastInitializedFile = fileName;
        
        try 
        {
            ResourceBundle rb = new PropertyResourceBundle(propertyFile);
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            
            for(Enumeration<String> keys = rb.getKeys(); keys.hasMoreElements();)
            {
                String key = keys.nextElement();
                try
                {
                    Field f = Text.class.getDeclaredField(key.toUpperCase());
                    if(f.getType() == String.class)
                    {
                        String value = rb.getString(key);
                        f.setAccessible(true);
                        modifiersField.setInt(f, f.getModifiers() & ~Modifier.FINAL);
                        f.set(null, value);

                        _translations.put(f.getName(), value);
                    }
                    else
                    {
                        Logger.warning("Translations file contains additional value: " + key);
                    }
                }
                catch(NoSuchFieldException e)
                {
                    Logger.warning("Translations file contains additional value: " + key);
                }
                catch(IllegalAccessException e)
                {
                    Logger.warning("Could not update translation for: " + key);
                }
            }
        }
        catch (Exception e) 
        {
            Logger.error(e);
        }
    }

    public static String format(String pattern)
    {
        for(Entry<String, String> e : _translations.entrySet())
        {
            pattern = pattern.replaceAll("\\{" + e.getKey() + "\\}", e.getValue());
        }
        return pattern;
    }

    public static String format(String pattern, Object... arguments)
    {
        return MessageFormat.format(format(pattern), arguments);
    }

    public static String format(String pattern, boolean cSharpStyle, Object... arguments)
    {
        if(cSharpStyle) return format(pattern, arguments);
        return String.format(format(pattern), arguments);
    }

    //endregion Everything Else
}