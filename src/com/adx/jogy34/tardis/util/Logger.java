package com.adx.jogy34.tardis.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jogy34.tardis.providers.ConfigProvider;
import com.adx.jogy34.tardis.providers.UtilProvider;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;

public final class Logger
{
    public static enum LogLevel
    {
        INFO("Info", true, false, "[TARDIS Info]", ChatColor.GRAY, false, false, false),
        DEBUG("Debug", false, false, "[TARDIS Debug]", ChatColor.GREEN, true, true, true),
        WARNING("Warning", true, true, "[TARDIS Warning]", ChatColor.GOLD, true, false, true),
        ERROR("Error", true, true, "[TARDIS Error]", ChatColor.DARK_RED, true, true, true);

        public final String configPath, header;
        public final boolean defaultConsole, defaultFile, printClass, printMethod, printLine;
        public final ChatColor logColor;
        private boolean consoleLog, fileLog;

        private LogLevel(String configPath, boolean defaultConsole, boolean defaultFile, String header, ChatColor logColor, boolean printClass, boolean printMethod, boolean printLine)
        {
            this.configPath = configPath;
            this.header = header;
            this.defaultConsole = defaultConsole;
            this.defaultFile = defaultFile;
            this.printClass = printClass;
            this.printMethod = printMethod;
            this.printLine = printLine;
            this.logColor = logColor;
        }

        public void addDefaults()
        {
            ConfigProvider.getConfig().addDefault("Logging.Console." + configPath, defaultConsole);
            ConfigProvider.getConfig().addDefault("Logging.File." + configPath, defaultFile);
        }

        public void init()
        {
            this.consoleLog = ConfigProvider.getConfig().getBoolean("Logging.Console." + configPath);
            this.fileLog = ConfigProvider.getConfig().getBoolean("Logging.File." + configPath);
        }

        public boolean shouldLogCodeInfo()
        {
            return this.printClass || this.printLine || this.printMethod;
        }

        public boolean shouldLogToConsole()
        {
            return this.consoleLog;
        }
        
        public boolean shouldLogToFile()
        {
            return this.fileLog;
        }
    }

    private static final String LOG_FILE_NAME = "Logs/tardis{0}{1}.log";

    private static long maxLogFileSizeBytes;
    private static int maxLogFileCount;
    private static File logFile;

    public static void addDefaults()
    {
        for(LogLevel l : LogLevel.values())
        {
            l.addDefaults();
        }
    }

    public static void init()
    {
        maxLogFileSizeBytes = (long) (ConfigProvider.getConfig().getDouble(ConfigProvider.Constants.LOGGING_FILE_MAX_SIZE) * 1048576L);
        maxLogFileCount = ConfigProvider.getConfig().getInt(ConfigProvider.Constants.LOGGING_FILE_NUMBER);

        for(LogLevel l : LogLevel.values())
        {
            l.init();
        }

        logFile = new File(UtilProvider.TARDISPlugin().getDataFolder(), MessageFormat.format(LOG_FILE_NAME, "", ""));
    }

    public static void info(Object... messages)
    {
        log(LogLevel.INFO, messages);
    }
    
    public static void info(Exception e)
    {
        log(LogLevel.INFO, e);
    }

    public static void debug(Object... messages)
    {
        log(LogLevel.DEBUG, messages);
    }

    public static void trace(Object... messages)
    {
        EList<Object> lines = new EList<Object>(messages);
        lines.AddAll(Thread.currentThread().getStackTrace());
        log(LogLevel.DEBUG, lines.toArray());
    }

    public static void debug(Exception e)
    {
        log(LogLevel.DEBUG, e);
    }

    public static void debug(IConfigSerializable item)
    {
        log(LogLevel.DEBUG, item);
    }

    public static void warning(Object... messages)
    {
        log(LogLevel.WARNING, messages);
    }

    public static void warning(Exception e)
    {
        log(LogLevel.WARNING, e);
    }

    public static void error(Object... messages)
    {
        log(LogLevel.ERROR, messages);
    }

    public static void error(Exception e)
    {
        log(LogLevel.ERROR, e);
    }

    public static void log(LogLevel level, IConfigSerializable item)
    {
        EMap<String, Object> props = ConfigProvider.getConfigProperties(item);
        log(level, props.Select(x -> MessageFormat.format("{0}: {1}", x.getKey(), x.getValue())).ToArray());
    }

    public static void log(LogLevel level, Exception e)
    {
        Throwable current = e;
        EList<String> lines = new EList<String>();
        while(current != null)
        {
            lines.add((lines.Any() ? "Caused By: " : "") + current.getClass().getName() + ": " + current.getMessage());
            lines.addAll(new EList<>(current.getStackTrace()).Select(x -> "    at " + x.toString()).ToList());
            current = current.getCause();
        }
        log(level, lines.ToArray());
    }

    public static void log(LogLevel level, Object... messages)
    {
        if(!level.shouldLogToConsole() && !level.shouldLogToFile()) return;

        EList<String> lines = new EList<String>();

        for(Object o : messages)
        {
            String s = "" + o;
            lines.add(s.replaceAll(ChatColor.RESET.toString(), ChatColor.RESET.toString() + level.logColor));
        }

        if(level.shouldLogCodeInfo())
        {
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            int callIndex = 1;
            while(elements[callIndex].getClassName().equals(Logger.class.getName()) && callIndex < elements.length - 1) callIndex++;
            
            if(level.printClass) lines.add("Class: " + elements[callIndex].getClassName());
            if(level.printMethod) lines.add("Method: " + elements[callIndex].getMethodName());
            if(level.printLine) lines.add("Line: " + elements[callIndex].getLineNumber());
        }

        if(level.shouldLogToConsole())
        {
            ConsoleCommandSender console = Bukkit.getConsoleSender();

            lines.ForEach(l -> console.sendMessage(level.logColor + level.header + " " + l));
        }

        if(level.shouldLogToFile())
        {
            if(!logFile.exists())
            {
                if (!logFile.getParentFile().exists()) logFile.getParentFile().mkdirs();
                try 
                {
                    logFile.createNewFile();
                }
                catch (IOException e)
                {
                    Bukkit.getConsoleSender().sendMessage(LogLevel.ERROR.logColor + LogLevel.ERROR.header + " Could not create log file: " + e.getMessage());
                    e.printStackTrace();
                    return;
				}
            }
            
            try
            {
                String header = getTimestamp(true) + " " + level.header + " ";
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(logFile, true)));
                lines.ForEach(l -> writer.println(header + ChatColor.stripColor(l)));
                writer.flush();
                writer.close();
            }
            catch(Exception e)
            {
                Bukkit.getConsoleSender().sendMessage(LogLevel.ERROR.logColor + LogLevel.ERROR.header + " Could not write to log file: " + e.getMessage());
                e.printStackTrace();
                return;
            }

            if(logFile.length() >= maxLogFileSizeBytes)
            {
                String timestamp = getTimestamp(false);
                int count = 2;
                String newLogFileName = MessageFormat.format(LOG_FILE_NAME, "_" + timestamp, "");
                File newLogFile = new File(UtilProvider.TARDISPlugin().getDataFolder(), newLogFileName);
                while(newLogFile.exists())
                {
                    newLogFileName = MessageFormat.format(LOG_FILE_NAME, "_" + timestamp, " (" + count + ")");
                    newLogFile = new File(UtilProvider.TARDISPlugin().getDataFolder(), newLogFileName);
                    count++;
                }

                logFile.renameTo(newLogFile);
                logFile = new File(UtilProvider.TARDISPlugin().getDataFolder(), MessageFormat.format(LOG_FILE_NAME, "", ""));
                
                IEnumerable<File> allLogFiles = new EList<File>(logFile.getParentFile().listFiles()).Where(x -> x.getName().endsWith(".log"));
                if(allLogFiles.Count() > maxLogFileCount)
                {
                    String[] names = allLogFiles.Select(x -> x.getName().substring(0, x.getName().length() - ".log".length())).ToArray(String.class);
                    Arrays.sort(names);
                    String fileName = names[0] + ".log";
                    File toDelete = allLogFiles.First(x -> x.getName().equals(fileName));
                    toDelete.delete();
                }
            }
        }
    }

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static String getTimestamp(boolean withTime)
    {
        return withTime ? dateTimeFormat.format(new Date()) : dateFormat.format(new Date());
    }
}