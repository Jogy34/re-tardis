package com.adx.jogy34.tardis.util.interfaces;

/**
 * Signifies that this class is set up to be serialized to config values.
 * Decorate properties that should be saved with 
 * {@link com.adx.jogy34.tardis.util.annotations.ConfigProperty ConfigProperty}.
 * <br/><br/>
 * <b>MUST have an empty constructor</b> <i>(can be private or protected)</i>
 */
public interface IConfigSerializable
{
}