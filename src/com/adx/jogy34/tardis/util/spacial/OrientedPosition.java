package com.adx.jogy34.tardis.util.spacial;

import java.util.UUID;

import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.helpers.BlockFaceHelper;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;

public class OrientedPosition extends Position
{
    @ConfigProperty("Facing")
    protected BlockFace facing;
    
    public OrientedPosition()
    {
        this(0, 0, 0, Bukkit.getWorlds().get(0).getUID());
    }

    public OrientedPosition(Location loc)
    {
        this(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), loc.getWorld().getUID());
    }

    public OrientedPosition(int x, int y, int z, UUID world)
    {
        this(x, y, z, world, BlockFace.NORTH);
    }

    public OrientedPosition(int x, int y, int z, UUID world, BlockFace facing)
    {
        super(x, y, z, world);
        this.facing = BlockFaceHelper.getCardinalDirection(facing);
    }

    public BlockFace getFacing() { return this.facing; }
    public void setFacing(BlockFace facing) { this.facing = BlockFaceHelper.getCardinalDirection(facing); }

    public void set(Position p) { this.set(p.getX(), p.getY(), p.getZ(), p.getWorldUID());}
    public void set(int x, int y, int z, UUID world, BlockFace facing) 
    { 
        super.set(x, y, z, world);
        this.facing = facing;
    }

    @Override
    public OrientedPosition add(int x, int y, int z) {
        return new OrientedPosition(this.x + x, this.y + y, this.z + z, world, facing);
    }

    @Override
    public String toString() 
    {
        String base = super.toString();
        return String.format("%s, Facing: %s", base, this.facing.toString());
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(super.equals(obj))
        {
            if(obj instanceof OrientedPosition)
            {
                return ((OrientedPosition) obj).facing == facing;
            }
            
            return true;
        }

        return false;
    }

    @Override
    public OrientedPosition clone()
    {
        return new OrientedPosition(x, y, z, world, facing);
    }
}