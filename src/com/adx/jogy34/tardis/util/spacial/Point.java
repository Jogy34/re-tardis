package com.adx.jogy34.tardis.util.spacial;

import java.text.MessageFormat;

import com.adx.jlinq.tuples.Tuples.Tuple;
import com.adx.jogy34.tardis.schematics.TardisSchematicSize;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.annotations.KnownTypes;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

@KnownTypes({Position.class, OrientedPosition.class})
public class Point implements IConfigSerializable
{
    @ConfigProperty("X")
    protected int x;
    @ConfigProperty("Y")
    protected int y;
    @ConfigProperty("Z")
    protected int z;

    public Point() {}

    public Point(Block b) { this(b.getX(), b.getY(), b.getZ()); }
    public Point(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public void setX(int x) { this.x = x; }
    public void setY(int y) { this.y = y; }
    public void setZ(int z) { this.z = z; }

    public int getX() { return this.x; }
    public int getY() { return this.y; }
    public int getZ() { return this.z; }

    public void set(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point subtract(TardisSchematicSize size) { return this.subtract(size.getX(), size.getY(), size.getZ()); }
    public Point subtract(Point p) { return this.subtract(p.getX(), p.getY(), p.getZ()); }
    public Point subtract(int x, int y, int z) { return add(-x, -y, -z); }

    public Point add(TardisSchematicSize size) { return this.add(size.getX(), size.getY(), size.getZ()); }
    public Point add(Point p) { return this.add(p.getX(), p.getY(), p.getZ()); }
    public Point add(int x, int y, int z)
    {
        return new Point(this.x + x, this.y + y, this.z + z);
    }

    public Point abs()
    {
        return new Point(Math.abs(x), Math.abs(y), Math.abs(z));
    }

    public Location toLocation(World w) { return toLocation(w, true); }
    public Location toLocation(World w, boolean center)
    {
        return new Location(w, x + (center ? 0.5 : 0), y, z + (center ? 0.5 : 0));
    }

    public Position toPosition(World w) {
        return new Position(x, y, z, w);
    }

    @Override
    public Point clone()
    {
        return new Point(x, y, z); 
    }

    public Block getBlock(World w)
    {
        return w.getBlockAt(x, y, z);
    }

    public Block getBlockRelative(World w, int x, int y, int z)
    {
        return w.getBlockAt(this.x + x, this.y + y, this.z + z);
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj == null) return false;

        if(obj instanceof Point)
        {
            Point other = (Point) obj;
            return other.x == x && other.y == y && other.z == z;
        }

        return false;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0}, {1}, {2}", x, y, z);
    }

    /**
     * Returns two new positions. The first posisition is the lowest in all directions
     * while the second is the highest. Both positions are in the world of this position
     * 
     * @param p2 The other position to order this one with
     * @return The two new positions
     */
    public Tuple<Point, Point> order(Point p2)
    {
        return new Tuple<Point,Point>(
            new Point(Math.min(x, p2.x), Math.min(y, p2.y), Math.min(z, p2.z)),
            new Point(Math.max(x, p2.x), Math.max(y, p2.y), Math.max(z, p2.z))
        );
    }
}