package com.adx.jogy34.tardis.util.spacial;

import java.text.MessageFormat;

import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

public class Bound<T extends Comparable<T>> implements IConfigSerializable
{
    @ConfigProperty("Min")
    private T min;
    @ConfigProperty("Max")
    private T max;

    public Bound()
    {
    }

    public Bound(T first, T second)
    {
        this.min = first;
        this.max = second;
        if(first.compareTo(second) > 0)
        {
            this.min = second;
            this.max = first;
        }
    }

    public boolean isWithinBound(T val)
    {
        if(val == null) return false;
        return val.compareTo(this.min) >= 0 && val.compareTo(this.max) <= 0;
    }

    public T getMin()
    {
        return this.min;
    }

    public T getMax()
    {
        return this.max;
    }

    @Override
    public String toString() 
    {
        return MessageFormat.format("{0} -> {1}", min, max);
    }
}