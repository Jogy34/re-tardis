package com.adx.jogy34.tardis.util.spacial;

import java.text.MessageFormat;
import java.util.UUID;

import com.adx.jlinq.tuples.Tuples.Tuple;
import com.adx.jogy34.tardis.schematics.TardisSchematicSize;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.annotations.KnownTypes;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

@KnownTypes({OrientedPosition.class})
public class Position extends Point implements IConfigSerializable
{
    @ConfigProperty("World")
    protected UUID world;

    //Save the world for convienance purposes
    protected World actualWorld;

    public Position()
    {
    }

    public Position(Position p)
    {
        this(p.x, p.y, p.z, p.world);
    }

    public Position(Location loc)
    {
        this(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), loc.getWorld().getUID());
    }

    public Position(Block block)
    {
        this(block.getX(), block.getY(), block.getZ(), block.getWorld().getUID());
    }

    public Position(int x, int y, int z, World world)
    {
        this(x, y, z, world.getUID());
    }

    public Position(int x, int y, int z, UUID world)
    {
        super(x, y, z);
        this.world = world;
        this.actualWorld = Bukkit.getWorld(world);
    }

    public World getWorld() { return this.actualWorld = this.actualWorld == null ? Bukkit.getWorld(this.world) : actualWorld; }
    public UUID getWorldUID() { return this.world; }

    public void setWorld(World w) {  this.setWorld(w.getUID()); }
    public void setWorld(UUID w) { 
        this.world = w; 
        this.actualWorld = Bukkit.getWorld(world);
    }

    public void set(int x, int y, int z, UUID world) 
    { 
        this.x = x;
        this.y = y;
        this.z = z;
        this.world = world;
        this.actualWorld = Bukkit.getWorld(world);
    }

    public Block getBlock() { return this.getBlock(this.getWorld()); }
    public Block getBlockRelative(int x, int y, int z) { return this.getBlockRelative(this.getWorld(), x, y, z); }

    public Location toLocation() { return toLocation(true); }
    public Location toLocation(boolean center) { return toLocation(this.getWorld(), center); }

    @Override
    public String toString() 
    {
        return MessageFormat.format("{0}: ({1}, {2}, {3})", this.getWorld().getName(), x, y, z);
    }
    
    public String toDebugString()
    {
        return String.format("X: %d, Y: %d, Z: %d, World: %s, World UID: %s", x, y, z, this.getWorld().getName(), world.toString());
    }


    @Override
    public Position subtract(TardisSchematicSize size) { return this.subtract(size.getX(), size.getY(), size.getZ()); }
    @Override
    public Position subtract(Point p) { return this.subtract(p.getX(), p.getY(), p.getZ()); }
    @Override
    public Position subtract(int x, int y, int z) { return add(-x, -y, -z); }

    @Override
    public Position add(TardisSchematicSize size) { return this.add(size.getX(), size.getY(), size.getZ()); }
    @Override
    public Position add(Point p) { return this.add(p.getX(), p.getY(), p.getZ()); }

    @Override
    public Position add(int x, int y, int z)
    {
        return new Position(this.x + x, this.y + y, this.z + z, world);
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj == null) return false;

        Position other = null;
        if(obj instanceof Position) other = (Position) obj;
        else if(obj instanceof Location) other = new Position((Location) obj);
        else if(obj instanceof Block) other = new Position((Block) obj);

        if(other == null) return false;

        return this.x == other.x && this.y == other.y && this.z == other.z && this.world.equals(other.world);
    }

    public Point toPoint()
    {
        return new Point(x, y, z);
    }

    @Override
    public Position clone()
    {
        return new Position(x, y, z, world);
    }

    /**
     * Returns two new positions. The first posisition is the lowest in all directions
     * while the second is the highest. Both positions are in the world of this position
     * 
     * @param p2 The other position to order this one with
     * @return The two new positions
     */
    public Tuple<Position, Position> order(Position p2)
    {
        return new Tuple<Position,Position>(
            new Position(Math.min(x, p2.x), Math.min(y, p2.y), Math.min(z, p2.z), world),
            new Position(Math.max(x, p2.x), Math.max(y, p2.y), Math.max(z, p2.z), world)
        );
    }
}