package com.adx.jogy34.tardis.util.spacial;

import com.adx.jlinq.tuples.Tuples.Tuple;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

public class BoundCube
{
    protected Point lower, upper;

    public BoundCube(Point p1, Point p2)
    {
        Tuple<Point, Point> ordered = p1.order(p2);

        lower = ordered.item1;
        upper = ordered.item2;
    }

    public boolean isWithinBounds(Point p)
    {
        return p.getX() >= lower.getX() && p.getX() <= upper.getX() &&
               p.getY() >= lower.getY() && p.getY() <= upper.getY() &&
               p.getZ() >= lower.getZ() && p.getZ() <= upper.getZ();
    }

    public Point getLower()
    {
        return lower;
    }

    public Point getUpper() 
    {
        return upper;
    }

    public boolean isClear(World w)
    {
        for(int x = lower.getX(); x <= upper.getX(); x++)
        {
            for(int y = lower.getY(); y <= upper.getY(); y++)
            {
                for(int z = lower.getZ(); z <= upper.getZ(); z++)
                {
                    if(w.getBlockAt(x, y, z).getType() != Material.AIR) return false;
                }
            }
        }

        return true;
    }

    public void updateLightingInBounds(World w)
    {
        for(int x = lower.getX(); x <= upper.getX(); x++)
        {
            for(int y = lower.getY(); y <= upper.getY(); y++)
            {
                for(int z = lower.getZ(); z <= upper.getZ(); z++)
                {
                    Block light = w.getBlockAt(x, y, z);
                    if(light.getType() != Material.AIR)
                    {
                        Chunk c = light.getChunk();
                        if(!c.isLoaded()) c.load(true);
                        boolean updated = false;
                        for(int i = -1; i <= 1 && !updated; i++)
                        {
                            for(int j = -1; j <= 1 && !updated; j++)
                            {
                                for(int k = -1; k <= 1 && !updated; k++)
                                {
                                    Block b = light.getRelative(i, j, k);
                                    if(b.getType() == Material.AIR)
                                    {
                                        b.setType(Material.HAY_BLOCK, false);
                                        b.setType(Material.AIR);
                                        updated = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }   
        }
    }
}