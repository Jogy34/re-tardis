package com.adx.jogy34.tardis.util.spacial;

import java.util.UUID;

import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;

public class NoFlyChunk implements IConfigSerializable
{
    @ConfigProperty("X")
    protected int x;
    @ConfigProperty("Z")
    protected int z;
    @ConfigProperty("World")
    protected UUID worldUid;

    protected World world;
    protected BoundCube bounds;

    @SuppressWarnings("unused")
    private NoFlyChunk() {}

    public NoFlyChunk(Chunk c)
    {
        this.x = c.getX();
        this.z = c.getZ();
        this.world = c.getWorld();
        this.worldUid = this.world.getUID();
    }

    public World getWorld()
    {
        if(this.world == null) this.world = Bukkit.getWorld(this.worldUid);

        return this.world;
    }

    public BoundCube getBoundCube()
    {
        if(bounds == null)
        {
            bounds = new BoundCube(new Point(x * 16, 0, z * 16), new Point((x + 1) * 16 - 1, this.getWorld().getMaxHeight(), (z + 1) * 16 - 1));
        }

        return bounds;
    }

    public Chunk getChunk()
    {
        return this.getWorld().getChunkAt(x, z);
    }

    public boolean isThisChunk(Chunk c)
    {
        return this.x == c.getX() && this.z == c.getZ() && c.getWorld().getUID().equals(this.worldUid);
    }

    public boolean canLand(Position p) 
    {
        return !p.getWorldUID().equals(this.worldUid) || !this.getBoundCube().isWithinBounds(p);
    }
}