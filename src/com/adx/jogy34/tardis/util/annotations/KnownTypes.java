package com.adx.jogy34.tardis.util.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface KnownTypes
{
    Class<? extends IConfigSerializable>[] value();
}