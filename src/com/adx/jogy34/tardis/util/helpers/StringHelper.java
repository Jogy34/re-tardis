package com.adx.jogy34.tardis.util.helpers;

import java.util.Arrays;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public final class StringHelper
{
    private static EMap<String, Boolean> boolValues = new EMap<String, Boolean>();
    private static EList<Class<?>> canConvert;

    static 
    {
        boolValues.put("0", false);
        boolValues.put("false", false);
        boolValues.put("no", false);
        boolValues.put("n", false);
        
        boolValues.put("1", true);
        boolValues.put("true", true);
        boolValues.put("yes", true);
        boolValues.put("y", true);

        canConvert =  new EList<Class<?>>(Arrays.asList(
            String.class, int.class, double.class, float.class, byte.class,
            short.class, long.class, char.class, boolean.class,
            Integer.class, Double.class, Float.class, Byte.class,
            Short.class, Long.class, Character.class, Boolean.class
        ));
    }

    public static String toDisplayCase(String s) 
    {
        final String ACTIONABLE_DELIMITERS = " -/";

        StringBuilder sb = new StringBuilder();
        boolean capNext = true;

        for (char c : s.toCharArray()) 
        {
            c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
            sb.append(c);
            capNext = (ACTIONABLE_DELIMITERS.indexOf((int) c) >= 0);
        }
        
        return sb.toString();
    }

    public static String getName(Material mat)
    {
        ItemMeta meta = new ItemStack(mat).getItemMeta();
        if(meta != null && meta.hasLocalizedName()) return meta.getLocalizedName();
        return toDisplayCase(mat.toString().replaceAll("_", " "));
    }

    public static boolean canConvert(Class<?> clazz)
    {
        return canConvert.contains(clazz) || clazz.isEnum();
    }

    public static <T> T[] convertArray(String[] items, Class<T> type)
    {
        return convert(items, type).ToArray(type);
    }

    public static <T> EList<T> convert(String[] items, Class<T> type)
    {
        return new EList<>(items).Select(x -> convert(x, type)).ToList();
    }

    @SuppressWarnings("unchecked")
    public static <T> T convert(String s, Class<T> type)
    {
        if(s == null) return null;

        Object result = null;

        if(type == String.class) result = s;
        else if(type == int.class || type == Integer.class) result = toIntOrDefault(s);
        else if(type == double.class || type == Double.class) result = toDoubleOrDefault(s);
        else if(type == float.class || type == Float.class) result = toFloatOrDefault(s);
        else if(type == byte.class || type == Byte.class) result = toBooleanOrDefault(s);
        else if(type == short.class || type == Short.class) result = toShortOrDefault(s);
        else if(type == long.class || type == Long.class) result = toLongOrDefault(s);
        else if(type == char.class || type == Character.class) result = toChar(s);
        else if(type == boolean.class || type == Boolean.class) result = toBooleanOrDefault(s);
        else if(type.isEnum()) result = new EList<Object>(type.getEnumConstants()).FirstOrDefault(e -> e.toString().equalsIgnoreCase(s));

        return (T) result;
    }

    public static String[] toArray(String s)
    {
        if(s == null || s.isEmpty()) return new String[0];

        if(s.startsWith("[")) s = s.substring(1);
        if(s.endsWith("]")) s = s.substring(0, s.length() - 1);

        return new EList<>(s.split(",")).Select(x -> x.trim()).ToArray(String.class);
    }

    public static String fromSnakeCase(String s)
    {
        return toDisplayCase(s.replaceAll("_", " "));
    }

    public static boolean isInt(String str) 
    {
        try 
        {
            Integer.parseInt(str);
            return true;
        }
        catch(Exception e) 
        {
            return false;
        }
    }

    public static boolean isDouble(String str)
    {
        try
        {
            Double.parseDouble(str);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    public static boolean isFloat(String str)
    {
        try
        {
            Float.parseFloat(str);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    public static boolean isBoolean(String str)
    {
        return str.equalsIgnoreCase("true") || str.equalsIgnoreCase("false");
    }

    public static boolean canBeBoolean(String str)
    {
        return boolValues.containsKey(str.toLowerCase());
    }

    public static boolean isMaterial(String str)
    {
        try
        {
            Material.valueOf(str);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    public static int toIntOrDefault(String str) { return toIntOrDefault(str, 0); }
    public static int toIntOrDefault(String str, int def)
    {
        try 
        {
            return Integer.parseInt(str);
        }
        catch(Exception e) 
        {
            return def;
        }
    }

    public static byte toByteOrDefault(String str) { return toByteOrDefault(str, (byte) 0); }
    public static byte toByteOrDefault(String str, byte def)
    {
        try 
        {
            return Byte.parseByte(str);
        }
        catch(Exception e) 
        {
            return def;
        }
    }

    public static short toShortOrDefault(String str) { return toShortOrDefault(str, (short) 0); }
    public static short toShortOrDefault(String str, short def)
    {
        try 
        {
            return Short.parseShort(str);
        }
        catch(Exception e) 
        {
            return def;
        }
    }

    public static long toLongOrDefault(String str) { return toLongOrDefault(str, 0l); }
    public static long toLongOrDefault(String str, long def)
    {
        try 
        {
            return Long.parseLong(str);
        }
        catch(Exception e) 
        {
            return def;
        }
    }

    public static float toFloatOrDefault(String str) { return toFloatOrDefault(str, 0f); }
    public static float toFloatOrDefault(String str, float def)
    {
        try 
        {
            return Float.parseFloat(str);
        }
        catch(Exception e) 
        {
            return def;
        }
    }

    public static double toDoubleOrDefault(String str) { return toDoubleOrDefault(str, 0d); }
    public static double toDoubleOrDefault(String str, double def)
    {
        try 
        {
            return Double.parseDouble(str);
        }
        catch(Exception e) 
        {
            return def;
        }
    }

    public static boolean toBoolean(String str)
    {
        return Boolean.parseBoolean(str);
    }

    public static boolean toBooleanOrDefault(String str) { return toBooleanOrDefault(str, false); }
    public static boolean toBooleanOrDefault(String str, boolean def)
    {
        return boolValues.getOrDefault(str.toLowerCase(), def);
    }

    public static Material toMaterialOrDefault(String str) { return toMaterialOrDefault(str, Material.AIR); }
    public static Material toMaterialOrDefault(String str, Material def)
    {
        try 
        {
            return Material.valueOf(str);
        }
        catch(Exception e) 
        {
            return def;
        }
    }

    public static char toChar(String s) 
    {
        if(s == null || s.length() == 0) return ' ';
        return s.charAt(0);
    }

    public static String leftPad(String s, char padding, int desiredLength)
    {
        int addAmount = desiredLength - s.length();
        if(addAmount > 0) 
        {
            for(int i = 0; i < addAmount; i++) s = padding + s;
        }
        
        return s;
    }

    public static String rightPad(String s, char padding, int desiredLength)
    {
        int addAmount = desiredLength - s.length();
        if(addAmount > 0) 
        {
            for(int i = 0; i < addAmount; i++) s += padding;
        }

        return s;
    }
}
