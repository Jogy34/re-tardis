package com.adx.jogy34.tardis.util.helpers;

import com.adx.jogy34.tardis.data.MiscMaterialData;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.Bisected.Half;
import org.bukkit.block.data.type.Door;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public final class BlockHelper
{
    public static void createDoor(Block doorBottom, Material doorMaterial, BlockFace facing)
    {
        Block doorTop = doorBottom.getRelative(0, 1, 0);

        Door doorTopData = (Door) Bukkit.createBlockData(doorMaterial);
        Door doorBottomData = (Door) Bukkit.createBlockData(doorMaterial);

        doorTopData.setFacing(facing);
        doorBottomData.setFacing(facing);

        doorTopData.setHalf(Half.TOP);
        doorBottomData.setHalf(Half.BOTTOM);

        doorTop.setBlockData(doorTopData, false);
        doorBottom.setBlockData(doorBottomData, false);
    }

    public static void createWallSign(Block signBlock, Material signType, BlockFace facing, String... text)
    {
        if(!MiscMaterialData.WALL_SIGNS.contains(signType)) {
            Logger.warning(new IllegalArgumentException(Text.format("{NOT_A_VALID_WALL_SIGN}", signType)));
            signType = Material.OAK_WALL_SIGN;
        }

        signBlock.setType(signType, false);

        WallSign wall = (WallSign) signBlock.getBlockData();
        wall.setFacing(facing);

        signBlock.setBlockData(wall);
        setSignText(signBlock, text);
    }

    public static void createSignPost(Block signBlock, Material signType, BlockFace facing, String... text)
    {
        if(!MiscMaterialData.SIGN_POSTS.contains(signType)) {
            Logger.warning(new IllegalArgumentException(Text.format("{NOT_A_VALID_SIGN_POST}", signType)));
            signType = Material.OAK_SIGN;
        }

        signBlock.setType(signType, false);

        org.bukkit.block.data.type.Sign sign = (org.bukkit.block.data.type.Sign) signBlock.getBlockData();
        sign.setRotation(facing);

        signBlock.setBlockData(sign);
        setSignText(signBlock, text);
    }

    public static Material toSignPostMaterial(Material wallSign)
    {
        if(MiscMaterialData.SIGN_POSTS.contains(wallSign)) return wallSign;
        if(wallSign == null) return Material.OAK_SIGN;

        switch(wallSign)
        {
            case ACACIA_WALL_SIGN: return Material.ACACIA_SIGN;
            case BIRCH_WALL_SIGN: return Material.BIRCH_SIGN;
            case DARK_OAK_WALL_SIGN: return Material.DARK_OAK_SIGN;
            case JUNGLE_WALL_SIGN: return Material.JUNGLE_SIGN;
            case SPRUCE_WALL_SIGN: return Material.SPRUCE_SIGN;
            
            case OAK_WALL_SIGN:
            default:
            {
                return Material.OAK_SIGN;
            }
        }
    }

    public static Material toWallSignMaterial(Material signPost)
    {
        if(MiscMaterialData.WALL_SIGNS.contains(signPost)) return signPost;
        if(signPost == null) return Material.OAK_WALL_SIGN;

        switch(signPost)
        {
            case ACACIA_SIGN: return Material.ACACIA_WALL_SIGN;
            case BIRCH_SIGN: return Material.BIRCH_WALL_SIGN;
            case DARK_OAK_SIGN: return Material.DARK_OAK_WALL_SIGN;
            case JUNGLE_SIGN: return Material.JUNGLE_WALL_SIGN;
            case SPRUCE_SIGN: return Material.SPRUCE_WALL_SIGN;

            case OAK_SIGN:
            default:
            {
                return Material.OAK_WALL_SIGN;
            }
        }
    }

    public static boolean setSignText(Block signBlock, String... text)
    {
        BlockState blockState = signBlock.getState();
        if(blockState instanceof Sign)
        {
            Sign state = (Sign) signBlock.getState();
            for(int i = 0; i < 4; i++)
            {
                String line = (text.length > i && text[i] != null) ? text[i] : "";
                state.setLine(i, line);
            }
    
            state.update(false, false);
            return true;
        }

        return false;
    }

    public static BlockFace getRealDirection(Player p)
    {
        Vector dir = p.getLocation().getDirection();
        BlockFace best = BlockFace.NORTH;
        float lowestAngle = Float.MAX_VALUE;

        for(BlockFace face : BlockFace.values())
        {
            if(face.getModY() == 0 && face != BlockFace.SELF)
            {
                float angle = Math.abs(face.getDirection().angle(dir));
                if(angle < lowestAngle) {
                    lowestAngle = angle;
                    best = face;
                }
            }
        }

        return best;
    }

    public static void setIfAir(Block b, Material mat) { setIfAir(b, mat, false); }
    public static void setIfAir(Block b, Material mat, boolean force)
    {
        if(force || b.getType() == Material.AIR) b.setType(mat);
    }
}