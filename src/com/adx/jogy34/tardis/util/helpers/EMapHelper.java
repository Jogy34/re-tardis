package com.adx.jogy34.tardis.util.helpers;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;

public class EMapHelper
{
    protected static String TRUE_STRING = String.valueOf(true).toLowerCase();
    protected static String FALSE_STRING = String.valueOf(false).toLowerCase();
    protected static String NULL_STRING = ("" + null).toLowerCase();

    public static EList<String> toKeyValueStrings(EMap<?, ?> map)
    {
        EList<String> results = new EList<String>();
        
        map.ForEach(x -> results.add(x.getKey() + ": " + x.getValue()));

        return results;
    }

    public static EMap<String, Object> flattenForConfig(EMap<?, ?> map)
    {
        EMap<String, Object> result = new EMap<String, Object>();

        map.ForEach(x -> {
            String key = x.getKey() + "";
            Object val = x.getValue();
            if(val instanceof EMap)
            {
                EMap<String, Object> children = flattenForConfig((EMap<?, ?>) val);
                children.ForEach(y -> result.put(key + "." + y.getKey(), y.getValue()));
            }
            else 
            {
                result.put(key, val);
            }

        });

        return result;
    }

    public static EMap<String, Object> unflattenFromConfig(EMap<String, String> map)
    {
        EMap<String, Object> result = new EMap<String, Object>();

        map.GroupBy(x -> x.getKey().split("\\.")[0]).ForEach(g -> {
            if(g.Count() > 1) 
            {
                String replace = g.getKey() + ".";
                EMap<String, String> nested = new EMap<String, String>();
                g.ForEach(y -> nested.put(y.getKey().substring(replace.length()), y.getValue()));
                result.put(g.getKey(), unflattenFromConfig(nested));
            }
            else 
            {
                String val = g.First().getValue();
                String testVal = val != null ? val.toLowerCase() : null;

                if(testVal == null || testVal.equals(NULL_STRING))
                {
                    result.put(g.getKey(), null);
                }
                else if(testVal.equals(TRUE_STRING) || testVal.equals(FALSE_STRING))
                {
                    result.put(g.getKey(), testVal.equals(TRUE_STRING));
                }
                else 
                {
                    result.put(g.getKey(), val);
                }
            }
        });

        return result;
    }
}