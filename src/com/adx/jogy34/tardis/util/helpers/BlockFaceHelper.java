package com.adx.jogy34.tardis.util.helpers;

import org.bukkit.block.BlockFace;

public class BlockFaceHelper
{
    public static BlockFace getCardinalDirection(BlockFace facing)
    {
        switch(facing)
        {
            case DOWN:
            case UP:
            case SELF:
            case NORTH:
            case NORTH_EAST:
            case NORTH_WEST:
            case NORTH_NORTH_EAST:
            case NORTH_NORTH_WEST:
                return BlockFace.NORTH;
            
            case EAST:
            case EAST_NORTH_EAST:
            case EAST_SOUTH_EAST:
                return BlockFace.EAST;
            
            case SOUTH:
            case SOUTH_EAST:
            case SOUTH_WEST:
            case SOUTH_SOUTH_EAST:
            case SOUTH_SOUTH_WEST:
                return BlockFace.SOUTH;

            case WEST:
            case WEST_NORTH_WEST:
            case WEST_SOUTH_WEST:
                return BlockFace.WEST;
        }

        return BlockFace.NORTH;
    }
}