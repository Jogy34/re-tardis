package com.adx.jogy34.tardis.util.helpers;

import java.lang.reflect.Array;
import java.util.Arrays;

import com.adx.jlinq.EList;

@SuppressWarnings("unchecked")
public final class ArrayHelper
{
    public static <T> T[] subArray(T[] arr, int start)
    {
        if(arr == null) return null;
        if(arr.length <= start - 1) return (T[]) Array.newInstance(arr.getClass().getComponentType(), 0);
        return Arrays.copyOfRange(arr, start, arr.length);
    }

    public static <T> String joinRange(T[] arr, String delimeter, int start)
    {
        return new EList<T>(subArray(arr, start)).ConcatenateToString(delimeter);
    }
}