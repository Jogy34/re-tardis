package com.adx.jogy34.tardis.util.helpers;

import java.lang.reflect.Field;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.providers.UtilProvider;
import com.adx.jogy34.tardis.util.Logger;

public final class ReflectionHelper
{
    public static EList<Field> getAllFields(Class<?> clazz)
    {
        EList<Field> fields = new EList<Field>();

        while(clazz != null)
        {
            fields.AddAll(clazz.getDeclaredFields());
            clazz = clazz.getSuperclass();
        }

        return fields;
    }

    public static EList<Class<?>> getClasses(String packageName) 
    {
        EList<Class<?>> classes = new EList<Class<?>>();
        try 
        {
            JarFile file = new JarFile(UtilProvider.TARDISPlugin().getJarFile()); 
            Enumeration<JarEntry> entry = file.entries();
            while (entry.hasMoreElements()) 
            { 
               JarEntry jarEntry = entry.nextElement();
               String name = jarEntry.getName().replace("/", ".");
               if(name.startsWith(packageName) && name.endsWith(".class"))
               {
                   classes.add(Class.forName(name.substring(0, name.length() - 6)));
               }
            }
            file.close();
        } 
        catch(Exception e) 
        {
            Logger.error(e);
        }
        return classes;
    }

    public static <T> EList<T> createEList(Class<T> type)
    {
        return new EList<T>();
    }
}