package com.adx.jogy34.tardis.util.helpers;

public final class IntHelper
{
    private IntHelper() {}

    public static int toNineDenom(int from)
    {
        return ((((from / 9) + ((from % 9 == 0) ? 0 : 1)) * 9));
    }

    public static int random(int max)
    {
        return (int) (Math.random() * max);
    }
}
