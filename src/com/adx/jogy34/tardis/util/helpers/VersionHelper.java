package com.adx.jogy34.tardis.util.helpers;

import java.text.MessageFormat;

import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.util.Logger;

import org.bukkit.Bukkit;

public class VersionHelper
{
    private VersionHelper() {}

    private static String versionString = null;
    private static EMap<String, Class<?>> loadedClasses = new EMap<String, Class<?>>();

    public static String getBukkitVersion()
    {
        if(versionString == null)
        {
            if(Bukkit.getServer() == null)
            {
                return null;
            }

            String name = Bukkit.getServer().getClass().getPackage().getName();
            versionString = name.substring(name.lastIndexOf('.') + 1);
        }

        return versionString;
    }

    private static Class<?> getVersionedClass(String path)
    {
        String classPath = MessageFormat.format(path, getBukkitVersion());

        if(loadedClasses.containsKey(classPath)) return loadedClasses.get(classPath);

        try
        {
            Class<?> clazz = Class.forName(classPath);
            loadedClasses.put(classPath, clazz);
            return clazz;
        }
        catch(Exception e)
        {
            Logger.error(e);
            return null;
        }
    }

    public static Class<?> getOBCClass(String path)
    {
        return getVersionedClass("org.bukkit.craftbukkit.{0}." + path);
    }
    
    public static Class<?> getNMSClass(String path)
    {
        return getVersionedClass("net.minecraft.server.{0}." + path);
    }
}