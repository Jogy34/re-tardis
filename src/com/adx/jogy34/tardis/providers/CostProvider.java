package com.adx.jogy34.tardis.providers;

import java.text.MessageFormat;

import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.schematics.Cost;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

public class CostProvider
{
    public static String CONFIG_PATH_BASE = "Costs.";

    public static enum Costs
    {
        CONSOLES(Material.IRON_INGOT, 2, "Console"),
        CLOCKS(Material.IRON_INGOT, 2, "Clock"),
        SPEED(Material.SUGAR, 32, "Speed"),
        ;

        private Cost cost;
        private String configPath;
        private Costs(Material defaultType, int defaultAmt, String configPath)
        {
            this.cost = new Cost(defaultAmt, defaultType);
            this.configPath = MessageFormat.format("{0}{1}", CONFIG_PATH_BASE, configPath);
        }

        public void addDefault(FileConfiguration config)
        {
            EMap<String, Object> data = ConfigProvider.getConfigProperties(cost);

            data.ForEach(x -> config.addDefault(configPath + "." + x.getKey(), x.getValue()));
        }

        public void init(FileConfiguration config)
        {
            EMap<String, String> properties = ConfigProvider.getAllConfigProperties(config.getConfigurationSection(configPath));
            cost = ConfigProvider.createFromConfigParameters(Cost.class, properties);
        }

        public Cost getCost() 
        {
            return cost;
        }
    }

    public static void addDefaults()
    {
        FileConfiguration config = ConfigProvider.getConfig();
        for(Costs c : Costs.values())
        {
            c.addDefault(config);
        }
    }

    public static void init()
    {
        FileConfiguration config = ConfigProvider.getConfig();
        for(Costs c : Costs.values())
        {
            c.init(config);
        }
    }
}