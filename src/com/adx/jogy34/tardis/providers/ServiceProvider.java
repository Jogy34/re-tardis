package com.adx.jogy34.tardis.providers;

import com.adx.jogy34.tardis.services.ActionService;
import com.adx.jogy34.tardis.services.CommandService;
import com.adx.jogy34.tardis.services.EventService;
import com.adx.jogy34.tardis.services.InventoryMenuService;
import com.adx.jogy34.tardis.services.ItemService;
import com.adx.jogy34.tardis.services.SchematicService;
import com.adx.jogy34.tardis.services.WorldService;
import com.adx.jogy34.tardis.services.interfaces.IActionService;
import com.adx.jogy34.tardis.services.interfaces.ICommandService;
import com.adx.jogy34.tardis.services.interfaces.IEventService;
import com.adx.jogy34.tardis.services.interfaces.IInventoryMenuService;
import com.adx.jogy34.tardis.services.interfaces.IItemService;
import com.adx.jogy34.tardis.services.interfaces.ISchematicService;
import com.adx.jogy34.tardis.services.interfaces.IWorldService;

public final class ServiceProvider
{
    private static IItemService _itemService;
    private static IInventoryMenuService _inventoryMenuService;
    private static ICommandService _commandService;
    private static IWorldService _worldService;
    private static IActionService _actionService;
    private static IEventService _eventService;
    private static ISchematicService _schematicService;
    
    
    public static IItemService itemService() { return _itemService = _itemService == null ? new ItemService() : _itemService; }
    public static void itemService(IItemService service) { _itemService = service; }

    public static IInventoryMenuService inventoryMenuService() { return _inventoryMenuService = _inventoryMenuService == null ? new InventoryMenuService() : _inventoryMenuService; }
    public static void inventoryMenuService(IInventoryMenuService service) { _inventoryMenuService = service; }

    public static ICommandService commandService() { return _commandService = _commandService == null ? new CommandService() : _commandService; }
    public static void commandService(ICommandService service) { _commandService = service; }

    public static IWorldService worldService() { return _worldService = _worldService == null ? new WorldService() : _worldService; }
    public static void worldService(IWorldService service) { _worldService = service; }

    public static IActionService actionService() { return _actionService = _actionService == null ? new ActionService() : _actionService; }
    public static void actionService(IActionService service) { _actionService = service; }

    public static IEventService eventService() { return _eventService = _eventService == null ? new EventService() : _eventService; }
    public static void eventService(IEventService service) { _eventService = service; }

    public static ISchematicService schematicService() { return _schematicService = _schematicService == null ? new SchematicService() : _schematicService; }
    public static void schematicService(ISchematicService service) { _schematicService = service; }
}
