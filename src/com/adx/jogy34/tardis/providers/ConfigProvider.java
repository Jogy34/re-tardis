package com.adx.jogy34.tardis.providers;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.helpers.ReflectionHelper;
import com.adx.jogy34.tardis.util.helpers.StringHelper;
import com.adx.jogy34.tardis.util.annotations.ConfigProperty;
import com.adx.jogy34.tardis.util.annotations.KnownTypes;
import com.adx.jogy34.tardis.util.interfaces.IConfigSerializable;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigProvider
{
    private static final EMap<String, File> _configFiles = new EMap<String, File>();
    private static final EMap<String, FileConfiguration> _configs = new EMap<String, FileConfiguration>();

    public static FileConfiguration getConfig()
    {
        return UtilProvider.TARDISPlugin().getConfig();
    }

    public static FileConfiguration getDataConfig()
    {
        return getConfig("Data");
    }

    public static void saveConfig()
    {
        UtilProvider.TARDISPlugin().saveConfig();
    }

    public static void saveDataConfig()
    {
        saveConfig("Data");
    }
    
    public static boolean saveDefaultConfig(String name)
    {
        return saveDefaultConfig(name, false);
    }

    public static boolean saveDefaultConfig(String name, boolean force)
    {
        File existing = new File(UtilProvider.TARDISPlugin().getDataFolder(), name);
        if(force && existing.exists()) existing.delete();

        if(force || !existing.exists())  
        {
            UtilProvider.TARDISPlugin().saveResource(name, false);
            return true;
        }
        return false;
    }

    public static FileConfiguration getConfig(String name)
    {
        if(!name.endsWith(".yml")) name += ".yml";
        if(_configs.containsKey(name)) return _configs.get(name);

        File configFile = new File(UtilProvider.TARDISPlugin().getDataFolder(), name);
        if(!configFile.exists())
        {
            configFile.getParentFile().mkdirs();
            try 
            {
                configFile.createNewFile();
            } 
            catch (IOException e)
            {
                Logger.error(e);
            }
        }

        FileConfiguration config = new YamlConfiguration();
        try 
        {
            config.load(configFile);
        } 
        catch (IOException | InvalidConfigurationException e) 
        {
            Logger.error(e);
        }

        _configFiles.put(name, configFile);
        _configs.put(name, config);

        return config;
    }
    
    public static void saveConfig(String name)
    {
        if(!name.endsWith(".yml")) name += ".yml";
        if(!_configs.containsKey(name)) return;

        try 
        {
            _configs.get(name).save(_configFiles.get(name));
        } 
        catch (IOException e)
         {
            Logger.error(e);
        }
    }

    public static boolean deleteConfig(String name)
    {
        if(!name.endsWith(".yml")) name += ".yml";
        if(!_configFiles.containsKey(name)) return false;

        if(_configFiles.get(name).delete())
        {
            _configFiles.remove(name);
            _configs.remove(name);
            return true;
        }

        return false;
    }

    public static void reloadConfig(String name)
    {
        if(!name.endsWith(".yml")) name += ".yml";
        if(_configs.containsKey(name)) _configs.remove(name);
    }

    public static void initDefaultConfig()
    {
        FileConfiguration config = getConfig();

        config.addDefault(Constants.TARDIS_WORLD, "world_TARDIS");
        config.addDefault(Constants.LOCALE, "en-US");
        config.addDefault(Constants.LOGGING_FILE_MAX_SIZE, 5d);
        config.addDefault(Constants.LOGGING_FILE_NUMBER, 5);
        config.addDefault(Constants.CLOCK_TIMEOUT, 5);
        config.addDefault(Constants.CLOCK_24HR, true);
        config.addDefault(Constants.SAVE_TIMEOUT, 20);
        config.addDefault(Constants.USE_QUOTES, false);
        config.addDefault(Constants.PHASING_ANIMATION, true);
        config.addDefault(Constants.PHASING_SOUND, true);

        List<World> worlds = Bukkit.getWorlds();
        String[] worldPositions = new String[Math.min(3, worlds.size())];
        for(int i = 0; i < worldPositions.length; i++)
        {
            World w = worlds.get(i);
            // Assuming here that the nether is index 1 and the overworld is index 0
            int index = i == 0 && worldPositions.length > 1 ? 1 : (i == 1 ? 0 : i);
            worldPositions[index] = w.getName();

            if(w.getEnvironment() == Environment.NETHER)
            {
                config.addDefault(Constants.MAX_WORLD_HEIGHT + "." + w.getName(), w.getMaxHeight() / 2);
            }
        }

        config.addDefault(Constants.WORLD_POSITIONS, worldPositions);
        Logger.addDefaults();
        CostProvider.addDefaults();
        
        config.options().copyDefaults(true);
        saveConfig();

        FileConfiguration dataConfig = getDataConfig();

        dataConfig.addDefault(Constants.Data.NEXT_WORLD_BLOCK, 0);

        dataConfig.options().copyDefaults(true);
        saveDataConfig();
    }

    public static void saveToConfig(FileConfiguration config, IConfigSerializable item) { saveToConfig(config, item, null); }

    public static void saveToConfig(FileConfiguration config, IConfigSerializable item, String prefix)
    {
        if(prefix == null) prefix = "";
        else if(!prefix.endsWith(".")) prefix += ".";
        
        EMap<String, String> currentProperties = getAllConfigProperties(prefix.isEmpty() ? config : config.getConfigurationSection(prefix.substring(0, prefix.length() - 1)));

        currentProperties.Select(x -> x.getKey().split("\\.")[0]).Distinct().ForEach(x -> config.set(x, null));

        String fPrefix = prefix; //[Java] Local variable prefix defined in an enclosing scope must be final or effectively final [536871575]

        EMap<String, Object> properties = getConfigProperties(item);
        properties.OrderBy(x -> {
            return new EList<>(x.getKey().split("\\.")).Select(p -> StringHelper.isInt(p) ? StringHelper.leftPad(p, '0', 21) : p).ConcatenateToString(".");
        }).ForEach(x -> config.set(fPrefix + x.getKey(), x.getValue()));
    }

    public static EMap<String, Object> getConfigProperties(IConfigSerializable item)
    {
        return getConfigProperties(item, null);
    }

    public static EMap<String, Object> getConfigProperties(IConfigSerializable item, Class<?> fieldType)
    {
        EMap<String, Object> results = new EMap<String, Object>();

        if(fieldType != null && fieldType.isAnnotationPresent(KnownTypes.class) && !item.getClass().getSimpleName().equals(fieldType.getSimpleName()))
        {
            results.put("_t", item.getClass().getSimpleName());
        }

        for(Field field : ReflectionHelper.getAllFields(item.getClass()))
        {
            if(field.isAnnotationPresent(ConfigProperty.class))
            {
                String propertyName = field.getAnnotation(ConfigProperty.class).value();
                boolean isAccessable = field.isAccessible();
                if(!isAccessable) field.setAccessible(true);

                try 
                {
                    Object prop = field.get(item);
                    if (prop != null)
                    {
                        if(IConfigSerializable.class.isAssignableFrom(prop.getClass()))
                        {
                            EMap<String, Object> nestedProps = getConfigProperties((IConfigSerializable) prop, field.getType());
                            nestedProps.ForEach(x -> results.put(propertyName + "." + x.getKey(), x.getValue()));
                        }
                        else if(prop instanceof EList)
                        {
                            EList<?> list = (EList<?>) prop;
                            if(list.size() > 0)
                            {
                                ParameterizedType generics = (ParameterizedType) field.getGenericType();
                                Class<?> type = (Class<?>) generics.getActualTypeArguments()[0];
                                if(IConfigSerializable.class.isAssignableFrom(type))
                                {
                                    for(int i = 0; i < list.size(); i++)
                                    {
                                        String prefix = propertyName + "." + i + ".";
                                        EMap<String, Object> nestedProps = getConfigProperties((IConfigSerializable) list.get(i), type);
                                        nestedProps.ForEach(x -> results.put(prefix + x.getKey(), x.getValue()));
                                    }
                                }
                                else 
                                {
                                    results.put(propertyName, prop);
                                }
                            }
                        }
                        else if(prop instanceof BlockData)
                        {
                            results.put(propertyName, ((BlockData) prop).getAsString());
                        }
                        else if(field.getType().isEnum() || prop instanceof UUID)
                        {
                            results.put(propertyName, prop.toString());
                        }
                        else 
                        {
                            results.put(propertyName, prop);
                        }
                    }
                } 
                catch (IllegalArgumentException | IllegalAccessException e) 
                {
                    Logger.error(e); //Shouldn't get here
                }

                if(!isAccessable) field.setAccessible(false);
            }
        }

        return results;
    }

    @SuppressWarnings("unchecked")
    public static <T extends IConfigSerializable> T createFromConfigParameters(Class<? extends T> clazz, EMap<String, String> properties, Type... fieldGenerics)
    {
        if(!properties.Any()) return null;

        try
        {
            if(properties.containsKey("_t") && clazz.isAnnotationPresent(KnownTypes.class))
            {
                EList<Class<? extends IConfigSerializable>> knownTypes = new EList<Class<? extends IConfigSerializable>>(clazz.getAnnotation(KnownTypes.class).value());
                knownTypes.add(clazz);
                String className = properties.get("_t");
                Class<? extends IConfigSerializable> newType = knownTypes.FirstOrDefault(t -> t.getSimpleName().equals(className));
                if(newType != null)
                {
                    if(clazz.isAssignableFrom(newType))
                    {
                        clazz = (Class<? extends T>) newType;
                    }
                    else
                    {
                        Logger.error("Type " + newType.getSimpleName() + " can not be assigned to " + clazz.getSimpleName());
                    }
                }
                else
                {
                    Logger.error("Unknown Type for " + clazz.getSimpleName() + ": " + className);
                }
            }
            
            T item;

            try
            {
                Constructor<? extends T> constructor = clazz.getDeclaredConstructor();
                constructor.setAccessible(true);
                item = constructor.newInstance();
            }
            catch(NoSuchMethodException e)
            {
                Logger.error(MessageFormat.format("Type {0} must contain an empty constructor", clazz));
                return null;
            }

            for(Field field : ReflectionHelper.getAllFields(clazz))
            {
                if(field.isAnnotationPresent(ConfigProperty.class))
                {
                    String propertyName = field.getAnnotation(ConfigProperty.class).value();
                    String val = properties.getOrDefault(propertyName, null);
                    boolean isAccessable = field.isAccessible();
                    Class<?> fieldType = field.getType();
                    
                    // My best effort to catch generic fields (eg. @see Bound) while ignoring things like EList<T> which 
                    // has a generic but is handled fine already. Assumes one generic so will need to figure out a way to
                    // handle multiple if that comes up
                    if(fieldGenerics.length > 0 && !field.getGenericType().getTypeName().startsWith(fieldType.getName()))
                    {
                        fieldType = (Class<?>) fieldGenerics[0];
                    }

                    if(!isAccessable) field.setAccessible(true);
    
                    if(IConfigSerializable.class.isAssignableFrom(fieldType))
                    {
                        EMap<String, String> nextProps = properties.Where(x -> x.getKey().startsWith(propertyName))
                                                                   .ToMap(x -> x.getKey().substring(propertyName.length() + 1), x -> x.getValue());
                        Object result = null;
                        if(field.getGenericType() instanceof ParameterizedType)
                        {
                            ParameterizedType generics = (ParameterizedType) field.getGenericType();
                            result = createFromConfigParameters((Class<? extends IConfigSerializable>) fieldType, nextProps, generics.getActualTypeArguments());
                        }
                        else
                        {
                            result = createFromConfigParameters((Class<? extends IConfigSerializable>) fieldType, nextProps);
                        }

                        if(result != null)
                        {
                            field.set(item, result);
                        }
                        
                    }
                    else if(EList.class.isAssignableFrom(fieldType))
                    {

                        ParameterizedType generics = (ParameterizedType) field.getGenericType();
                        Class<?> type = (Class<?>) generics.getActualTypeArguments()[0];
                        if(IConfigSerializable.class.isAssignableFrom(type))
                        {
                            EList<?> list =  ReflectionHelper.createEList(type);
                            Method add = ArrayList.class.getDeclaredMethod("add", Object.class);
                            EMap<String, String> listProps = properties.Where(x -> x.getKey().startsWith(propertyName))
                                    .ToMap(x -> x.getKey().substring(propertyName.length() + 1), x -> x.getValue());

                            boolean stop = false;
                            for(int i = 0; !stop; i++)
                            {
                                String indexString = i + "";
                                EMap<String, String> itemProps = listProps.Where(x -> x.getKey().startsWith(indexString))
                                        .ToMap(x -> x.getKey().substring(indexString.length() + 1), x -> x.getValue());

                                if(itemProps.Any())
                                {
                                    IConfigSerializable o = createFromConfigParameters((Class<? extends IConfigSerializable>) type, itemProps);
                                    add.invoke(list, o);
                                }
                                else 
                                {
                                    stop = true;
                                }
                            }

                            field.set(item, list);
                        }
                        else if(StringHelper.canConvert(type))
                        {
                            EList<?> list = StringHelper.convert(StringHelper.toArray(val), type);
                            field.set(item, list);
                        }
                    }
                    else if(val != null)
                    {
                        if(UUID.class.isAssignableFrom(fieldType))
                        {
                            field.set(item, UUID.fromString(val));
                        }
                        else if(BlockData.class.isAssignableFrom(fieldType))
                        {
                            field.set(item, Bukkit.createBlockData(val));
                        }
                        else //handle primative types
                        {
                            Object result = StringHelper.convert(val, fieldType);
                            field.set(item, result);
                        }
                    }
    
                    if(!isAccessable) field.setAccessible(false);
                }
            }

            return item;
        }
        catch (Exception e) 
        {
            Logger.error(e);
		}

        return null;
    }

    public static EMap<String, String> getAllConfigProperties(String configName)
    {
        return getAllConfigProperties(getConfig(configName));
    }

    public static EMap<String, String> getAllConfigProperties(ConfigurationSection configSection)
    {
        if(configSection == null) return new EMap<String, String>();
        
        return new EMap<String, Object>(configSection.getValues(true))
                                    .Where(x -> !(x.getValue() instanceof MemorySection))
                                    .ToMap(x -> x.getKey(), x -> x.getValue().toString());
    }

    public static class Constants
    {
        public static final String TARDIS_WORLD = "TARDIS-World";
        public static final String LOCALE = "Locale";
        public static final String LOGGING_CONSOLE = "Logging.Console";
        public static final String LOGGING_FILE = "Logging.File";
        public static final String LOGGING_FILE_MAX_SIZE = "Logging.File.MaxSizeMB";
        public static final String LOGGING_FILE_NUMBER = "Logging.File.MaxFiles";
        public static final String CLOCK_TIMEOUT = "Clocks.TimeoutSeconds";
        public static final String CLOCK_24HR = "Clocks.24HourFormat";
        public static final String SAVE_TIMEOUT = "SaveTimeoutMinutes";
        public static final String WORLD_POSITIONS = "WorldPositions";
        public static final String USE_QUOTES = "UseQuotes (This gets quotes from the internet)";
        public static final String MAX_WORLD_HEIGHT = "WorldHeight";
        public static final String PHASING_ANIMATION = "Phasing.Animation";
        public static final String PHASING_SOUND = "Phasing.Sound";

        public static class Data
        {
            public static final String NEXT_WORLD_BLOCK = "NextWorldBlock";
            public static final String NO_FLY_CHUNKS = "NoFlyChunks";
        }
    }
}