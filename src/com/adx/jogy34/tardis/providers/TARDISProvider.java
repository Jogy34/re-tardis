package com.adx.jogy34.tardis.providers;

import java.io.File;
import java.util.UUID;
import java.util.Map.Entry;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jogy34.tardis.enums.TARDISWorlds;
import com.adx.jogy34.tardis.tardis.TARDIS;
import com.adx.jogy34.tardis.util.Logger;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public final class TARDISProvider
{
    private static EMap<UUID, TARDIS> tardises = new EMap<UUID, TARDIS>();
    private static EMap<UUID, TARDIS> tempTardises = new EMap<UUID, TARDIS>();

    public static TARDIS getTardisForPlayer(Player player) { return getTardisForPlayer(player.getUniqueId()); }
    public static void setTardisForPlayer(Player player, TARDIS tardis) { setTardisForPlayer(player.getUniqueId(), tardis); }
    
    public static TARDIS getTardisForPlayer(UUID player) { return tardises.getOrDefault(player, null); }
    public static void setTardisForPlayer(UUID player, TARDIS tardis) { tardises.put(player, tardis); }

    public static TARDIS getTardisForPlayer(String username) { return tardises.Select(x -> x.getValue()).FirstOrDefault(t -> t.getPlayerName().equalsIgnoreCase(username)); }

    public static void addTemporaryTardis(UUID player, TARDIS tardis) { tempTardises.put(player, tardis); }
    public static void removeTemporaryTardis(UUID player) { tempTardises.remove(player); }
    public static void removeTemporaryTardis(TARDIS tardis) {
        Entry<UUID, TARDIS> toRemove = tempTardises.FirstOrDefault(x -> x.getValue() == tardis);
        if(toRemove != null) removeTemporaryTardis(toRemove.getKey());
    }

    public static void removeTardis(UUID player)
    {
        tardises.remove(player);
    }

    public static boolean isPreviewingTardis(UUID player) { return tempTardises.containsKey(player); }

    public static void onPluginDisable() 
    {
        tardises.ForEach(t -> t.getValue().saveToConfig());

        tardises.ForEach(x -> x.getValue().remove(false, false));
        tempTardises.ForEach(x -> x.getValue().remove(false, false));
    }

    public static TARDIS getTardisForLocation(Block b) { return getTardisForLocation(b.getX(), b.getY(), b.getZ(), b.getWorld()); }
    public static TARDIS getTardisForLocation(Location l) { return getTardisForLocation(l.getBlockX(), l.getBlockY(), l.getBlockZ(), l.getWorld()); }

    public static TARDIS getTardisForLocation(int x, int y, int z, World world) 
    {
        TARDIS tardis = tardises.Select(t -> t.getValue()).FirstOrDefault(t -> t.isInsideTardis(x, y, z, world));
        if(tardis == null) tardis = tempTardises.Select(t -> t.getValue()).FirstOrDefault(t -> t.isInsideTardis(x, y, z, world));

        return tardis;
    }

    public static TARDIS getTardisForInterior(Location l) { return getTardisForInterior(l.getBlock()); }
    
    public static TARDIS getTardisForInterior(Block b) 
    {
        if(ServiceProvider.worldService().getTypeForWorld(b.getWorld()) == TARDISWorlds.TARDIS_WORLD)
        {
            return getTardisForInterior(b.getX()); 
        }
        
        return null;
    }

    public static TARDIS getTardisForInterior(int x) 
    {
        return tardises.Select(t -> t.getValue()).FirstOrDefault(t -> t.isWithinInteriorBounds(x));
    }

    public static EList<TARDIS> getAllTardises()
    {
        return tardises.Select(t -> t.getValue()).ToList();
    }

    public static void loadTardises()
    {
        Logger.info("Loading TARDISes...");
        File tardisFolder = new File(UtilProvider.TARDISPlugin().getDataFolder(), TARDIS.CONFIG_PATH);
        if(tardisFolder.exists() && tardisFolder.isDirectory())
        {
            File[] children = tardisFolder.listFiles();
            for(File f : children)
            {
                String name = f.getName();
                if(f.isFile() && name.endsWith(".yml"))
                {
                    name = name.substring(0, name.length() - 4);
                    TARDIS tardis = ConfigProvider.createFromConfigParameters(TARDIS.class, ConfigProvider.getAllConfigProperties(TARDIS.CONFIG_PATH + name));
                    tardis.init();
                }
            }
        }

        Logger.info("... " + tardises.size() + " TARDIS" + (tardises.size() == 1 ? "" : "es") + " loaded");

        int signTimeout = ConfigProvider.getConfig().getInt(ConfigProvider.Constants.CLOCK_TIMEOUT);
        if(signTimeout <= 0) signTimeout = 5;
        signTimeout *= 20;

        new BukkitRunnable(){
            @Override
            public void run() {
                tardises.ForEach(t -> t.getValue().getSettings().getInterior().updateClocks());
            }
        }.runTaskTimer(UtilProvider.TARDISPlugin(), signTimeout, signTimeout);
        
        int saveTimeout = ConfigProvider.getConfig().getInt(ConfigProvider.Constants.SAVE_TIMEOUT);

        if(saveTimeout <= 0) saveTimeout = 20;
        saveTimeout *= 20 * 60 * 60;

        new BukkitRunnable(){
            @Override
            public void run() {
                tardises.ForEach(t -> t.getValue().saveToConfig());
            }
        }.runTaskTimer(UtilProvider.TARDISPlugin(), saveTimeout, saveTimeout);
        
        int speedTimeout = 5 * 20;

        new BukkitRunnable(){
            @Override
            public void run() {
                World w = ServiceProvider.worldService().getWorld(TARDISWorlds.TARDIS_WORLD);
                for(Player p : w.getPlayers())
                {
                    TARDIS in = TARDISProvider.getTardisForInterior(p.getLocation());
                    if(in != null && in.getSettings().getInterior().isFast())
                    {
                        p.removePotionEffect(PotionEffectType.SPEED);
                        p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, speedTimeout + 20, 3, true, false));
                    }
                }
            }
        }.runTaskTimer(UtilProvider.TARDISPlugin(), speedTimeout, speedTimeout);
    }
}