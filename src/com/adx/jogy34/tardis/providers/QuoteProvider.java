package com.adx.jogy34.tardis.providers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.MessageFormat;

import com.adx.jlinq.EList;
import com.adx.jogy34.tardis.util.Logger;
import com.adx.jogy34.tardis.util.Text;

import org.bukkit.ChatColor;
import org.json.JSONArray;
import org.json.JSONObject;

public final class QuoteProvider
{
    private static final String QUOTE_URL = "http://www.randomquote.tk/?api&count=10&category=doctor_who";
    private static EList<String> quotes = new EList<String>();
    private static boolean useQuotes = false;

    public static void init()
    {
        useQuotes = ConfigProvider.getConfig().getBoolean(ConfigProvider.Constants.USE_QUOTES);

        if(useQuotes) grabQuotes();
    }

    public static boolean canUseQuotes()
    {
        return useQuotes;
    }

    public static String getQuote()
    {
        if(quotes.size() == 0) grabQuotes();
        return quotes.remove(0);
    }

    private static void grabQuotes()
    {
        if(!useQuotes) {
            Logger.error("Shouldn't have gotten here but somehow tried to use a quote when quotes were turned off...");
            throw new IllegalArgumentException("Quotes are turned off. Go into the config to turn them on.");
        }
        String json = "[{\"Text\":\"Always take a banana to a party, Rose: bananas are good!\",\"Who\":\"The Doctor\"}]";
        BufferedReader reader = null;
        try 
        {
            URL url = new URL(QUOTE_URL);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1) buffer.append(chars, 0, read);
            json = buffer.toString();
        }
        catch(Exception e)
        {
            Logger.error(e);
            Logger.warning(Text.DISABLING_QUOTES);
            useQuotes = false;
        } 
        finally 
        {
            if (reader != null)
            {
                try 
                {
                    reader.close();
                } 
                catch (IOException e) 
                {
                    Logger.error(e);
                }
            }
        }

        JSONArray arr = new JSONArray(json);
        for(int i = 0; i < arr.length(); i++)
        {
            JSONObject obj = arr.getJSONObject(i);
            String text = obj.getString("Text");
            String who = obj.getString("Who");

            quotes.add(MessageFormat.format("{0}{1} {2}~{3}", ChatColor.GOLD, text, ChatColor.DARK_PURPLE, who));
        }
    }
}