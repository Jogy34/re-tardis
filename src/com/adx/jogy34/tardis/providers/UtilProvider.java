package com.adx.jogy34.tardis.providers;

import com.adx.jogy34.tardis.general.TARDISMain;

public final class UtilProvider
{
    private static TARDISMain _tardisPlugin;
    
    public static TARDISMain TARDISPlugin() { return _tardisPlugin; }
    public static void TARDISPlugin(TARDISMain plugin) { _tardisPlugin = plugin; }
}
