package com.adx.jlinq;

import java.util.Collection;

import com.adx.jlinq.interfaces.IGrouping;

public class Grouping<K, T> extends EList<T> implements IGrouping<K, T>
{
    private static final long serialVersionUID = 1L;

    private K _key;

    public Grouping(K key)
    {
        super();
        this._key = key;
    }

    public Grouping(K key, Collection<T> coll)
    {
        super(coll);
        this._key = key;
    }

    public Grouping(K key, T[] values)
    {
        super(values);
        this._key = key;
    }

    @Override
    public K getKey() { return _key; }
}