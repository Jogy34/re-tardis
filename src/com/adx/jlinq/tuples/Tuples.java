package com.adx.jlinq.tuples;

/**
 * Holds the actual Tuple classes
 */
public final class Tuples
{
    private Tuples() {}
    
    public static class Tuple1<T1>
    {
        public final T1 item1;

        public Tuple1(T1 item1)
        {
            this.item1 = item1;
        }
    }

    public static class Tuple<T1, T2>
    {
        public final T1 item1;
        public final T2 item2;

        public Tuple(T1 item1, T2 item2)
        {
            this.item1 = item1;
            this.item2 = item2;
        }
    }

    public static class Tuple3<T1, T2, T3>
    {
        public final T1 item1;
        public final T2 item2;
        public final T3 item3;

        public Tuple3(T1 item1, T2 item2, T3 item3)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
        }
    }

    public static class Tuple4<T1, T2, T3, T4>
    {
        public final T1 item1;
        public final T2 item2;
        public final T3 item3;
        public final T4 item4;

        public Tuple4(T1 item1, T2 item2, T3 item3, T4 item4)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
            this.item4 = item4;
        }
    }

    public static class Tuple5<T1, T2, T3, T4, T5>
    {
        public final T1 item1;
        public final T2 item2;
        public final T3 item3;
        public final T4 item4;
        public final T5 item5;

        public Tuple5(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
            this.item4 = item4;
            this.item5 = item5;
        }
    }

    public static class Tuple6<T1, T2, T3, T4, T5, T6>
    {
        public final T1 item1;
        public final T2 item2;
        public final T3 item3;
        public final T4 item4;
        public final T5 item5;
        public final T6 item6;

        public Tuple6(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
            this.item4 = item4;
            this.item5 = item5;
            this.item6 = item6;
        }
    }

    public static class Tuple7<T1, T2, T3, T4, T5, T6, T7>
    {
        public final T1 item1;
        public final T2 item2;
        public final T3 item3;
        public final T4 item4;
        public final T5 item5;
        public final T6 item6;
        public final T7 item7;

        public Tuple7(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
            this.item4 = item4;
            this.item5 = item5;
            this.item6 = item6;
            this.item7 = item7;
        }
    }

    public static class Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>
    {
        public final T1 item1;
        public final T2 item2;
        public final T3 item3;
        public final T4 item4;
        public final T5 item5;
        public final T6 item6;
        public final T7 item7;
        public final T8 item8;

        public Tuple8(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
            this.item4 = item4;
            this.item5 = item5;
            this.item6 = item6;
            this.item7 = item7;
            this.item8 = item8;
        }
    }

    public static class Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>
    {
        public final T1 item1;
        public final T2 item2;
        public final T3 item3;
        public final T4 item4;
        public final T5 item5;
        public final T6 item6;
        public final T7 item7;
        public final T8 item8;
        public final T9 item9;

        public Tuple9(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8, T9 item9)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
            this.item4 = item4;
            this.item5 = item5;
            this.item6 = item6;
            this.item7 = item7;
            this.item8 = item8;
            this.item9 = item9;
        }
    }
}