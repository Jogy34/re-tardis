package com.adx.jlinq;

import java.util.Map.Entry;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.adx.jlinq.exceptions.EnumerableException;
import com.adx.jlinq.interfaces.Actions.IAction;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jlinq.interfaces.Functions.IFunction;
import com.adx.jlinq.interfaces.IGrouping;
import com.adx.jlinq.interfaces.IOrderedEnumerable;

public class EMap<K, V> extends HashMap<K, V> implements IEnumerable<Entry<K, V>>
{
    private static final long serialVersionUID = 6141541776850299288L;

    public EMap()
    {
    }

    public EMap(Map<K, V> values)
    {
        super(values);
    }

    @Override
    public void ForEach(IAction<Entry<K, V>> action)
    {
        for(Entry<K, V> i :this.entrySet()) action.invoke(i);
    }

    @Override
    public <R> IEnumerable<R> Select(IFunction<Entry<K, V>, R> func)
    {
        EList<R> result = new EList<R>();
        for(Entry<K, V> i : this.entrySet()) 
        {
            result.add(func.invoke(i));
        }
        return result;
    }

    @Override
    public IEnumerable<Entry<K, V>> Where(IFunction<Entry<K, V>, Boolean> func)
    {
        EMap<K, V> result = new EMap<K, V>();
        for(Entry<K, V> i : this.entrySet()) 
        {
            if(func.invoke(i)) 
            {
                result.put(i.getKey(), i.getValue());
            }
        }
        return result;
    }

    @Override
    public Entry<K, V> First()
    {
        for(Entry<K, V> i : this.entrySet()) return i;

        throw new EnumerableException("First(): Enumerable must contain at least one element");
    }

    @Override
    public Entry<K, V> First(IFunction<Entry<K, V>, Boolean> func)
    {
        for(Entry<K, V> i : this.entrySet()) 
        {
            if(func.invoke(i)) 
            {
                return i;
            }
        }
        
        throw new EnumerableException("First(): Enumerable must contain at least one element that matches predicate function");
    }

    @Override
    public Entry<K, V> FirstOrDefault()
    {

        for(Entry<K, V> i : this.entrySet()) return i;
        return null;
    }
    
    @Override
    public Entry<K, V>  FirstOrDefault(Entry<K, V>  def) 
    {
        Entry<K, V>  result = FirstOrDefault();
        return result == null ? def : result;
    }

    public Entry<K, V> FirstOrDefault( K defKey, V defVal)
    {
        return FirstOrDefault(new SimpleEntry<K, V>(defKey, defVal));
    }

    @Override
    public Entry<K, V> FirstOrDefault(IFunction<Entry<K, V>, Boolean> func)
    {
        for(Entry<K, V> i : this.entrySet()) 
        {
            if(func.invoke(i)) 
            {
                return i;
            }
        }
        return null;
    }
    
    @Override
    public Entry<K, V>  FirstOrDefault(IFunction<Entry<K, V>, Boolean> func, Entry<K, V>  def) 
    {
        Entry<K, V>  result = FirstOrDefault(func);
        return result == null ? def : result;
    }
    
    public Entry<K, V> FirstOrDefault(IFunction<Entry<K, V>, Boolean> func, K defKey, V defVal)
    {
        return FirstOrDefault(func, new SimpleEntry<K, V>(defKey, defVal));
    }

    @Override
    public Entry<K, V> Last()
    {
        return this.ToList().Last();
    }

    @Override
    public Entry<K, V> Last(IFunction<Entry<K, V>, Boolean> func)
    {
        return this.ToList().Last(func);
    }

    @Override
    public Entry<K, V> LastOrDefault()
    {
        return this.ToList().LastOrDefault();
    }
    
    @Override
    public Entry<K, V>  LastOrDefault(Entry<K, V>  def) 
    {
        Entry<K, V>  result = LastOrDefault();
        return result == null ? def : result;
    }

    public Entry<K, V> LastOrDefault( K defKey, V defVal)
    {
        return LastOrDefault(new SimpleEntry<K, V>(defKey, defVal));
    }

    @Override
    public Entry<K, V> LastOrDefault(IFunction<Entry<K, V>, Boolean> func)
    {
        return this.ToList().LastOrDefault(func);
    }
    
    @Override
    public Entry<K, V>  LastOrDefault(IFunction<Entry<K, V>, Boolean> func, Entry<K, V>  def) 
    {
        Entry<K, V>  result = LastOrDefault(func);
        return result == null ? def : result;
    }
    
    public Entry<K, V> LastOrDefault(IFunction<Entry<K, V>, Boolean> func, K defKey, V defVal)
    {
        return LastOrDefault(func, new SimpleEntry<K, V>(defKey, defVal));
    }

    @Override
    public boolean Any()
    {
        return this.size() > 0;
    }

    @Override
    public boolean Any(IFunction<Entry<K, V>, Boolean> func)
    {
        for(Entry<K, V> i : this.entrySet()) 
        {
            if(func.invoke(i)) 
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean All(IFunction<Entry<K, V>, Boolean> func)
    {
        for(Entry<K, V> i : this.entrySet()) 
        {
            if(!func.invoke(i)) 
            {
                return false;
            }
        }
        
        return true;
    }

    @Override
    public <G extends Comparable<G>> IEnumerable<? extends IGrouping<G, Entry<K, V>>> GroupBy(IFunction<Entry<K, V>, G> func) 
    {
        EList<Grouping<G, Entry<K, V>>> result = new EList<Grouping<G, Entry<K, V>>>();

        this.ForEach(x -> {
            G key = func.invoke(x);
            Grouping<G, Entry<K, V>> group = result.FirstOrDefault(y -> y.getKey().compareTo(key) == 0);
            if(group == null) 
            {
                result.add(new Grouping<G, Entry<K, V>>(key, Arrays.asList(x)));
            }
            else 
            {
                group.add(x);
            }
        });

        return result;
    }

    @Override
    public int Count() 
    {
        return this.size();
    }

    @Override
    public EList<Entry<K, V>> ToList() 
    {
        EList<Entry<K, V>> result = new EList<Entry<K, V>>();
        this.ForEach(x -> result.add(x));
        return result;
    }

    @Override
    public <T> EMap<T, Entry<K, V>> ToMap(IFunction<Entry<K, V>, T> keyFunction) 
    {
        EMap<T, Entry<K, V>> result = new EMap<T, Entry<K, V>>();
        this.ForEach(x -> result.put(keyFunction.invoke(x), x));
        return result;
    }

    @Override
    public <T, T2> EMap<T, T2> ToMap(IFunction<Entry<K, V>, T> keyFunction, IFunction<Entry<K, V>, T2> valueFunction) 
    {
        EMap<T, T2> result = new EMap<T, T2>();
        this.ForEach(x -> result.put(keyFunction.invoke(x), valueFunction.invoke(x)));
        return result;
    }

    @Override
    public String ConcatenateToString(String separator) 
    {
        String result = "";
        boolean addSeparator = false;
        for(Entry<K, V> item : this.entrySet())
        {
            if(addSeparator) result += separator;
            addSeparator = true;
            result += item.getValue() + "";
        }

        return result;
    }

    @Override
    public String ConcatenateToString(IFunction<Entry<K, V>, Object> selectFunction, String separator) 
    {
        String result = "";
        boolean addSeparator = false;
        for(Entry<K, V> item : this.entrySet())
        {
            if(addSeparator) result += separator;
            addSeparator = true;
            result += selectFunction.invoke(item) + "";
        }

        return result;
    }

    public <G> G[] ToArray(Class<G> type)
    {
        return this.ToList().Select(x -> x.getValue()).ToArray(type);
    }

    public Object[] ToArray()
    {
        return this.ToList().Select(x -> x.getValue()).ToArray();
    }

    @Override
    public Iterator<Entry<K, V>> iterator() 
    {
        return this.ToList().iterator();
    }

    @Override
    public <G extends Comparable<G>> IOrderedEnumerable<Entry<K, V>, G> OrderBy(IFunction<Entry<K, V>, G> selectFunction) 
    {
        return new EOrderedList<>(this, selectFunction);
    }

    @Override
    public <G extends Comparable<G>> IOrderedEnumerable<Entry<K, V>, G> OrderByDescending(IFunction<Entry<K, V>, G> selectFunction) 
    {
        return new EOrderedList<>(this, selectFunction, true);
    }

    @Override
    public IEnumerable<Entry<K, V>> Union(IEnumerable<? extends Entry<K, V>> with) 
    {
        EMap<K, V> map = new EMap<K, V>(this);
        with.ForEach(x -> map.put(x.getKey(), x.getValue()));
        return map;
    }

    @Override
    public IEnumerable<Entry<K, V>> Distinct() 
    {
        EMap<K, V> result = new EMap<K, V>();

        this.ForEach(x -> {
            V v1 = x.getValue();
            if(!result.Any(y -> {
                V v2 = y.getValue();
                return v1 == v2 || (v1 != null && v1.equals(v2)) || (v2 != null && v2.equals(v1));
            }))
            {
                result.put(x.getKey(), v1);
            }
        });

        return result;
    }

    @Override
    public <G extends Comparable<G>> IEnumerable<Entry<K, V>> DistinctBy(IFunction<Entry<K, V>, G> selectFunction) 
    {
        EMap<K, V> result = new EMap<K, V>();

        this.ForEach(x -> {
            G compareWith = selectFunction.invoke(x);
            if(!result.Any(y -> {
                G compareTo = selectFunction.invoke(y);
                return compareWith.compareTo(compareTo) == 0;
            }))
            {
                result.put(x.getKey(), x.getValue());
            }
        });

        return result;
    }

    @Override
    public IEnumerable<IEnumerable<Entry<K, V>>> Batch(int amt) 
    {
        EList<IEnumerable<Entry<K, V>>> result = new EList<IEnumerable<Entry<K, V>>>();
        EList<K> keys = new EList<K>(this.keySet());

        for(int i = 0; i < keys.size(); i += amt)
        {
            EMap<K, V> curr = new EMap<K, V>();
            for(int j = 0; j < amt && i + j < this.Count(); j++)
            {
                K key = keys.get(i + j);
                curr.put(key, this.get(key));
            }
            result.add(curr);
        }

        return result;
    }
}
