package com.adx.jlinq.exceptions;

public class EnumerableException extends Error
{
    private static final long serialVersionUID = 1193577266510373991L;
    
    public EnumerableException(String message) {
        super(message);
    }
}
