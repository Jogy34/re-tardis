package com.adx.jlinq;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.adx.jlinq.exceptions.EnumerableException;
import com.adx.jlinq.interfaces.Actions.IAction;
import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jlinq.interfaces.Functions.IFunction;
import com.adx.jlinq.interfaces.IGrouping;
import com.adx.jlinq.interfaces.IOrderedEnumerable;

public class EList<T> extends ArrayList<T> implements IEnumerable<T>
{
    private static final long serialVersionUID = -6007053472362652879L;
    protected Class<T> generic;

    public static <T> EList<T> from(Collection<T> coll) { return new EList<T>(coll); }
    public static <T> EList<T> from(T[] arr) { return new EList<T>(arr); }

    public EList()
    {
        super();
    }
    
    public EList(Collection<T> coll) 
    {
        super(coll);
    }
    
    public EList(T[] data)
    {
        super(Arrays.asList(data));
    }

    @Override
    public void ForEach(IAction<T> action)
    {
        for(T i : this) action.invoke(i);
    }

    @Override
    public <V> IEnumerable<V> Select(IFunction<T, V> func)
    {
        EList<V> result = new EList<V>();
        for(T i : this) result.add(func.invoke(i));
        
        return result;
    }

    @Override
    public IEnumerable<T> Where(IFunction<T, Boolean> func)
    {
        EList<T> result = new EList<T>();
        for(T i : this) 
        {
            if(func.invoke(i)) {
                result.add(i);
            }
        }
        
        return result;
    }

    @Override
    public T First()
    {
        if(this.size() >= 1) return this.get(0);

        throw new EnumerableException("First(): Enumerable must contain at least one element");
    }

    @Override
    public T First(IFunction<T, Boolean> func)
    {
        for(T i : this) 
        {
            if(func.invoke(i)) {
                return i;
            }
        }
        
        throw new EnumerableException("First(): Enumerable must contain at least one element that matches predicate function");
    }

    @Override
    public T FirstOrDefault()
    {
        if(this.size() >= 1) return this.get(0);
        return null;
    }

    @Override
    public T FirstOrDefault(T def) 
    {
        T result = FirstOrDefault();
        return result == null ? def : result;
    }

    @Override
    public T FirstOrDefault(IFunction<T, Boolean> func)
    {
        for(T i : this) 
        {
            if(func.invoke(i)) {
                return i;
            }
        }
        return null;
    }

    @Override
    public T FirstOrDefault(IFunction<T, Boolean> func, T def) 
    {
        T result = FirstOrDefault(func);
        return result == null ? def : result;
    }

    @Override
    public T Last()
    {
        if(this.size() >= 1) return this.get(this.size() - 1);

        throw new EnumerableException("Last(): Enumerable must contain at least one element");
    }

    @Override
    public T Last(IFunction<T, Boolean> func)
    {
        for(int i = this.size() - 1; i >= 0; i--) 
        {
            T item = this.get(i);
            if(func.invoke(item)) {
                return item;
            }
        }
        
        throw new EnumerableException("Last(): Enumerable must contain at least one element that matches predicate function");
    }

    @Override
    public T LastOrDefault()
    {
        if(this.size() >= 1) return this.get(this.size() - 1);
        return null;
    }

    @Override
    public T LastOrDefault(T def) 
    {
        T result = LastOrDefault();
        return result == null ? def : result;
    }

    @Override
    public T LastOrDefault(IFunction<T, Boolean> func)
    {
        for(int i = this.size() - 1; i >= 0; i--) 
        {
            T item = this.get(i);
            if(func.invoke(item)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public T LastOrDefault(IFunction<T, Boolean> func, T def) 
    {
        T result = LastOrDefault(func);
        return result == null ? def : result;
    }

    @Override
    public boolean Any()
    {
        return this.size() > 0;
    }

    @Override
    public boolean Any(IFunction<T, Boolean> func)
    {
        for(T i : this) 
        {
            if(func.invoke(i)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean All(IFunction<T, Boolean> func)
    {
        for(T i : this) 
        {
            if(!func.invoke(i)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <K extends Comparable<K>> IEnumerable<? extends IGrouping<K, T>> GroupBy(IFunction<T, K> func) 
    {
        EList<Grouping<K, T>> result = new EList<Grouping<K, T>>();

        this.ForEach(x -> {
            K key = func.invoke(x);
            Grouping<K, T> group = result.FirstOrDefault(y -> y.getKey().compareTo(key) == 0);
            if(group == null) 
            {
                result.add(new Grouping<K, T>(key, Arrays.asList(x)));
            }
            else 
            {
                group.add(x);
            }
        });

        return result;
    }

    @Override
    public int Count()
    {
        return this.size();
    }

    @Override
    public EList<T> ToList() 
    {
        return this;
    }

    @Override
    public <V> EMap<V, T> ToMap(IFunction<T, V> keyFunction) 
    {
        EMap<V, T> result = new EMap<V, T>();
        this.ForEach(x -> result.put(keyFunction.invoke(x), x));
        return result;
    }

    @Override
    public <V, T2> EMap<V, T2> ToMap(IFunction<T, V> keyFunction, IFunction<T, T2> valueFunction) 
    {
        EMap<V, T2> result = new EMap<V, T2>();
        this.ForEach(x -> result.put(keyFunction.invoke(x), valueFunction.invoke(x)));
        return result;
    }

    @Override
    public String ConcatenateToString(String separator) 
    {
        String result = "";
        boolean addSeparator = false;
        for(T item : this)
        {
            if(addSeparator) result += separator;
            addSeparator = true;
            result += item + "";
        }

        return result;
    }

    @Override
    public String ConcatenateToString(IFunction<T, Object> selectFunction, String separator) 
    {
        String result = "";
        boolean addSeparator = false;
        for(T item : this)
        {
            if(addSeparator) result += separator;
            addSeparator = true;
            result += selectFunction.invoke(item) + "";
        }

        return result;
    }

    public boolean AddAll(T[] items)
    {
        return super.addAll(Arrays.asList(items));
    }

    public boolean AddAll(IEnumerable<? extends T> items)
    {
        return super.addAll(items.ToList());
    }

    @SuppressWarnings("unchecked")
    public <G> G[] ToArray(Class<G> type)
    {
        return (G[]) this.toArray((G[]) Array.newInstance(type, 0));
    }

    public Object[] ToArray()
    {
        return this.toArray();
    }

    @Override
    public EList<T> clone()
    {
        return new EList<T>(this);
    }

    @Override
    public <G extends Comparable<G>> IOrderedEnumerable<T, G> OrderBy(IFunction<T, G> selectFunction) 
    {
        return new EOrderedList<>(this, selectFunction);
    }

    @Override
    public <G extends Comparable<G>> IOrderedEnumerable<T, G> OrderByDescending(IFunction<T, G> selectFunction) 
    {
        return new EOrderedList<>(this, selectFunction, true);
    }

    @Override
    public IEnumerable<T> Union(IEnumerable<? extends T> with) 
    {
        EList<T> list = new EList<T>(this);
        list.AddAll(with);
        return list;
    }

    @Override
    public IEnumerable<T> Distinct() 
    {
        EList<T> result = new EList<T>();

        this.ForEach(x -> {
            if(!result.Any(y -> x == y || (x != null && x.equals(y)) || (y != null && y.equals(x))))
            {
                result.add(x);
            }
        });

        return result;
    }

    @Override
    public <G extends Comparable<G>> IEnumerable<T> DistinctBy(IFunction<T, G> selectFunction) 
    {
        EList<T> result = new EList<T>();

        this.ForEach(x -> {
            G compareWith = selectFunction.invoke(x);
            if(!result.Any(y -> {
                G compareTo = selectFunction.invoke(y);
                return compareWith.compareTo(compareTo) == 0;
            }))
            {
                result.add(x);
            }
        });

        return result;
    }

    @Override
    public IEnumerable<IEnumerable<T>> Batch(int amt) 
    {
        EList<IEnumerable<T>> result = new EList<IEnumerable<T>>();

        for(int i = 0; i < this.Count(); i += amt)
        {
            EList<T> curr = new EList<T>();
            for(int j = 0; j < amt && i + j < this.Count(); j++)
            {
                curr.add(this.get(i + j));
            }
            result.add(curr);
        }

        return result;
    }
}
