package com.adx.jlinq;

import java.util.Collection;

import com.adx.jlinq.interfaces.IEnumerable;
import com.adx.jlinq.interfaces.IOrderedEnumerable;
import com.adx.jlinq.interfaces.Functions.IFunction;

public class EOrderedList<T, TOrdered extends Comparable<TOrdered>> extends EList<T> implements IOrderedEnumerable<T, TOrdered>
{
    private static final long serialVersionUID = 3660697259150781359L;
    protected IFunction<T, TOrdered> orderFunction;
    protected boolean descending;
    
    public EOrderedList(IFunction<T, TOrdered> orderFunction)
    {
        this(orderFunction, false);
    }

    public EOrderedList(IFunction<T, TOrdered> orderFunction, boolean descending)
    {
        this.orderFunction = orderFunction;
        this.descending = descending;
    }

    public EOrderedList(IEnumerable<T> items, IFunction<T, TOrdered> orderFunction)
    {
        this(items, orderFunction, false);
    }

    public EOrderedList(IEnumerable<T> items, IFunction<T, TOrdered> orderFunction, boolean descending)
    {
        this(orderFunction, descending);
        this.AddAll(items);
    }

    @Override
    public boolean add(T e) 
    {
        TOrdered insertVal = e == null ? null : orderFunction.invoke(e);
        if(insertVal == null || this.size() == 0)
        {
            super.add(0, e);
            return true;
        }
        
        int start = 0;
        int stop = this.size() - 1;
        int index = (start + stop) / 2;

        while(stop > start) 
        {
            T item = this.get(index);
            TOrdered val = orderFunction.invoke(item);

            int dir = val == null ? 1 : insertVal.compareTo(val);
            if(descending) dir *= -1;
            
            if(dir < 0) 
            {
                stop = index - 1;
            }
            else if(dir > 0) 
            {
                start = index + 1;
            } 
            else if(dir == 0) 
            {
                start = stop = index;
            }

            index = (start + stop) / 2;

        }

        T item = this.get(index);
        TOrdered val = orderFunction.invoke(item);

        int dir = val == null ? 1 : insertVal.compareTo(val);
        if(dir <= 0) 
        {
            super.add(index, e);
        }
        else if(dir > 0) 
        {
            super.add(index + 1, e);
        } 

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) 
    {
        boolean result = true;
        for(T item : c)
        {
            if(!this.add(item)) result = false;
        }
        return result;
    }

    @Override
    public boolean AddAll(IEnumerable<? extends T> items) 
    {
        boolean result = true;
        for(T item : items)
        {
            if(!this.add(item)) result = false;
        }
        return result;
    }

    @Override
    public boolean AddAll(T[] items) 
    {
        boolean result = true;
        for(T item : items)
        {
            if(!this.add(item)) result = false;
        }
        return result;
    }
    

    /**
     * Do not use, equilivent to add(T e)
     * 
     * @deprecated Cannot insert value at index for an ordered list
     */
    @Override
    @Deprecated
    public void add(int index, T element) { throw new UnsupportedOperationException("Cannot insert at an index to an ordered list"); }

    /**
     * Do not use, equilivent to addAll(Collection<? extends T> c)
     * 
     * @deprecated Cannot insert value at index for an ordered list
     */
    @Override
    @Deprecated
    public boolean addAll(int index, Collection<? extends T> c) { throw new UnsupportedOperationException("Cannot insert at an index to an ordered list"); }

    @Override
    public EList<T> ToList() {
        return new EList<T>(this);
    }
}