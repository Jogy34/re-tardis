package com.adx.jlinq.interfaces;

import com.adx.jlinq.EList;
import com.adx.jlinq.EMap;
import com.adx.jlinq.interfaces.Actions.IAction;
import com.adx.jlinq.interfaces.Functions.IFunction;

/**
 * @author Jogy34
 * Basic C# IEnumerable clone that doesn't support deferred execution
 * 
 * note:
 * Made forEach capital because of a conflict with ArrayList and HashMap so also making everything else capital
 * plus I like it better because I'm coming from C#
 * 
 * @param <T> The type stored in this IEnumerable
 */
public interface IEnumerable<T> extends Iterable<T>
{
    void ForEach(IAction<T> action);
    <V> IEnumerable<V> Select(IFunction<T, V> func);
    IEnumerable<T> Where(IFunction<T, Boolean> func);
    T First();
    T First(IFunction<T, Boolean> func);
    T FirstOrDefault();
    T FirstOrDefault(IFunction<T, Boolean> func);
    T FirstOrDefault(T def);
    T FirstOrDefault(IFunction<T, Boolean> func, T def);
    T Last();
    T Last(IFunction<T, Boolean> func);
    T LastOrDefault();
    T LastOrDefault(IFunction<T, Boolean> func);
    T LastOrDefault(T def);
    T LastOrDefault(IFunction<T, Boolean> func, T def);
    boolean Any();
    boolean Any(IFunction<T, Boolean> func);
    boolean All(IFunction<T, Boolean> func);
    <K extends Comparable<K>> IEnumerable<? extends IGrouping<K, T>> GroupBy(IFunction<T, K> func);
    int Count();
    EList<T> ToList();
    <V> EMap<V, T> ToMap(IFunction<T, V> keyFunction);
    <V, T2> EMap<V, T2> ToMap(IFunction<T, V> keyFunction, IFunction<T, T2> valueFunction);
    String ConcatenateToString(String separator);
    String ConcatenateToString(IFunction<T, Object> selectFunction, String separator);
    <G> G[] ToArray(Class<G> clazz);
    Object[] ToArray();
    <G extends Comparable<G>> IOrderedEnumerable<T, G> OrderBy(IFunction<T, G> selectFunction);
    <G extends Comparable<G>> IOrderedEnumerable<T, G> OrderByDescending(IFunction<T, G> selectFunction);
    IEnumerable<T> Union(IEnumerable<? extends T> with);
    IEnumerable<T> Distinct();
    <G extends Comparable<G>> IEnumerable<T> DistinctBy(IFunction<T, G> selectFunction);
    IEnumerable<IEnumerable<T>> Batch(int amt);
}
