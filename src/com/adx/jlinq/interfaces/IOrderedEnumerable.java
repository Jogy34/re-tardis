package com.adx.jlinq.interfaces;

public interface IOrderedEnumerable<T, TOrderedType extends Comparable<TOrderedType>> extends IEnumerable<T>
{
}