package com.adx.jlinq.interfaces;

public interface IGrouping<K, T> extends IEnumerable<T>
{
    K getKey();
} 