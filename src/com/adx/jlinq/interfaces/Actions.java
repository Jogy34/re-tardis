package com.adx.jlinq.interfaces;

public class Actions
{
    public static interface IAction0
    {
        void invoke();
    }

    public static interface IAction<T>
    {
        void invoke(T arg);
    }

    public static interface IAction2<T1, T2>
    {
        void invoke(T1 arg1, T2 arg2);
    }

    public static interface IAction3<T1, T2, T3>
    {
        void invoke(T1 arg1, T2 arg2, T3 arg3);
    }

    public static interface IAction4<T1, T2, T3, T4>
    {
        void invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    }
}