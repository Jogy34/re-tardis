package com.adx.jlinq.interfaces;

public class Functions
{
    public static interface IFunction0<TResult>
    {
        TResult invoke();
    }

    public static interface IFunction<T, TResult>
    {
        TResult invoke(T arg);
    }

    public static interface IFunction2<T1, T2, TResult>
    {
        TResult invoke(T1 arg1, T2 arg2);
    }

    public static interface IFunction3<T1, T2, T3, TResult>
    {
        TResult invoke(T1 arg1, T2 arg2, T3 arg3);
    }

    public static interface IFunction4<T1, T2, T3, T4, TResult>
    {
        TResult invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    }
}
